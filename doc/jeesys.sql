/*
SQLyog Ultimate v11.27 (64 bit)
MySQL - 5.6.14-log : Database - jeesys
*********************************************************************
*/

/*!40101 SET NAMES utf8 */;

/*!40101 SET SQL_MODE=''*/;

/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;
CREATE DATABASE /*!32312 IF NOT EXISTS*/`jeesys` /*!40100 DEFAULT CHARACTER SET utf8 */;

USE `jeesys`;

/*Table structure for table `y_bigcate` */

DROP TABLE IF EXISTS `y_bigcate`;

CREATE TABLE `y_bigcate` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT COMMENT '主键',
  `bigCateName` varchar(20) NOT NULL COMMENT '大类名称',
  `description` varchar(200) DEFAULT NULL COMMENT '描述',
  `state` int(11) NOT NULL DEFAULT '1' COMMENT '状态',
  `seq` int(11) DEFAULT '0' COMMENT '序号',
  `createdBy` bigint(20) NOT NULL COMMENT '创建人id',
  `createTime` datetime NOT NULL COMMENT '创建时间',
  `deletedBy` bigint(20) DEFAULT NULL COMMENT '删除人id',
  `deleteTime` datetime DEFAULT NULL COMMENT '删除时间',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8;

/*Data for the table `y_bigcate` */

insert  into `y_bigcate`(`id`,`bigCateName`,`description`,`state`,`seq`,`createdBy`,`createTime`,`deletedBy`,`deleteTime`) values (1,'机械','魂牵梦萦需要',1,0,1,'2014-08-24 20:49:37',1,'2014-08-24 21:40:16'),(2,'电气','电气',1,0,1,'2014-08-24 21:46:06',NULL,NULL),(3,'仪器','仪器',1,0,1,'2014-08-28 00:47:24',NULL,NULL);

/*Table structure for table `y_category` */

DROP TABLE IF EXISTS `y_category`;

CREATE TABLE `y_category` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT COMMENT '主键',
  `categoryName` varchar(50) NOT NULL COMMENT '类型名称',
  `description` varchar(200) DEFAULT NULL COMMENT '备注说明',
  `seq` int(11) NOT NULL DEFAULT '0' COMMENT '序号',
  `state` int(11) NOT NULL DEFAULT '1' COMMENT '状态，默认为1，0为删除',
  `createdBy` bigint(20) NOT NULL COMMENT '创建人id',
  `createTime` datetime NOT NULL COMMENT '创建时间默认Now()',
  `deletedBy` bigint(20) DEFAULT NULL COMMENT '删除人id',
  `deleteTime` datetime DEFAULT NULL COMMENT '删除时间',
  `bigCateId` bigint(20) NOT NULL COMMENT '大类id',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=9 DEFAULT CHARSET=utf8;

/*Data for the table `y_category` */

insert  into `y_category`(`id`,`categoryName`,`description`,`seq`,`state`,`createdBy`,`createTime`,`deletedBy`,`deleteTime`,`bigCateId`) values (1,'2吨电瓶叉车',NULL,0,1,1,'2014-08-09 17:29:30',NULL,NULL,1),(2,'拉伸机',NULL,0,1,1,'2014-08-09 17:30:02',NULL,NULL,1),(3,'10出冲杯机',NULL,0,1,1,'2014-08-09 17:30:11',NULL,NULL,1),(4,'电导率仪',NULL,0,1,1,'2014-08-09 17:30:21',NULL,NULL,1),(5,'低速离心机',NULL,0,1,1,'2014-08-09 17:30:31',NULL,NULL,1),(6,'仪器','仪器',0,1,1,'2014-08-23 21:51:29',NULL,NULL,3),(7,'机械111',NULL,0,1,1,'2014-08-24 21:44:13',NULL,NULL,1),(8,'电气11圭','电气土土土',0,1,1,'2014-08-24 21:46:24',NULL,NULL,2);

/*Table structure for table `y_check_record` */

DROP TABLE IF EXISTS `y_check_record`;

CREATE TABLE `y_check_record` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT COMMENT '主键',
  `deviceId` bigint(20) NOT NULL COMMENT '设备id',
  `checkDate` date NOT NULL COMMENT '校验日期',
  `checkUnit` varchar(20) NOT NULL COMMENT '校验单位',
  `result` varchar(200) NOT NULL COMMENT '校验结果',
  `checkCount` int(11) NOT NULL COMMENT '校验次数',
  `createdBy` bigint(20) NOT NULL COMMENT '创建人员id',
  `createTime` datetime NOT NULL COMMENT '创建时间',
  `deletedBy` bigint(20) DEFAULT NULL COMMENT '删除人员id',
  `deleteTime` datetime DEFAULT NULL COMMENT '删除时间',
  `description` text COMMENT '备注',
  `state` int(11) NOT NULL DEFAULT '1' COMMENT '状态，默认，0为删除',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=utf8;

/*Data for the table `y_check_record` */

insert  into `y_check_record`(`id`,`deviceId`,`checkDate`,`checkUnit`,`result`,`checkCount`,`createdBy`,`createTime`,`deletedBy`,`deleteTime`,`description`,`state`) values (1,1,'2014-08-18','武汉中建三局','合格',1,1,'2014-08-18 00:37:58',NULL,NULL,'fdsafdsa魂牵梦萦魂牵梦萦',1),(2,1,'2014-08-28','中建三局','合格',2,1,'2014-08-28 00:53:43',NULL,NULL,'超大规模暮云春树暮云春树暮云春树工',1),(3,2,'2014-08-20','中建总局','过',1,1,'2014-08-29 00:59:09',NULL,NULL,'魂牵梦萦魂牵梦萦魂牵梦萦魂牵梦萦朝秦暮楚f \r\naf在sa地',1),(4,6,'2014-08-28','魂牵梦萦','fdafdsafdsa',1,1,'2014-08-28 00:58:48',NULL,NULL,NULL,1);

/*Table structure for table `y_component` */

DROP TABLE IF EXISTS `y_component`;

CREATE TABLE `y_component` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT COMMENT '主键',
  `name` varchar(20) NOT NULL COMMENT '备件名称',
  `description` varchar(200) DEFAULT NULL COMMENT '备件描述',
  `state` int(11) NOT NULL DEFAULT '1' COMMENT '状态，默认为1，0为删除',
  `createdBy` bigint(20) NOT NULL COMMENT '创建人id',
  `deletedBy` bigint(20) DEFAULT NULL COMMENT '删除人id',
  `createTime` datetime NOT NULL COMMENT '创建时间',
  `deleteTime` datetime DEFAULT NULL COMMENT '删除时间',
  `seq` int(11) NOT NULL DEFAULT '0' COMMENT '排序序号',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8;

/*Data for the table `y_component` */

insert  into `y_component`(`id`,`name`,`description`,`state`,`createdBy`,`deletedBy`,`createTime`,`deleteTime`,`seq`) values (1,'主电机驱动皮带',NULL,1,1,NULL,'2014-08-09 17:32:28',NULL,0),(2,'润滑油齿轮泵',NULL,1,1,NULL,'2014-08-09 17:32:39',NULL,0);

/*Table structure for table `y_dept` */

DROP TABLE IF EXISTS `y_dept`;

CREATE TABLE `y_dept` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `createTime` datetime NOT NULL COMMENT '创建时间',
  `state` int(11) NOT NULL DEFAULT '1' COMMENT '状态，默认为1,0为删除',
  `createdBy` bigint(20) NOT NULL COMMENT '创建人id',
  `deptName` varchar(50) NOT NULL COMMENT '部门名称',
  `seq` bigint(20) DEFAULT '0' COMMENT '排序序号',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=43 DEFAULT CHARSET=utf8;

/*Data for the table `y_dept` */

insert  into `y_dept`(`id`,`createTime`,`state`,`createdBy`,`deptName`,`seq`) values (1,'2013-09-29 00:06:48',1,1,'管理部',0),(39,'2014-07-01 23:22:35',1,1,'工程部',1),(40,'2014-07-01 23:22:51',1,1,'生产部',2),(41,'2014-07-01 23:23:05',1,1,'质控部',3),(42,'2014-07-01 23:23:21',1,1,'储运部',4);

/*Table structure for table `y_detect_item` */

DROP TABLE IF EXISTS `y_detect_item`;

CREATE TABLE `y_detect_item` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT COMMENT '主键',
  `categoryId` bigint(20) NOT NULL COMMENT '类型id',
  `name` varchar(255) NOT NULL COMMENT '类型名称',
  `standard` varchar(255) DEFAULT NULL COMMENT '标准',
  `notice` text COMMENT '注意事项',
  `createdBy` bigint(20) NOT NULL COMMENT '创建人id',
  `createTime` datetime NOT NULL COMMENT '创建时间',
  `seq` int(11) NOT NULL DEFAULT '0' COMMENT '排序序号',
  `state` int(11) NOT NULL DEFAULT '1' COMMENT '默认为1，0为删除',
  `deletedBy` bigint(20) DEFAULT NULL COMMENT '删除人id',
  `deleteTime` datetime DEFAULT NULL COMMENT '删除时间',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=utf8;

/*Data for the table `y_detect_item` */

insert  into `y_detect_item`(`id`,`categoryId`,`name`,`standard`,`notice`,`createdBy`,`createTime`,`seq`,`state`,`deletedBy`,`deleteTime`) values (1,1,'检查主油箱油位','好',NULL,114,'2014-08-09 17:47:21',0,1,NULL,NULL),(2,3,'离合器刹车气压（油压）','70-100psi',NULL,114,'2014-08-09 17:47:48',0,1,NULL,NULL),(3,6,'无',NULL,NULL,1,'2014-08-23 22:07:30',0,1,NULL,NULL),(4,8,'魂牵梦萦','fdsafdsa','fdsafdsa',1,'2014-08-26 22:33:03',0,1,NULL,NULL);

/*Table structure for table `y_detect_record` */

DROP TABLE IF EXISTS `y_detect_record`;

CREATE TABLE `y_detect_record` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT COMMENT '主键',
  `deviceId` bigint(20) NOT NULL COMMENT '设备编号',
  `detectItemId` bigint(20) NOT NULL COMMENT '检测项目编号',
  `description` text COMMENT '描述',
  `state` int(11) NOT NULL DEFAULT '1' COMMENT '默认为1，0为删除',
  `createdBy` bigint(20) NOT NULL COMMENT '创建(录入)人id',
  `deletedBy` bigint(20) DEFAULT NULL COMMENT '删除人id',
  `detectId` bigint(20) DEFAULT NULL COMMENT '检测人员id',
  `createTime` datetime NOT NULL COMMENT '默认Now()',
  `deleteTime` datetime DEFAULT NULL COMMENT '删除时间',
  `detectDate` date NOT NULL COMMENT '检测时间',
  `result` text NOT NULL COMMENT '检测结果',
  `detectUserName` varchar(20) NOT NULL COMMENT '检测人员',
  `count` int(11) NOT NULL DEFAULT '1' COMMENT '第几次检测',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8;

/*Data for the table `y_detect_record` */

insert  into `y_detect_record`(`id`,`deviceId`,`detectItemId`,`description`,`state`,`createdBy`,`deletedBy`,`detectId`,`createTime`,`deleteTime`,`detectDate`,`result`,`detectUserName`,`count`) values (1,2,2,NULL,1,1,NULL,NULL,'2014-08-11 19:18:27',NULL,'2014-08-11','通过','张三dfas',1),(2,2,2,NULL,1,1,NULL,NULL,'2014-08-28 01:24:20',NULL,'2014-08-28','魂牵梦萦','fdsa',2);

/*Table structure for table `y_device` */

DROP TABLE IF EXISTS `y_device`;

CREATE TABLE `y_device` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT COMMENT '主键',
  `no` varchar(20) NOT NULL DEFAULT '0' COMMENT '设备编号',
  `name` varchar(50) NOT NULL COMMENT '设备名称',
  `deptId` bigint(20) NOT NULL COMMENT '设备所属部门',
  `categoryId` bigint(20) NOT NULL COMMENT '设备类型id',
  `type` int(11) NOT NULL COMMENT '1日检，2周检，3月检，4季检，5年检,6其它',
  `seq` int(11) NOT NULL DEFAULT '0' COMMENT '序号,默认为0,排序',
  `state` int(11) NOT NULL DEFAULT '1' COMMENT '状态，默认为1，0为删除',
  `createdBy` bigint(20) NOT NULL COMMENT '创建人id',
  `createTime` datetime NOT NULL COMMENT '默认Now()',
  `startDate` date DEFAULT NULL COMMENT '开始时间',
  `description` varchar(200) DEFAULT NULL COMMENT '描述',
  `producer` varchar(50) DEFAULT NULL COMMENT '生产厂家',
  `modelNumber` varchar(20) DEFAULT NULL COMMENT '型号',
  `usedSite` varchar(20) DEFAULT NULL COMMENT '使用地点',
  `lineType` varchar(20) DEFAULT NULL COMMENT '线型',
  `deleteTime` datetime DEFAULT NULL COMMENT '删除时间',
  `deletedBy` bigint(20) DEFAULT NULL COMMENT '删除人id',
  `checkType` int(11) NOT NULL DEFAULT '6' COMMENT '1日校，2周校，3月校，4季校，5年校,6其它',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=7 DEFAULT CHARSET=utf8;

/*Data for the table `y_device` */

insert  into `y_device`(`id`,`no`,`name`,`deptId`,`categoryId`,`type`,`seq`,`state`,`createdBy`,`createTime`,`startDate`,`description`,`producer`,`modelNumber`,`usedSite`,`lineType`,`deleteTime`,`deletedBy`,`checkType`) values (1,'111','拉伸机11#',39,2,2,0,1,113,'2014-08-09 17:34:36','2014-08-10',NULL,NULL,NULL,NULL,NULL,NULL,NULL,6),(2,'0','10出冲杯机',39,3,2,0,1,113,'2014-08-09 17:39:52','2014-08-10',NULL,NULL,NULL,NULL,NULL,NULL,NULL,6),(3,'11','电导率仪',40,8,6,0,1,114,'2014-08-09 17:42:16','2014-08-10',NULL,NULL,NULL,NULL,NULL,NULL,NULL,2),(4,'0','低速离心机',40,5,2,0,1,114,'2014-08-09 17:43:19','2014-08-10',NULL,NULL,NULL,NULL,NULL,NULL,NULL,6),(5,'2','10出冲杯机2',40,3,3,0,1,1,'2014-08-11 19:25:35','2014-08-12',NULL,NULL,NULL,NULL,NULL,NULL,NULL,6),(6,'0','测量仪器',1,6,6,0,1,1,'2014-08-28 00:48:55','2014-08-29',NULL,NULL,NULL,NULL,NULL,NULL,NULL,3);

/*Table structure for table `y_device_compt` */

DROP TABLE IF EXISTS `y_device_compt`;

CREATE TABLE `y_device_compt` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT COMMENT '主键',
  `deviceId` bigint(20) NOT NULL COMMENT '设备id',
  `compId` bigint(20) NOT NULL COMMENT '备件id',
  `cycle` int(11) NOT NULL COMMENT '替换周期',
  `lastReplTime` datetime DEFAULT NULL COMMENT '最后替换时间',
  `lastReplPer` bigint(20) DEFAULT NULL COMMENT '最后替换人id',
  `createdBy` bigint(20) NOT NULL COMMENT '创建人id',
  `createTime` datetime NOT NULL COMMENT '创建时间',
  `startDate` date NOT NULL COMMENT '开始时间',
  `count` int(11) NOT NULL DEFAULT '1' COMMENT '数量',
  `state` int(11) NOT NULL DEFAULT '1' COMMENT '状态,默认为1，0为删除',
  `deletedBy` bigint(20) DEFAULT NULL COMMENT '删除人id',
  `deleteTime` datetime DEFAULT NULL COMMENT '删除时间',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8;

/*Data for the table `y_device_compt` */

insert  into `y_device_compt`(`id`,`deviceId`,`compId`,`cycle`,`lastReplTime`,`lastReplPer`,`createdBy`,`createTime`,`startDate`,`count`,`state`,`deletedBy`,`deleteTime`) values (1,2,1,5,NULL,NULL,113,'2014-08-09 17:39:52','2014-08-10',3,1,113,'2014-08-09 17:51:57'),(2,2,2,5,NULL,NULL,113,'2014-08-09 17:39:52','2014-08-10',1,1,113,'2014-08-09 17:51:57'),(3,5,2,5,NULL,NULL,1,'2014-08-11 19:25:35','2014-08-12',1,1,NULL,NULL);

/*Table structure for table `y_device_oil` */

DROP TABLE IF EXISTS `y_device_oil`;

CREATE TABLE `y_device_oil` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT COMMENT '主键',
  `deviceId` bigint(20) NOT NULL COMMENT '设备id',
  `oilId` bigint(20) NOT NULL COMMENT '润滑项目id',
  `cycle` int(11) NOT NULL DEFAULT '1' COMMENT '润滑周期',
  `startDate` date NOT NULL COMMENT '开始日期',
  `createdBy` bigint(20) NOT NULL COMMENT '创建人id',
  `createTime` datetime NOT NULL COMMENT '创建时间',
  `state` int(11) NOT NULL DEFAULT '1' COMMENT '状态,默认1，0为删除',
  `deletedBy` bigint(20) DEFAULT NULL COMMENT '删除人id',
  `deleteTime` datetime DEFAULT NULL COMMENT '删除时间',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8;

/*Data for the table `y_device_oil` */

insert  into `y_device_oil`(`id`,`deviceId`,`oilId`,`cycle`,`startDate`,`createdBy`,`createTime`,`state`,`deletedBy`,`deleteTime`) values (1,4,1,30,'2014-08-12',1,'2014-08-11 22:58:57',1,NULL,NULL);

/*Table structure for table `y_info` */

DROP TABLE IF EXISTS `y_info`;

CREATE TABLE `y_info` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT COMMENT '主键',
  `content` text COMMENT '内容',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8;

/*Data for the table `y_info` */

insert  into `y_info`(`id`,`content`) values (1,'<p style=\"text-align:center;\">\r\n	fdsafdsa\r\n魂<strong><em><u>牵梦萦\r\nfdsa魂牵梦萦</u></em></strong>\r\n</p>\r\n<p>\r\n	魂牵梦萦\r\n</p>\r\n<p>\r\n	魂牵梦萦\r\n</p>\r\n<p>\r\n	fsdaf大本营\r\n</p>');

/*Table structure for table `y_item` */

DROP TABLE IF EXISTS `y_item`;

CREATE TABLE `y_item` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT COMMENT '主键',
  `name` varchar(255) NOT NULL COMMENT '检测项目',
  `standard` varchar(255) NOT NULL COMMENT '检测标准',
  `notice` text COMMENT '注意事项',
  `categoryId` bigint(20) NOT NULL COMMENT '设备类型id',
  `seq` int(11) NOT NULL DEFAULT '0' COMMENT '序号,默认为0,排序',
  `state` int(11) NOT NULL DEFAULT '1' COMMENT '状态，默认为1，0为删除',
  `createdBy` bigint(20) NOT NULL COMMENT '创建人id',
  `createTime` datetime NOT NULL COMMENT '默认Now()',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

/*Data for the table `y_item` */

/*Table structure for table `y_oil` */

DROP TABLE IF EXISTS `y_oil`;

CREATE TABLE `y_oil` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT COMMENT '主键',
  `name` varchar(20) NOT NULL COMMENT '润滑项目',
  `state` int(11) NOT NULL DEFAULT '1' COMMENT '状态,默认为1，0为删除',
  `seq` int(11) NOT NULL DEFAULT '0' COMMENT '排序序号',
  `description` varchar(200) DEFAULT NULL COMMENT '描述',
  `createdBy` bigint(20) NOT NULL COMMENT '创建人id',
  `createTime` datetime NOT NULL COMMENT '创建时间',
  `deletedBy` bigint(20) DEFAULT NULL COMMENT '删除人id',
  `deleteTime` datetime DEFAULT NULL COMMENT '删除时间',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8;

/*Data for the table `y_oil` */

insert  into `y_oil`(`id`,`name`,`state`,`seq`,`description`,`createdBy`,`createTime`,`deletedBy`,`deleteTime`) values (1,'节点润滑',1,0,NULL,114,'2014-08-09 17:44:17',NULL,NULL);

/*Table structure for table `y_oil_record` */

DROP TABLE IF EXISTS `y_oil_record`;

CREATE TABLE `y_oil_record` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT COMMENT '主键',
  `deviceOilId` bigint(20) NOT NULL COMMENT '设备润滑项目id',
  `cost` double NOT NULL COMMENT '费用',
  `state` int(11) NOT NULL DEFAULT '1' COMMENT '状态，默认为1，0为删除',
  `createdBy` bigint(20) NOT NULL COMMENT '创建人id',
  `oiledBy` bigint(20) DEFAULT NULL COMMENT '润滑人id',
  `oiledDate` date NOT NULL COMMENT '润滑日期',
  `createTime` datetime NOT NULL COMMENT '添加日期',
  `deleteTime` datetime DEFAULT NULL COMMENT '删除日期',
  `deletedBy` bigint(20) DEFAULT NULL COMMENT '删除人员id',
  `description` varchar(200) DEFAULT NULL COMMENT '描述',
  `oiledUserName` varchar(20) NOT NULL COMMENT '润滑人员',
  `count` int(11) NOT NULL DEFAULT '1' COMMENT '第几次润滑',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8;

/*Data for the table `y_oil_record` */

insert  into `y_oil_record`(`id`,`deviceOilId`,`cost`,`state`,`createdBy`,`oiledBy`,`oiledDate`,`createTime`,`deleteTime`,`deletedBy`,`description`,`oiledUserName`,`count`) values (1,1,2323,1,1,NULL,'2014-08-11','2014-08-11 23:33:47',NULL,NULL,'械魂牵梦萦霜','李四ddddsfa',1),(2,1,431,1,1,NULL,'2014-08-28','2014-08-28 00:12:33',NULL,NULL,NULL,'4321432',2),(3,1,432,1,1,NULL,'2014-08-28','2014-08-28 00:13:10',NULL,NULL,'23423','32',3);

/*Table structure for table `y_power` */

DROP TABLE IF EXISTS `y_power`;

CREATE TABLE `y_power` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `createTime` datetime NOT NULL COMMENT '创建时间',
  `fatherId` bigint(20) DEFAULT '0' COMMENT '父类id',
  `masterName` varchar(50) NOT NULL,
  `powerName` varchar(50) NOT NULL COMMENT '权限名称',
  `seq` bigint(20) NOT NULL DEFAULT '0' COMMENT '排序序号',
  `iconCls` varchar(255) DEFAULT NULL COMMENT '图标类',
  `url` varchar(255) DEFAULT NULL COMMENT 'url地址',
  `type` varchar(11) NOT NULL DEFAULT 'F' COMMENT '类型，F为菜单，O为操作',
  `used` int(11) NOT NULL DEFAULT '1' COMMENT '是否启用，1为启用，0为禁用',
  `description` varchar(255) DEFAULT NULL COMMENT '描述',
  `createdBy` bigint(20) NOT NULL COMMENT '创建人id',
  `target` varchar(20) NOT NULL DEFAULT 'center' COMMENT '显示位置',
  `status` varchar(20) NOT NULL DEFAULT 'closed' COMMENT '状态，展开还是折叠',
  `child` int(11) NOT NULL DEFAULT '1' COMMENT '是否是根节点,1为是，0为否',
  `updateTime` datetime DEFAULT NULL COMMENT '更新时间',
  `position` varchar(20) DEFAULT NULL COMMENT '显示位置',
  PRIMARY KEY (`id`),
  UNIQUE KEY `masterName` (`masterName`)
) ENGINE=InnoDB AUTO_INCREMENT=257 DEFAULT CHARSET=utf8;

/*Data for the table `y_power` */

insert  into `y_power`(`id`,`createTime`,`fatherId`,`masterName`,`powerName`,`seq`,`iconCls`,`url`,`type`,`used`,`description`,`createdBy`,`target`,`status`,`child`,`updateTime`,`position`) values (11,'2013-09-29 00:17:53',15,'power_all','菜单管理',2,'icon-my-shipping','power/all','F',1,NULL,1,'center','closed',1,'2014-04-23 22:34:21','[top]'),(14,'2013-09-29 00:18:24',0,'usermgr','用户管理',1,'icon-my-shipping','','F',1,NULL,0,'center','closed',0,'2014-04-25 20:22:16','[top]'),(15,'2013-09-29 00:18:24',0,'sysmgr','系统管理',3,'icon-my-shipping','','F',1,NULL,0,'center','closed',0,'2014-02-24 13:41:48','[top]'),(16,'2013-09-29 00:18:24',14,'user_all','用户信息',1,'icon-role','user/all','F',1,NULL,0,'center','closed',1,'2014-04-25 20:22:27','[top]'),(18,'2013-09-29 00:18:24',14,'dept_all','部门信息',3,'icon-pro','dept/all','F',1,NULL,0,'center','closed',1,'2014-04-04 22:51:56','[top]'),(19,'2013-09-29 00:18:24',15,'role_all','角色信息',1,'icon-role','role/all','F',1,NULL,1,'center','closed',1,'2014-04-23 22:34:22',NULL),(52,'2014-02-19 10:43:21',16,'user_add','添加',2,'icon-add','toMyAdd','O',1,NULL,1,'center','open',1,'2014-04-29 10:52:03','[top]'),(65,'2014-02-20 16:18:28',16,'user_upd','修改',3,'icon-edit','updMySelected','O',1,'用户修改',1,'center','open',1,'2014-04-29 13:32:12','[top, middle]'),(70,'2014-02-21 15:49:37',16,'user_del','删除',4,'icon-remove','delSelected','O',1,NULL,1,'center','open',1,'2014-03-11 11:44:55','[top, middle]'),(71,'2014-02-22 10:40:00',16,'user_view','查看',1,'icon-search','viewMySelected','O',1,NULL,1,'center','open',1,'2014-04-29 11:51:08','[top, middle]'),(72,'2014-02-22 11:38:36',11,'power_add','添加',2,'icon-add','toMyAdd','O',1,NULL,1,'center','open',1,'2014-04-23 22:36:22','[top]'),(74,'2014-02-22 11:50:49',18,'dept_view','查看',0,'icon-search','viewSelected','O',1,NULL,1,'center','open',1,'2014-03-10 15:29:11','[top, middle]'),(75,'2014-02-22 11:53:28',19,'role_view','查看',1,'icon-search','viewSelected','O',1,NULL,1,'center','open',1,'2014-03-06 16:05:24','[top, middle]'),(89,'2014-02-24 11:36:20',11,'power_upd','修改',3,'icon-edit','updSelected','O',1,NULL,1,'center','open',1,'2014-04-23 22:36:24','[top]'),(96,'2014-02-24 17:26:48',11,'power_del','删除',4,'icon-remove','delMySelected','O',1,NULL,1,'center','open',1,'2014-04-23 22:36:23','[top]'),(111,'2014-02-25 17:20:42',18,'dept_add','添加',0,'icon-add','toAdd','O',1,NULL,1,'center','open',1,'2014-03-11 17:28:27','[top]'),(112,'2014-02-27 10:12:35',18,'dept_upd','修改',0,'icon-edit','updSelected','O',1,'修改部门',1,'center','open',1,'2014-03-12 10:55:51','[top, middle]'),(113,'2014-02-27 10:13:51',18,'dept_del','删除',4,'icon-remove','delMySelected','O',1,'删除部门',1,'center','open',1,'2014-03-12 11:06:44','[top, middle]'),(114,'2014-02-27 10:23:25',19,'role_add','添加',2,'icon-add','toAdd','O',1,'添加角色',1,'center','open',1,'2014-03-06 16:05:06','[top]'),(115,'2014-02-27 10:25:46',19,'role_upd','修改',3,'icon-edit','updSelected','O',1,'修改角色',1,'center','open',1,'2014-03-06 16:05:51','[middle]'),(116,'2014-02-27 10:27:06',19,'role_del','删除',4,'icon-remove','delMySelected','O',1,'删除角色',1,'center','open',1,'2014-03-11 17:29:34','[top]'),(139,'2014-03-06 16:11:02',19,'role_auth','授权',5,'icon-config','auth','O',1,NULL,1,'center','open',1,'2014-04-25 17:00:18','[middle]'),(141,'2014-04-16 13:24:03',11,'power_view','查看',1,'icon-search','viewSelected','O',1,NULL,1,'center','open',1,'2014-04-16 13:27:28','[middle]'),(142,'2014-04-23 23:07:06',0,'datamgr','数据管理',2,'icon-config','','F',1,NULL,1,'center','closed',0,'2014-04-23 23:40:02','[top]'),(192,'2014-05-23 18:30:26',142,'basedatamgr','基础数据管理',1,'icon-db','','F',1,'基础数据管理',1,'center','closed',0,NULL,'[top]'),(197,'2014-05-23 18:57:50',142,'detectRecord_all','检修记录管理',2,'icon-sys','detectRecord/all','F',1,NULL,1,'center','closed',1,NULL,'[top]'),(198,'2014-05-23 18:59:20',197,'detectRecord_add','添加',2,'icon-add','toAdd','O',1,NULL,1,'center','open',1,NULL,'[top]'),(199,'2014-05-23 19:01:46',197,'detectRecord_view','查看',1,'icon-search','viewSelected','O',1,NULL,1,'center','open',1,NULL,'[top, middle]'),(200,'2014-05-23 19:02:45',197,'detectRecord_upd','修改',3,'icon-edit','updSelected','O',1,NULL,1,'center','open',1,NULL,'[top, middle]'),(201,'2014-05-23 19:03:19',197,'detectRecord_del','删除',4,'icon-remove','delSelected','O',1,NULL,1,'center','open',1,NULL,'[top, middle]'),(202,'2014-07-01 23:29:21',192,'category_all','设备类型管理',1,'icon-comp','category/all','F',1,'设备类型管理',1,'center','closed',1,NULL,'[top]'),(203,'2014-07-01 23:58:15',202,'category_view','查看',1,'icon-search','viewSelected','O',1,NULL,1,'center','open',1,NULL,'[top, middle]'),(204,'2014-07-02 00:41:59',202,'category_add','添加',2,'icon-add','toAdd','O',1,NULL,1,'center','open',1,NULL,'[top]'),(205,'2014-07-02 00:42:45',202,'category_upd','修改',3,'icon-edit','updSelected','O',1,NULL,1,'center','open',1,NULL,'[top, middle]'),(206,'2014-07-02 00:43:42',202,'category_del','删除',4,'icon-remove','delMySelected','O',1,NULL,1,'center','open',1,NULL,'[top, middle]'),(207,'2014-07-03 22:13:57',192,'component_all','备件管理',2,'icon-sys','component/all','F',1,NULL,1,'center','closed',1,NULL,'[top]'),(208,'2014-07-03 22:15:26',207,'component_view','查看',1,'icon-search','viewSelected','O',1,NULL,1,'center','open',1,NULL,'[top, middle]'),(209,'2014-07-03 22:16:37',207,'component_add','添加',2,'icon-add','toAdd','O',1,NULL,1,'center','open',1,NULL,'[top]'),(210,'2014-07-03 22:17:42',207,'component_upd','修改',3,'icon-edit','updSelected','O',1,NULL,1,'center','open',1,NULL,'[top, middle]'),(211,'2014-07-03 22:19:14',207,'component_del','删除',4,'icon-remove','delSelected','O',1,NULL,1,'center','open',1,NULL,'[top, middle]'),(212,'2014-07-03 22:52:24',192,'device_all','设备仪器管理',3,'icon-pro','device/all','F',1,NULL,1,'center','closed',1,NULL,'[top]'),(213,'2014-07-03 22:53:00',212,'device_view','查看',1,'icon-search','viewSelected','O',1,NULL,1,'center','open',1,NULL,'[top, middle]'),(214,'2014-07-03 22:53:38',212,'device_add','添加',2,'icon-add','toMyAdd','O',1,NULL,1,'center','open',1,NULL,'[top]'),(215,'2014-07-03 22:54:19',212,'device_upd','修改',3,'icon-edit','updMySelected','O',1,NULL,1,'center','open',1,NULL,'[top, middle]'),(216,'2014-07-03 22:55:12',212,'device_del','删除',4,'icon-remove','delMySelected','O',1,NULL,1,'center','open',1,NULL,'[top, middle]'),(217,'2014-07-05 21:34:24',192,'oil_all','润滑项目管理',4,'icon-db','oil/all','F',1,NULL,1,'center','closed',1,NULL,'[top]'),(218,'2014-07-05 21:35:13',217,'oil_view','查看',1,'icon-search','viewSelected','O',1,NULL,1,'center','open',1,NULL,'[top, middle]'),(219,'2014-07-05 21:35:42',217,'oil_add','添加',2,'icon-add','toAdd','O',1,NULL,1,'center','open',1,NULL,'[top]'),(220,'2014-07-05 21:36:27',217,'oil_upd','修改',3,'icon-edit','updSelected','O',1,NULL,1,'center','open',1,NULL,'[top, middle]'),(221,'2014-07-05 21:37:01',217,'oil_del','删除',4,'icon-remove','delSelected','O',1,NULL,1,'center','open',1,NULL,'[top, middle]'),(222,'2014-07-06 00:45:59',192,'detectItem_all','检修项目管理',5,'icon-config','detectItem/all','F',1,NULL,1,'center','closed',1,NULL,'[top]'),(223,'2014-07-06 00:46:36',222,'detectItem_view','查看',1,'icon-search','viewSelected','O',1,NULL,1,'center','open',1,NULL,'[top, middle]'),(224,'2014-07-06 00:47:09',222,'detectItem_add','添加',2,'icon-add','toAdd','O',1,NULL,1,'center','open',1,NULL,'[top]'),(225,'2014-07-06 00:47:47',222,'detectItem_upd','修改',3,'icon-edit','updSelected','O',1,NULL,1,'center','open',1,NULL,'[top, middle]'),(226,'2014-07-06 00:48:22',222,'detectItem_del','删除',4,'icon-remove','delSelected','O',1,NULL,1,'center','open',1,NULL,'[top, middle]'),(227,'2014-07-06 16:41:01',142,'replaceRecord_all','更换记录管理',3,'icon-save','replaceRecord/all','F',1,NULL,1,'center','closed',1,NULL,'[top]'),(228,'2014-07-06 16:41:39',142,'oilRecord_all','润滑记录管理',4,'icon-comp','oilRecored/all','F',1,NULL,1,'center','closed',1,NULL,'[top]'),(229,'2014-07-06 16:47:46',142,'query_all','查询记录管理',5,'icon-search','','F',1,NULL,1,'center','closed',0,NULL,'[top]'),(230,'2014-07-06 16:49:52',229,'queryDetect_all','检修记录查询',1,'icon-comp','query/detect/all','F',1,NULL,1,'center','closed',1,NULL,'[top]'),(231,'2014-07-06 16:50:58',229,'queryReplace_all','更换记录查询',2,'icon-save','query/replace/all','F',1,NULL,1,'center','closed',1,NULL,'[top]'),(232,'2014-07-06 16:52:04',229,'queryOil_all','润滑记录查询',3,'icon-sys','query/oil/all','F',1,NULL,1,'center','closed',1,NULL,'[top]'),(233,'2014-07-06 17:24:02',142,'help_all','维修帮助信息管理',6,'icon-tip','help/all','F',1,NULL,1,'center','closed',1,NULL,'[top]'),(234,'2014-07-13 21:46:10',227,'replaceRecord_view','查看',1,'icon-search','viewSelected','O',1,NULL,1,'center','open',1,NULL,'[top, middle]'),(235,'2014-07-13 21:47:12',227,'replaceRecord_add','添加',2,'icon-add','toAdd','O',1,NULL,1,'center','open',1,NULL,'[top]'),(236,'2014-07-13 21:48:51',227,'replaceRecord_upd','修改',3,'icon-edit','updSelected','O',1,NULL,1,'center','open',1,NULL,'[top, middle]'),(237,'2014-07-13 21:49:36',227,'replaceRecord_del','删除',4,'icon-remove','delSelected','O',1,NULL,1,'center','open',1,NULL,'[top, middle]'),(238,'2014-07-13 21:59:38',228,'oilRecord_view','查看',1,'icon-search','viewSelected','O',1,NULL,1,'center','open',1,NULL,'[top, middle]'),(239,'2014-07-13 22:00:24',228,'oilRecord_add','添加',2,'icon-add','toAdd','O',1,NULL,1,'center','open',1,NULL,'[top]'),(240,'2014-07-13 22:02:54',228,'oilRecord_upd','修改',3,'icon-edit','updSelected','O',1,NULL,1,'center','open',1,NULL,'[top, middle]'),(241,'2014-07-13 22:04:21',228,'oilRecord_del','删除',4,'icon-remove','delSelected','O',1,NULL,1,'center','open',1,NULL,'[top, middle]'),(242,'2014-07-28 21:14:42',0,'viewOwnerDevice','查看本部门设备',7,'icon-pro','','P',1,'查看本部门设备',1,'center','closed',0,NULL,'[top]'),(243,'2014-07-28 21:16:07',0,'viewAllDevice','查看所有部门设备',8,'icon-role','','P',1,NULL,1,'center','closed',0,NULL,'[top]'),(244,'2014-08-08 20:15:33',15,'data_all','数据备份及恢复',3,'icon-sys','data/all','F',1,NULL,1,'center','closed',1,NULL,'[top]'),(245,'2014-08-05 23:02:26',244,'view_data','查看',1,'icon-search','viewSelected','O',1,NULL,1,'center','open',1,NULL,'[top]'),(246,'2014-08-09 11:02:16',142,'checkRecord_all','校验记录管理',4,'icon-pro','checkRecord/all','F',1,NULL,1,'center','closed',1,NULL,'[top]'),(247,'2014-08-09 11:02:54',246,'checkRecord_view','查看',1,'icon-search','viewSelected','O',1,NULL,1,'center','open',1,NULL,'[top, middle]'),(248,'2014-08-09 11:04:33',229,'queryCheck_all','检验记录查询',4,'icon-role','query/check/all','F',1,NULL,1,'center','closed',1,NULL,'[top]'),(249,'2014-08-09 14:14:30',246,'checkRecord_add','添加',2,'icon-add','toAdd','O',1,NULL,1,'center','open',1,NULL,'[top]'),(250,'2014-08-09 14:15:05',246,'checkRecord_upd','修改',3,'icon-edit','updSelected','O',1,NULL,1,'center','open',1,NULL,'[top, middle]'),(251,'2014-08-09 14:15:45',246,'checkRecord_del','删除',4,'icon-remove','delSelected','O',1,NULL,1,'center','open',1,NULL,'[top, middle]'),(252,'2014-08-24 20:10:19',192,'bigCate_all','设备大类管理',0,'icon-item','bigCate/all','F',1,NULL,1,'center','closed',1,NULL,'[top]'),(253,'2014-08-24 20:11:42',252,'bigCate_view','查看',1,'icon-search','viewSelected','O',1,NULL,1,'center','open',1,NULL,'[top, middle]'),(254,'2014-08-24 20:12:24',252,'bigCate_add','添加',2,'icon-add','toAdd','O',1,NULL,1,'center','open',1,NULL,'[top]'),(255,'2014-08-24 20:13:01',252,'bigCate_upd','修改',3,'icon-edit','updSelected','O',1,NULL,1,'center','open',1,NULL,'[top, middle]'),(256,'2014-08-24 20:13:36',252,'bigCate_del','删除',4,'icon-remove','delSelected','O',1,NULL,1,'center','open',1,NULL,'[top, middle]');

/*Table structure for table `y_record_compt` */

DROP TABLE IF EXISTS `y_record_compt`;

CREATE TABLE `y_record_compt` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT COMMENT '主键',
  `devcmpId` bigint(20) NOT NULL COMMENT '设备备件中间表id',
  `createdBy` bigint(20) NOT NULL COMMENT '录入人id',
  `replacedBy` bigint(20) NOT NULL COMMENT '替换人id',
  `createTime` datetime NOT NULL COMMENT '创建时间',
  `replaceTime` datetime NOT NULL COMMENT '替换时间',
  `description` text COMMENT '描述',
  `cost` double NOT NULL COMMENT '花费',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

/*Data for the table `y_record_compt` */

/*Table structure for table `y_repair` */

DROP TABLE IF EXISTS `y_repair`;

CREATE TABLE `y_repair` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT COMMENT '主键',
  `deviceId` bigint(20) NOT NULL COMMENT '设备id',
  `itemId` bigint(20) NOT NULL COMMENT '维修项目id',
  `cost` double DEFAULT NULL COMMENT '维修费用',
  `person` varchar(20) DEFAULT NULL COMMENT '维修人员',
  `description` text COMMENT '维修结果',
  `state` int(11) NOT NULL DEFAULT '1' COMMENT '默认为1，0为删除',
  `createdBy` bigint(20) NOT NULL COMMENT '创建(录入)人id',
  `deletedBy` bigint(20) DEFAULT NULL COMMENT '删除人id',
  `createTime` datetime NOT NULL COMMENT '默认Now()',
  `deleteTime` datetime DEFAULT NULL COMMENT '删除时间',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

/*Data for the table `y_repair` */

/*Table structure for table `y_replace_record` */

DROP TABLE IF EXISTS `y_replace_record`;

CREATE TABLE `y_replace_record` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT COMMENT '主键',
  `deviceCompId` bigint(20) NOT NULL COMMENT '设备备件id',
  `cost` double NOT NULL COMMENT '花费',
  `state` int(11) NOT NULL DEFAULT '1' COMMENT '状态,默认1，0为删除',
  `createdBy` bigint(20) NOT NULL COMMENT '创建人id',
  `replacedBy` bigint(20) DEFAULT NULL COMMENT '更换人id',
  `replacedDate` date NOT NULL COMMENT '更换日期',
  `createTime` datetime NOT NULL COMMENT '创建时间',
  `deletedBy` bigint(20) DEFAULT NULL COMMENT '删除人员id',
  `deleteTime` datetime DEFAULT NULL COMMENT '删除时间',
  `description` varchar(200) DEFAULT NULL COMMENT '备注',
  `replaceUserName` varchar(20) NOT NULL COMMENT '更换人员',
  `count` int(11) NOT NULL DEFAULT '1' COMMENT '第几次更换',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=utf8;

/*Data for the table `y_replace_record` */

insert  into `y_replace_record`(`id`,`deviceCompId`,`cost`,`state`,`createdBy`,`replacedBy`,`replacedDate`,`createTime`,`deletedBy`,`deleteTime`,`description`,`replaceUserName`,`count`) values (1,1,31,1,113,NULL,'2014-08-09','2014-08-09 18:14:45',NULL,NULL,'魂牵梦萦','张三',1),(2,2,3332,1,1,NULL,'2014-08-11','2014-08-11 21:22:51',NULL,NULL,'好了','李王',1),(3,1,3232,1,1,NULL,'2014-08-26','2014-08-27 22:40:18',NULL,NULL,'暮云春树鞋柜','魂牵梦萦',2),(4,2,99,1,1,NULL,'2014-08-27','2014-08-27 23:17:42',NULL,NULL,NULL,'99',2);

/*Table structure for table `y_role` */

DROP TABLE IF EXISTS `y_role`;

CREATE TABLE `y_role` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `createTime` datetime NOT NULL,
  `state` int(11) NOT NULL DEFAULT '1',
  `createdBy` bigint(20) NOT NULL,
  `roleName` varchar(50) NOT NULL,
  `seq` bigint(20) DEFAULT '0',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=53 DEFAULT CHARSET=utf8;

/*Data for the table `y_role` */

insert  into `y_role`(`id`,`createTime`,`state`,`createdBy`,`roleName`,`seq`) values (1,'2013-09-29 00:05:19',1,1,'超级管理员',1),(50,'2014-08-09 17:16:37',1,1,'工厂厂长',2),(51,'2014-08-09 17:16:50',1,1,'工程部经理',3),(52,'2014-08-09 17:17:00',1,1,'生产部经理',4);

/*Table structure for table `y_rolepower` */

DROP TABLE IF EXISTS `y_rolepower`;

CREATE TABLE `y_rolepower` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `powerId` bigint(20) NOT NULL,
  `roleId` bigint(20) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=5637 DEFAULT CHARSET=utf8;

/*Data for the table `y_rolepower` */

insert  into `y_rolepower`(`id`,`powerId`,`roleId`) values (546,11,1),(547,14,1),(548,15,1),(549,16,1),(550,18,1),(551,19,1),(552,26,1),(553,27,1),(554,30,1),(555,31,1),(556,32,1),(557,33,1),(558,34,1),(559,35,1),(560,37,1),(561,40,1),(562,41,1),(563,42,1),(564,43,1),(565,45,1),(566,52,1),(567,62,1),(568,63,1),(569,64,1),(570,65,1),(571,66,1),(572,67,1),(573,68,1),(574,69,1),(575,70,1),(576,71,1),(577,72,1),(578,73,1),(579,74,1),(580,75,1),(581,76,1),(582,77,1),(583,78,1),(584,79,1),(585,80,1),(586,81,1),(609,82,1),(610,83,1),(646,89,1),(647,90,1),(648,92,1),(649,93,1),(650,94,1),(651,95,1),(652,96,1),(653,97,1),(654,98,1),(655,99,1),(656,100,1),(657,101,1),(658,102,1),(659,103,1),(660,104,1),(661,105,1),(662,106,1),(663,107,1),(686,108,1),(687,109,1),(688,110,1),(690,111,1),(691,112,1),(692,113,1),(694,114,1),(695,115,1),(696,116,1),(707,117,1),(761,117,1),(762,118,1),(763,119,1),(764,120,1),(765,121,1),(766,122,1),(767,123,1),(768,124,1),(769,125,1),(770,126,1),(771,127,1),(772,128,1),(773,129,1),(774,130,1),(775,131,1),(776,132,1),(777,133,1),(778,134,1),(779,135,1),(780,136,1),(781,137,1),(782,138,1),(835,139,1),(836,140,1),(837,141,1),(1153,142,1),(1154,143,1),(1194,144,1),(1195,145,1),(1196,146,1),(1769,147,1),(1853,148,1),(1897,149,1),(2157,150,1),(2158,151,1),(2159,152,1),(2160,153,1),(2161,154,1),(2577,155,1),(2578,156,1),(2607,157,1),(2695,158,1),(2696,159,1),(2697,160,1),(2698,161,1),(2706,140,1),(2707,141,1),(2708,142,1),(2709,143,1),(2710,144,1),(2711,145,1),(2712,146,1),(2713,147,1),(2714,148,1),(2715,149,1),(2716,150,1),(2717,151,1),(2718,152,1),(2719,153,1),(2720,154,1),(2721,155,1),(2722,156,1),(2723,157,1),(2724,158,1),(2725,159,1),(2726,160,1),(2727,161,1),(2728,162,1),(2729,163,1),(2828,164,1),(2829,165,1),(2830,166,1),(2831,167,1),(2832,168,1),(2833,169,1),(2834,170,1),(2835,171,1),(2836,172,1),(2837,173,1),(2838,174,1),(2839,175,1),(2840,176,1),(2841,177,1),(2842,178,1),(2843,179,1),(2844,180,1),(2845,181,1),(2846,182,1),(2847,183,1),(2848,184,1),(2849,185,1),(2850,186,1),(2851,187,1),(2852,188,1),(2853,189,1),(2854,190,1),(2855,191,1),(2856,192,1),(2857,193,1),(2858,194,1),(2859,195,1),(2860,196,1),(2861,197,1),(2862,198,1),(2863,199,1),(2864,200,1),(2865,201,1),(2866,203,1),(2867,202,1),(2868,204,1),(2869,205,1),(2870,206,1),(2871,207,1),(2872,208,1),(2873,209,1),(2874,210,1),(2875,211,1),(2876,212,1),(2877,213,1),(2878,214,1),(2879,215,1),(2880,216,1),(2881,217,1),(2882,218,1),(2883,219,1),(2884,220,1),(2885,221,1),(2886,222,1),(2887,223,1),(2888,224,1),(2889,225,1),(2890,226,1),(2891,227,1),(2892,228,1),(2893,229,1),(2894,230,1),(2895,231,1),(2896,232,1),(2897,233,1),(2898,234,1),(2899,235,1),(2900,236,1),(2901,237,1),(2902,238,1),(2903,239,1),(2904,240,1),(2905,241,1),(2993,242,1),(2994,243,1),(2995,244,1),(3592,244,1),(3652,245,1),(3877,246,1),(3878,247,1),(3879,248,1),(3934,249,1),(3935,250,1),(3936,251,1),(4288,202,52),(4289,203,52),(4290,204,52),(4291,205,52),(4292,206,52),(4293,212,52),(4294,213,52),(4295,214,52),(4296,215,52),(4297,216,52),(4298,217,52),(4299,218,52),(4300,219,52),(4301,220,52),(4302,221,52),(4303,222,52),(4304,223,52),(4305,224,52),(4306,225,52),(4307,226,52),(4308,197,52),(4309,199,52),(4310,198,52),(4311,200,52),(4312,201,52),(4313,227,52),(4314,234,52),(4315,235,52),(4316,236,52),(4317,237,52),(4318,228,52),(4319,238,52),(4320,239,52),(4321,240,52),(4322,241,52),(4323,246,52),(4324,247,52),(4325,249,52),(4326,250,52),(4327,251,52),(4328,229,52),(4329,230,52),(4330,231,52),(4331,232,52),(4332,248,52),(4333,242,52),(4334,142,52),(4335,192,52),(5457,14,50),(5458,16,50),(5459,71,50),(5460,52,50),(5461,65,50),(5462,70,50),(5463,18,50),(5464,74,50),(5465,111,50),(5466,112,50),(5467,113,50),(5468,142,50),(5469,192,50),(5470,202,50),(5471,203,50),(5472,204,50),(5473,205,50),(5474,206,50),(5475,207,50),(5476,208,50),(5477,209,50),(5478,210,50),(5479,211,50),(5480,212,50),(5481,213,50),(5482,214,50),(5483,215,50),(5484,216,50),(5485,217,50),(5486,218,50),(5487,219,50),(5488,220,50),(5489,221,50),(5490,222,50),(5491,223,50),(5492,224,50),(5493,225,50),(5494,226,50),(5495,197,50),(5496,199,50),(5497,198,50),(5498,200,50),(5499,201,50),(5500,227,50),(5501,234,50),(5502,235,50),(5503,236,50),(5504,237,50),(5505,228,50),(5506,238,50),(5507,239,50),(5508,240,50),(5509,241,50),(5510,246,50),(5511,247,50),(5512,249,50),(5513,250,50),(5514,251,50),(5515,229,50),(5516,230,50),(5517,231,50),(5518,232,50),(5519,248,50),(5520,233,50),(5521,243,50),(5579,142,51),(5580,192,51),(5581,202,51),(5582,203,51),(5583,204,51),(5584,205,51),(5585,206,51),(5586,207,51),(5587,208,51),(5588,209,51),(5589,210,51),(5590,211,51),(5591,212,51),(5592,213,51),(5593,214,51),(5594,215,51),(5595,216,51),(5596,217,51),(5597,218,51),(5598,219,51),(5599,220,51),(5600,221,51),(5601,222,51),(5602,223,51),(5603,224,51),(5604,225,51),(5605,226,51),(5606,197,51),(5607,199,51),(5608,198,51),(5609,200,51),(5610,201,51),(5611,227,51),(5612,234,51),(5613,235,51),(5614,236,51),(5615,237,51),(5616,228,51),(5617,238,51),(5618,239,51),(5619,240,51),(5620,241,51),(5621,246,51),(5622,247,51),(5623,249,51),(5624,250,51),(5625,251,51),(5626,229,51),(5627,230,51),(5628,231,51),(5629,232,51),(5630,248,51),(5631,242,51),(5632,252,1),(5633,253,1),(5634,254,1),(5635,255,1),(5636,256,1);

/*Table structure for table `y_user` */

DROP TABLE IF EXISTS `y_user`;

CREATE TABLE `y_user` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `createTime` datetime NOT NULL COMMENT '创建时间',
  `state` int(11) NOT NULL DEFAULT '1' COMMENT '状态默认为1，0为删除',
  `createdBy` bigint(20) NOT NULL COMMENT '创建人id',
  `deptId` bigint(20) NOT NULL COMMENT '部门id',
  `identity` int(11) NOT NULL DEFAULT '1' COMMENT '标记位,admin是2，默认为1',
  `post` varchar(255) DEFAULT NULL COMMENT '职位',
  `seq` int(20) DEFAULT '0' COMMENT '排序序号',
  `sex` varchar(5) NOT NULL COMMENT '性别',
  `userName` varchar(50) NOT NULL COMMENT '用户名',
  `userPwd` varchar(50) DEFAULT NULL COMMENT '密码',
  `number` varchar(20) DEFAULT NULL COMMENT '用户编号',
  `email` varchar(20) DEFAULT NULL COMMENT '电子邮箱',
  `telphone` varchar(20) DEFAULT NULL COMMENT '电话号码',
  `realName` varchar(20) NOT NULL COMMENT '真实姓名',
  PRIMARY KEY (`id`),
  UNIQUE KEY `number` (`number`),
  KEY `FK_USER_DEPT` (`deptId`)
) ENGINE=InnoDB AUTO_INCREMENT=115 DEFAULT CHARSET=utf8;

/*Data for the table `y_user` */

insert  into `y_user`(`id`,`createTime`,`state`,`createdBy`,`deptId`,`identity`,`post`,`seq`,`sex`,`userName`,`userPwd`,`number`,`email`,`telphone`,`realName`) values (1,'2014-01-07 15:43:13',1,1,1,2,'超级管理员',1,'男','admin','c4ca4238a0b923820dcc509a6f75849b','0',NULL,NULL,'超级管理员'),(112,'2014-08-09 17:18:55',1,1,1,1,'厂长',0,'男','hby','c4ca4238a0b923820dcc509a6f75849b',NULL,NULL,NULL,'黄宝勇'),(113,'2014-08-09 17:19:28',1,1,39,1,'工程部经理',0,'男','xp','c4ca4238a0b923820dcc509a6f75849b',NULL,NULL,NULL,'许平'),(114,'2014-08-09 17:19:50',1,1,40,1,'生产部经理',0,'男','ymf','c4ca4238a0b923820dcc509a6f75849b',NULL,NULL,NULL,'叶梅芳');

/*Table structure for table `y_userrole` */

DROP TABLE IF EXISTS `y_userrole`;

CREATE TABLE `y_userrole` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT COMMENT '主键',
  `userId` bigint(20) NOT NULL COMMENT '用户id',
  `roleId` bigint(20) NOT NULL COMMENT '角色id',
  PRIMARY KEY (`id`),
  KEY `FK_USER_ID` (`userId`)
) ENGINE=InnoDB AUTO_INCREMENT=222 DEFAULT CHARSET=utf8;

/*Data for the table `y_userrole` */

insert  into `y_userrole`(`id`,`userId`,`roleId`) values (2,1,1),(217,112,50),(220,113,51),(221,114,52);

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;
