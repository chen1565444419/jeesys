package com.yzt.insys.beans;

import com.yzt.insys.beans.BaseEntity;

/**
 * 创建人:朱顺福<br />
 * 创建日期:2014年8月24日<br />
 * 创建时间:下午8:14:48<br />
 * 说      明:<br />
 */
public class BigCate extends BaseEntity {
	private static final long serialVersionUID = -6359992086927717373L;
	private String bigCateName;
	public String getBigCateName() {
		return bigCateName;
	}
	public void setBigCateName(String bigCateName) {
		this.bigCateName = bigCateName;
	}

	
}
