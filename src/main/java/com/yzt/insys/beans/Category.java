/**
 * 
 */
package com.yzt.insys.beans;

import java.io.Serializable;

/**
 * 创建人:朱顺福<br />
 * 创建日期:2014年6月15日<br />
 * 创建时间:下午2:29:51<br />
 * 说      明:设备类型实体类
 */
public class Category extends BaseEntity implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1332948084383052478L;
	private String categoryName;
	private Long bigCateId;
	private BigCate bigCate;
	
	public String getCategoryName() {
		return categoryName;
	}
	public void setCategoryName(String categoryName) {
		this.categoryName = categoryName;
	}
	public BigCate getBigCate() {
		return bigCate;
	}
	public void setBigCate(BigCate bigCate) {
		this.bigCate = bigCate;
	}
	public Long getBigCateId() {
		return bigCateId;
	}
	public void setBigCateId(Long bigCateId) {
		this.bigCateId = bigCateId;
	}
	public static long getSerialversionuid() {
		return serialVersionUID;
	}
	
	
}
