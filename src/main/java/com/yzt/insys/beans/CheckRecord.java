/**
 * 
 */
package com.yzt.insys.beans;

import java.io.Serializable;
import java.util.Date;


/**
 * 创建人:朱顺福<br />
 * 创建日期:2014年8月9日<br />
 * 创建时间:下午1:28:27<br />
 * 说      明:<br />
 */
public class CheckRecord extends BaseEntity implements Serializable{

	/**
	 * 
	 */
	private static final long serialVersionUID = 86087673528685834L;
	
	private Date checkDate;//校验日期
	private String checkUnit;//校验单位 
	private String result;//校验结果
	private User createdUser;
	private int checkCount;//校验次数
	private Long deviceId;
	private Device device;
	
	public Date getCheckDate() {
		return checkDate;
	}
	public void setCheckDate(Date checkDate) {
		this.checkDate = checkDate;
	}
	public String getCheckUnit() {
		return checkUnit;
	}
	public void setCheckUnit(String checkUnit) {
		this.checkUnit = checkUnit;
	}
	public String getResult() {
		return result;
	}
	public void setResult(String result) {
		this.result = result;
	}
	public User getCreatedUser() {
		return createdUser;
	}
	public void setCreatedUser(User createdUser) {
		this.createdUser = createdUser;
	}
	public int getCheckCount() {
		return checkCount;
	}
	public void setCheckCount(int checkCount) {
		this.checkCount = checkCount;
	}
	public Long getDeviceId() {
		return deviceId;
	}
	public void setDeviceId(Long deviceId) {
		this.deviceId = deviceId;
	}
	public Device getDevice() {
		return device;
	}
	public void setDevice(Device device) {
		this.device = device;
	}
	public static long getSerialversionuid() {
		return serialVersionUID;
	}

}
