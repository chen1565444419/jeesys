/**
 * 
 */
package com.yzt.insys.beans;

import java.io.Serializable;
import java.sql.Date;

/**
 * 创建人:朱顺福<br />
 * 创建日期:2014年7月1日<br />
 * 创建时间:下午9:35:26<br />
 * 说 明:<br />
 */
public class Component extends BaseEntity implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = -1633470389256334240L;
	private String name;
	private int cycle;
	private int count;
	private Date startDate;
	
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public int getCycle() {
		return cycle;
	}
	public void setCycle(int cycle) {
		this.cycle = cycle;
	}
	public int getCount() {
		return count;
	}
	public void setCount(int count) {
		this.count = count;
	}
	public Date getStartDate() {
		return startDate;
	}
	public void setStartDate(Date startDate) {
		this.startDate = startDate;
	}
	public static long getSerialversionuid() {
		return serialVersionUID;
	}
	
}
