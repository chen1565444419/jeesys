/**
 * 
 */
package com.yzt.insys.beans;

import java.io.Serializable;

/**
 * 创建人:朱顺福<br />
 * 创建日期:2014年7月6日<br />
 * 创建时间:下午7:48:57<br />
 * 说      明:<br />
 */
public class DetectItem extends BaseEntity implements Serializable{

	private static final long serialVersionUID = 1275117454019479023L;

	private String name;
	private String standard;
	private String notice;
	private Long categoryId;
	
	private Category category;
	
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public String getStandard() {
		return standard;
	}
	public void setStandard(String standard) {
		this.standard = standard;
	}
	public String getNotice() {
		return notice;
	}
	public void setNotice(String notice) {
		this.notice = notice;
	}
	public Long getCategoryId() {
		return categoryId;
	}
	public void setCategoryId(Long categoryId) {
		this.categoryId = categoryId;
	}
	public static long getSerialversionuid() {
		return serialVersionUID;
	}
	public Category getCategory() {
		return category;
	}
	public void setCategory(Category category) {
		this.category = category;
	}
}
