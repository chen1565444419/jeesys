/**
 * 
 */
package com.yzt.insys.beans;

import java.io.Serializable;
import java.util.Date;

/**
 * 创建人:朱顺福<br />
 * 创建日期:2014年6月15日<br />
 * 创建时间:下午2:39:16<br />
 * 说      明:设备检修结果表实体类
 */
public class DetectRecord extends BaseEntity implements Serializable{

	private static final long serialVersionUID = -5914614400044472987L;
	private Long deviceId;
	private Long detectItemId;
	private Long detectId;
	private User detectUser;
	private Long deleteBy;
	private Device device;
	private DetectItem detectItem;
	private Date detectDate;
	private String result;
	private User createdUser;
	private String detectUserName;
	private int count;//次数
	public Long getDeviceId() {
		return deviceId;
	}
	public void setDeviceId(Long deviceId) {
		this.deviceId = deviceId;
	}
	public Long getDetectItemId() {
		return detectItemId;
	}
	public void setDetectItemId(Long detectItemId) {
		this.detectItemId = detectItemId;
	}
	public Long getDetectId() {
		return detectId;
	}
	public void setDetectId(Long detectId) {
		this.detectId = detectId;
	}
	public Long getDeleteBy() {
		return deleteBy;
	}
	public void setDeleteBy(Long deleteBy) {
		this.deleteBy = deleteBy;
	}
	public static long getSerialversionuid() {
		return serialVersionUID;
	}
	public Device getDevice() {
		return device;
	}
	public void setDevice(Device device) {
		this.device = device;
	}
	public DetectItem getDetectItem() {
		return detectItem;
	}
	public void setDetectItem(DetectItem detectItem) {
		this.detectItem = detectItem;
	}
	
	public Date getDetectDate() {
		return detectDate;
	}
	public void setDetectDate(Date detectDate) {
		this.detectDate = detectDate;
	}
	public String getResult() {
		return result;
	}
	public void setResult(String result) {
		this.result = result;
	}
	public User getDetectUser() {
		return detectUser;
	}
	public void setDetectUser(User detectUser) {
		this.detectUser = detectUser;
	}
	public User getCreatedUser() {
		return createdUser;
	}
	public void setCreatedUser(User createdUser) {
		this.createdUser = createdUser;
	}
	public String getDetectUserName() {
		return detectUserName;
	}
	public void setDetectUserName(String detectUserName) {
		this.detectUserName = detectUserName;
	}
	public int getCount() {
		return count;
	}
	public void setCount(int count) {
		this.count = count;
	}
	
	
}
