/**
 * 
 */
package com.yzt.insys.beans;

import java.io.Serializable;
import java.util.Date;
import java.util.List;

/**
 * 创建人:朱顺福<br />
 * 创建日期:2014年6月15日<br />
 * 创建时间:下午2:37:07<br />
 * 说      明:设备实体类
 */
public class Device extends BaseEntity implements Serializable{

	/**
	 * 
	 */
	private static final long serialVersionUID = 3769864087215839532L;
	private String no;
	private String name;
	private String producer;
	private String modelNumber;
	private String usedSite;
	private String lineType;
	private Long deptId;
	private Dept dept;
	private Long categoryId;
	private Category category;
	private List<Component> components;
	private List<Oil> oils;
	private int type;
	private Date startDate;
	private Date expireDetectDate;/*到期检修日期*/
	private Date expireOilDate;//到期润滑日期*/
	private int checkType;//校验类型
	public String getNo() {
		return no;
	}
	public void setNo(String no) {
		this.no = no;
	}
	public Dept getDept() {
		return dept;
	}
	public void setDept(Dept dept) {
		this.dept = dept;
	}
	public Category getCategory() {
		return category;
	}
	public void setCategory(Category category) {
		this.category = category;
	}
	public List<Component> getComponents() {
		return components;
	}
	public void setComponents(List<Component> components) {
		this.components = components;
	}
	public int getType() {
		return type;
	}
	public void setType(int type) {
		this.type = type;
	}
	public Long getDeptId() {
		return deptId;
	}
	public void setDeptId(Long deptId) {
		this.deptId = deptId;
	}
	public Long getCategoryId() {
		return categoryId;
	}
	public void setCategoryId(Long categoryId) {
		this.categoryId = categoryId;
	}
	public Date getStartDate() {
		return startDate;
	}
	public void setStartDate(Date startDate) {
		this.startDate = startDate;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public String getProducer() {
		return producer;
	}
	public void setProducer(String producer) {
		this.producer = producer;
	}
	public String getModelNumber() {
		return modelNumber;
	}
	public void setModelNumber(String modelNumber) {
		this.modelNumber = modelNumber;
	}
	public String getUsedSite() {
		return usedSite;
	}
	public void setUsedSite(String usedSite) {
		this.usedSite = usedSite;
	}
	public String getLineType() {
		return lineType;
	}
	public void setLineType(String lineType) {
		this.lineType = lineType;
	}
	public List<Oil> getOils() {
		return oils;
	}
	public void setOils(List<Oil> oils) {
		this.oils = oils;
	}
	public static long getSerialversionuid() {
		return serialVersionUID;
	}
	public Date getExpireDetectDate() {
		return expireDetectDate;
	}
	public void setExpireDetectDate(Date expireDetectDate) {
		this.expireDetectDate = expireDetectDate;
	}
	public Date getExpireOilDate() {
		return expireOilDate;
	}
	public void setExpireOilDate(Date expireOilDate) {
		this.expireOilDate = expireOilDate;
	}
	public int getCheckType() {
		return checkType;
	}
	public void setCheckType(int checkType) {
		this.checkType = checkType;
	}
	
	
	
	
}
