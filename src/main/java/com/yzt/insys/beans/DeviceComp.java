package com.yzt.insys.beans;

import java.io.Serializable;

/**
 * 创建人:朱顺福<br />
 * 创建日期:2014年7月15日<br />
 * 创建时间:下午11:58:02<br />
 * 说      明:<br />
 */
public class DeviceComp implements Serializable {

	private static final long serialVersionUID = -665659436478902662L;

	private Long deviceId;
	private Long compId;
	private Long deviceCompId;
	private Component component;
	public Long getDeviceId() {
		return deviceId;
	}
	public void setDeviceId(Long deviceId) {
		this.deviceId = deviceId;
	}
	public Long getCompId() {
		return compId;
	}
	public void setCompId(Long compId) {
		this.compId = compId;
	}
	public Long getDeviceCompId() {
		return deviceCompId;
	}
	public void setDeviceCompId(Long deviceCompId) {
		this.deviceCompId = deviceCompId;
	}
	public Component getComponent() {
		return component;
	}
	public void setComponent(Component component) {
		this.component = component;
	}
	public static long getSerialversionuid() {
		return serialVersionUID;
	}
	
	
}
