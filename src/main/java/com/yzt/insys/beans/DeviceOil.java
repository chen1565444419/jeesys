package com.yzt.insys.beans;

import java.io.Serializable;

/**
 * 创建人:朱顺福<br />
 * 创建日期:2014年7月15日<br />
 * 创建时间:下午11:58:02<br />
 * 说      明:<br />
 */
public class DeviceOil extends BaseEntity implements Serializable {

	private static final long serialVersionUID = -665659436478902662L;

	private Long deviceId;
	private Long oilId;
	private Long deviceOilId;
	private Oil oil;
	
	public Long getDeviceId() {
		return deviceId;
	}
	public void setDeviceId(Long deviceId) {
		this.deviceId = deviceId;
	}
	public Long getOilId() {
		return oilId;
	}
	public void setOilId(Long oilId) {
		this.oilId = oilId;
	}
	public Long getDeviceOilId() {
		return deviceOilId;
	}
	public void setDeviceOilId(Long deviceOilId) {
		this.deviceOilId = deviceOilId;
	}
	public Oil getOil() {
		return oil;
	}
	public void setOil(Oil oil) {
		this.oil = oil;
	}
	public static long getSerialversionuid() {
		return serialVersionUID;
	}
	
	
}
