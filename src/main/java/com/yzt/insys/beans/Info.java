/**
 * 
 */
package com.yzt.insys.beans;

import java.io.Serializable;

/**
 * 创建人:朱顺福<br />
 * 创建日期:2014年8月2日<br />
 * 创建时间:下午2:28:05<br />
 * 说      明:<br />
 */
public class Info extends BaseEntity implements Serializable{

	/**
	 * 
	 */
	private static final long serialVersionUID = 801985829632709326L;

	private String content;

	public String getContent() {
		return content;
	}

	public void setContent(String content) {
		this.content = content;
	}

	public static long getSerialversionuid() {
		return serialVersionUID;
	}
	
	
}
