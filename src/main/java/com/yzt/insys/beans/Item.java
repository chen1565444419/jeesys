/**
 * 
 */
package com.yzt.insys.beans;

import java.io.Serializable;

/**
 * 创建人:朱顺福<br />
 * 创建日期:2014年6月15日<br />
 * 创建时间:下午2:35:32<br />
 * 说      明:设备检修(维修)项目实体类
 */
public class Item extends BaseEntity implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 7208234334839414740L;
	private String name;
	private String standard;
	private String notice;
	private Long categoryId;
	
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public String getStandard() {
		return standard;
	}
	public void setStandard(String standard) {
		this.standard = standard;
	}
	public String getNotice() {
		return notice;
	}
	public void setNotice(String notice) {
		this.notice = notice;
	}
	public Long getCategoryId() {
		return categoryId;
	}
	public void setCategoryId(Long categoryId) {
		this.categoryId = categoryId;
	}
	
}
