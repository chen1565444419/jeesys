/**
 * 
 */
package com.yzt.insys.beans;

import java.io.Serializable;
import java.util.Date;

/**
 * 创建人:朱顺福<br />
 * 创建日期:2014年7月7日<br />
 * 创建时间:下午9:14:39<br />
 * 说      明:<br />
 */
public class Oil extends BaseEntity implements Serializable{

	private static final long serialVersionUID = 2835021172169479574L;

	private String name;
	private int cycle;
	private Date startDate;
	
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public static long getSerialversionuid() {
		return serialVersionUID;
	}
	public int getCycle() {
		return cycle;
	}
	public void setCycle(int cycle) {
		this.cycle = cycle;
	}
	public Date getStartDate() {
		return startDate;
	}
	public void setStartDate(Date startDate) {
		this.startDate = startDate;
	}
	
}
