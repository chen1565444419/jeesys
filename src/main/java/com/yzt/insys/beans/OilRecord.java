/**
 * 
 */
package com.yzt.insys.beans;

import java.io.Serializable;
import java.util.Date;

/**
 * 创建人:朱顺福<br />
 * 创建日期:2014年6月15日<br />
 * 创建时间:下午2:39:16<br />
 * 说      明:设备检修结果表实体类
 */
public class OilRecord extends BaseEntity implements Serializable{

	private static final long serialVersionUID = -5914614400044472987L;
	private Long deviceOilId;
	private Device device;
	private Oil oil;
	private Long oiledBy;
	private User oiledUser;
	private Long deleteBy;
	private Date oiledDate;
	private User createdUser;
	private Double cost;
	private String oiledUserName;
	private int count;//次数
	public Long getDeviceOilId() {
		return deviceOilId;
	}
	public void setDeviceOilId(Long deviceOilId) {
		this.deviceOilId = deviceOilId;
	}
	public Device getDevice() {
		return device;
	}
	public void setDevice(Device device) {
		this.device = device;
	}
	public Oil getOil() {
		return oil;
	}
	public void setOil(Oil oil) {
		this.oil = oil;
	}
	public Long getOiledBy() {
		return oiledBy;
	}
	public void setOiledBy(Long oiledBy) {
		this.oiledBy = oiledBy;
	}
	public User getOiledUser() {
		return oiledUser;
	}
	public void setOiledUser(User oiledUser) {
		this.oiledUser = oiledUser;
	}
	public Long getDeleteBy() {
		return deleteBy;
	}
	public void setDeleteBy(Long deleteBy) {
		this.deleteBy = deleteBy;
	}
	public Date getOiledDate() {
		return oiledDate;
	}
	public void setOiledDate(Date oiledDate) {
		this.oiledDate = oiledDate;
	}
	public User getCreatedUser() {
		return createdUser;
	}
	public void setCreatedUser(User createdUser) {
		this.createdUser = createdUser;
	}
	public static long getSerialversionuid() {
		return serialVersionUID;
	}
	public Double getCost() {
		return cost;
	}
	public void setCost(Double cost) {
		this.cost = cost;
	}
	public String getOiledUserName() {
		return oiledUserName;
	}
	public void setOiledUserName(String oiledUserName) {
		this.oiledUserName = oiledUserName;
	}
	public int getCount() {
		return count;
	}
	public void setCount(int count) {
		this.count = count;
	}
	
	
	
}
