package com.yzt.insys.beans;

import java.io.Serializable;

public class Power extends BaseEntity implements Serializable{
    /**
	 * 
	 */
	private static final long serialVersionUID = -6014763757576297289L;

    private String powerName;//权限名

    private Long fatherId;//父类id

    private String masterName;//编码

    private String iconCls;//图标
    
    private String url;//地址
    
    private String type;//类型，1:菜单,2操作
    
    private String description;//描述
    
    private String target;
    
    private Integer child;//根菜单
    
    private String position;
    
    private String status;//状态，closed或open
    
    private int used;//是否启用，1为启用，0为禁用
    
	public Integer getChild() {
		return child;
	}

	public void setChild(Integer child) {
		this.child = child;
	}

    
    public String getPowerName() {
        return powerName;
    }

    public void setPowerName(String powerName) {
        this.powerName = powerName == null ? null : powerName.trim();
    }

    public Long getFatherId() {
        return fatherId;
    }

    public void setFatherId(Long fatherId) {
        this.fatherId = fatherId;
    }

    public String getMasterName() {
        return masterName;
    }

    public void setMasterName(String masterName) {
        this.masterName = masterName == null ? null : masterName.trim();
    }

	public String getIconCls() {
		return iconCls;
	}

	public void setIconCls(String iconCls) {
		this.iconCls = iconCls;
	}

	public String getUrl() {
		return url;
	}

	public void setUrl(String url) {
		this.url = url;
	}

	public String getType() {
		return type;
	}

	public void setType(String type) {
		this.type = type;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public String getTarget() {
		return target;
	}

	public void setTarget(String target) {
		this.target = target;
	}

	public String getStatus() {
		return status;
	}

	public void setStatus(String status) {
		this.status = status;
	}

	public int getUsed() {
		return used;
	}

	public void setUsed(int used) {
		this.used = used;
	}

	public String getPosition() {
		return position;
	}

	public void setPosition(String position) {
		this.position = position;
	}
    
}