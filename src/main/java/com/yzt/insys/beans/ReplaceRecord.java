/**
 * 
 */
package com.yzt.insys.beans;

import java.io.Serializable;
import java.util.Date;

/**
 * 创建人:朱顺福<br />
 * 创建日期:2014年6月15日<br />
 * 创建时间:下午2:39:16<br />
 */
public class ReplaceRecord extends BaseEntity implements Serializable{

	private static final long serialVersionUID = -5914614400044472987L;
	private Long deviceCompId;
	private Device device;
	private Component component;
	private Double cost;
	private User replacedUser;
	private User createdUser;
	private Long replacedBy;
	private Date replacedDate;
	private String replaceUserName;
	private int count;//次数
	
	public Long getDeviceCompId() {
		return deviceCompId;
	}
	public void setDeviceCompId(Long deviceCompId) {
		this.deviceCompId = deviceCompId;
	}
	public Device getDevice() {
		return device;
	}
	public void setDevice(Device device) {
		this.device = device;
	}
	public Component getComponent() {
		return component;
	}
	public void setComponent(Component component) {
		this.component = component;
	}
	public Double getCost() {
		return cost;
	}
	public void setCost(Double cost) {
		this.cost = cost;
	}
	public User getReplacedUser() {
		return replacedUser;
	}
	public void setReplacedUser(User replacedUser) {
		this.replacedUser = replacedUser;
	}
	public Long getReplacedBy() {
		return replacedBy;
	}
	public void setReplacedBy(Long replacedBy) {
		this.replacedBy = replacedBy;
	}
	public Date getReplacedDate() {
		return replacedDate;
	}
	public void setReplacedDate(Date replacedDate) {
		this.replacedDate = replacedDate;
	}
	public static long getSerialversionuid() {
		return serialVersionUID;
	}
	public User getCreatedUser() {
		return createdUser;
	}
	public void setCreatedUser(User createdUser) {
		this.createdUser = createdUser;
	}
	public String getReplaceUserName() {
		return replaceUserName;
	}
	public void setReplaceUserName(String replaceUserName) {
		this.replaceUserName = replaceUserName;
	}
	public int getCount() {
		return count;
	}
	public void setCount(int count) {
		this.count = count;
	}
	
	
}
