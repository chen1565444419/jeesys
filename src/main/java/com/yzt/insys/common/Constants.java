package com.yzt.insys.common;


public interface Constants {

	public static final String USER_KEY_IN_SESSION = "USER_KEY_IN_SESSION";
	
	public static final int PAGE_SIZE=100000;
	
	public static final String ENCODEING="utf-8";
	
	public static final String POWER_RESOURCES="POWER_RESOURCES";
	
	public static final String DATE_FORMAT="yyyy-MM-dd";
	
	public static final String DATE_TIME_FORMAT="yyyy-MM-dd HH:mm:ss";
	
}
