package com.yzt.insys.common;

import java.util.List;

import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.support.GeneratedKeyHolder;

public class MyJdbcTemplete extends JdbcTemplate {
	
	
	
	/**
	 * 插入数据并返回主键(Long类型)值
	 * @param sql
	 * @param paramList 参数集合
	 * @return 返回主键
	 */
	public Long insert(String sql,List<Object> paramList) {
		GeneratedKeyHolder keyHolder=new GeneratedKeyHolder();
		return super.update(new MyPsCreator(sql,paramList),keyHolder)>=1?keyHolder.getKey().longValue():null;
	}
	
	
	/**
	 * 查询对象
	 * @param sql
	 * @param paramList 参数集合
	 * @param mappedClass 被映射的类
	 * @return
	 */
	public <T> T queryObject(String sql,List<Object> paramList,Class<T> mappedClass){
		if(paramList==null||paramList.size()==0){
			return queryForObject(sql,mappedClass);
		}else{
			return super.queryForObject(sql, paramList.toArray(),MyRowMapper.newInstance(mappedClass));
		}
	}

	/**
	 * 查询对象
	 * @param sql
	 * @param mappedClass 被映射的类
	 */
	public <T> T queryObject(String sql,Class<T> mappedClass){
		return super.queryForObject(sql, MyRowMapper.newInstance(mappedClass));
	}
	

	/**
	 * 更新数据
	 * @param sql
	 * @param paramList 参数集合
	 * @return 返回所影响的行数
	 */
	public int update(String sql,List<Object> paramList){
		if(paramList==null||paramList.size()==0){
			return super.update(sql);
		}else{
			return super.update(sql, paramList.toArray());			
		}
	}

	
	/**
	 * 查询数据
	 * @param sql
	 * @param paramList 参数集合
	 * @param mappedClass 被映射的类
	 * @return 返回被映射类的集合
	 */
	public <T> List<T> queryList(String sql,List<Object> paramList,Class<T> mappedClass){
		if(paramList==null||paramList.size()==0){
			return super.query(sql,MyRowMapper.newInstance(mappedClass));
		}else{
			return super.query(sql,paramList.toArray(),MyRowMapper.newInstance(mappedClass));			
		}
	}

	/**
	 * 查询数量
	 * @param sql
	 * @param paramList 参数集合
	 * @return
	 */
	@SuppressWarnings("deprecation")
	public int queryInt(String sql,List<Object> paramList){
		if(paramList==null||paramList.size()==0){
			return super.queryForInt(sql);
		}else{
			return super.queryForInt(sql, paramList.toArray());
		}
	}
}
