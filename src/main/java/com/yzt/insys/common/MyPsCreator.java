package com.yzt.insys.common;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.List;

import org.springframework.jdbc.core.PreparedStatementCreator;

public class MyPsCreator implements PreparedStatementCreator {
	
	private String sql;
	private List<Object> paramList;
	
	public MyPsCreator(String sql,List<Object> paramList){
		this.sql=sql;
		this.paramList=paramList;
	}
	
	public MyPsCreator(String sql){
		this.sql=sql;
	}
	
	@Override
	public PreparedStatement createPreparedStatement(Connection conn)
			throws SQLException {
		PreparedStatement ps=conn.prepareStatement(sql,Statement.RETURN_GENERATED_KEYS);
		if(paramList!=null&&paramList.size()>0){
			for(int i=1,len=paramList.size();i<=len;i++){
				ps.setObject(i,paramList.get(i-1));				
			}
		}
		return ps;
	}

}
