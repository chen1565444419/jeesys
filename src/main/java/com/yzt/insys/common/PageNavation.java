package com.yzt.insys.common;

import java.io.Serializable;
import java.util.Map;


/**
 * 
 * @作者 zsf
 * @时间 2013-6-24
 */
public class PageNavation implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = -2758558522988842114L;

	/**
	 * 总页数
	 */
	private Integer totalPage;
	
	/**
	 * 当前页数
	 */
	private Integer currentPage=1;
	
	/**
	 * 总记录数
	 */
	private Integer totalRecords;
	
	/**
	 * 下一页
	 */
	private Integer nextPage;
	
	/**
	 * 上一页
	 */
	private Integer prePage;
	
	/**
	 * 每页显示的数量
	 */
	private Integer pageSize=Constants.PAGE_SIZE;

	/**
	 * 开始位置
	 */
	private Integer startIndex;
	
	/**
	 * 首页
	 */
	private Integer firstPage=1;
	
	/**
	 * 末页
	 */
	private Integer lastPage;
	
	/**
	 * paramMap
	 */
	private Map<String,Object> paramMap;
	
	private String order;
	
	private String sort;
	
	
	public PageNavation() {
		super();
	}

	public PageNavation(Integer totalPage, Integer currentPage, Integer totalRecords,
			Integer nextPage, Integer prePage, Integer pageSize,Integer startIndex,
			Integer firstPage,Integer lastPage) {
		super();
		this.totalPage = totalPage;
		this.currentPage = currentPage;
		this.totalRecords = totalRecords;
		this.nextPage = nextPage;
		this.prePage = prePage;
		this.pageSize = pageSize;
		this.startIndex=startIndex;
		this.firstPage=firstPage;
		this.lastPage=lastPage;
	}

	public Integer getTotalPage() {
		return totalPage;
	}

	public void setTotalPage(Integer totalPage) {
		this.totalPage = totalPage;
	}

	public Integer getCurrentPage() {
		return currentPage;
	}

	public void setCurrentPage(Integer currentPage) {
		this.currentPage = currentPage;
	}

	public Integer getTotalRecords() {
		return totalRecords;
	}

	public void setTotalRecords(Integer totalRecords) {
		this.totalRecords = totalRecords;
	}

	public Integer getNextPage() {
		return nextPage;
	}

	public void setNextPage(Integer nextPage) {
		this.nextPage = nextPage;
	}

	public Integer getPrePage() {
		return prePage;
	}

	public void setPrePage(Integer prePage) {
		this.prePage = prePage;
	}

	public Integer getPageSize() {
		return pageSize;
	}

	public void setPageSize(Integer pageSize) {
		this.pageSize = pageSize;
	}

	public static long getSerialversionuid() {
		return serialVersionUID;
	}

	public Integer getStartIndex() {
		return startIndex;
	}

	public void setStartIndex(Integer startIndex) {
		this.startIndex = startIndex;
	}

	public Integer getFirstPage() {
		return firstPage;
	}

	public void setFirstPage(Integer firstPage) {
		this.firstPage = firstPage;
	}

	public Integer getLastPage() {
		return lastPage;
	}

	public void setLastPage(Integer lastPage) {
		this.lastPage = lastPage;
	}

	public Map<String, Object> getParamMap() {
		return paramMap;
	}

	public void setParamMap(Map<String, Object> paramMap) {
		this.paramMap = paramMap;
	}

	public String getOrder() {
		return order;
	}

	public void setOrder(String order) {
		this.order = order;
	}

	public String getSort() {
		return sort;
	}

	public void setSort(String sort) {
		this.sort = sort;
	}
	
}
