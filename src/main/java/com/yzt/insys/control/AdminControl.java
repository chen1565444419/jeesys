package com.yzt.insys.control;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;

import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;

import com.yzt.insys.common.Constants;
import com.yzt.insys.dto.UserDto;
import com.yzt.insys.service.PowerService;

/**
 * @作者 zsf
 * @时间 2013-6-20
 */
@Controller
@Scope("prototype")
@RequestMapping("/admin")
public class AdminControl extends BaseControl{
	
	@Resource
	@Qualifier("powerService")
	private PowerService pService;
	
	@RequestMapping("/commons/{path}")
	public String commons(@PathVariable("path")String path){
		return "commons/"+path;
	}
	/**
	 * 加载相应菜单页面
	 */
	@RequestMapping(value="/menu/**")
	public String menu(HttpServletRequest req,Long id) {
		String name=getViewNameForRequest(req).replace("admin/","").replace("menu/","");
		UserDto user=(UserDto)session.getAttribute(Constants.USER_KEY_IN_SESSION);
		reqAddAttr("id",id);
		paramMap.clear();
		paramMap.put("id",id);
		paramMap.put("roles",user.getRoles());
		reqAddAttr("paramMap",paramMap=getParamMap());
		reqAddAttr("powerList",pService.queryPowersByFatherId(paramMap));
		return "menu/"+name;
	}
	/**
	 * 加载左边菜单
	 */
	@RequestMapping("/left")
	public String left(Long id) {
		UserDto user=(UserDto)session.getAttribute(Constants.USER_KEY_IN_SESSION);
		paramMap.clear();
		paramMap.put("id",id);
		paramMap.put("roles",user.getRoles());
		reqAddAttr("powerList",pService.queryPowersByFatherId(paramMap));
		return "commons/left";
	}
	/**
	 * 跳转到显示流程图片页面
	 */
	@RequestMapping("/toViewImg/{id}")
	public String toViewImg(@PathVariable("id")String id) {
		reqAddAttr("id", id);
		return "commons/viewImg";
	}
}
