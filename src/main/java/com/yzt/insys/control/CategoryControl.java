/**
 * 
 */
package com.yzt.insys.control;

import java.util.List;

import javax.annotation.Resource;

import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import com.yzt.insys.beans.Category;
import com.yzt.insys.common.Constants;
import com.yzt.insys.common.PageNavation;
import com.yzt.insys.service.BigCateService;
import com.yzt.insys.service.CategoryService;
import com.yzt.insys.service.ComponentService;
import com.yzt.insys.service.DetectRecordService;
import com.yzt.insys.service.DeviceService;
import com.yzt.insys.service.OilRecordService;
import com.yzt.insys.service.OilService;
import com.yzt.insys.service.PowerService;
import com.yzt.insys.service.ReplaceRecordService;

/**
 * 创建人:朱顺福<br />
 * 创建日期:2014年6月15日<br />
 * 创建时间:下午3:01:34<br />
 * 说      明:设备类型Control<br />
 */
@Scope("prototype")
@Controller
@RequestMapping("/category")
public class CategoryControl extends BaseControl {

	@Resource
	@Qualifier("categoryService")
	private CategoryService cateService;
	
	@Resource
	private DeviceService deviceService;
	
	@Resource
	private DetectRecordService detectRecordService;
	
	@Resource
	private OilService oilService;
	
	@Resource
	private ReplaceRecordService replaceRecordService;
	
	@Resource
	private OilRecordService oilRecordService;
	
	@Resource
	private ComponentService componentService;
	
	@Resource
	private BigCateService bigCateService;
	
	@Resource
	private PowerService powerService;
	
	@RequestMapping(value = "/all/{masterName}")
	public synchronized String toAll(@PathVariable("masterName")String masterName) {
		reqAddAttr("powers",powerService.querySubPowerByMasterName(masterName));
		reqAddAttr("bigCateList",bigCateService.queryModelList(getPage()));
		return "menu/category/all";
	}
	
	@ResponseBody
	@RequestMapping(value="/getData",method={RequestMethod.POST,RequestMethod.GET})
	public String getData(PageNavation page){
		StringBuilder sb = new StringBuilder();
		paramMap=getParamMap();
		if(paramMap.get("page")==null){
			paramMap.put("page",1);
		}
		if(paramMap.get("rows")==null){
			paramMap.put("rows",Constants.PAGE_SIZE);
		}
		page.setParamMap(paramMap);
		page.setCurrentPage(Integer.parseInt(paramMap.get("page").toString()));//获取当前页码
		page.setPageSize(Integer.parseInt(paramMap.get("rows").toString()));//获取每页显示数量
		page.setStartIndex((page.getCurrentPage()-1)*page.getPageSize());//设置查询开始位置
		int totalCount=cateService.queryModelCount(paramMap);
		List<Category> list=null;
		sb.append("{\"total\":"+totalCount+",");
		if(totalCount!=0){
			list=cateService.queryModelList(page);//获取集合对象	
			sb.append("\"rows\":"+toJSONStringWithDateFormat(list,null)+"}");			
		}else{
			sb.append("\"rows\":[]}");			
		}
		return sb.toString();
	}
	
	@ResponseBody
	@RequestMapping("/checkCategoryName")
	public Object checkCategoryName(){
		return cateService.isNameExist(getParamMap());
	}
	
	@ResponseBody
	@RequestMapping(value="/add",method=RequestMethod.POST)
	public Object add(Category cate){
		cate.setCreatedBy(getUserFromSession().getId());
		return cateService.insert(cate);
	}
	
	@RequestMapping("/toAdd")
	public synchronized String toAdd(){
		reqAddAttr("paramMap",getParamMap());
		reqAddAttr("bigCateList",bigCateService.queryModelList(getPage()));
		return forward_toAdd();
	}
	
	@RequestMapping("/toUpd/{id}")
	public String toUpd(@PathVariable("id") Long id){
		reqAddAttr("paramMap",getParamMap());
		reqAddAttr("cate",cateService.queryByPK(id));
		reqAddAttr("bigCateList",bigCateService.queryModelList(getPage()));
		return forward_toUpd();
	}
	
	@ResponseBody
	@RequestMapping(value="/upd",method=RequestMethod.POST)
	public Object upd(Category category){
		return cateService.updateByPK(category);
	}
	
	@ResponseBody
	@RequestMapping(value="/del",method=RequestMethod.POST)
	public Object del(@RequestParam("ids")List<Long> ids){
		boolean flg=false;
		paramMap=getDeletedMap(ids);
		flg=cateService.delete(paramMap);//删除类型
		/*
		List<Long> deviceIds=deviceService.queryIdsByCateoryIds(paramMap);//获取设备id
		if(deviceIds!=null&&deviceIds.size()>0){
			paramMap.put("ids",deviceIds);
			deviceService.delete(paramMap);//删除设备
			replaceRecordService.deleteByDeviceIds(paramMap);//删除y_replace_record表中的数据
			detectRecordService.deleteByDeviceIds(paramMap);	
			//paramMap.put("ids",deviceIds);
			oilService.deleteByDeviceId(paramMap);//删除y_device_oil表中的数据
			componentService.deleteByDeviceIds(paramMap);//删除y_device_comp表中的数据
			List<Long> deviceOilIds=oilService.queryIdsByDeviceIds(paramMap);//获取deviceOilId
			if(deviceOilIds.size()>0){
				paramMap.put("ids",deviceOilIds);				
				oilRecordService.delete(paramMap);//删除y_oil_record表中的数据
			}
		}*/
		return flg;
	}
	
	@RequestMapping("/view/{id}")
	public String view(@PathVariable("id")Long id){
		reqAddAttr("cate",cateService.queryByPK(id));
		return forward_toView();
	}
	
	@ResponseBody
	@RequestMapping(value="/getAllCategory",method=RequestMethod.POST)
	public Object getAllCategory(){
		return cateService.queryModelList(getPage());
	}
}
