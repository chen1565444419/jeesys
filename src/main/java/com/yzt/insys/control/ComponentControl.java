/**
 * 
 */
package com.yzt.insys.control;

import java.util.List;

import javax.annotation.Resource;

import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import com.alibaba.fastjson.JSON;
import com.yzt.insys.beans.Component;
import com.yzt.insys.common.PageNavation;
import com.yzt.insys.service.ComponentService;

/**
 * 创建人:朱顺福<br />
 * 创建日期:2014年7月2日<br />
 * 创建时间:上午12:46:14<br />
 * 说      明:<br />
 */
@Scope("prototype")
@Controller
@RequestMapping("/component")
public class ComponentControl extends BaseControl {

	@Resource
	@Qualifier("componentService")
	private ComponentService componentService;
	
	@ResponseBody
	@RequestMapping(value="/getData",method={RequestMethod.POST,RequestMethod.GET})
	public String getData(PageNavation page){
		StringBuilder sb=new StringBuilder();
		paramMap=getParamMap();
		page.setParamMap(paramMap);
		page.setCurrentPage(Integer.parseInt(paramMap.get("page").toString()));//获取当前页码
		page.setPageSize(Integer.parseInt(paramMap.get("rows").toString()));//获取每页显示数量
		page.setStartIndex((page.getCurrentPage()-1)*page.getPageSize());//设置查询开始位置
		int totalCount=componentService.queryModelCount(paramMap);
		List<Component> list=null;
		sb.append("{\"total\":"+totalCount+",");
		if(totalCount!=0){
			list=componentService.queryModelList(page);//获取集合对象
			sb.append("\"rows\":"+toJSONStringWithDateFormat(list)+"}");			
		}else{
			sb.append("\"rows\":[]}");
		}
		return sb.toString();
	}
	
	@ResponseBody
	@RequestMapping("/getComponents")
	public Object getAllComponents(){
		List<Component> components=componentService.queryModelList(getPage());
		return JSON.toJSONString(components);
	}
	
	@ResponseBody
	@RequestMapping("/checkCompName")
	public Object checkCompName(){
		return componentService.isNameExist(getParamMap());
	}
	
	@ResponseBody
	@RequestMapping(value="/add",method=RequestMethod.POST)
	public Object add(Component component){
		component.setCreatedBy(getUserFromSession().getId());
		return componentService.insert(component);
	}
	
	@RequestMapping("/toUpd/{id}")
	public String toUpd(@PathVariable("id") Long id){
		reqAddAttr("paramMap",getParamMap());
		reqAddAttr("component",componentService.queryByPK(id));
		return forward_toUpd();
	}
	
	@ResponseBody
	@RequestMapping(value="/upd",method=RequestMethod.POST)
	public Object upd(Component component){
		return componentService.updateByPK(component);
	}
	
	@ResponseBody
	@RequestMapping(value="/del",method=RequestMethod.POST)
	public Object del(@RequestParam("ids")List<Long> ids){
		return componentService.delete(getDeletedMap(ids));
	}
	
	@RequestMapping("/view/{id}")
	public Object view(@PathVariable("id")Long id){
		reqAddAttr("component",componentService.queryByPK(id));
		return forward_toView();
	}
}
