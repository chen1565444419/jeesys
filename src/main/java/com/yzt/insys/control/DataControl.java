/**
 * 
 */
package com.yzt.insys.control;

import java.io.File;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;
import java.util.UUID;

import org.apache.commons.io.FileUtils;
import org.apache.log4j.Logger;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.multipart.MultipartHttpServletRequest;

import com.yzt.insys.util.CMDUtil;

/**
 * 创建人:朱顺福<br />
 * 创建日期:2014年8月5日<br />
 * 创建时间:下午10:57:31<br />
 * 说      明:<br />
 */
@Scope("prototype")
@Controller
@RequestMapping("/data")
public class DataControl extends BaseControl {

	private Logger log=Logger.getLogger(DataControl.class);
	
	public DataControl() {
		log.info("=============="+this.getClass().getName());
	}

	private SimpleDateFormat simple=new SimpleDateFormat("yyyy-MM-dd");
	
	@RequestMapping(value = "/all/{masterName}")
	public synchronized String toAll(@PathVariable("masterName")String masterName) {
		return "menu/data/all";
	}
	
	@ResponseBody
	@RequestMapping("/dump")
	public Object dump(){
		logger.info("开始备份......");
		String fileName=simple.format(new Date())+".sql";
		StringBuilder sql=new StringBuilder();
		sql.append("cmd /c mysqldump");
		sql.append(" --add-drop-table");
		sql.append(" --default-character-set=utf8");
		sql.append(" -hlocalhost");
		sql.append(" -uroot");
		sql.append(" -p123456");
		sql.append(" jeesys");
		sql.append(" >");
		sql.append(" D:/BackUp/"+fileName);
		logger.info("执行命令:"+sql.toString());
		CMDUtil.executeCmdFlash(sql.toString());
		logger.info("备份结束......");
		paramMap.clear();
		paramMap.put("filePath","D:/BackUp/"+fileName);
		paramMap.put("result",true);
		return paramMap;
	}
	
	@ResponseBody
	@RequestMapping(value = "/imp", method = RequestMethod.POST)
	public String imp(MultipartHttpServletRequest request,String editor) {
		List<MultipartFile> files = request.getFiles("file");
		String rooePath=File.separator+"resources";
		String basePath=session.getServletContext().getRealPath(rooePath);
		String flg="false";
		String relativePath=File.separator+"uploadFiles"+File.separator+"file"+File.separator;
		try {
			for (MultipartFile file : files) {
				if (file.isEmpty()) {
					continue;
				}
				String suffix = file.getOriginalFilename();
				suffix = suffix.substring(suffix.lastIndexOf("."));
				String fileName=UUID.randomUUID().toString()+suffix;
				FileUtils.copyInputStreamToFile(file.getInputStream(),new File(basePath+relativePath+fileName));
				flg=rooePath+relativePath+fileName;
				impSql(basePath+relativePath+fileName);
			}
		} catch (IOException e) {
			e.printStackTrace();
		}
		return flg;
	}
	
	public void impSql(String filePath){
		logger.info("开始将数据导入到数据库。。。。。");
		StringBuilder sql=new StringBuilder();
		sql.append("cmd /c mysql");
		sql.append(" -hlocahost");
		sql.append(" -uroot");
		sql.append(" -p123456");
		sql.append(" jeesys");
		sql.append(" <");
		sql.append(" "+filePath);
		sql.append(" --default-character-set=utf8");
		logger.info("执行命令:"+sql.toString());
		CMDUtil.executeCmdFlash(sql.toString());
		logger.info("导入到数据库结束......");
	}
}
