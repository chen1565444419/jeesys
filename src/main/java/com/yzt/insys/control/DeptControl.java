package com.yzt.insys.control;

import java.util.List;

import javax.annotation.Resource;

import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import com.alibaba.fastjson.JSON;
import com.yzt.insys.beans.Dept;
import com.yzt.insys.common.Constants;
import com.yzt.insys.common.PageNavation;
import com.yzt.insys.dto.UserDto;
import com.yzt.insys.service.DeptService;

@Scope("prototype")
@Controller
@RequestMapping("/dept")
public class DeptControl extends BaseControl {

	@Resource
	@Qualifier("deptService")
	private DeptService dService;
	
	/**
	 * 获取部门集合
	 */
	@ResponseBody
	@RequestMapping(value="/getData",method=RequestMethod.POST)
	public String getData(PageNavation page){
		paramMap=getParamMap();
		page.setParamMap(paramMap);
		page.setCurrentPage(Integer.parseInt(paramMap.get("page").toString()));//获取当前页码
		page.setPageSize(Integer.parseInt(paramMap.get("rows").toString()));//获取每页显示数量
		page.setStartIndex((page.getCurrentPage()-1)*page.getPageSize());//设置查询开始位置
		int totalCount=dService.queryDeptCount();//获取总记录数
		List<Dept> deptList=dService.queryModelList(page);//获取集合对象
		StringBuilder sb=new StringBuilder();
		sb.append("{\"total\":"+totalCount+",");
		sb.append("\"rows\":"+JSON.toJSON(deptList)+"}");
		return sb.toString();
	}
	
	/**
	 * 添加部门
	 */
	@ResponseBody
	@RequestMapping(value="/add",method=RequestMethod.POST)
	public Object add(Dept dept){
		UserDto user=(UserDto)session.getAttribute(Constants.USER_KEY_IN_SESSION);
		dept.setCreatedBy(user.getId());
		return dService.insert(dept);
	}
	/**
	 * 跳转到修改部门页面
	 */
	@RequestMapping(value="/toUpd/{id}",method=RequestMethod.GET)
	public String updDept(@PathVariable("id") Long id){
		reqAddAttr("dept",dService.queryByPK(id));
		return "menu/dept/toUpd";
	}
	/**
	 * 修改部门
	 */
	@ResponseBody
	@RequestMapping(value="/upd",method=RequestMethod.POST)
	public Object updDept(Dept dept){
		return dService.updateByPK(dept);
	}
	/**
	 * 批量删除部门
	 */
	@ResponseBody
	@RequestMapping(value="/del",method=RequestMethod.POST)
	public Object deleteByPK(@RequestParam("ids")List<Long> ids){
		return dService.deleteByPKs(ids);
	}
	/**
	 * 跳转到查看部门对象页面
	 */
	@RequestMapping(value="/view/{id}")
	public Object view(@PathVariable("id") Long id){
		reqAddAttr("dept",dService.queryByPK(id));
		return "menu/dept/view";
	}
	/**
	 * 检修部门名是否存在
	 */
	@ResponseBody
	@RequestMapping(value="/checkDeptName",method=RequestMethod.POST)
	public Object checkDeptName(){
		paramMap=getParamMap();
		return dService.isDeptNameExist(paramMap);
	}
	/**
	 * 检修部门集合中是否有用户
	 */
	@ResponseBody
	@RequestMapping(value="/checkExistUser",method=RequestMethod.POST)
	public Object checkExistUser(@RequestParam("ids")List<Long> ids){
		boolean b=dService.checkExistUser(ids);
		return b;
	}
}
