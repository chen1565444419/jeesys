/**
 * 
 */
package com.yzt.insys.control;

import java.util.List;

import javax.annotation.Resource;

import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import com.yzt.insys.beans.DetectItem;
import com.yzt.insys.common.PageNavation;
import com.yzt.insys.service.BigCateService;
import com.yzt.insys.service.CategoryService;
import com.yzt.insys.service.DetectItemService;
import com.yzt.insys.service.PowerService;

/**
 * 创建人:朱顺福<br />
 * 创建日期:2014年7月6日<br />
 * 创建时间:下午7:47:57<br />
 * 说 明:<br />
 */
@Scope("prototype")
@Controller
@RequestMapping("/detectItem")
public class DetectItemControl extends BaseControl {

	@Resource
	@Qualifier("detectItemService")
	private DetectItemService detectItemService;

	@Resource
	@Qualifier("categoryService")
	private CategoryService categoryService;
	
	@Resource
	private PowerService powerService;
	
	@Resource
	private BigCateService bigCateService;
	
	
	@RequestMapping(value = "/all/{masterName}")
	public synchronized String toAll(@PathVariable("masterName")String masterName) {
		reqAddAttr("powers",powerService.querySubPowerByMasterName(masterName));
		return "menu/detectItem/all";
	}
	
	@ResponseBody
	@RequestMapping(value = "/getData", method = {RequestMethod.POST,RequestMethod.GET})
	public String getData(PageNavation page) {
		paramMap = getParamMap();
		StringBuilder sb = new StringBuilder();
		page.setParamMap(paramMap);
		page.setCurrentPage(Integer.parseInt(paramMap.get("page").toString()));// 获取当前页码
		page.setPageSize(Integer.parseInt(paramMap.get("rows").toString()));// 获取每页显示数量
		page.setStartIndex((page.getCurrentPage() - 1) * page.getPageSize());// 设置查询开始位置
		int totalCount = detectItemService.queryModelCount(paramMap);
		List<DetectItem> list = null;
		sb.append("{\"total\":"+totalCount+",");
		if (totalCount != 0) {
			list = detectItemService.queryModelList(page);// 获取集合对象
			sb.append("\"rows\":"+toJSONStringWithDateFormat(list,null)+"}");
		}else{
			sb.append("\"rows\":[]}");
		}
		return sb.toString();
	}

	@RequestMapping("/toAdd")
	public String toAdd() {
		reqAddAttr("paramMap", getParamMap());
		reqAddAttr("categoryList",categoryService.queryModelList(getPage()));
		reqAddAttr("bigCateList",bigCateService.queryModelList(getPage()));
		return forward_toAdd();
	}
	
	@ResponseBody
	@RequestMapping(value="/add",method=RequestMethod.POST)
	public Object add(DetectItem detectItem){
		detectItem.setCreatedBy(getUserFromSession().getId());
		return detectItemService.insert(detectItem);
	}
	
	@RequestMapping("/toUpd/{id}")
	public String toUpd(@PathVariable("id") Long id){
		reqAddAttr("categoryList",categoryService.queryModelList(getPage()));
		reqAddAttr("paramMap",getParamMap());
		reqAddAttr("detectItem",detectItemService.queryByPK(id));
		reqAddAttr("bigCateList",bigCateService.queryModelList(getPage()));
		return forward_toUpd();
	}
	
	@ResponseBody
	@RequestMapping(value="/upd",method=RequestMethod.POST)
	public Object upd(DetectItem detectItem){
		return detectItemService.updateByPK(detectItem);
	}
	
	@ResponseBody
	@RequestMapping(value="/del",method=RequestMethod.POST)
	public Object del(@RequestParam("ids")List<Long> ids){
		return detectItemService.delete(getDeletedMap(ids));
	}
	
	@RequestMapping("/view/{id}")
	public Object view(@PathVariable("id")Long id){
		reqAddAttr("detectItem",detectItemService.queryByPK(id));
		return forward_toView();
	}
	
}
