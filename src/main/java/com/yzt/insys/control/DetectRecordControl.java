/**
 * 
 */
package com.yzt.insys.control;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.annotation.Resource;

import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import com.yzt.insys.beans.DetectRecord;
import com.yzt.insys.common.PageNavation;
import com.yzt.insys.dto.CountMap;
import com.yzt.insys.service.BigCateService;
import com.yzt.insys.service.CategoryService;
import com.yzt.insys.service.DetectItemService;
import com.yzt.insys.service.DetectRecordService;
import com.yzt.insys.service.UserService;
import com.yzt.insys.util.tag.JSTLUtil;

/**
 * 创建人:朱顺福<br />
 * 创建日期:2014年7月6日<br />
 * 创建时间:下午7:47:57<br />
 * 说 明:<br />
 */
@Scope("prototype")
@Controller
@RequestMapping("/detectRecord")
public class DetectRecordControl extends BaseControl {

	@Resource
	@Qualifier("detectRecordService")
	private DetectRecordService detectRecordService;

	@Resource
	private CategoryService categoryService;
	
	@Resource
	private DetectItemService detectItemService;
	
	@Resource
	private UserService uService;
	
	@Resource
	private BigCateService bigCateService;
	
	@ResponseBody
	@RequestMapping(value = "/getData", method = {RequestMethod.POST,RequestMethod.GET})
	public String getData(PageNavation page) {
		paramMap = getParamMap();
		StringBuilder sb = new StringBuilder();
		page.setParamMap(paramMap);
		page.setCurrentPage(Integer.parseInt(paramMap.get("page").toString()));// 获取当前页码
		page.setPageSize(Integer.parseInt(paramMap.get("rows").toString()));// 获取每页显示数量
		page.setStartIndex((page.getCurrentPage() - 1) * page.getPageSize());// 设置查询开始位置
		int totalCount = detectRecordService.queryModelCount(paramMap);
		List<DetectRecord> list = null;
		sb.append("{\"total\":"+totalCount+",");
		if (totalCount != 0) {
			list = detectRecordService.queryModelList(page);// 获取集合对象
			sb.append("\"rows\":"+toJSONStringWithDateFormat(list,null)+"}");
		}else{
			sb.append("\"rows\":[]}");
		}
		return sb.toString();
	}

	@RequestMapping("/toAdd")
	public String toAdd() {
		reqAddAttr("paramMap", getParamMap());
		reqAddAttr("categoryList",categoryService.queryModelList(getPage()));
		reqAddAttr("bigCateList",bigCateService.queryModelList(getPage()));
		return forward_toAdd();
	}
	
	@ResponseBody
	@RequestMapping(value="/add",method=RequestMethod.POST)
	public Object add(DetectRecord detectRecord){
		detectRecord.setCreatedBy(getUserFromSession().getId());
		return detectRecordService.insert(detectRecord);
	}
	
	@RequestMapping("/toUpd/{id}")
	public String toUpd(@PathVariable("id") Long id){
		reqAddAttr("paramMap",getParamMap());
		reqAddAttr("detectRecord",detectRecordService.queryByPK(id));
		reqAddAttr("bigCateList",bigCateService.queryModelList(getPage()));
		return forward_toUpd();
	}
	
	@ResponseBody
	@RequestMapping(value="/upd",method=RequestMethod.POST)
	public Object upd(DetectRecord detectRecord){
		return detectRecordService.updateByPK(detectRecord);
	}
	
	@ResponseBody
	@RequestMapping(value="/del",method=RequestMethod.POST)
	public Object del(@RequestParam("ids")List<Long> ids){
		return detectRecordService.delete(getDeletedMap(ids));
	}
	
	@RequestMapping("/view/{id}")
	public Object view(@PathVariable("id")Long id){
		DetectRecord detectRecord=detectRecordService.queryByPK(id);
		reqAddAttr("detectRecord",detectRecord);
		reqAddAttr("category",categoryService.queryByPK(detectRecord.getDevice().getCategoryId()));
		return forward_toView();
	}
	
	@ResponseBody
	@RequestMapping("/getCountMap")
	public Object getCountMap(Long deviceId,Long categoryId){
		paramMap.clear();
		paramMap.put("deviceId",deviceId);
		List<CountMap> countList=detectRecordService.queryDetectCount(paramMap);
		paramMap.put("categoryId",categoryId);
		int detectItemCount=detectItemService.queryModelCount(paramMap);
		List<Map<String,Object>> dataList=new ArrayList<Map<String,Object>>();
		if(countList!=null&&countList.size()>0){
			for(CountMap cp:countList){
				if(detectItemCount!=cp.getTime()){//如果检修项目数和检修次数不相等
					paramMap=new HashMap<String,Object>();
					paramMap.put("count",cp.getCount());
					paramMap.put("times","第"+JSTLUtil.num2Str(cp.getCount())+"次");
					dataList.add(paramMap);		
				}
			}
			paramMap=new HashMap<String,Object>();
			paramMap.put("count",countList.get(countList.size()-1).getCount()+1);
			paramMap.put("times","第"+JSTLUtil.num2Str(countList.get(countList.size()-1).getCount()+1)+"次");
			dataList.add(paramMap);
		}else{
			paramMap=new HashMap<String,Object>();
			paramMap.put("count",1);
			paramMap.put("times","第一次");
			dataList.add(paramMap);
		}
		return dataList;
	}
}
