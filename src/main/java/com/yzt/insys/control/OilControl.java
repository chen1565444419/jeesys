/**
 * 
 */
package com.yzt.insys.control;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import javax.annotation.Resource;

import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import com.yzt.insys.beans.DeviceOil;
import com.yzt.insys.beans.Oil;
import com.yzt.insys.common.PageNavation;
import com.yzt.insys.service.DeviceService;
import com.yzt.insys.service.OilService;

/**
 * 创建人:朱顺福<br />
 * 创建日期:2014年7月7日<br />
 * 创建时间:下午9:12:58<br />
 * 说      明:<br />
 */
@Scope("prototype")
@Controller
@RequestMapping("/oil")
public class OilControl extends BaseControl {

	@Resource
	@Qualifier("oilService")
	private OilService oilService;
	
	@Resource
	private DeviceService deviceService;
	
	@ResponseBody
	@RequestMapping(value = "/getData", method = {RequestMethod.POST,RequestMethod.GET})
	public String getData(PageNavation page) {
		paramMap = getParamMap();
		StringBuilder sb = new StringBuilder();
		page.setParamMap(paramMap);
		page.setCurrentPage(Integer.parseInt(paramMap.get("page").toString()));// 获取当前页码
		page.setPageSize(Integer.parseInt(paramMap.get("rows").toString()));// 获取每页显示数量
		page.setStartIndex((page.getCurrentPage() - 1) * page.getPageSize());// 设置查询开始位置
		int totalCount = oilService.queryModelCount(paramMap);
		List<Oil> list = null;
		sb.append("{\"total\":"+totalCount+",");
		if (totalCount != 0) {
			list = oilService.queryModelList(page);// 获取集合对象
			sb.append("\"rows\":"+toJSONStringWithDateFormat(list,null)+"}");
		}else{
			sb.append("\"rows\":[]}");
		}
		return sb.toString();
	}
	
	@ResponseBody
	@RequestMapping("/getAllOil")
	public Object getAllOil(){
		return oilService.queryModelList(getPage());
	}

	@RequestMapping("/toAdd")
	public String toAdd() {
		reqAddAttr("paramMap", getParamMap());
		return forward_toAdd();
	}
	
	@ResponseBody
	@RequestMapping(value="/add",method=RequestMethod.POST)
	public Object add(Oil oil){
		oil.setCreatedBy(getUserFromSession().getId());
		return oilService.insert(oil);
	}
	
	@RequestMapping("/toUpd/{id}")
	public String toUpd(@PathVariable("id") Long id){
		reqAddAttr("paramMap",getParamMap());
		reqAddAttr("oil",oilService.queryByPK(id));
		return forward_toUpd();
	}
	
	@ResponseBody
	@RequestMapping(value="/upd",method=RequestMethod.POST)
	public Object upd(Oil oil){
		return oilService.updateByPK(oil);
	}
	
	@ResponseBody
	@RequestMapping(value="/del",method=RequestMethod.POST)
	public Object del(@RequestParam("ids")List<Long> ids){
		paramMap.clear();
		paramMap.put("deletedBy",getUserFromSession().getId());
		paramMap.put("ids",ids);
		return oilService.delete(paramMap);
	}
	
	@RequestMapping("/view/{id}")
	public Object view(@PathVariable("id")Long id){
		reqAddAttr("oil",oilService.queryByPK(id));
		return forward_toView();
	}
	
	@ResponseBody
	@RequestMapping(value="/getAllOilByDeviceId")
	public Object getAllOilByDeviceId(Long deviceId,Integer count){
		StringBuffer sb=new StringBuffer();
		if(deviceId==null){
			sb.append("{\"total\":0,");
			sb.append("\"rows\":[]}");
			return sb.toString();
		}
		paramMap.clear();
		paramMap.put("deviceId",deviceId);
		paramMap.put("count",count);
		List<DeviceOil> deviceOils=deviceService.getAllOilByDeviceId(paramMap);
		if(deviceOils.size()==0||deviceOils.get(0)==null){
			sb.append("{\"total\":0,");
			sb.append("\"rows\":");
			sb.append("[]}");
			return sb.toString();
		}
		sb.append("{\"total\":"+deviceOils.size()+",");
		sb.append("\"rows\":");
		paramList=new ArrayList<Object>();
		for(DeviceOil diOil:deviceOils){
			paramMap=new HashMap<String,Object>();
			paramMap.put("deviceOilId",diOil.getDeviceOilId());
			paramMap.put("name",diOil.getOil().getName());
			paramMap.put("cycle",diOil.getOil().getCycle());
			paramMap.put("startDate",diOil.getOil().getStartDate());
			paramList.add(paramMap);
		}
		sb.append(toJSONStringWithDateFormat(paramList,"yyyy-MM-dd")+"}");
		return sb.toString();
	}
}
