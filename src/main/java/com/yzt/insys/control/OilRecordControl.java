package com.yzt.insys.control;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import javax.annotation.Resource;

import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import com.yzt.insys.beans.OilRecord;
import com.yzt.insys.common.PageNavation;
import com.yzt.insys.service.BigCateService;
import com.yzt.insys.service.CategoryService;
import com.yzt.insys.service.OilRecordService;
import com.yzt.insys.util.tag.JSTLUtil;

/**
 * 创建人:朱顺福<br />
 * 创建日期:2014年7月13日<br />
 * 创建时间:下午9:50:27<br />
 * 说      明:<br />
 */
@Scope("prototype")
@Controller
@RequestMapping("/oilRecord")
public class OilRecordControl extends BaseControl {

	@Resource
	private OilRecordService oilRecordService;
	
	@Resource
	private CategoryService categoryService;
	
	@Resource
	private BigCateService bigCateService;
	
	@ResponseBody
	@RequestMapping(value = "/getData", method = {RequestMethod.POST,RequestMethod.GET})
	public String getData(PageNavation page) {
		paramMap = getParamMap();
		StringBuilder sb = new StringBuilder();
		page.setParamMap(paramMap);
		page.setCurrentPage(Integer.parseInt(paramMap.get("page").toString()));// 获取当前页码
		page.setPageSize(Integer.parseInt(paramMap.get("rows").toString()));// 获取每页显示数量
		page.setStartIndex((page.getCurrentPage() - 1) * page.getPageSize());// 设置查询开始位置
		int totalCount = oilRecordService.queryModelCount(paramMap);
		List<OilRecord> list = null;
		sb.append("{\"total\":"+totalCount+",");
		if (totalCount != 0) {
			list = oilRecordService.queryModelList(page);// 获取集合对象
			sb.append("\"rows\":"+toJSONStringWithDateFormat(list,null)+"}");
		}else{
			sb.append("\"rows\":[]}");
		}
		return sb.toString();
	}
	
	@RequestMapping("/toAdd")
	public String toAdd() {
		reqAddAttr("paramMap", getParamMap());
		reqAddAttr("categoryList",categoryService.queryModelList(getPage()));
		reqAddAttr("bigCateList",bigCateService.queryModelList(getPage()));
		return forward_toAdd();
	}
	
	@ResponseBody
	@RequestMapping(value="/add",method=RequestMethod.POST)
	public Object add(OilRecord oilRecord){
		oilRecord.setCreatedBy(getUserFromSession().getId());
		return oilRecordService.insert(oilRecord);
	}
	
	@RequestMapping("/toUpd/{id}")
	public String toUpd(@PathVariable("id") Long id){
		reqAddAttr("paramMap",getParamMap());
		reqAddAttr("oilRecord",oilRecordService.queryByPK(id));
		reqAddAttr("bigCateList",bigCateService.queryModelList(getPage()));
		return forward_toUpd();
	}
	
	@ResponseBody
	@RequestMapping(value="/upd",method=RequestMethod.POST)
	public Object upd(OilRecord oilRecord){
		return oilRecordService.updateByPK(oilRecord);
	}
	
	@ResponseBody
	@RequestMapping(value="/del",method=RequestMethod.POST)
	public Object del(@RequestParam("ids")List<Long> ids){
		return oilRecordService.delete(getDeletedMap(ids));
	}
	
	@RequestMapping("/view/{id}")
	public Object view(@PathVariable("id")Long id){
		OilRecord oilRecord=oilRecordService.queryByPK(id);
		reqAddAttr("oilRecord",oilRecord);
		reqAddAttr("category",categoryService.queryByPK(oilRecord.getDevice().getCategoryId()));
		return forward_toView();
	}
	
	@ResponseBody
	@RequestMapping("/getCountMap")
	public Object getCountMap(Long deviceId){
		paramMap.clear();
		paramMap.put("deviceId",deviceId);
		int count=oilRecordService.queryOilCount(paramMap);
		paramList=new ArrayList<Object>();
		if(count>=1){
			for(int i=1;i<=count+1;i++){
				paramMap=new HashMap<String,Object>();
				paramMap.put("count",i);
				paramMap.put("times","第"+JSTLUtil.num2Str(i)+"次");
				if(i==count+1){
					paramMap.put("selected",true);
				}
				paramList.add(paramMap);				
			}
		}else{
			paramMap.clear();
			paramMap.put("count",count+1);
			paramMap.put("times","第"+JSTLUtil.num2Str(count+1)+"次");
			paramList.add(paramMap);	
		}
		return paramList;
	}

}
