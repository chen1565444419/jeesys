package com.yzt.insys.control;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import javax.annotation.Resource;

import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.serializer.SerializerFeature;
import com.yzt.insys.common.PageNavation;
import com.yzt.insys.dto.PowerDto;
import com.yzt.insys.service.PowerService;
import com.yzt.insys.service.RolePowerService;

@Scope("prototype")
@Controller
@RequestMapping("/power")
public class PowerControl extends BaseControl {

	@Resource
	@Qualifier("rolePowerService")
	private RolePowerService rpService;
	
	@Resource
	@Qualifier("powerService")
	private PowerService pService;
	
	/*@RequestMapping(value = "/all/{masterName}")
	public synchronized String toAll(@PathVariable("masterName")String masterName) {
		reqAddAttr("powers",pService.querySubPowerByMasterName(masterName));
		return "menu/power/all";
	}*/
	@ResponseBody
	@RequestMapping(value="/getData",method={RequestMethod.POST,RequestMethod.GET})
	public String getData(PageNavation page,Long id){
		paramMap=getParamMap();
		page.setParamMap(paramMap);
		if(id!=null){
			paramMap.put("fatherId",id);
		}
		List<PowerDto> powerList=pService.queryModelList(page);//获取原始集合对象
		
		String jsonResult=JSON.toJSONStringWithDateFormat(powerList,"yyyy-MM-dd HH:mm:ss",SerializerFeature.WriteDateUseDateFormat);
		jsonResult=jsonResult.replaceAll(",\"status\":",",\"state\":").replaceAll(",\"target\":",",\"targets\":").replaceAll(",\"powerName\":",",\"text\":");
		return jsonResult;
	}
	/**
	 * 授权*编辑菜单*查询全部权限
	 * @param page
	 * @param id
	 * @return
	 */
	@ResponseBody
	@RequestMapping(value="/getPowerEdit",method={RequestMethod.POST,RequestMethod.GET})
	public String getPowerEdit(PageNavation page,Long id){
		paramMap=getParamMap();
		page.setParamMap(paramMap);
		if(id!=null){
			paramMap.put("fatherId",id);
		}
		if(paramMap.get("mark")==null){
			paramMap.put("type","F");
		}
		List<PowerDto> powerList=pService.queryPowerList(page);//获取集合对象
		String jsonResult=JSON.toJSONStringWithDateFormat(powerList,"yyyy-MM-dd HH:mm:ss",SerializerFeature.WriteDateUseDateFormat);
		if(paramMap.get("mark")!=null){
			jsonResult=jsonResult.replaceAll("status","state").replaceAll("target","targets").replaceAll("powerName","text").replaceAll("closed","open");
		}
		return jsonResult;
	}
	/**
	 * 跳转到修改页面
	 * @param id
	 * @return
	 */
	@RequestMapping(value="/toUpd/{id}")
	public String toUpd(@PathVariable("id") Long id){
		reqAddAttr("power",pService.queryByPK(id));
		return "menu/power/toUpd";
	}
	/**
	 * 添加菜单
	 * @param power
	 * @return
	 */
	@ResponseBody
	@RequestMapping(value="/add",method={RequestMethod.POST})
	public Object add(PowerDto power,String[] position){
		power.setCreatedBy(getUserFromSession().getId());
		power.setStatus(power.getType().equals("F")?"closed":"open");
		power.setPosition(""+Arrays.asList(position));
		boolean flg=false;
		flg=pService.insert(power);
		paramMap.clear();
		paramMap.put("roleId",1);
		List<Long> powerList=new ArrayList<Long>();
		powerList.add(power.getId());
		paramMap.put("powerList",powerList);
		return flg&&rpService.saveRolePower(paramMap);
	}
	/**
	 * 修改菜单
	 * @param power
	 * @return
	 */
	@ResponseBody
	@RequestMapping(value="/upd",method={RequestMethod.POST})
	public Object upd(PowerDto power,String[] position){
		power.setStatus(power.getType().equals("F")?"closed":"open");
		power.setPosition(""+Arrays.asList(position));
		return pService.updateByPK(power);
	}
	/**
	 * 删除菜单
	 * @param id
	 * @return
	 */
	@ResponseBody
	@RequestMapping(value="/rem/{id}",method={RequestMethod.POST})
	public Object remove(@PathVariable("id")Long id){
		return pService.deleteByPK(id);
	}
	/**
	 * 校验编码名
	 * @param power
	 * @return
	 */
	@ResponseBody
	@RequestMapping(value="/checkMasterName",method={RequestMethod.POST})
	public Object checkMasterName(PowerDto power){
		Object b=pService.checkMasterName(power)>=1?true:false;
		return b;
	}
	/**
	 * 校验菜单下是否有子菜单
	 * @param id
	 * @return
	 */
	@ResponseBody
	@RequestMapping(value="/checkPower/{id}",method={RequestMethod.POST})
	public Object checkPower(@PathVariable("id")Long id){
		return pService.checkPower(id)>=1?true:false;
	}
}
