package com.yzt.insys.control;

import javax.annotation.Resource;

import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;

import com.yzt.insys.service.BigCateService;
import com.yzt.insys.service.CategoryService;
import com.yzt.insys.service.DeptService;

/**
 * 创建人:朱顺福<br />
 * 创建日期:2014年7月22日<br />
 * 创建时间:上午12:16:00<br />
 * 说      明:<br />
 */
@Scope("prototype")
@Controller
@RequestMapping("/query")
public class QueryControl extends BaseControl {

	@Resource
	private DeptService deptService;
	
	@Resource
	private CategoryService categoryService;
	
	@Resource
	private BigCateService bigCateService;
	
	@RequestMapping(value = "/{path}/all/{masterName}")
	public synchronized String toAll(@PathVariable String path,@PathVariable("masterName")String masterName) {
		reqAddAttr("paramMap",getParamMap());
		reqAddAttr("deptList",deptService.queryModelList(getPage()));
		reqAddAttr("categoryList",categoryService.queryModelList(getPage()));
		reqAddAttr("bigCateList",bigCateService.queryModelList(getPage()));
		return "menu/query/"+path+"/all";
	}
}
