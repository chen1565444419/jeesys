package com.yzt.insys.control;

import java.util.ArrayList;
import java.util.List;

import javax.annotation.Resource;

import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import com.alibaba.fastjson.JSON;
import com.yzt.insys.beans.Role;
import com.yzt.insys.common.Constants;
import com.yzt.insys.common.PageNavation;
import com.yzt.insys.dto.RolePowerDto;
import com.yzt.insys.dto.UserDto;
import com.yzt.insys.service.RolePowerService;
import com.yzt.insys.service.RoleService;

@Scope("prototype")
@Controller
@RequestMapping("/role")
public class RoleControl extends BaseControl {

	@Resource
	@Qualifier("roleService")
	private RoleService rService;
	
	@Resource
	@Qualifier("rolePowerService")
	private RolePowerService rpService;
		
	/**
	 * 查询全部角色数据
	 * @param page
	 * @return
	 */
	@ResponseBody
	@RequestMapping(value="/getData",method=RequestMethod.POST)
	public String getData(PageNavation page){
		paramMap=getParamMap();
		page.setParamMap(paramMap);
		page.setCurrentPage(Integer.parseInt(paramMap.get("page").toString()));//获取当前页码
		page.setPageSize(Integer.parseInt(paramMap.get("rows").toString()));//获取每页显示数量
		page.setStartIndex((page.getCurrentPage()-1)*page.getPageSize());//设置查询开始位置
		int totalCount=rService.queryRoleCount();//获取总记录数
		List<Role> roleList=rService.queryModelList(page);//获取集合对象
		StringBuilder sb=new StringBuilder();
		sb.append("{\"total\":"+totalCount+",");
		sb.append("\"rows\":"+JSON.toJSON(roleList)+"}");
		return sb.toString();
	}
	/**
	 * 增加角色
	 * @param role
	 * @return
	 */
	@ResponseBody
	@RequestMapping(value="/add",method=RequestMethod.POST)
	public Object add(Role role){
		if(role.getSeq()==null){role.setSeq(0);}
		boolean flg=false;
		UserDto user=(UserDto)session.getAttribute(Constants.USER_KEY_IN_SESSION);
		role.setCreatedBy(user.getId());
		List<Long> powerList=new ArrayList<Long>();
		if(rService.insert(role)){
			powerList.add(new Long(1));
			paramMap.clear();
			paramMap.put("roleId",role.getId());
			paramMap.put("powerList",powerList);
			flg=rpService.saveRolePower(paramMap);			
		}
		return flg;
	}
	
	/**
	 * 进入修改页面
	 * @param id
	 * @return
	 */
	@RequestMapping(value="/toUpd/{id}",method=RequestMethod.GET)
	public String updRole(@PathVariable("id")Long id){
		reqAddAttr("role",rService.queryByPK(id));
		reqAddAttr("paramMap",paramMap);
		return "menu/role/toUpd";
	}
	
	/**
	 * 批量删除
	 * @param ids
	 * @return
	 */
	@ResponseBody
	@RequestMapping(value="/del",method=RequestMethod.POST)
	public Object delRole(@RequestParam("ids")List<Long> ids){
		//先删除用户角色表信息
		rService.deleteUrByPKRoles(ids);
		return rService.deleteByPKs(ids);
	}
	
	/**
	 * 检查角色下的用户
	 * @param ids
	 * @return
	 */
	@ResponseBody
	@RequestMapping(value="/queryUsersCount",method=RequestMethod.POST)
	public Object queryUsers(@RequestParam("roleIds")List<Long> roleIds){
		return rService.queryUsersByRoleId(roleIds);
	}
	
	/**
	 * 修改
	 * @param role
	 * @return
	 */
	@ResponseBody
	@RequestMapping(value="/upd",method=RequestMethod.POST)
	public Object updRole(Role role){
		if(role.getSeq()==null){role.setSeq(0);}
		return rService.updateByPK(role);
	}
	
	/**
	 * 验证角色名称是否存在
	 * @param roleName
	 * @return
	 */
	@ResponseBody
	@RequestMapping(value="/checkRoleName",method=RequestMethod.POST)
	public Object checkRoleName(Role role){
		return rService.checkRoleIsExist(role);
	}
	
	/**
	 * 查看界面
	 * @param id
	 * @return
	 */
	@RequestMapping(value="/view/{id}",method=RequestMethod.GET)
	public String ViewRole(@PathVariable("id")Long id){
		reqAddAttr("role",rService.queryByPK(id));
		reqAddAttr("paramMap",paramMap);
		return "menu/role/view";
	}
	/**
	 * 跳转到授权页面
	 * @param id
	 * @return
	 */
	@RequestMapping(value="/toAuth/{id}",method=RequestMethod.GET)
	public String toAuth(@PathVariable("id")Long id){
		List<RolePowerDto> rpList=rpService.queryPowersByRoleId(id);//查询该角色对应的权限列表
		reqAddAttr("rpList",rpList);
		reqAddAttr("role",rService.queryByPK(id));
		return "menu/role/toAuth";
	}
	
}
