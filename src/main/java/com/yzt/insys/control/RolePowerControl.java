package com.yzt.insys.control;

import java.util.Arrays;

import javax.annotation.Resource;

import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import com.yzt.insys.service.PowerService;
import com.yzt.insys.service.RolePowerService;
import com.yzt.insys.service.RoleService;

@Scope("prototype")
@Controller
@RequestMapping("/rolePower")
public class RolePowerControl extends BaseControl {

	@Resource
	@Qualifier("roleService")
	private RoleService rService;
	
	@Resource
	@Qualifier("powerService")
	private PowerService pService;
	
	@Resource
	@Qualifier("rolePowerService")
	private RolePowerService rpService;
	
	@RequestMapping("/showPower")
	public String getAllPowers(@RequestParam("id")Long id){
		reqAddAttr("role",rService.queryByPK(id));
		reqAddAttr("powerMap",pService.queryAllPower());
		reqAddAttr("ownPower",rpService.queryPowersByRoleId(id));
		reqAddAttr("paramMap",paramMap);
		return "menu/sys/showPower";
	}
	
	@ResponseBody
	@RequestMapping(value="/savePower",method=RequestMethod.POST)
	public Object savePower(Long roleId,Long[] power){
		rpService.delPowersByRoleId(roleId);
		paramMap.clear();
		paramMap.put("roleId",roleId);
		paramMap.put("powerList",Arrays.asList(power));
		return rpService.saveRolePower(paramMap);
	}
}
