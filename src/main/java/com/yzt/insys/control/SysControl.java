package com.yzt.insys.control;

import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

/**
 * 
 * @作者 zsf
 * @时间 2013-7-3
 */
@Scope("prototype")
@Controller
@RequestMapping("/sys")
public class SysControl extends BaseControl {

	@ResponseBody
	@RequestMapping("/back")
	public Object backData(){
		return null;
	}

}
