/**
 * 
 */
package com.yzt.insys.dao;

import java.util.Map;

import com.yzt.insys.beans.BigCate;

/**
 * 创建人:朱顺福<br />
 * 创建日期:2014年6月15日<br />
 * 创建时间:下午2:51:57<br />
 * 说      明:设备类型Mapper<br />
 */
public interface BigCateMapper extends GenericDao<BigCate,Long> {

	int isNameExist(Map<String,Object> paramMap);
}
