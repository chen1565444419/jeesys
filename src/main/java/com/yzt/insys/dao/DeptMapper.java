package com.yzt.insys.dao;

import java.util.List;
import java.util.Map;

import org.springframework.stereotype.Repository;

import com.yzt.insys.beans.Dept;

@Repository
public interface DeptMapper extends GenericDao<Dept,Long>{
    
    int queryDeptCount();
    
    int isDeptNameExist(Map<String,Object> paramMap);
    
    int checkExistUser(List<Long> ids);
}