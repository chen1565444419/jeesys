/**
 * 
 */
package com.yzt.insys.dao;

import com.yzt.insys.beans.DetectItem;

/**
 * 创建人:朱顺福<br />
 * 创建日期:2014年6月15日<br />
 * 创建时间:下午2:51:57<br />
 * 说      明:设备备件Mapper<br />
 */
public interface DetectItemMapper extends GenericDao<DetectItem,Long> {

}
