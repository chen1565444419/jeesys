/**
 * 
 */
package com.yzt.insys.dao;

import java.util.List;
import java.util.Map;

import com.yzt.insys.beans.DetectRecord;
import com.yzt.insys.dto.CountMap;

/**
 * 创建人:朱顺福<br />
 * 创建日期:2014年6月15日<br />
 * 创建时间:下午2:54:30<br />
 * 说      明:检修结果Mapper<br />
 */
public interface DetectRecordMapper extends GenericDao<DetectRecord,Long> {

	List<CountMap> queryDetectCount(Map<String, Object> paramMap);
	
	int deleteByDeviceIds(Map<String,Object> paramMap);
	
}
