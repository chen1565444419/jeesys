/**
 * 
 */
package com.yzt.insys.dao;

import java.util.List;
import java.util.Map;

import com.yzt.insys.beans.Device;
import com.yzt.insys.beans.DeviceComp;
import com.yzt.insys.beans.DeviceOil;

/**
 * 创建人:朱顺福<br />
 * 创建日期:2014年6月15日<br />
 * 创建时间:下午2:52:40<br />
 * 说      明:设备Mapper<br />
 */
public interface DeviceMapper extends GenericDao<Device,Long> {

	List<DeviceComp> getAllCompByDeviceId(Map<String, Object> paramMap);
	
	List<DeviceOil> getAllOilByDeviceId(Map<String, Object> paramMap);
	
	List<Device> queryExpireDetect(Map<String,Object> paramMap);
	
	int deleteByCateIds(Map<String,Object> paramMap);
	
	List<Long> queryIdsByCateoryIds(Map<String,Object> paramMap);
	
	List<Device> queryExpireCheck(Map<String, Object> paramMap);
}
