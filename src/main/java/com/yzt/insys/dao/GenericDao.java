package com.yzt.insys.dao;

import java.io.Serializable;
import java.util.List;
import java.util.Map;

import com.yzt.insys.beans.BaseEntity;
import com.yzt.insys.common.PageNavation;

public interface GenericDao<T extends BaseEntity, ID extends Serializable> {
	
	int deleteByPK(ID id);
	
	int deleteByPKs(List<ID> ids);
	
	int delete(Map<String,Object> paramMap);

	Long insert(T obj);
	
	int updateByPK(T obj);
	
	T queryByPK(ID id);
	
	List<T> queryModelList(PageNavation page);
	
	int queryModelCount(Map<String,Object> map);
	
}
