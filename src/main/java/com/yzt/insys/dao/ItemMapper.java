/**
 * 
 */
package com.yzt.insys.dao;

import com.yzt.insys.beans.Item;

/**
 * 创建人:朱顺福<br />
 * 创建日期:2014年6月15日<br />
 * 创建时间:下午2:53:12<br />
 * 说      明:检修(维修)项目Mapper<br />
 */
public interface ItemMapper extends GenericDao<Item,Long> {

}
