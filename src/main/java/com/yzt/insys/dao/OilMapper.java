/**
 * 
 */
package com.yzt.insys.dao;

import java.util.List;
import java.util.Map;

import com.yzt.insys.beans.Oil;

/**
 * 创建人:朱顺福<br />
 * 创建日期:2014年6月15日<br />
 * 创建时间:下午2:51:57<br />
 */
public interface OilMapper extends GenericDao<Oil,Long> {

	int batchInsert(Map<String,Object> paramMap);
	
	int delDeviceOil(Map<String,Object> paramMap);
	
	int deleteByDeviceId(Map<String,Object> paramMap);
	
	List<Long> queryIdsByDeviceIds(Map<String,Object> paramMap);
}
