package com.yzt.insys.dao;

import java.util.List;
import java.util.Map;

import com.yzt.insys.beans.OilRecord;
import com.yzt.insys.dto.OilDeviceDto;


public interface OilRecordMapper extends GenericDao<OilRecord,Long> {

	int queryOilCount(Map<String, Object> map);
	
	List<OilDeviceDto> queryExpireOil(Map<String, Object> map);
	
}
