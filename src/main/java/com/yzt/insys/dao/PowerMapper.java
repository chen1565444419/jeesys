package com.yzt.insys.dao;

import java.util.List;
import java.util.Map;

import com.yzt.insys.common.PageNavation;
import com.yzt.insys.dto.PowerDto;

public interface PowerMapper extends GenericDao<PowerDto,Long> {
    
	List<PowerDto> queryPowersByFatherId(Map<String,Object> map);
	
	List<PowerDto> querySupPowerList();
	
	int queryPowerCount(Long fatherId);
	
	Integer checkMasterName(PowerDto power);
	
	Integer checkPower(Long powerId);
	
	List<PowerDto> queryPowerList(PageNavation page);
	
	List<PowerDto> querySubPowerByMasterName(String masterName);
}