/**
 * 
 */
package com.yzt.insys.dao;

import com.yzt.insys.beans.Repair;

/**
 * 创建人:朱顺福<br />
 * 创建日期:2014年6月15日<br />
 * 创建时间:下午2:53:54<br />
 * 说      明:维修记录Mapper<br />
 */
public interface RepairMapper extends GenericDao<Repair,Long> {

}
