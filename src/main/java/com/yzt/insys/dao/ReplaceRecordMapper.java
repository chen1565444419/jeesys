/**
 * 
 */
package com.yzt.insys.dao;

import java.util.List;
import java.util.Map;

import com.yzt.insys.beans.ReplaceRecord;
import com.yzt.insys.dto.CompDeviceDto;


public interface ReplaceRecordMapper extends GenericDao<ReplaceRecord,Long> {

	int queryReplaceCount(Map<String, Object> map);
	
	List<CompDeviceDto> queryExpireReplace(Map<String,Object> map);
	
	int deleteByDeviceIds(Map<String,Object> paramMap);
}
