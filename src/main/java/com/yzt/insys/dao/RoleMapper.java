package com.yzt.insys.dao;

import java.util.List;

import com.yzt.insys.beans.Role;


public interface RoleMapper extends GenericDao<Role,Long> {
	
    int queryRoleCount();
    
    int checkRoleIsExist(Role role);
    
    int queryUsersCountByRoleId(List<Long> roleIds);
    
    int deleteUrByPKUsers(List<Long> roleIds);
    
    int deleteUrByPKRoles(List<Long> roleIds);
    
    List<String> queryRoleNameByUserId(Long id);
}