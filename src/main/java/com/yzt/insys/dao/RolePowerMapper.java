package com.yzt.insys.dao;

import java.util.List;
import java.util.Map;

import com.yzt.insys.dto.RolePowerDto;

public interface RolePowerMapper extends GenericDao<RolePowerDto,Long> {
    
    List<RolePowerDto> queryAllSupPowersByRoleIds(List<Long> roleIds);
    
    List<RolePowerDto> queryAllPowersByRoleIds(List<Long> roleIds);
    
    List<RolePowerDto> queryPowersByRoleId(Long roleId);
    
    int delPowersByRoleId(Long roleId);
    
    int saveRolePower(Map<String,Object> map);
}