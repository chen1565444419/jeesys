package com.yzt.insys.dao;

import java.util.List;
import java.util.Map;

import com.yzt.insys.beans.User;
import com.yzt.insys.dto.UserDto;

public interface UserMapper extends GenericDao<UserDto,Long> {
	
    UserDto queryByNameAndPwd(User record);
    
    int queryUserCount();
    
    int isUserNameExist(Map<String,Object> paramMap);
    
    List<User> queryUserByDept(Long deptId);
    
    List<User> queryUserByDeptPower(Map<String,Object> map);
    
    <T> Long insertRole(Map<String,Object> map);
    
    boolean delRoleByPKUser(Long id);
    
    List<String> queryUserByRoleName(String name);
    
    int isUserNumberExist(Map<String,Object> paramMap);
    
}