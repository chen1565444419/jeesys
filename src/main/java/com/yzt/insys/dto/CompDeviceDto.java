/**
 * 
 */
package com.yzt.insys.dto;

import java.io.Serializable;
import java.util.Date;

/**
 * 创建人:朱顺福<br />
 * 创建日期:2014年8月8日<br />
 * 创建时间:下午2:39:50<br />
 * 说      明:<br />
 */
public class CompDeviceDto implements Serializable {

	private static final long serialVersionUID = 2973564627041811931L;

	private String deviceName;
	private String no;
	private String compName;
	private String deptName;
	private String categoryName;
	private Date expireReplaceDate;
	public String getDeviceName() {
		return deviceName;
	}
	public void setDeviceName(String deviceName) {
		this.deviceName = deviceName;
	}
	public String getNo() {
		return no;
	}
	public void setNo(String no) {
		this.no = no;
	}
	public String getCompName() {
		return compName;
	}
	public void setCompName(String compName) {
		this.compName = compName;
	}
	public String getDeptName() {
		return deptName;
	}
	public void setDeptName(String deptName) {
		this.deptName = deptName;
	}
	public String getCategoryName() {
		return categoryName;
	}
	public void setCategoryName(String categoryName) {
		this.categoryName = categoryName;
	}
	public Date getExpireReplaceDate() {
		return expireReplaceDate;
	}
	public void setExpireReplaceDate(Date expireReplaceDate) {
		this.expireReplaceDate = expireReplaceDate;
	}
	public static long getSerialversionuid() {
		return serialVersionUID;
	}
	
	
}
