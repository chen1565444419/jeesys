/**
 * 
 */
package com.yzt.insys.dto;

import java.io.Serializable;

/**
 * 创建人:朱顺福<br />
 * 创建日期:2014年8月3日<br />
 * 创建时间:上午1:46:51<br />
 * 说      明:次数对应的数量<br />
 */
public class CountMap implements Serializable{

	private static final long serialVersionUID = -8774606679010095235L;

	private int count;//次数
	private int time;//数量
	public int getCount() {
		return count;
	}
	public void setCount(int count) {
		this.count = count;
	}
	public int getTime() {
		return time;
	}
	public void setTime(int time) {
		this.time = time;
	}
	public static long getSerialversionuid() {
		return serialVersionUID;
	}
	
	
}
