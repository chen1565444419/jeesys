/**
 * 
 */
package com.yzt.insys.dto;

import java.io.Serializable;
import java.util.Date;

/**
 * 创建人:朱顺福<br />
 * 创建日期:2014年8月8日<br />
 * 创建时间:上午8:14:43<br />
 * 说      明:<br />
 */
public class OilDeviceDto implements Serializable {

	private static final long serialVersionUID = 3575575330607485148L;

	private String deviceName;
	private String no;
	private String oilName;
	private String deptName;
	private String categoryName;
	private Date expireOilDate;
	public String getDeviceName() {
		return deviceName;
	}
	public void setDeviceName(String deviceName) {
		this.deviceName = deviceName;
	}
	public String getNo() {
		return no;
	}
	public void setNo(String no) {
		this.no = no;
	}
	public String getOilName() {
		return oilName;
	}
	public void setOilName(String oilName) {
		this.oilName = oilName;
	}
	public String getDeptName() {
		return deptName;
	}
	public void setDeptName(String deptName) {
		this.deptName = deptName;
	}
	public String getCategoryName() {
		return categoryName;
	}
	public void setCategoryName(String categoryName) {
		this.categoryName = categoryName;
	}
	public Date getExpireOilDate() {
		return expireOilDate;
	}
	public void setExpireOilDate(Date expireOilDate) {
		this.expireOilDate = expireOilDate;
	}
	public static long getSerialversionuid() {
		return serialVersionUID;
	}
	
	
}
