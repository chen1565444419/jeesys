package com.yzt.insys.dto;

import com.yzt.insys.beans.Power;

public class PowerDto extends Power{

	private static final long serialVersionUID = -6742328518536925293L;
	
	private String fatherName;
	
	private String fatherMasterName;

	public String getFatherName() {
		return fatherName;
	}
	public void setFatherName(String fatherName) {
		this.fatherName = fatherName;
	}
	public String getFatherMasterName() {
		return fatherMasterName;
	}
	public void setFatherMasterName(String fatherMasterName) {
		this.fatherMasterName = fatherMasterName;
	}
	public static long getSerialversionuid() {
		return serialVersionUID;
	}
}
