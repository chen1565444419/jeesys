package com.yzt.insys.dto;

import java.io.Serializable;

import com.yzt.insys.beans.User;

public class UserDto extends User implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	
	private String deptName;

	private int goodCount;
	
	private int betterCount;
	
	private int badCount;
	
	private String styleName; 
	
	public String getDeptName() {
		return deptName;
	}
	public void setDeptName(String deptName) {
		this.deptName = deptName;
	}
	public static long getSerialversionuid() {
		return serialVersionUID;
	}
	public int getGoodCount() {
		return goodCount;
	}
	public void setGoodCount(int goodCount) {
		this.goodCount = goodCount;
	}
	public int getBetterCount() {
		return betterCount;
	}
	public void setBetterCount(int betterCount) {
		this.betterCount = betterCount;
	}
	public int getBadCount() {
		return badCount;
	}
	public void setBadCount(int badCount) {
		this.badCount = badCount;
	}
	public String getStyleName() {
		return styleName;
	}
	public void setStyleName(String styleName) {
		this.styleName = styleName;
	}
	
}
