package com.yzt.insys.interceptor;

import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.web.servlet.handler.HandlerInterceptorAdapter;

import com.yzt.insys.util.UserUtil;

/**
 * 用户拦截器
 */


public class CommonUserLoginInterceptor extends HandlerInterceptorAdapter {

	private List<String> uncheckUrls;

	@Override
	public boolean preHandle(HttpServletRequest request,HttpServletResponse response, Object handler) throws Exception {

		//如果session中没有user对象
		if (null==UserUtil.getUserFromSession(request.getSession())){
			String contextPath = request.getContextPath();
			String uri = request.getRequestURI();
 			uri=uri.substring(uri.indexOf(contextPath)+contextPath.length());
			uri=uri.substring(0,uri.lastIndexOf("."));
			if (uncheckUrls.contains(uri)) {
				return true;
			}else {
				// 普通页面请求
				response.sendRedirect(contextPath);
			}
			return false;
		}
		return true;

	}

	public List<String> getUncheckUrls() {
		return uncheckUrls;
	}

	public void setUncheckUrls(List<String> uncheckUrls) {
		this.uncheckUrls = uncheckUrls;
	}
	
	

}
