/**
 * 
 */
package com.yzt.insys.interceptor;

import java.io.IOException;
import java.sql.Timestamp;
import java.text.SimpleDateFormat;
import java.util.Date;

import org.codehaus.jackson.JsonGenerator;
import org.codehaus.jackson.JsonProcessingException;
import org.codehaus.jackson.map.JsonSerializer;
import org.codehaus.jackson.map.ObjectMapper;
import org.codehaus.jackson.map.SerializerProvider;
import org.codehaus.jackson.map.ser.CustomSerializerFactory;

/**
 * 创建人:朱顺福<br />
 * 创建日期:2014年7月8日<br />
 * 创建时间:下午6:06:40<br />
 * 说      明:<br />
 */
/**
 * 解决SpringMVC使用@ResponseBody返回json时，日期格式默认显示为时间戳的问题。
 * 需配合<mvc:message-converters>使用
 */
public class DateObjectMapper extends ObjectMapper {

	public DateObjectMapper() {
		CustomSerializerFactory factory = new CustomSerializerFactory();
		factory.addGenericMapping(Date.class, new JsonSerializer<Date>() {
			@Override
			public void serialize(Date value, JsonGenerator jsonGenerator,
					SerializerProvider provider) throws IOException, JsonProcessingException {
				SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
				jsonGenerator.writeString(sdf.format(value));
			}
		});
		factory.addGenericMapping(Timestamp.class,new JsonSerializer<Timestamp>(){
			@Override
			public void serialize(Timestamp value, JsonGenerator jsonGenerator,
					SerializerProvider arg2) throws IOException,
					JsonProcessingException {
				SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
				jsonGenerator.writeString(sdf.format(value));
			}
			
		});
		this.setSerializerFactory(factory);
	}
}