/**
 * 
 */
package com.yzt.insys.service;

import java.util.Map;

import com.yzt.insys.beans.BigCate;

/**
 * 创建人:朱顺福<br />
 * 创建日期:2014年6月15日<br />
 * 创建时间:下午2:56:33<br />
 * 说      明:设备类型Service<br />
 */
public interface BigCateService extends GenericService<BigCate,Long> {

	public boolean isNameExist(Map<String,Object> paramMap);
}
