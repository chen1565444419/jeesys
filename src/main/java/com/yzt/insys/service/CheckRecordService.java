/**
 * 
 */
package com.yzt.insys.service;

import java.util.List;
import java.util.Map;

import com.yzt.insys.beans.CheckRecord;
import com.yzt.insys.dto.CountMap;

/**
 * 创建人:朱顺福<br />
 * 创建日期:2014年6月15日<br />
 * 创建时间:下午3:04:31<br />
 */
public interface CheckRecordService extends GenericService<CheckRecord,Long> {

	List<CountMap> queryCheckCount(Map<String,Object> paramMap);
	
}
