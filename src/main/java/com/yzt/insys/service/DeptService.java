package com.yzt.insys.service;

import java.util.List;
import java.util.Map;

import com.yzt.insys.beans.Dept;


public interface DeptService extends GenericService<Dept,Long>{

	public int queryDeptCount();

	public boolean isDeptNameExist(Map<String,Object> paramMap);
	
	public boolean checkExistUser(List<Long> ids);
}
