/**
 * 
 */
package com.yzt.insys.service;

import com.yzt.insys.beans.DetectItem;

/**
 * 创建人:朱顺福<br />
 * 创建日期:2014年6月15日<br />
 * 创建时间:下午3:02:35<br />
 * 说      明:设备Service<br />
 */
public interface DetectItemService extends GenericService<DetectItem,Long> {

}
