/**
 * 
 */
package com.yzt.insys.service;

import java.util.List;
import java.util.Map;

import com.yzt.insys.beans.DetectRecord;
import com.yzt.insys.dto.CountMap;

/**
 * 创建人:朱顺福<br />
 * 创建日期:2014年6月15日<br />
 * 创建时间:下午3:04:31<br />
 * 说      明:检修结果记录Service<br />
 */
public interface DetectRecordService extends GenericService<DetectRecord,Long> {

	List<CountMap> queryDetectCount(Map<String,Object> paramMap);
	
	/**根据设备id删除检修记录*/
	boolean deleteByDeviceIds(Map<String,Object> paramMap);
}
