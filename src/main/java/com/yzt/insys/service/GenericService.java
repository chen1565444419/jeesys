package com.yzt.insys.service;

import java.io.Serializable;
import java.util.List;
import java.util.Map;

import com.yzt.insys.beans.BaseEntity;
import com.yzt.insys.common.PageNavation;

public interface GenericService<T extends BaseEntity, ID extends Serializable> {
	
	boolean deleteByPK(ID id);

	boolean deleteByPKs(List<ID> ids);

	boolean delete(Map<String,Object> paramMap);
	
	boolean insert(T obj);

	boolean updateByPK(T obj);

	T queryByPK(ID id);

	List<T> queryModelList(PageNavation page);

	int queryModelCount(Map<String,Object> map);
}
