/**
 * 
 */
package com.yzt.insys.service;

import com.yzt.insys.beans.Info;

/**
 * 创建人:朱顺福<br />
 * 创建日期:2014年6月15日<br />
 * 创建时间:下午3:04:31<br />
 * 说      明:检修结果记录Service<br />
 */
public interface InfoService extends GenericService<Info,Long> {

}
