/**
 * 
 */
package com.yzt.insys.service;

import com.yzt.insys.beans.Item;

/**
 * 创建人:朱顺福<br />
 * 创建日期:2014年6月15日<br />
 * 创建时间:下午3:03:11<br />
 * 说      明:检修(维修)项目Service<br />
 */
public interface ItemService extends GenericService<Item,Long> {

}
