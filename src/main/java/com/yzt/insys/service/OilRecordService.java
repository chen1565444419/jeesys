/**
 * 
 */
package com.yzt.insys.service;

import java.util.List;
import java.util.Map;

import com.yzt.insys.beans.OilRecord;
import com.yzt.insys.dto.OilDeviceDto;

/**
 * 创建人:朱顺福<br />
 * 创建日期:2014年6月15日<br />
 * 创建时间:下午3:04:31<br />
 * 说      明:检修结果记录Service<br />
 */
public interface OilRecordService extends GenericService<OilRecord,Long> {

	int queryOilCount(Map<String,Object> map);
	
	List<OilDeviceDto> queryExpireOil(Map<String, Object> map);
}
