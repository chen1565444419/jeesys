/**
 * 
 */
package com.yzt.insys.service;

import java.util.List;
import java.util.Map;

import com.yzt.insys.beans.Oil;

/**
 * 创建人:朱顺福<br />
 * 创建日期:2014年6月15日<br />
 * 创建时间:下午3:02:35<br />
 * 说      明:设备Service<br />
 */
public interface OilService extends GenericService<Oil,Long> {

	boolean batchInsert(Map<String,Object> paramMap);
	
	boolean delDeviceOil(Map<String,Object> paramMap);
	
	boolean deleteByDeviceId(Map<String,Object> paramMap);
	
	List<Long> queryIdsByDeviceIds(Map<String,Object> paramMap);
}
