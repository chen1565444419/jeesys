package com.yzt.insys.service;

import java.util.List;
import java.util.Map;

import com.yzt.insys.common.PageNavation;
import com.yzt.insys.dto.PowerDto;

public interface PowerService extends GenericService<PowerDto,Long>{
	
	List<PowerDto> queryPowersByFatherId(Map<String,Object> map);
	
	Map<PowerDto,List<PowerDto>> queryAllPower();
	
	int queryPowerCount(Long fatherId);
	
	List<PowerDto> queryModelList(PageNavation page);
	
	Integer checkMasterName(PowerDto power);
	
	Integer checkPower(Long powerId);
	
	List<PowerDto> queryPowerList(PageNavation page);
	
	List<PowerDto> querySubPowerByMasterName(String masterName);
	
	
	
}
