/**
 * 
 */
package com.yzt.insys.service;

import com.yzt.insys.beans.Repair;

/**
 * 创建人:朱顺福<br />
 * 创建日期:2014年6月15日<br />
 * 创建时间:下午3:03:55<br />
 * 说      明:维修记录Service<br />
 */
public interface RepairService extends GenericService<Repair,Long> {

}
