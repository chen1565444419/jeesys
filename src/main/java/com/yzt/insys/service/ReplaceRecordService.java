/**
 * 
 */
package com.yzt.insys.service;

import java.util.List;
import java.util.Map;

import com.yzt.insys.beans.ReplaceRecord;
import com.yzt.insys.dto.CompDeviceDto;

/**
 * 创建人:朱顺福<br />
 * 创建日期:2014年6月15日<br />
 * 创建时间:下午3:04:31<br />
 * 说      明:检修结果记录Service<br />
 */
public interface ReplaceRecordService extends GenericService<ReplaceRecord,Long> {
	
	int queryReplaceCount(Map<String,Object> map);
	
	List<CompDeviceDto> queryExpireReplace(Map<String,Object> map);
	
	boolean deleteByDeviceIds(Map<String,Object> paramMap);
}
