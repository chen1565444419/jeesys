package com.yzt.insys.service;

import java.util.List;
import java.util.Map;

import com.yzt.insys.dto.RolePowerDto;

public interface RolePowerService{
	
	public List<RolePowerDto> queryAllSupPowersByRoleIds(List<Long> roleIds);
	
	public List<RolePowerDto> queryAllPowersByRoleIds(List<Long> roleIds);
	
	public List<RolePowerDto> queryPowersByRoleId(Long roleId);
	
	public boolean delPowersByRoleId(Long roleId);
	
	public boolean saveRolePower(Map<String,Object> map);
}
