package com.yzt.insys.service;

import java.util.List;

import com.yzt.insys.beans.Role;


public interface RoleService extends GenericService<Role,Long>{

	int queryRoleCount();
	
	int checkRoleIsExist(Role role);
	
	boolean queryUsersByRoleId(List<Long> roleIds);
	
	boolean deleteUrByPKUsers(List<Long> roleIds);
	
	boolean deleteUrByPKRoles(List<Long> roleIds);

	List<String> queryRoleNameByUserId(Long id);
}
