package com.yzt.insys.service;

import java.util.List;
import java.util.Map;

import com.yzt.insys.beans.User;
import com.yzt.insys.dto.UserDto;

public interface UserService extends GenericService<UserDto, Long>{

	UserDto queryByNameAndPwd(User user);
	
	boolean isUserNameExist(Map<String,Object> paramMap);

	int queryUserCount();
	
	List<User> queryUserByDept(Long deptId);

	List<User> queryUserByDeptPower(Map<String,Object> map);
	
	Long insertRole(Map<String,Object> map);
	
	boolean delRoleByPKUser(Long id);
	
	List<String> queryUserByRoleName(String name);
	
	boolean isUserNumberExist(Map<String,Object> paramMap);
	
}
