package com.yzt.insys.service.impl;

import java.io.Serializable;
import java.lang.reflect.ParameterizedType;
import java.lang.reflect.Type;
import java.util.List;
import java.util.Map;

import org.apache.log4j.Logger;
import org.springframework.stereotype.Service;

import com.yzt.insys.beans.BaseEntity;
import com.yzt.insys.common.PageNavation;
import com.yzt.insys.dao.GenericDao;
import com.yzt.insys.service.GenericService;

@Service("abstractGenericService")
public abstract class AbstractGenericService<T extends BaseEntity, ID extends Serializable> implements GenericService<T, ID> {

	private Logger log=Logger.getLogger(getClass());
	
	public AbstractGenericService() {
		log.info("AbstractGenericService>>>>>>>>>>>>>>>>>>");
	}

	@SuppressWarnings({"unused", "unchecked"})
	private Class<T> getTClass() {
		Type type=getClass().getGenericSuperclass();
		ParameterizedType parameterizedType=(ParameterizedType)type;
		Type types[]=parameterizedType.getActualTypeArguments();
		return ((Class<T>)types[0]);
	}
	
	protected abstract GenericDao<T, ID> getMapper();

	@Override
	public boolean deleteByPK(ID id) {
		return getMapper().deleteByPK(id)>0;
	}

	@Override
	public boolean deleteByPKs(List<ID> ids) {
		return getMapper().deleteByPKs(ids)>0;
	}

	@Override
	public boolean delete(Map<String,Object> paramMap){
		return getMapper().delete(paramMap)>0;
	}
	@Override
	public boolean insert(T obj) {
		return getMapper().insert(obj)!=null;
	}

	@Override
	public boolean updateByPK(T obj) {
		return getMapper().updateByPK(obj)>0;
	}

	@Override
	public T queryByPK(ID id) {
		return getMapper().queryByPK(id);
	}

	@Override
	public List<T> queryModelList(PageNavation page) {
		return getMapper().queryModelList(page);
	}

	@Override
	public int queryModelCount(Map<String,Object> map) {
		return getMapper().queryModelCount(map);
	}
	
	
}
