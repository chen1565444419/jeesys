package com.yzt.insys.service.impl;

import java.util.Map;

import javax.annotation.Resource;

import org.apache.log4j.Logger;
import org.springframework.stereotype.Service;

import com.yzt.insys.beans.BigCate;
import com.yzt.insys.dao.BigCateMapper;
import com.yzt.insys.dao.GenericDao;
import com.yzt.insys.service.BigCateService;

@Service("bigCateService")
@SuppressWarnings("unused")
public class BigCateServiceImpl extends AbstractGenericService<BigCate,Long> implements BigCateService {

	private Logger log=Logger.getLogger(getClass());
	
	@Resource
	private BigCateMapper bigCateMapper;
	
	@Override
	protected GenericDao<BigCate, Long> getMapper() {
		return bigCateMapper;
	}

	@Override
	public boolean isNameExist(Map<String, Object> paramMap) {
		return bigCateMapper.isNameExist(paramMap)==1;
	}

}
