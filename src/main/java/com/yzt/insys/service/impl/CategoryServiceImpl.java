package com.yzt.insys.service.impl;

import java.util.Map;

import javax.annotation.Resource;

import org.apache.log4j.Logger;
import org.springframework.stereotype.Service;

import com.yzt.insys.beans.Category;
import com.yzt.insys.dao.CategoryMapper;
import com.yzt.insys.dao.GenericDao;
import com.yzt.insys.service.CategoryService;

@Service("categoryService")
@SuppressWarnings("unused")
public class CategoryServiceImpl extends AbstractGenericService<Category,Long> implements CategoryService {

	private Logger log=Logger.getLogger(getClass());
	
	@Resource
	private CategoryMapper categoryMapper;
	
	@Override
	protected GenericDao<Category, Long> getMapper() {
		return categoryMapper;
	}

	@Override
	public boolean isNameExist(Map<String, Object> paramMap) {
		return categoryMapper.isNameExist(paramMap)==1;
	}

}
