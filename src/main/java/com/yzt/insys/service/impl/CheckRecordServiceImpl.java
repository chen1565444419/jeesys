package com.yzt.insys.service.impl;

import java.util.List;
import java.util.Map;

import javax.annotation.Resource;

import org.apache.log4j.Logger;
import org.springframework.stereotype.Service;

import com.yzt.insys.beans.CheckRecord;
import com.yzt.insys.beans.DetectRecord;
import com.yzt.insys.dao.CheckRecordMapper;
import com.yzt.insys.dao.DetectRecordMapper;
import com.yzt.insys.dao.GenericDao;
import com.yzt.insys.dto.CountMap;
import com.yzt.insys.service.CheckRecordService;

@Service("checkRecordService")
@SuppressWarnings("unused")
public class CheckRecordServiceImpl extends AbstractGenericService<CheckRecord,Long> implements CheckRecordService {

	private Logger log=Logger.getLogger(getClass());
	
	@Resource
	private CheckRecordMapper recordMapper;
	
	@Override
	protected GenericDao<CheckRecord, Long> getMapper() {
		return recordMapper;
	}

	@Override
	public List<CountMap> queryCheckCount(Map<String, Object> paramMap) {
		return recordMapper.queryCheckCount(paramMap);
	}

}
