package com.yzt.insys.service.impl;

import java.util.List;
import java.util.Map;

import javax.annotation.Resource;

import org.apache.log4j.Logger;
import org.springframework.stereotype.Service;

import com.yzt.insys.beans.Category;
import com.yzt.insys.beans.Component;
import com.yzt.insys.dao.ComponentMapper;
import com.yzt.insys.dao.GenericDao;
import com.yzt.insys.service.ComponentService;

@Service("componentService")
@SuppressWarnings("unused")
public class ComponentServiceImpl extends AbstractGenericService<Component,Long> implements ComponentService {

	private Logger log=Logger.getLogger(getClass());
	
	@Resource
	private ComponentMapper componentMapper;
	
	@Override
	protected GenericDao<Component, Long> getMapper() {
		return componentMapper;
	}

	@Override
	public boolean isNameExist(Map<String, Object> paramMap) {
		return componentMapper.isNameExist(paramMap)==1;
	}

	@Override
	public boolean batchInsert(Map<String, Object> paramMap) {
		return componentMapper.batchInsert(paramMap)>0;
	}

	@Override
	public boolean delDeviceComp(Map<String, Object> paramMap) {
		return componentMapper.delDeviceComp(paramMap)>0;
	}

	@Override
	public boolean deleteByDeviceIds(Map<String, Object> paramMap) {
		return componentMapper.deleteByDeviceIds(paramMap)>0;
	}

}
