package com.yzt.insys.service.impl;

import java.util.List;
import java.util.Map;

import javax.annotation.Resource;

import org.apache.log4j.Logger;
import org.springframework.stereotype.Service;

import com.yzt.insys.beans.Dept;
import com.yzt.insys.dao.DeptMapper;
import com.yzt.insys.dao.GenericDao;
import com.yzt.insys.service.impl.AbstractGenericService;
import com.yzt.insys.service.DeptService;

@Service("deptService")
public class DeptServiceImpl extends AbstractGenericService<Dept,Long> implements DeptService {

	private Logger log=Logger.getLogger(getClass());
	
	@Resource
	private DeptMapper dMapper;
	
	public DeptServiceImpl() {
		log.info("DeptServiceImpl>>>>>>>>>>>>>>>>>>>>>>>.");
	}

	@Override
	public boolean isDeptNameExist(Map<String,Object> paramMap) {
		return dMapper.isDeptNameExist(paramMap)>=1;
	}

	@Override
	public boolean checkExistUser(List<Long> ids) {
		return dMapper.checkExistUser(ids)>0;
	}

	@Override
	public int queryDeptCount() {
		return dMapper.queryDeptCount();
	}

	@Override
	protected GenericDao<Dept, Long> getMapper() {
		return dMapper;
	}

}
