package com.yzt.insys.service.impl;

import javax.annotation.Resource;

import org.apache.log4j.Logger;
import org.springframework.stereotype.Service;

import com.yzt.insys.beans.DetectItem;
import com.yzt.insys.beans.Item;
import com.yzt.insys.dao.DetectItemMapper;
import com.yzt.insys.dao.GenericDao;
import com.yzt.insys.service.DetectItemService;

@Service("detectItemService")
@SuppressWarnings("unused")
public class DetectItemServiceImpl extends AbstractGenericService<DetectItem,Long> implements DetectItemService {

	private Logger log=Logger.getLogger(getClass());
	
	@Resource
	private DetectItemMapper detectItemMapper;
	
	@Override
	protected GenericDao<DetectItem, Long> getMapper() {
		return detectItemMapper;
	}

}
