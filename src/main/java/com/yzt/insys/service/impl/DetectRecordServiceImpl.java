package com.yzt.insys.service.impl;

import java.util.List;
import java.util.Map;

import javax.annotation.Resource;

import org.apache.log4j.Logger;
import org.springframework.stereotype.Service;

import com.yzt.insys.beans.DetectRecord;
import com.yzt.insys.dao.DetectRecordMapper;
import com.yzt.insys.dao.GenericDao;
import com.yzt.insys.dto.CountMap;
import com.yzt.insys.service.DetectRecordService;

@Service("detectRecordService")
@SuppressWarnings("unused")
public class DetectRecordServiceImpl extends AbstractGenericService<DetectRecord,Long> implements DetectRecordService {

	private Logger log=Logger.getLogger(getClass());
	
	@Resource
	private DetectRecordMapper recordMapper;
	
	@Override
	protected GenericDao<DetectRecord, Long> getMapper() {
		return recordMapper;
	}

	@Override
	public List<CountMap> queryDetectCount(Map<String, Object> paramMap) {
		return recordMapper.queryDetectCount(paramMap);
	}

	@Override
	public boolean deleteByDeviceIds(Map<String, Object> paramMap) {
		return recordMapper.deleteByDeviceIds(paramMap)>0;
	}

}
