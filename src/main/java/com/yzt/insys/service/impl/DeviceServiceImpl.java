package com.yzt.insys.service.impl;

import java.util.List;
import java.util.Map;

import javax.annotation.Resource;

import org.apache.log4j.Logger;
import org.springframework.stereotype.Service;

import com.yzt.insys.beans.Device;
import com.yzt.insys.beans.DeviceComp;
import com.yzt.insys.beans.DeviceOil;
import com.yzt.insys.dao.DeviceMapper;
import com.yzt.insys.dao.GenericDao;
import com.yzt.insys.service.DeviceService;

@Service("deviceService")
@SuppressWarnings("unused")
public class DeviceServiceImpl extends AbstractGenericService<Device,Long> implements DeviceService {

	private Logger log=Logger.getLogger(getClass());

	@Resource
	private DeviceMapper deviceMapper;
	@Override
	protected GenericDao<Device, Long> getMapper() {
		return deviceMapper;
	}
	@Override
	public List<DeviceComp> getAllCompByDeviceId(Map<String, Object> paramMap) {
		return deviceMapper.getAllCompByDeviceId(paramMap);
	}
	@Override
	public List<DeviceOil> getAllOilByDeviceId(Map<String, Object> paramMap) {
		return deviceMapper.getAllOilByDeviceId(paramMap);
	}
	@Override
	public List<Device> queryExpireDetect(Map<String,Object> paramMap) {
		return deviceMapper.queryExpireDetect(paramMap);
	}
	@Override
	public boolean deleteByCateIds(Map<String, Object> paramMap) {
		return deviceMapper.deleteByCateIds(paramMap)>0;
	}
	@Override
	public List<Long> queryIdsByCateoryIds(Map<String, Object> paramMap) {
		return deviceMapper.queryIdsByCateoryIds(paramMap);
	}
	@Override
	public List<Device> queryExpireCheck(Map<String, Object> paramMap) {
		return deviceMapper.queryExpireCheck(paramMap);
	}

}
