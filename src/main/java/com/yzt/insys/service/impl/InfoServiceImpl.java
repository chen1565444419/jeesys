package com.yzt.insys.service.impl;

import javax.annotation.Resource;

import org.apache.log4j.Logger;
import org.springframework.stereotype.Service;

import com.yzt.insys.beans.Info;
import com.yzt.insys.dao.GenericDao;
import com.yzt.insys.dao.InfoMapper;
import com.yzt.insys.service.InfoService;

@Service("infoService")
@SuppressWarnings("unused")
public class InfoServiceImpl extends AbstractGenericService<Info,Long> implements InfoService {

	private Logger log=Logger.getLogger(getClass());
	
	@Resource
	private InfoMapper infoMapper;
	
	@Override
	protected GenericDao<Info,Long> getMapper() {
		return infoMapper;
	}

}
