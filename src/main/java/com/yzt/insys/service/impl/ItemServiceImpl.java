package com.yzt.insys.service.impl;

import javax.annotation.Resource;

import org.apache.log4j.Logger;
import org.springframework.stereotype.Service;

import com.yzt.insys.beans.Item;
import com.yzt.insys.dao.GenericDao;
import com.yzt.insys.dao.ItemMapper;
import com.yzt.insys.service.ItemService;

@Service("itemService")
@SuppressWarnings("unused")
public class ItemServiceImpl extends AbstractGenericService<Item,Long> implements ItemService {

	private Logger log=Logger.getLogger(getClass());
	
	@Resource
	private ItemMapper itemMapper;
	
	@Override
	protected GenericDao<Item, Long> getMapper() {
		return itemMapper;
	}

}
