package com.yzt.insys.service.impl;

import java.util.List;
import java.util.Map;

import javax.annotation.Resource;

import org.apache.log4j.Logger;
import org.springframework.stereotype.Service;

import com.yzt.insys.beans.OilRecord;
import com.yzt.insys.dao.GenericDao;
import com.yzt.insys.dao.OilRecordMapper;
import com.yzt.insys.dto.CountMap;
import com.yzt.insys.dto.OilDeviceDto;
import com.yzt.insys.service.OilRecordService;

@Service("oilRecordService")
@SuppressWarnings("unused")
public class OilRecordServiceImpl extends AbstractGenericService<OilRecord,Long> implements OilRecordService {

	private Logger log=Logger.getLogger(getClass());
	
	@Resource
	private OilRecordMapper oilRecordMapper;
	
	@Override
	protected GenericDao<OilRecord, Long> getMapper() {
		return oilRecordMapper;
	}

	@Override
	public int queryOilCount(Map<String, Object> map) {
		return oilRecordMapper.queryOilCount(map);
	}

	@Override
	public List<OilDeviceDto> queryExpireOil(Map<String, Object> map) {
		return oilRecordMapper.queryExpireOil(map);
	}

}
