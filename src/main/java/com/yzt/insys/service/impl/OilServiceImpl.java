package com.yzt.insys.service.impl;

import java.util.List;
import java.util.Map;

import javax.annotation.Resource;

import org.apache.log4j.Logger;
import org.springframework.stereotype.Service;

import com.yzt.insys.beans.Oil;
import com.yzt.insys.dao.GenericDao;
import com.yzt.insys.dao.OilMapper;
import com.yzt.insys.service.OilService;

@Service("oilService")
@SuppressWarnings("unused")
public class OilServiceImpl extends AbstractGenericService<Oil,Long> implements OilService {

	private Logger log=Logger.getLogger(getClass());
	
	@Resource
	private OilMapper oilMapper;
	
	@Override
	protected GenericDao<Oil, Long> getMapper() {
		return oilMapper;
	}

	@Override
	public boolean batchInsert(Map<String,Object> paramMap) {
		return oilMapper.batchInsert(paramMap)>0;
	}

	@Override
	public boolean delDeviceOil(Map<String, Object> paramMap) {
		return oilMapper.delDeviceOil(paramMap)>0;
	}

	@Override
	public boolean deleteByDeviceId(Map<String, Object> paramMap) {
		return oilMapper.deleteByDeviceId(paramMap)>0;
	}

	@Override
	public List<Long> queryIdsByDeviceIds(Map<String, Object> paramMap) {
		return oilMapper.queryIdsByDeviceIds(paramMap);
	}

}
