package com.yzt.insys.service.impl;

import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

import javax.annotation.Resource;

import org.springframework.stereotype.Service;

import com.yzt.insys.common.PageNavation;
import com.yzt.insys.dao.GenericDao;
import com.yzt.insys.dao.PowerMapper;
import com.yzt.insys.dto.PowerDto;
import com.yzt.insys.service.PowerService;

@Service("powerService")
public class PowerServiceImpl extends AbstractGenericService<PowerDto,Long> implements PowerService{

	@Resource
	private PowerMapper pMapper;
	
	@Override
	public List<PowerDto> queryPowersByFatherId(Map<String,Object> map) {
		return pMapper.queryPowersByFatherId(map);
	}

	@Override
	public Map<PowerDto, List<PowerDto>> queryAllPower() {
		List<PowerDto> supList=pMapper.querySupPowerList();
		Map<PowerDto, List<PowerDto>> powerMap=new LinkedHashMap<PowerDto, List<PowerDto>>();
		Map<String,Object> paramMap=new LinkedHashMap<String,Object>();
		for(PowerDto p:supList){
			paramMap.clear();
			paramMap.put("id",p.getId());
			powerMap.put(p,pMapper.queryPowersByFatherId(paramMap));
		}
		return powerMap;
	}

	@Override
	public int queryPowerCount(Long fatherId) {
		return pMapper.queryPowerCount(fatherId);
	}

	@Override
	public List<PowerDto> queryModelList(PageNavation page) {
		return pMapper.queryModelList(page);
	}

	@Override
	public Integer checkMasterName(PowerDto power) {
		return pMapper.checkMasterName(power);
	}

	@Override
	public Integer checkPower(Long powerId) {
		return pMapper.checkPower(powerId);
	}

	@Override
	public List<PowerDto> queryPowerList(PageNavation page) {
		return pMapper.queryPowerList(page);
	}

	@Override
	public List<PowerDto> querySubPowerByMasterName(String masterName) {
		List<PowerDto> list = pMapper.querySubPowerByMasterName(masterName);
		return list;
	}

	@Override
	protected GenericDao<PowerDto, Long> getMapper() {
		return pMapper;
	}
}
