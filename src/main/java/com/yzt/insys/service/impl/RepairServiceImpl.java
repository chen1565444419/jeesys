package com.yzt.insys.service.impl;

import javax.annotation.Resource;

import org.apache.log4j.Logger;
import org.springframework.stereotype.Service;

import com.yzt.insys.beans.Repair;
import com.yzt.insys.dao.GenericDao;
import com.yzt.insys.dao.RepairMapper;
import com.yzt.insys.service.RepairService;

@Service("repairService")
@SuppressWarnings("unused")
public class RepairServiceImpl extends AbstractGenericService<Repair,Long> implements RepairService {

	private Logger log=Logger.getLogger(getClass());
	
	@Resource
	private RepairMapper repairMapper;
	
	@Override
	protected GenericDao<Repair, Long> getMapper() {
		return repairMapper;
	}

}
