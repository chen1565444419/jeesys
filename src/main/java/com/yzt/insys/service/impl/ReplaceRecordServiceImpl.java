package com.yzt.insys.service.impl;

import java.util.List;
import java.util.Map;

import javax.annotation.Resource;

import org.apache.log4j.Logger;
import org.springframework.stereotype.Service;

import com.yzt.insys.beans.ReplaceRecord;
import com.yzt.insys.dao.GenericDao;
import com.yzt.insys.dao.ReplaceRecordMapper;
import com.yzt.insys.dto.CompDeviceDto;
import com.yzt.insys.service.ReplaceRecordService;

@Service("replaceRecordService")
@SuppressWarnings("unused")
public class ReplaceRecordServiceImpl extends AbstractGenericService<ReplaceRecord,Long> implements ReplaceRecordService {

	private Logger log=Logger.getLogger(getClass());
	
	@Resource
	private ReplaceRecordMapper replaceRecordMapper;
	
	@Override
	protected GenericDao<ReplaceRecord, Long> getMapper() {
		return replaceRecordMapper;
	}

	@Override
	public int queryReplaceCount(Map<String, Object> map) {
		return replaceRecordMapper.queryReplaceCount(map);
	}

	@Override
	public List<CompDeviceDto> queryExpireReplace(Map<String, Object> map) {
		return replaceRecordMapper.queryExpireReplace(map);
	}

	@Override
	public boolean deleteByDeviceIds(Map<String, Object> paramMap) {
		return replaceRecordMapper.deleteByDeviceIds(paramMap)>0;
	}

}
