package com.yzt.insys.service.impl;

import java.util.List;
import java.util.Map;

import javax.annotation.Resource;

import org.springframework.stereotype.Service;

import com.yzt.insys.dao.RolePowerMapper;
import com.yzt.insys.dto.RolePowerDto;
import com.yzt.insys.service.RolePowerService;

@Service("rolePowerService")
public class RolePowerServiceImpl implements RolePowerService {
	
	@Resource
	private RolePowerMapper rpMapper;
	
	@Override
	public List<RolePowerDto> queryPowersByRoleId(Long roleId) {
		return rpMapper.queryPowersByRoleId(roleId);
	}

	@Override
	public boolean delPowersByRoleId(Long id) {
		return rpMapper.delPowersByRoleId(id)>=1;
	}

	@Override
	public boolean saveRolePower(Map<String,Object> map) {
		return rpMapper.saveRolePower(map)>0;
	}

	@Override
	public List<RolePowerDto> queryAllSupPowersByRoleIds(List<Long> roleIds) {
		return rpMapper.queryAllSupPowersByRoleIds(roleIds);
	}

	@Override
	public List<RolePowerDto> queryAllPowersByRoleIds(List<Long> roleIds) {
		return rpMapper.queryAllPowersByRoleIds(roleIds);
	}
}
