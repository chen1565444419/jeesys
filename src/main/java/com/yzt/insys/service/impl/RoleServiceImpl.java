package com.yzt.insys.service.impl;

import java.util.List;

import javax.annotation.Resource;

import org.apache.log4j.Logger;
import org.springframework.stereotype.Service;

import com.yzt.insys.beans.Role;
import com.yzt.insys.dao.GenericDao;
import com.yzt.insys.dao.RoleMapper;
import com.yzt.insys.service.RoleService;

@Service("roleService")
public class RoleServiceImpl extends AbstractGenericService<Role,Long> implements RoleService {
	
	private Logger log=Logger.getLogger(getClass());

	@Resource
	private RoleMapper rMapper;

	public RoleServiceImpl() {
		log.info("RoleServiceImpl>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>");
	}

	@Override
	public int queryRoleCount() {
		return rMapper.queryRoleCount();
	}

	@Override
	public int checkRoleIsExist(Role role) {
		return rMapper.checkRoleIsExist(role);
	}

	@Override
	public boolean queryUsersByRoleId(List<Long> roleIds) {
		return rMapper.queryUsersCountByRoleId(roleIds)>=1;
	}
	@Override
	public boolean deleteUrByPKUsers(List<Long> roleIds) {
		return rMapper.deleteUrByPKUsers(roleIds)>=1;
	}

	@Override
	public boolean deleteUrByPKRoles(List<Long> roleIds) {
		return rMapper.deleteUrByPKRoles(roleIds)>=1;
	}

	@Override
	public List<String> queryRoleNameByUserId(Long id) {
		return rMapper.queryRoleNameByUserId(id);
	}

	@Override
	protected GenericDao<Role, Long> getMapper() {
		return rMapper;
	}
	
}
