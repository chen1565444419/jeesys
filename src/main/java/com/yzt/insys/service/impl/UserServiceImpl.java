/**
 * 
 */
package com.yzt.insys.service.impl;

import java.util.List;
import java.util.Map;

import javax.annotation.Resource;

import org.apache.log4j.Logger;
import org.springframework.stereotype.Service;

import com.yzt.insys.beans.User;
import com.yzt.insys.dao.GenericDao;
import com.yzt.insys.dao.UserMapper;
import com.yzt.insys.dto.UserDto;
import com.yzt.insys.service.UserService;

/**
 * @作者 zsf
 * @时间 2013-6-20
 */
@Service("userService")
public class UserServiceImpl extends AbstractGenericService<UserDto,Long> implements UserService {

	private Logger log=Logger.getLogger(getClass());
	
	public UserServiceImpl() {
		log.info("UserServiceImple>>>>>>>>>>>>>>>>>>>>>>>>>");
	}


	@Resource
	private UserMapper uMapper;

	@Override
	protected GenericDao<UserDto, Long> getMapper() {
		return uMapper;
	}

	@Override
	public UserDto queryByNameAndPwd(User user) {
		return uMapper.queryByNameAndPwd(user);
	}

	@Override
	public boolean isUserNameExist(Map<String, Object> paramMap) {
		return uMapper.isUserNameExist(paramMap)>0;
	}

	@Override
	public int queryUserCount() {
		return uMapper.queryUserCount();
	}

	@Override
	public List<User> queryUserByDept(Long deptId) {
		return uMapper.queryUserByDept(deptId);
	}

	@Override
	public List<User> queryUserByDeptPower(Map<String, Object> map) {
		return uMapper.queryUserByDeptPower(map);
	}

	@Override
	public Long insertRole(Map<String, Object> map) {
		return uMapper.insertRole(map);
	}

	@Override
	public boolean delRoleByPKUser(Long id) {
		return uMapper.delRoleByPKUser(id);
	}

	@Override
	public List<String> queryUserByRoleName(String name) {
		return uMapper.queryUserByRoleName(name);
	}


	@Override
	public boolean isUserNumberExist(Map<String, Object> paramMap) {
		return uMapper.isUserNumberExist(paramMap)>0;
	}

}
