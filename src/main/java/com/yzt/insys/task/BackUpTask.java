package com.yzt.insys.task;

import javax.annotation.Resource;

import com.yzt.insys.util.DBUtil;

public class BackUpTask {

	@Resource
	private DBUtil dbUtil;
	
	public void backup() {
		dbUtil.backDB();
		dbUtil.impLocal();
    }
	
}
