package com.yzt.insys.util;

import java.io.File;
import java.text.SimpleDateFormat;
import java.util.Date;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;


/**
 * 数据库操作类
 * @author ZSF
 *
 */
@Component("dbUtil")
public class DBUtil {
	
	private Logger logger=Logger.getLogger(DBUtil.class.getClass());
	
	@Value("${jdbc.host}")
	private String host;
	
	@Value("${jdbc.username}")
	private String username;
	
	@Value("${jdbc.password}")
	private String password;
	
	@Value("${jdbc.database}")
	private String database;
	
	@Value("${backup.filePath}")
	private String backupFilePath;
	
	@Value("${backup.host}")
	private String backupHost;
	
	@Value("${backup.username}")
	private String backupUserName;
	
	@Value("${backup.password}")
	private String backupPwd;
	
	@Value("${backup.database}")
	private String backupDatabase;
	
	@Value("${backup.isBackupToLocal}")
	private boolean isBackupToLocal;
	
	@Value("${backup.isBackupToOther}")
	private boolean isBackupToOther;
	
	private SimpleDateFormat simple=new SimpleDateFormat("yyyy-MM-dd");
	/**
	 * 备份数据库
	 */
	public void backDB(){
		if(isBackupToLocal){
			createBackUpFilePath();
			logger.info("开始备份......");
			String fileName=simple.format(new Date())+".sql";
			StringBuilder sql=new StringBuilder();
			sql.append("cmd /c mysqldump");
			sql.append(" --add-drop-table");
			sql.append(" --default-character-set=utf8");
			sql.append(" -h"+host);
			sql.append(" -u"+username);
			sql.append(" -p"+password);
			sql.append(" "+database);
			sql.append(" >");
			sql.append(" "+backupFilePath+"/"+fileName);
			logger.info("执行命令:"+sql.toString());
			CMDUtil.executeCmdFlash(sql.toString());
			logger.info("备份结束......");
		}
	}

	/**
	 * 将数据备份到指定的服务器
	 */
	public void impLocal(){
		if(isBackupToOther){
			logger.info("开始将数据导入到"+backupHost+"/"+backupDatabase+"......");
			String fileName=simple.format(new Date())+".sql";
			StringBuilder sql=new StringBuilder();
			sql.append("cmd /c mysql");
			sql.append(" -h"+backupHost);
			sql.append(" -u"+backupUserName);
			sql.append(" -p"+backupPwd);
			sql.append(" "+backupDatabase);
			sql.append(" <");
			sql.append(" "+backupFilePath+"/"+fileName);
			sql.append(" --default-character-set=utf8");
			logger.info("执行命令:"+sql.toString());
			CMDUtil.executeCmdFlash(sql.toString());
			logger.info("导入到"+backupHost+"/"+backupDatabase+"结束......");
		}
	}
	
	/**
	 * 创建备份文件夹
	 */
	private void createBackUpFilePath(){
		File fileDir=new File(backupFilePath);
		if(!fileDir.exists()){
			fileDir.mkdirs();
		}
	}
	
	public void impSql(String sqlFilePath){
			logger.info("开始将数据导入到数据库。。。。。");
			StringBuilder sql=new StringBuilder();
			sql.append("cmd /c mysql");
			sql.append(" -h"+backupHost);
			sql.append(" -u"+backupUserName);
			sql.append(" -p"+backupPwd);
			sql.append(" "+backupDatabase);
			sql.append(" <");
			sql.append(" "+sqlFilePath);
			sql.append(" --default-character-set=utf8");
			logger.info("执行命令:"+sql.toString());
			CMDUtil.executeCmdFlash(sql.toString());
			logger.info("导入到数据库结束......");
	}
}
