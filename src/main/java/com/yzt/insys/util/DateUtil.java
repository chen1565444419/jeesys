package com.yzt.insys.util;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.GregorianCalendar;

import java.util.Calendar;

/**
 * 日期时间操作类
 * @作者 zsf
 * @时间 2013-6-18
 */
public final class DateUtil {
	
	public final static String DATE_FORMAT = "yyyy-MM-dd";
	public final static String DATE_TIME_FORMAT = "yyyy-MM-dd HH:mm:ss";
	private DateUtil(){}
	
	
	/**
	 * 将字符串类型的日期格式化成Date类型,strDate格式为yyyy-MM-dd HH:mm:ss
	 * @param strDate 字符串类型的时间,格式为yyyy-MM-dd HH:mm:ss
	 * @return
	 */
	public static Date parse(String strDate) {
		if (strDate == null || strDate.trim().equals(""))
			return null;
		return parse(strDate,DATE_TIME_FORMAT);
	}

	/**
	 * 将日期格式化成指定的格式
	 * @param date 要格式化的日期
	 * @param format 日期的格式
	 * @return
	 */
	public static Date parse(String date,String format) {
		if (date == null || date.trim().equals(""))
			return null;
		SimpleDateFormat dtFormatdB = null;
		try {
			dtFormatdB = new SimpleDateFormat(format);
			return dtFormatdB.parse(date);
		} catch (Exception e) {
			e.printStackTrace();
		}
		return null;
	}

	/**
	 * 将日期格式化成指定的格式
	 * @param date 要格式化的日期
	 * @param format 日期的格式
	 * @return
	 */
	public static String formatDate(Date date,String format) {
		if (date == null)
			return "";
		return new SimpleDateFormat(format).format(date);
	}

	/**
	 * 将date与现在时间比较，返回long类型的毫秒数
	 * @param date Date
	 * @return long类型，若为负数，则date大于当前时间
	 */
	public static long compareDayWithCurrent(Date date) {
		long n1 = date.getTime();// 返回距1970-1-1的毫秒数
		long n2 =new Date().getTime();// 返回距1970-1-1的毫秒数
		return n1-n2;
	}

	/**
	 * 将date1与date2比较，返回long类型的毫秒数
	 * @param date Date
	 * @return long类型，若为负数，则date1小于date2
	 */
	public static long compareDate(Date date1,Date date2) {
		long t1 = date1.getTime();
		long t2 =date2.getTime();
		return t1-t2;
	}

	/**
	 * 获取某个日期的下一天
	 * @param date
	 * @return Date
	 */
	public static Date getNextDay(Date date){
		Calendar cal=Calendar.getInstance();
		cal.setTime(date);
		cal.add(Calendar.DAY_OF_YEAR,1);
		return cal.getTime();
	}
	
	/**
	* 在日期上增加数个整月
	* @param date 日期
	* @param n 要增加的月数
	* @return
	*/
	public static Date addMonth(Date date, int n) {
		Calendar cal = Calendar.getInstance();
		cal.setTime(date);
		cal.add(Calendar.MONTH, n);
		return cal.getTime();
	}

	
	/**
	 * 给一个日期增加天数
	 * @param date ,add_day
	 * @return Date
	 */
	public static Date addDay(Date date,int add_day) {
		GregorianCalendar worldTour = new GregorianCalendar();
		worldTour.setTime(date);
		worldTour.add(GregorianCalendar.DATE, add_day);
		return worldTour.getTime();
	}
	
	/**
	 * 根据年月确定当月的最大天数
	 * @param year
	 * @param month
	 * @return
	 */
	public static int getMaxDayByYearMonth(int year, int month) {
		int maxDay = 0;
		int day = 1;
		/**
		 * 与其他语言环境敏感类一样，Calendar 提供了一个类方法 getInstance， 以获得此类型的一个通用的对象。Calendar 的
		 * getInstance 方法返回一 个 Calendar 对象，其日历字段已由当前日期和时间初始化：
		 */
		Calendar calendar = Calendar.getInstance();
		/**
		 * 实例化日历各个字段,这里的day为实例化使用
		 */
		calendar.set(year, month - 1, day);
		/**
		 * Calendar.Date:表示一个月中的某天 calendar.getActualMaximum(int
		 * field):返回指定日历字段可能拥有的最大值
		 */
		maxDay = calendar.getActualMaximum(Calendar.DATE);
		return maxDay;
	}
	
}