package com.yzt.insys.util;

import java.security.*;

public class MD5Util {

	// MD5加码。32位
	public static String encode(String inStr) {
		MessageDigest md5 = null;
		try {
			md5 = MessageDigest.getInstance("MD5");
		} catch (Exception e) {
			System.out.println(e.toString());
			e.printStackTrace();
			return "";
		}
		char[] charArray = inStr.toCharArray();
		byte[] byteArray = new byte[charArray.length];

		for (int i = 0; i < charArray.length; i++)
			byteArray[i] = (byte) charArray[i];

		byte[] md5Bytes = md5.digest(byteArray);

		StringBuffer hexValue = new StringBuffer();

		for (int i = 0; i < md5Bytes.length; i++) {
			int val = ((int) md5Bytes[i]) & 0xff;
			if (val < 16)
				hexValue.append("0");
			hexValue.append(Integer.toHexString(val));
		}

		return hexValue.toString();
	}

	// 可逆的加密算法
	public static String KL(String inStr) {
		// String s = new String(inStr);
		char[] a = inStr.toCharArray();
		for (int i = 0; i < a.length; i++) {
			a[i] = (char) (a[i] ^ 't');
		}
		String s = new String(a);
		return s;
	}

	// 加密后解密
	public static String dencode(String inStr) {
		char[] a = inStr.toCharArray();
		for (int i = 0; i < a.length; i++) {
			a[i] = (char) (a[i] ^ 't');
		}
		String k = new String(a);
		return k;
	}

	// 测试主函数
	public static void main(String args[]) {
		String s ="123";
		System.out.println("原始：" + s);
		System.out.println("MD5后：" + encode(s));
		//System.out.println("MD5后再加密：" + KL(MD5(s)));
		//System.out.println("解密为MD5后的：" + JM(KL(MD5(s))));
		//System.out.println("加密的：" + KL(s));
		System.out.println("解密的：" + dencode(KL(s)));
		
		String str = getEncode("MD5", "123");// 用MD5方式加密  
        System.out.println(str);  
        // fc3ff98e8c6a0d3087d515c0473f8677  
        String str1 = getEncode("SHA", "hello world!");// 用SHA方式加密  
        System.out.println(str1);  
        // 430ce34d020724ed75a196dfc2ad67c77772d169  
	}
	
	public static String getEncode(String codeType, String content) {  
        try {  
            MessageDigest digest = MessageDigest.getInstance(codeType);// 获取一个实例，并传入加密方式  
            digest.reset();// 清空一下  
            digest.update(content.getBytes());// 写入内容,可以指定编码方式content.getBytes("utf-8");  
            StringBuilder builder = new StringBuilder();  
            for (byte b : digest.digest()) {  
                builder.append(Integer.toHexString((b >>4) & 0xf));  
                builder.append(Integer.toHexString(b & 0xf));  
            }  
            return builder.toString();  
        } catch (NoSuchAlgorithmException e) {  
            e.printStackTrace();  
        }  
        return null;  
    }  
	
	
}
