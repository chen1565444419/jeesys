package com.yzt.insys.util;

import java.util.List;

import javax.annotation.Resource;

import org.apache.log4j.Logger;
import org.apache.shiro.SecurityUtils;
import org.apache.shiro.authc.AuthenticationException;
import org.apache.shiro.authc.AuthenticationInfo;
import org.apache.shiro.authc.AuthenticationToken;
import org.apache.shiro.authc.SimpleAuthenticationInfo;
import org.apache.shiro.authc.UnknownAccountException;
import org.apache.shiro.authz.AuthorizationInfo;
import org.apache.shiro.authz.SimpleAuthorizationInfo;
import org.apache.shiro.cache.Cache;
import org.apache.shiro.realm.AuthorizingRealm;
import org.apache.shiro.subject.PrincipalCollection;
import org.apache.shiro.subject.SimplePrincipalCollection;
import org.apache.shiro.subject.Subject;

import com.yzt.insys.beans.Role;
import com.yzt.insys.dto.RolePowerDto;
import com.yzt.insys.dto.UserDto;
import com.yzt.insys.service.RolePowerService;
import com.yzt.insys.service.RoleService;
import com.yzt.insys.service.UserService;

public class MonitorRealm extends AuthorizingRealm {

	private Logger log=Logger.getLogger(getClass());
	
	@Resource
	private UserService userService;
	
	@Resource
	private RoleService roleService;
	
	@Resource
	private RolePowerService rpService;
	
	public MonitorRealm() {
		log.info("monitorRealm>>>>>>>>>>>>>>>>>>>>>>>>");
		setAuthenticationTokenClass(UserToken.class);
	}
	/**
	 * 验证
	 */
	@Override
	protected AuthenticationInfo doGetAuthenticationInfo(AuthenticationToken authcToken) throws AuthenticationException {
		/* 这里编写认证代码 */
		UserToken token = (UserToken) authcToken;
		UserDto user=userService.queryByNameAndPwd(token.getUser());
		if(user==null){
			throw new UnknownAccountException("用户名或密码错误");
		}
		Subject subject=SecurityUtils.getSubject();
		UserUtil.saveUserToSession(subject.getSession(), user);
		SimpleAuthenticationInfo saInfo = new SimpleAuthenticationInfo(user,user.getUserPwd(), getName());
		return saInfo;
	}

	/**
	 * 授权
	 */
	@Override
	protected AuthorizationInfo doGetAuthorizationInfo(PrincipalCollection principals) {
		UserDto user = (UserDto) principals.fromRealm(getName()).iterator().next();
		if (user != null) {
			SimpleAuthorizationInfo info = new SimpleAuthorizationInfo();
			if(user.getRoles()!=null){
				for(Role role:user.getRoles()){
					info.addRole(role.getRoleName());
				}
			}
			List<RolePowerDto> rpList=rpService.queryAllPowersByRoleIds(UserUtil.getRoleIds(user.getRoles()));
			if(rpList!=null&&rpList.size()>0){
				String permission=null;
				for(int i=0,len=rpList.size();i<len;i++){
					permission=rpList.get(i).getMasterName();
					if(permission!=null){
						info.addStringPermission(permission);
					}
				}
			}
			return info;
		}
		return null;
	}
	
	/**
	 * 更新用户授权信息缓存.
	 */
	public void clearCachedAuthorizationInfo(String principal) {
		SimplePrincipalCollection principals = new SimplePrincipalCollection(principal, getName());
		clearCachedAuthorizationInfo(principals);
	}

	/**
	 * 清除所有用户授权信息缓存.
	 */
	public void clearAllCachedAuthorizationInfo() {
		Cache<Object, AuthorizationInfo> cache = getAuthorizationCache();
		if (cache != null) {
			for (Object key : cache.keys()) {
				cache.remove(key);
			}
		}
	}
}
