package com.yzt.insys.util;

import org.springframework.mail.javamail.JavaMailSenderImpl;

public class MyMailSender extends JavaMailSenderImpl {

	private String [] receivers;

	public String[] getReceivers() {
		return receivers;
	}

	public void setReceivers(String[] receivers) {
		this.receivers = receivers;
	}
	
	
}
