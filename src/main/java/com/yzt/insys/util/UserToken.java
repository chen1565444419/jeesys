package com.yzt.insys.util;

import org.apache.shiro.authc.UsernamePasswordToken;

import com.yzt.insys.beans.User;

public class UserToken extends UsernamePasswordToken {

	/**
	 * 
	 */
	private static final long serialVersionUID = 6787344269886036610L;

	private User user;
	
	

	@Override
	public String getUsername() {
		return user.getUserName();
	}

	@Override
	public char[] getPassword() {
		return user.getUserPwd().toCharArray();
	}

	public UserToken() {
		super();
	}

	public UserToken(User user,boolean rememberMe) {
		super(user.getUserName(),user.getUserPwd(),rememberMe);
		this.user = user;
	}

	public User getUser() {
		return user;
	}

	public void setUser(User user) {
		this.user = user;
	}

	public static long getSerialversionuid() {
		return serialVersionUID;
	}

	@Override
	public Object getPrincipal() {
		return user;
	}

	@Override
	public Object getCredentials() {
		return user.getUserPwd();
	}
	
	
}
