package com.yzt.insys.util.tag;

import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.List;

import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.serializer.SerializerFeature;
/**
 * JSTL工具类
 * @作者 zsf
 * @时间 2013-6-21
 */
public class JSTLUtil {

	/**
	 * 字符串替换访求
	 * 
	 * @param str
	 * @param oldStr
	 * @param newStr
	 * @return
	 */
	public static String replaceStr(String str, String oldStr, String newStr) {
		if (oldStr == null || oldStr.length() == 0) {
			return str;
		}
		if (newStr == null || newStr.length() == 0) {
			return str;
		}
		if (oldStr.length() != newStr.length()) {
			return str;
		}
		String[] oldStrArray = oldStr.split(",");
		String[] newStrArray = newStr.split(",");
		for (int i = 0, len = oldStrArray.length; i < len; i++) {
			str = str.replace(oldStrArray[i], newStrArray[i]);
		}
		return str;
	}

	/**
	 * 格式化日期
	 * @param format
	 * @return
	 */
	public static String formatCurrentDate(String format) {
		SimpleDateFormat simple = new SimpleDateFormat(format);
		return simple.format(new Date());
	}
	
	/**
	 * 将日期转换成long类型
	 * @param date
	 * @return
	 */
	public static long currDateToLong(){
		return new Date().getTime();
	}
	/**
	 * 
	 */
	public static String getBoolean(Integer i){
		if(i==1){
			return "是";
		}else{
			return "否";
		}
	}
	/**
	 * 格式化日期
	 * @param date
	 * @param format
	 * @return
	 */
	public static String formatDate(Date date,String format) {
		if(date==null){
			return null;
		}
		SimpleDateFormat simple = new SimpleDateFormat(format);
		return simple.format(date);
	}
	

	/**
	 * 字符串截取
	 * 
	 * @param str
	 * @param index
	 * @return
	 */
	public static String subStr(String str, int index) {
		if (str == null || str.length() == 0) {
			return str;
		}
		if (index >= str.length()) {
			return str;
		}
		return str.substring(index);
	}

	/**
	 * 将数字转换成汉字。例:101，转换后为一百零
	 * @param val
	 * @return
	 */
	public static String num2Str(Integer val) {
		String result = "";
		String valStr = "" + val;
		switch (valStr.length()) {
		case 1:
			result = num2Char(val);
			break;
		case 2:
			result = num2Char(Integer.parseInt("" + valStr.charAt(0))) + "十";
			if(valStr.charAt(1)!='0'){
				result+=num2Char(Integer.parseInt("" + valStr.charAt(1)));
			}
			break;
		case 3:
			result = num2Char(Integer.parseInt("" + valStr.charAt(0))) + "百";
			if(valStr.charAt(1)=='0'&&valStr.charAt(2)=='0'){
				
			}else if(valStr.charAt(1)=='0'&&valStr.charAt(2)!='0'){
				result+=num2Char(0)+num2Char(Integer.parseInt(""+valStr.charAt(2)));
			}else if(valStr.charAt(1)!='0'&&valStr.charAt(2)=='0'){
					result+=num2Char(Integer.parseInt("" + valStr.charAt(1))) + "十";
			}else{
				result+=num2Char(Integer.parseInt("" + valStr.charAt(1))) + "十"+num2Char(Integer.parseInt(""+valStr.charAt(2)));
			}
			break;
		}
		return result;
	}

	private static String num2Char(Integer val) {
		String result = "";
		switch (val) {
		case 0:
			result = "零";
			break;
		case 1:
			result = "一";
			break;
		case 2:
			result = "二";
			break;
		case 3:
			result = "三";
			break;
		case 4:
			result = "四";
			break;
		case 5:
			result = "五";
			break;
		case 6:
			result = "六";
			break;
		case 7:
			result = "七";
			break;
		case 8:
			result = "八";
			break;
		case 9:
			result = "九";
			break;
		}
		return result;
	}

	/**
	 * 判断obj是否在list中
	 * @param dataList
	 * @param obj
	 * @return
	 */
	public static Boolean contains(List<Object> dataList,Object obj){
		if(dataList==null||obj==null||dataList.size()==0){
			return false;
		}
		for(int i=0,len=dataList.size();i<len;i++){
			if(dataList.get(i).equals(obj)){
				return true;
			}
		}
		return false;
	}
	
	/**
	 * 判断obj是否在list<?>中
	 * @param dataList
	 * @param obj
	 * @return
	 */
	public static Boolean isContains(List<?> dataList,Object obj){
		if(dataList==null||obj==null||dataList.size()==0){
			return false;
		}
		for(int i=0,len=dataList.size();i<len;i++){
			if(dataList.get(i).equals(obj)){
				return true;
			}
		}
		return false;
	}
	
	public static void main(String[] args) throws ParseException {
		// System.out.println(replaceStr("《廉政准则》学习体会(22-19375).doc","（,）,(,),《,》","︵,︶,︵,︶,︽,︾"));
		// System.out.println(isEquals("aa","bbb"));

		System.out.println(num2Str(11));
		System.out.println(num2Str(10));
		System.out.println(num2Str(101));
		System.out.println(num2Str(111));
		Object obj="134124321";
		System.out.println(formatNumStr(obj));
		DateFormat simpleDateFormat=new SimpleDateFormat("yyyy-MM-dd");
		Date endTime=simpleDateFormat.parse("2014-07-26");
		System.out.println(getDay(new Date(), endTime));
	}
	
	public static String formatNumStr(Object numstr){
		if(numstr==null||numstr.toString().trim().equals("")){
			return "";
		}else if(numstr.toString().contains("E10")){
			return numstr.toString().replace("E10","");
		}else{
			return numstr.toString();
		}
	}
	/**
	 * 判断两个日期相关多少天(endTime-startTime)
	 * @param startTime
	 * @param endTime
	 * @return 返回天数
	 */
	public static int getDay(Date startTime,Date endTime){
		long countDay=endTime.getTime()-startTime.getTime();
		int result=Integer.parseInt(""+(countDay/(24*60*60*1000)));
		return result+1;
	}
	
	/**
	 * 将list转换成json字符串
	 * @param list
	 * @return
	 */
	public static String toJson(List<?> list,String dateformat){
		if(list==null||list.size()==0){
			return "[]";
		}
		if(dateformat==null||dateformat.trim().equals("")){
			dateformat="yyyy-MM-dd HH:mm:ss";
		}
		return JSON.toJSONStringWithDateFormat(list,dateformat,SerializerFeature.WriteDateUseDateFormat);
	}
	
	/**
	 * 获取检修类型
	 * @param typeId
	 * @return
	 */
	public static String getType(Integer typeId){
		String result="检修";
		switch (typeId) {
			case 1:
				result="日"+result;
				break;
			case 2:
				result="周"+result;
				break;
			case 3:
				result="月"+result;
				break;
			case 4:
				result="季"+result;
				break;
			case 5:
				result="年"+result;
				break;
			default :
				result="其它";
				break;
		}
		return result;
	}
	
	/**
	 * 获取当前星期
	 * @return
	 */
	public static String getCurrentWeek(){
		Calendar calendar=Calendar.getInstance();
		int weekDay=calendar.get(Calendar.DAY_OF_WEEK);
		String[] weekArray=new String[]{"日","一","二","三","四","五","六"};
		return weekArray[weekDay-1];
	}
}
