<%@ page contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>500访问错误页面</title>

<%@include file="taglibs.jsp" %>
<%@include file="jquery.jsp" %>

<link rel="stylesheet" type="text/css" href="${base}/resources/css/login.css">
<script type="text/javascript">
	$(function(){
		$(".main").css("margin-top",(document.documentElement.clientHeight-400)/2);
	});
</script>
<style type="text/css">
.TS_title{
	font-size: 80px;
	font-weight: bold;
	margin: auto;
	padding-top: 50px;
}

</style>
</head>
<body>
	<div class="top">
		<div class="top_logo"></div>
		<div class="top_2"></div>
	</div>
	<div class=main>
		<div class="m_1"></div>
		<div class="m_2" style="text-align: center;">
			<div class="TS_title">500错误！</div>
		</div>
		<div class="m_3" style="font-size: 15px;">
		服务器异常,请联系管理员处理。<a href="javascript:top.location.href='${base}';">返回首页</a>
		</div>
	</div>
	<div class="bottom">
	</div>
</body>
</html>