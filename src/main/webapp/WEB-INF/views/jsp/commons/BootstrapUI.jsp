<link rel="stylesheet" type="text/css" href="${base}/resources/plugins/BootstrapUI/assets/css/bootstrap.min.css" />
<link rel="stylesheet" type="text/css" href="${base}/resources/plugins/BootstrapUI/css/custom-theme/jquery-ui-1.10.3.custom.css" />
<link rel="stylesheet" type="text/css" href="${base}/resources/plugins/BootstrapUI/css/custom-theme/hbxh_style.css" />

<script type="text/javascript" src="${base}/resources/plugins/BootstrapUI/assets/js/bootstrap.min.js"></script>
<script type="text/javascript" src="${base}/resources/plugins/BootstrapUI/assets/js/jquery-ui-1.10.3.custom.min.js"></script>