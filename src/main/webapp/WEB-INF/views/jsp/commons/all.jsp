<%@ page contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<%@include file="taglibs.jsp" %>
<%@include file="jquery.jsp" %>
<%@include file="easyUI.jsp" %>
<%@include file="baseCSS.jsp"%>
<%@include file="base.jsp" %>
<%@include file="event.jsp" %>
<c:set var="rootPath" value="${powers[0].masterName}" scope="page"/>
<%-- <%@include file="generic-all-jquery.jsp"%> --%>
<script type="text/javascript" src="${base}/resources/js/${fn:substring(rootPath,0,fn:indexOf(rootPath,'_'))}/all.js" charset="UTF-8"></script>
<%@include file="permission.jsp"%>
<title>${initParam.title}</title>
</head>
<body>
<table id="resultTable"></table>
</body>
</html>