<%@ page language="java" import="java.util.*" contentType="text/html; charset=UTF-8"%>
<%
String easyuiTheme = "";//指定如果用户未选择样式，那么初始化一个默认样式
Cookie[] cookies = request.getCookies();
if (null != cookies) {
	for (Cookie cookie : cookies) {
		if(cookie.getName().equals("easyuiTheme")){
			easyuiTheme=cookie.getValue();
			break;
		}
	}
	if(easyuiTheme.equals("")){
		easyuiTheme="metro-blue";
		Cookie cookie=new Cookie("easyuiTheme",easyuiTheme);
		cookie.setMaxAge(365*24*60*60);
		response.addCookie(cookie);
	}
}else{
	easyuiTheme = "metro-blue";
	Cookie cookie=new Cookie("easyuiTheme",easyuiTheme);
	cookie.setMaxAge(365*24*60*60);
	response.addCookie(cookie);
}
%>
<link id="easyuiTheme" rel="stylesheet" href="${base}/resources/plugins/easyui-1.3.4/themes/${cookie.easyuiTheme.value}/easyui.css" type="text/css">
<link rel="stylesheet" type="text/css" href="${base}/resources/plugins/easyui-1.3.4/themes/icon.css" />
<script type="text/javascript" src="${base}/resources/plugins/easyui-1.3.4/jquery.easyui.min.js" charset="UTF-8"></script>
<script type="text/javascript" src="${base}/resources/plugins/easyui-1.3.4/locale/easyui-lang-zh_CN.js" charset="UTF-8"></script>
<script type="text/javascript" src="${base}/resources/plugins/easyui-1.3.4/datagrid-detailview.js" charset="UTF-8"></script>
<script type="text/javascript" src="${base}/resources/js/common/easyUIExtend.js" charset="UTF-8"></script>