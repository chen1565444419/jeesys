<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<link rel="stylesheet" href="${base}/resources/plugins/kindeditor/themes/default/default.css" />
<link rel="stylesheet" href="${base}/resources/plugins/kindeditor/plugins/code/prettify.css" />
<script charset="utf-8" src="${base}/resources/plugins/kindeditor/kindeditor.js"></script>
<script charset="utf-8" src="${base}/resources/plugins/kindeditor/lang/zh_CN.js"></script>
<script charset="utf-8" src="${base}/resources/plugins/kindeditor/plugins/code/prettify.js"></script>
<script>
		KindEditor.ready(function(K){
			var editor= K.create('textarea[name="content"]', {
				uploadJson :base+"/help/uploadFile"+suffix+"?editor=editor",
				fileManagerJson : 'kindeditor/file_manager_json.jsp',
				allowFileManager : false,
				height:428,
				width:970,
				resizeType:0,
				afterCreate : function() {
					var self = this;
					K.ctrl(document, 13, function() {
						self.sync();
						document.forms['info'].submit();
					});
					K.ctrl(self.edit.doc, 13, function() {
						self.sync();
						document.forms['info'].submit();
					});
				},
			afterBlur: function(){this.sync();}
			});
			prettyPrint();
		});
	</script>