<%@ page language="java" contentType="text/html; charset=UTF-8"%>
 <script type="text/javascript" src="${base}/resources/plugins/lhgdialog/lhgdialog.min.js?self=false&skin=iblack"></script>
 <script type="text/javascript">
	 $(document).ready(function(){
		 (function(config){
			    config['extendDrag'] = true; // 注意，此配置参数只能在这里使用全局配置，在调用窗口的传参数使用无效
			    config['lock'] = true;
			    config['fixed'] = true;
			    config['okVal'] = '确定';
			    config['cancelVal'] = '取消';
			    min:false;
	    		max:false;
			    // [more..]
			})($.dialog.setting);
	 });
 </script>