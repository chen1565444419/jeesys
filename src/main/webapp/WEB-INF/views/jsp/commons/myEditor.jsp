<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<link rel="stylesheet" href="${base}/resources/plugins/kindeditor/themes/default/default.css" />
<link rel="stylesheet" href="${base}/resources/plugins/kindeditor/plugins/code/prettify.css" />
<script charset="utf-8" src="${base}/resources/plugins/kindeditor/kindeditor.js"></script>
<script charset="utf-8" src="${base}/resources/plugins/kindeditor/lang/zh_CN.js"></script>
<script charset="utf-8" src="${base}/resources/plugins/kindeditor/plugins/code/prettify.js"></script>