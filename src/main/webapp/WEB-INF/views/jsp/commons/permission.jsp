<%@ page language="java" contentType="text/html; charset=UTF-8"%>
<c:set var="flg" value="false" />
<script type="text/javascript">
var rootPath="${rootPath}";
rootPath="/"+rootPath.substring(0,rootPath.indexOf("_"));
var toolbar=[
<c:forEach items="${powers}" var="p"  varStatus="sta">
	<c:if test="${fn:containsIgnoreCase(p.position,'top')}">
		<shiro:hasPermission name="${p.masterName}">
		<c:if test="${flg}">,"-",</c:if>
		{
			text:"${p.powerName}",
			iconCls:"${p.iconCls}",
			handler:${p.url}
		}
		<c:if test="${(p.url eq 'viewSelected')&&(p.target eq '_blank')}">
			<c:set var="viewTarget" value="_blank" />
		</c:if>
		<c:set value="true" var="flg" />
		</shiro:hasPermission>
	</c:if>
</c:forEach>
];

<c:set value="false" var="flg" />
function operateFunction(value,row,index){
	var html="";
	if(row==null){return html;}
	<c:forEach items="${powers}" var="p"  varStatus="sta">
		<c:if test="${fn:containsIgnoreCase(p.position,'middle')}">
			<shiro:hasPermission name="${p.masterName}">
				<c:if test="${flg}">html+=" | ";</c:if>
				html+="<span class='span_a' onclick='javascript:${p.url}(\""+row.id+"\");'>${p.powerName}</span>";
				<c:set value="true" var="flg" />
			</shiro:hasPermission>
		</c:if>
	</c:forEach>
	return html;
}

var viewTarget="${viewTarget}";
</script>
