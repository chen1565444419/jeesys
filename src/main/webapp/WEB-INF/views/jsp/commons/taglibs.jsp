<%@ page contentType="text/html; charset=UTF-8" pageEncoding="UTF-8" trimDirectiveWhitespaces="true"%>
<%@ taglib prefix="c" uri="/WEB-INF/tlds/c.tld"%>
<%@ taglib prefix="fn" uri="/WEB-INF/tlds/fn.tld"%>
<%@ taglib prefix="fmt" uri="/WEB-INF/tlds/fmt.tld" %>
<%@ taglib prefix="myFn" uri="http://www.yzt-cn.com" %>
<%@ taglib prefix="shiro" uri="http://shiro.apache.org/tags" %>
<%@include file="nocache.jsp" %>
<c:set var="base" value="${pageContext.request.contextPath}" />
<c:set var="resources" value="${base}/resources" />
<c:set var="format" value="yyyy-MM-dd HH:mm:ss" />
<link rel="shortcut icon" href="${base}/resources/images/icon/favicon.ico" />
<script type="text/javascript">
	var base = "${base}";
	var suffix="${initParam.suffix}";
	var resources="${resources}";
	var birtBaseUrl="http://192.168.2.16:8080/birt/frameset?__report=reports/";
</script>
<%-- 
<script type="text/javascript" src="${base}/resources/js/common/forbidenLeftMouse.js" charset="UTF-8"></script>
<script type="text/javascript" src="${base}/resources/js/common/forbidenRightMouse.js" charset="UTF-8"></script>
--%>
