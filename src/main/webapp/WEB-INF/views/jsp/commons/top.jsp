<%@ page language="java" import="java.util.*" contentType="text/html; charset=UTF-8"%>
<%@include file="taglibs.jsp" %>
<script type="text/javascript" src="${base}/resources/js/common/cookie.js"></script>
<script type="text/javascript" src="${base}/resources/js/top.js"></script>
<div id="hbox">
	<div id="bs_bannercenter">
		<div id="bs_bannerleft">
			<div id="bs_bannerright"></div>
		</div>
	</div>
<div>
	<div id="logoTitle" style="line-height:60px;font-size:20px;font-weight: bolder;">${initParam.title}</div>
</div>
<div id="sessionInfoDiv" style="position: absolute; right: 10px; top: 5px;">
	欢迎您,${sessionScope.USER_KEY_IN_SESSION.realName}
</div>
<div style="position: absolute; right: 0px; bottom: 0px;">
	<span style="margin-right:30px;">
		当前时间:<span id="currTime">${myFn:formatCurrentDate('yyyy年M月d日')}&emsp;星期${myFn:getCurrentWeek()}&emsp;${myFn:formatCurrentDate('HH:mm:ss')}
		</span>
	</span>
	<a href="${base}/help/view${initParam.suffix}" target="_blank" style="color:#FFFFFF;">在线帮助</a>
	<a href="javascript:void(0);" class="easyui-menubutton" data-options="menu:'#layout_north_pfMenu',iconCls:'ext-icon-rainbow'">更换皮肤</a> <a href="javascript:void(0);" class="easyui-menubutton" data-options="menu:'#layout_north_kzmbMenu',iconCls:'ext-icon-cog'">控制面板</a> <a href="javascript:void(0);" class="easyui-menubutton" data-options="menu:'#layout_north_zxMenu',iconCls:'ext-icon-disconnect'">注销</a>
</div>
<div id="layout_north_pfMenu" style="width: 120px; display: none;">
	<div onclick="changeTheme('default');" title="default">default</div>
	<div onclick="changeTheme('gray');" title="gray">gray</div>
	<div onclick="changeTheme('metro');" title="metro">metro</div>
	<div onclick="changeTheme('bootstrap');" title="bootstrap">bootstrap</div>
	<div onclick="changeTheme('black');" title="black">black</div>
	<div class="menu-sep"></div>
	<div onclick="changeTheme('metro-blue');" title="metro-blue">metro-blue</div>
	<div onclick="changeTheme('metro-gray');" title="metro-gray">metro-gray</div>
	<div onclick="changeTheme('metro-green');" title="metro-green">metro-green</div>
	<div onclick="changeTheme('metro-orange');" title="metro-orange">metro-orange</div>
	<div onclick="changeTheme('metro-red');" title="metro-red">metro-red</div>
</div>
<div id="layout_north_kzmbMenu" style="width: 100px; display: none;">
	<div data-options="iconCls:'ext-icon-user_edit'" onclick="$('#passwordDialog').dialog('open');">修改密码</div>
	<div class="menu-sep"></div>
	<div data-options="iconCls:'ext-icon-user'" onclick="showMyInfoFun('${sessionScope.USER_KEY_IN_SESSION.id}');">我的信息</div>
</div>
<div id="layout_north_zxMenu" style="width: 100px; display: none;">
	<!-- <div data-options="iconCls:'ext-icon-lock'" onclick="lockWindowFun();">锁定窗口</div>
	<div class="menu-sep"></div> -->
	<div data-options="iconCls:'ext-icon-door_out'" onclick="logoutFun();">退出系统</div>
</div>
</div>