<%@ page contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>${initParam.title}</title>
<%@include file="taglibs.jsp" %>
<%@include file="jquery.jsp" %>
<%@include file="easyUI.jsp" %>
<%@include file="portal.jsp" %>
<style type="text/css">
body{
	width: 100%;
	height:100%;
	margin:auto;
}
</style>
<script type="text/javascript">
$(document).ready(function(){
	var documentHeight=$(document).height();
	var documentWidth=$(document).width();
	$("#portal").css({height:documentHeight});
	var options={
			 url:base+"/dept/getData"+suffix,
			 queryParams:{_d:Math.random()},
			 sortName:"seq",
			 sortOrder:"desc",
			 idField:"id",
			 columns:[[
			       {field:"no",checkbox:true},
			       {field:"id",title:"id",width:100,hidden:true,checkbox:true},
			       {field:"deptName",title:"部门名称",align:"center",sortable:true},
			       {field:"seq",title:"排序序号",align:"center",sortable:true,hidden:true},
			       {field:"createTime",title:"创建时间",align:"center",sortable:true},
			       {field:"operate",title:"操作",align:"center"}
			 ]]
		};
	$("#monthgrid").datagrid(options);
	$("#seasongrid").datagrid(options);
	$("#yeargrid").datagrid(options);
	$("#compgrid").datagrid(options);
	$("#oilgrid").datagrid(options);
	$('#daygrid').datagrid(options);
	$("#portal").portal({
		border:false,
		fit:true
	});
	
	$("#portal").portal("resize");
});
</script>
</head>
<body>
	<div id="portal">
		<div style="width:500px;">
			<div title="日检修设备"  data-options="collapsible:true" style="padding: 0px;">
				<table id="daygrid" style="width:500px;padding: 0px;"></table>
			</div>
			<div title="月检修设备" data-options="collapsible:true" style="padding: 0px;">
				<table id="monthgrid" style="width:500px;padding: 0px;"></table>
			</div>
			<div title="季检修设备" data-options="collapsible:true" style="padding: 0px;">
				<table id="seasongrid" style="width:500px;padding: 0px;"></table>
			</div>
			<div title="年检修设备" data-options="collapsible:true" style="padding: 0px;">
				<table id="yeargrid" style="width:500px;padding: 0px;"></table>
			</div>
		</div>
		<div style="width:500px;">
			<div title="定期更换设备" data-options="collapsible:true" style="padding: 0px;">
				<table id="compgrid" style="width:500px;padding: 0px;"></table>
			</div>
			<div title="定期润滑设备" data-options="collapsible:true" style="padding: 0px;">
				<table id="oilgrid" style="width:500px;padding: 0px;"></table>
			</div>
		</div>
	</div>
</body>
</html>