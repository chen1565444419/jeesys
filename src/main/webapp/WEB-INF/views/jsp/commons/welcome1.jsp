<%@ page contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>${initParam.title}</title>
<%@include file="taglibs.jsp" %>
<%@include file="jquery.jsp" %>
<%@include file="easyUI.jsp" %>
<%@include file="base.jsp" %>
<%@include file="event.jsp" %>
<%@include file="portal.jsp" %>
<script>
		$(function(){
			$('#pp').portal({
				border:false,
				fit:true
			});
			//add();
		});
		function add(){
			for(var i=0; i<3; i++){
				var p = $('<div/>').appendTo('body');
				p.panel({
					title:'Title'+i,
					content:'<div style="padding:5px;">Content'+(i+1)+'</div>',
					height:100,
					closable:false,
					collapsible:true
				});
				$('#pp').portal('add', {
					panel:p,
					columnIndex:i
				});
			}
			$('#pp').portal('resize');
		}
		function remove(){
			$('#pp').portal('remove',$('#pgrid'));
			$('#pp').portal('resize');
		}
	</script>
	<style>
	.datagrid-cell{
		color:#FF0000;
	}	
	</style>
</head>
<c:set var="now" value="<%=new Date()%>" />
<body class="easyui-layout">
	<div data-options="region:'north',border:false">
		<div id="pp" style="position:relative">
			<div style="width:50%;">
			    <div title="即将到期需检修的设备" data-options="collapsible:true,closable:false" style="height:230px;padding:0px;">
					<table class="easyui-datagrid" data-options="pagination:false,remoteSort:false,width:560,onBeforeLoad:function(parama){return true;}">
						<thead>
							<tr>
								<th data-options="field:'name',width:150">设备名称</th>
								<th data-options="field:'no',width:50">编号</th>
								<th data-options="field:'category',width:100">设备类型</th>
								<th data-options="field:'deptName',width:80">所属部门</th>
								<th data-options="field:'type',width:80">检修类型</th>
								<th data-options="field:'endDate',width:100">到期日期</th>
								<th data-options="field:'leaveDate',width:80,align:'center'">剩余天数</th>
							</tr>
						</thead>
						<tbody>
							<c:forEach items="${expireDetectDevices}" var="d">
								<tr>
									<td>${d.name}</td>
									<td>${d.no}</td>
									<td>${d.category.categoryName}</td>
									<td>${d.dept.deptName}</td>
									<td>${myFn:getType(d.type)}</td>
									<td><font color='#FF0000'>${myFn:formatDate(d.expireDetectDate,'yyyy-MM-dd')}</font></td>
									<td><font color='#FF0000'>${myFn:getDay(now,d.expireDetectDate)}</font></td>
								</tr>
							</c:forEach>
						</tbody>
					</table>
				</div>
			    
			    <div title="即将到期需润滑的项目" data-options="collapsible:true,closable:false" style="height:230px;padding:5px;">
			    	<table class="easyui-datagrid" data-options="pagination:false,remoteSort:false,width:560,onBeforeLoad:function(parama){return true;}">
						<thead>
							<tr>
								<th data-options="field:'oilName',width:80">润滑项目</th>
								<th data-options="field:'deviceName',width:150">所属设备</th>
								<th data-options="field:'no',width:50">编号</th>
								<th data-options="field:'categoryName',width:100">设备类型</th>
								<th data-options="field:'deptName',width:80">所属部门</th>
								<th data-options="field:'expireOilDate',width:100">到期日期</th>
								<th data-options="field:'leaveDate',width:80,align:'center'">剩余天数</th>
							</tr>
						</thead>
						<tbody>
							<c:forEach items="${expireOilDevices}" var="d">
								<tr>
									<td>${d.oilName}</td>
									<td>${d.deviceName}</td>
									<td>${d.no}</td>
									<td>${d.categoryName}</td>
									<td>${d.deptName}</td>
									<td><font color='#FF0000'>${myFn:formatDate(d.expireOilDate,'yyyy-MM-dd')}</font></td>
									<td><font color='#FF0000'>${myFn:getDay(now,d.expireOilDate)}</font></td>
								</tr>
							</c:forEach>
						</tbody>
					</table>
			    </div>
			</div>
			<div style="width:50%;">
				<div id="pgrid" title="即将到期需更换的备件" data-options="collapsible:true,closable:false" style="height:230px;">
					<table class="easyui-datagrid" data-options="pagination:false,remoteSort:false,width:560,onBeforeLoad:function(parama){return true;}">
						<thead>
							<tr>
								<th data-options="field:'compName',width:80">备件名称</th>
								<th data-options="field:'deviceName',width:150">所属设备</th>
								<th data-options="field:'no',width:50">编号</th>
								<th data-options="field:'categoryName',width:100">设备类型</th>
								<th data-options="field:'deptName',width:80">所属部门</th>
								<th data-options="field:'expireReplaceDate',width:100">到期日期</th>
								<th data-options="field:'leaveDate',width:80,align:'center'">剩余天数</th>
							</tr>
						</thead>
						<tbody>
							<c:forEach items="${expireReplaceDevices}" var="d">
								<tr>
									<td>${d.compName}</td>
									<td>${d.deviceName}</td>
									<td>${d.no}</td>
									<td>${d.categoryName}</td>
									<td>${d.deptName}</td>
									<td><font color='#FF0000'>${myFn:formatDate(d.expireReplaceDate,'yyyy-MM-dd')}</font></td>
									<td><font color='#FF0000'>${myFn:getDay(now,d.expireReplaceDate)}</font></td>
								</tr>
							</c:forEach>
						</tbody>
					</table>
				</div>
				<div id="pgrid" title="即将到期需要校验的设备" data-options="collapsible:true,closable:false" style="height:230px;">
					<table class="easyui-datagrid" data-options="pagination:false,remoteSort:false,width:560,onBeforeLoad:function(parama){return true;}">
						<thead>
							<tr>
								<th data-options="field:'devName',width:80">设备名称</th>
								<th data-options="field:'no',width:50">编号</th>
								<th data-options="field:'categoryName',width:100">设备类型</th>
								<th data-options="field:'deptName',width:80">所属部门</th>
								<th data-options="field:'cycle',width:80">校验周期</th>
								<th data-options="field:'expireReplaceDate',width:100">到期日期</th>
								<th data-options="field:'leaveDate',width:80,align:'center'">剩余天数</th>
							</tr>
						</thead>
						<tbody>
							<c:forEach items="${expireCheckDevices}" var="d">
								<tr>
									<td>${d.name}</td>
									<td>${d.no}</td>
									<td>${d.category.categoryName}</td>
									<td>${d.dept.deptName}</td>
									<td>${myFn:getType(d.type)}</td>
									<td><font color='#FF0000'>${myFn:formatDate(d.expireDetectDate,'yyyy-MM-dd')}</font></td>
									<td><font color='#FF0000'>${myFn:getDay(now,d.expireDetectDate)}</font></td>
								</tr>
							</c:forEach>
						</tbody>
					</table>
				</div>
			</div>
		</div>
	</div>
</body>
</html>