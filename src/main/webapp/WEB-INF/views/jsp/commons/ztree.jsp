<%@ page language="java" contentType="text/html; charset=UTF-8"%>
<link rel="stylesheet" href="${base}/resources/plugins/ztree/css/demo.css" type="text/css">
<link rel="stylesheet" href="${base}/resources/plugins/ztree/css/zTreeStyle/zTreeStyle.css" type="text/css">
<script type="text/javascript" src="${base}/resources/plugins/ztree/js/jquery.ztree.core-3.5.min.js"></script>
<script type="text/javascript" src="${base}/resources/plugins/ztree/js/jquery.ztree.excheck-3.5.min.js"></script>