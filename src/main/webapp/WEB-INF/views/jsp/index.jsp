<%@ page contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<%@include file="commons/taglibs.jsp" %>
<%@include file="commons/jquery.jsp" %>
<%@include file="commons/easyUI.jsp" %>
<%@include file="commons/base.jsp" %>
<%@include file="commons/event.jsp" %>
<script type="text/javascript" src="${base}/resources/js/common/MD5.js"></script>
<script type="text/javascript" src="${base}/resources/js/index.js"></script>
<style>
.logo{background-color:#00A3C9;color:#FFFFFF;}
.tree-node {padding: 8px;white-space: nowrap;cursor: pointer;}
.panel-header{padding: 10px;position: relative;}
</style>
<title>${initParam.title}</title>
</head>
<body id="mainLayout" class="easyui-layout">
	<div data-options="region:'north',border:false,href:'admin/commons/top${initParam.suffix}'" style="height:70px;overflow: hidden;" class="logo easyui-panel"></div>
	<div data-options="region:'south',border:true,split:false" style="height:30px; vertical-align: middle;">
		<div style="text-align: center;font-size:16;font-weight: bolder;line-height:30px;">技术支持：武汉悦木子科技有限公司●软件部 &emsp;版本:1.7</div>
	</div>
	<div data-options="region:'west',split:true,plain:true,border:true,panelHeight:'auto'" title="管理菜单" style="width:200px;">
		<div id="leftMenu" class="easyui-accordion" data-options="border:false,fit:true">
			<c:forEach items="${sessionScope.supPowers}" var="rp" varStatus="sta">
				<div title="${rp.powerName}" data-expand="false" data-options="iconCls:'${rp.iconCls }',collapsible:true,onBeforeExpand:expandMenu">
					 <ul class="easyui-tree" data-url="${base}/tree${initParam.suffix}?fatherId=${rp.powerId}" data-options="iconCls:'${rp.iconCls}'"></ul>
    			</div> 
			</c:forEach>
		</div>
	</div>
	<div data-options="region:'center',border:true" style="padding:0px;">
		<div id="mainTabs"  data-options="fit:true" style="height:99.4%;">
			<div title="首页" data-options="iconCls:'icon-my-heart',fit:true">
				<iframe name="iframe" id="iframe" src="device/index${initParam.suffix}" allowTransparency="true" style="border: 0; width: 100%; height:100%;overflow: scroll;" scrolling="auto" frameBorder="0"></iframe>
			</div>
		</div>
		<div id="tabsMenu" style="width: 120px; display: none;">
			<div type="refresh">刷新</div>
			<div class="menu-sep"></div>
			<div type="close">关闭</div>
			<div type="closeOther">关闭其他</div>
			<div type="closeAll">关闭所有</div>
		</div>
	</div>
	<div id="loginDialog" title="解锁登录" style="display: none;">
		<form method="post" class="form" onsubmit="return false;">
			<table class="table">
				<tr>
					<th width="50">登录名</th>
					<td>${sessionScope.USER_KEY_IN_SESSION.realName}<input type="hidden" name="userName" value="${sessionScope.USER_KEY_IN_SESSION.userName}" /></td>
				</tr>
				<tr>
					<th>密码</th>
					<td><input name="userPwd" id="userPwd" type="password" class="easyui-validatebox" data-options="required:true" /></td>
				</tr>
			</table>
		</form>
	</div>
	<div id="passwordDialog" title="修改密码" style="display: none;">
		<form method="post" id="formPwd" class="form" onsubmit="return false;">
			<table class="table">
				<tr>
					<th>原始密码</th>
					<td>
					<input id="oldPwd" name="oldPwd" type="password" class="easyui-validatebox" data-options="required:true" /></td>
				</tr>
				<tr>
					<th>新密码</th>
					<td><input id="pwd" name="userPwd" type="password" class="easyui-validatebox" data-options="required:true" /></td>
				</tr>
				<tr>
					<th>重复密码</th>
					<td><input type="password" class="easyui-validatebox" data-options="required:true,validType:'eqPwd[\'#pwd\']'" /></td>
				</tr>
			</table>
		</form>
	</div>
</body>
</html>