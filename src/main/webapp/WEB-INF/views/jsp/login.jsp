<%@ page contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>${initParam.title}</title>
<%@include file="commons/taglibs.jsp" %>
<%@include file="commons/jquery.jsp" %>
<%@include file="commons/easyUI.jsp" %>
<%@include file="commons/base.jsp" %>
<%@include file="commons/event.jsp" %>
<link rel="stylesheet" type="text/css" href="${base}/resources/css/login.css">
<script type="text/javascript" src="${base}/resources/js/common/MD5.js"></script>
<script type="text/javascript" src="${base}/resources/js/login.js"></script>
<script type="text/javascript">
var msg="${msg}";
if (window!=top){
	top.location.href =base+"/";	
}
/*
if(top!=self){
	if(top.location != self.location){
		alert('由于您长时间未操作，为了确保安全请重新登录!'); 
		top.location=self.location; 
	}
}
*/
</script>
</head>
<body>
	<div class="logintop"></div>
	<div class="loginbottom">
		<div class="loginform">
			<form action="login.shtml" method="POST">
				<div style="float:right;">
					<div class="username">
						用户名：<input type="text" name="userName" id="username"/>
						<br />
						<br />
						密&emsp;码：<input type="password"  name="userPwd" value="" id="password" />
					</div>
					<div class="password">
					</div>
				</div>
				<div style="clear:both;"></div>
				<div class="btsub">
					<input type="reset"  class="login_set" value=""/>
					<input type="image"  class="login_to" src="${base}/resources/images/login_to.png" />
				</div>
			</form>
		</div>
	</div>
</body>
</html>