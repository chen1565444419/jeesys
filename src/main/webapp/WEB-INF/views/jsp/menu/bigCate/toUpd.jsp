<%@ page contentType="text/html; charset=UTF-8"  pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
<head>
<%@include file="../../commons/taglibs.jsp" %>
<%@include file="../../commons/jquery.jsp" %>
<%@include file="../../commons/easyUI.jsp" %>
<%@include file="../../commons/baseCSS.jsp" %>
<%@include file="../../commons/base.jsp" %>
<%@include file="../../commons/event.jsp" %>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
<script type="text/javascript" src="${base}/resources/js/${paramMap.path}/toUpd.js"></script>
<script type="text/javascript">
var path="${paramMap.path}";
</script>
<style type="text/css">
.xh-table{
	margin-left: 50px;
	margin-right: 50px;
}
</style>
<title>${initParam.title}</title>
</head>
<body>
<form id="frm" method="POST">
<table class="xh-table">
	<tr>
		<td class="xh-field" colspan="2">工程分类：
			<input type="hidden" name="id" id="cateId" value="${bigCate.id}" />
			<input type="text" name="bigCateName" id="bigCateName" value="${bigCate.bigCateName}" maxlength="20" class="easyui-validatebox" data-options="required:true" />
		</td>
		<%-- <td class="xh-field">排序序号： 
			<input type="text" name="seq" value="${cate.seq}" class="easyui-numberbox" data-options="min:0" />
		</td> --%>
	</tr>
	<tr>
		<td class="xh-field" colspan="2">描述： 
			<textarea name="description" id="description" value="" maxlength="100" style="width:83%;height:50px;">${bigCate.description }</textarea>
		</td>
	</tr>
</table>
</form>
</body>
</html>