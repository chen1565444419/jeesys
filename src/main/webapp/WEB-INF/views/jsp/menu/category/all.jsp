<%@ page contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<%@include file="../../commons/taglibs.jsp" %>
<%@include file="../../commons/jquery.jsp" %>
<%@include file="../../commons/easyUI.jsp" %>
<%@include file="../../commons/baseCSS.jsp"%>
<%@include file="../../commons/base.jsp" %>
<%@include file="../../commons/event.jsp" %>
<c:set var="rootPath" value="${powers[0].masterName}" scope="page"/>
<script type="text/javascript" src="${base}/resources/js/${fn:substring(rootPath,0,fn:indexOf(rootPath,'_'))}/all.js" charset="UTF-8"></script>
<%@include file="../../commons/permission.jsp"%>
<title>${initParam.title}</title>
</head>
<body>
<table id="resultTable"></table>
<div id="searchbar" style="width:120px;white-space: nowrap;">按类型查找:
		工程分类:
		<select id="bigCateId" class="easyui-combobox" name="bigCateId" data-options="width:155,editable:false">
			<option value="0">请选择</option>
			<c:forEach items="${bigCateList}" var="b">
				<option value="${b.id}">${b.bigCateName}</option>
			</c:forEach>
		</select>
    	<a id="searchBtn" href="#" class="easyui-linkbutton" data-options="iconCls:'icon-search'">查询</a>
	</div>
</body>
</html>