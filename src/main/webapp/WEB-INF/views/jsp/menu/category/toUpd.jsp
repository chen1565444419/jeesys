<%@ page contentType="text/html; charset=UTF-8"  pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
<head>
<%@include file="../../commons/taglibs.jsp" %>
<%@include file="../../commons/jquery.jsp" %>
<%@include file="../../commons/easyUI.jsp" %>
<%@include file="../../commons/baseCSS.jsp" %>
<%@include file="../../commons/base.jsp" %>
<%@include file="../../commons/event.jsp" %>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
<script type="text/javascript" src="${base}/resources/js/${paramMap.path}/toUpd.js"></script>
<script type="text/javascript">
var path="${paramMap.path}";
</script>
<style type="text/css">
.xh-table{
	margin-left: 50px;
	margin-right: 50px;
}
</style>
<title>${initParam.title}</title>
</head>
<body>
<form id="frm" method="POST">
<table class="xh-table">
	<tr>
		<td>
			工程分类:
			<select name="bigCateId" id="bigCateId" class="easyui-combobox" style="width:155px" data-options="fit:true,required:true,editable:false,panelHeight:200">
				<c:forEach items="${bigCateList}" var="b">
					<option value="${b.id}" <c:if test="${b.id eq cate.bigCate.id}">selected="selected"</c:if>>${b.bigCateName}</option>
				</c:forEach>
			</select>
		</td>
		<td class="xh-field">类型名称：
			<input type="hidden" name="id" id="cateId" value="${cate.id}" />
			<input type="text" name="categoryName" id="categoryName" value="${cate.categoryName}" maxlength="20" class="easyui-validatebox" data-options="required:true" />
		</td>
		<%-- <td class="xh-field">排序序号： 
			<input type="text" name="seq" value="${cate.seq}" class="easyui-numberbox" data-options="min:0" />
		</td> --%>
	</tr>
	<tr>
		<td class="xh-field" colspan="2">类型描述： 
			<textarea name="description" id="description" value="" maxlength="100" style="width:83%;height:50px;">${cate.description }</textarea>
		</td>
	</tr>
</table>
</form>
</body>
</html>