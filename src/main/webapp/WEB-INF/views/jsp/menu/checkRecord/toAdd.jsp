<%@ page contentType="text/html; charset=UTF-8"  pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
<head>
<%@include file="../../commons/taglibs.jsp" %>
<%@include file="../../commons/jquery.jsp" %>
<%@include file="../../commons/calendar.jsp" %>
<%@include file="../../commons/easyUI.jsp" %>
<%@include file="../../commons/baseCSS.jsp" %>
<%@include file="../../commons/base.jsp" %>
<%@include file="../../commons/event.jsp" %>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
<script type="text/javascript">
var path="${paramMap.path}";
var deptIdVal=null;
<shiro:hasPermission name="viewOwnerDevice">
	deptIdVal=${sessionScope.USER_KEY_IN_SESSION.deptId};
</shiro:hasPermission>
</script>
<title>${initParam.title}</title>
<script type="text/javascript" src="${resources}/js/${paramMap.path}/toAdd.js"></script>
</head>
<body>
<form id="frm" method="post">
<table class="xh-table">
	<tr>
		<td class="xh-field">
			部门分类：
			<select id="bigCateId" class="easyui-combobox" name="bigCateId" data-options="width:155,panelHeight:200,editable:false,required:true">
				<c:forEach items="${bigCateList}" var="b">
					<option value="${b.id}">${b.bigCateName}</option>
				</c:forEach>
			</select>
		</td>
		<td class="xh-field">设备类型：
			<input name="categoryId" id="categoryId" class="easyui-combobox" />
			<%-- 
			<select id="categoryId" class="easyui-combobox" style="width:155px" data-options="fit:true,panelHeight:200,editable:false">
				<c:forEach items="${categoryList}" var="c">
					<option value="${c.id}">${c.categoryName}</option>
				</c:forEach>
			</select> 
			--%>		
		</td>
		
	</tr>
	<tr>
		<td class="xh-field">设备名称：
			<input type="text" name="deviceId" id="deviceId" class="easyui-validatebox" data-options="required:true" /> 
		</td>
		<td class="xh-field">校验时间： 
			<input type="text" name="checkDate" id="checkDate" readonly="readonly" onClick="WdatePicker({dateFmt:'yyyy-MM-dd',maxDate:'%y-%M-%d'})" class="xh-date"  />
		</td>
	</tr>
	<tr>
		<td class="xh-field">校验次数：
			<select id="checkCount" class="easyui-combobox" name="checkCount" style="width:155px;" data-options="panelHeight:'auto',editable:false,required:true">
			</select>
		</td>
		<td class="xh-field">校验单位：
			<input type="text" id="checkUnit" name="checkUnit" class="easyui-validatebox"  data-options="required:true" />
		</td>
	</tr>
	<tr>
		<td class="xh-field" colspan="2">检修结果：
			<textarea name="result" id="result" maxlength="200" style="width:100%;height:50px;" class="easyui-validatebox" data-options="required:true"></textarea>
		</td>
	</tr>
	<tr>
		<td class="xh-field" colspan="2">备&emsp;&emsp;注：
			<textarea name="description" style="width:100%;height:50px;"></textarea>
		</td>
	</tr>
</table>
</form>
</body>
</html>