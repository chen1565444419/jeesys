<%@ page contentType="text/html; charset=UTF-8"  pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
<head>
<%@include file="../../commons/taglibs.jsp" %>
<%@include file="../../commons/jquery.jsp" %>
<%@include file="../../commons/easyUI.jsp" %>
<%@include file="../../commons/baseCSS.jsp" %>
<%@include file="../../commons/base.jsp" %>
<%@include file="../../commons/event.jsp" %>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
<script type="text/javascript">
var path="${paramMap.path}";
</script>
<title>${initParam.title}</title>
</head>
<body>
<form id="frm" method="post">
<table class="xh-table">
	<tr>
		<td class="xh-field">
			<span class="lab">部门分类：</span>
			<span class="content">${category.bigCate.bigCateName}</span>
		</td>
		<td class="xh-field">
			<span class="lab">设备类型：</span>
			<span class="content">${category.categoryName}</span>
		</td>
		
	</tr>
	<tr>
		<td class="xh-field">
			<span class="lab">设备名称：</span>
			<span class="content">${checkRecord.device.name}</span> 
		</td>
		<td class="xh-field">
			<span class="lab">校验时间：</span>
			 <span class="content">${myFn:formatDate(checkRecord.checkDate,'yyyy-MM-dd')}</span>
		</td>
	</tr>
	<tr>
		<td class="xh-field">
			<span class="lab">校验次数：</span>
			<span class="content">第${myFn:num2Str(checkRecord.checkCount)}次</span>
		</td>
		<td class="xh-field">
			<span class="lab">校验单位：</span>
			<span class="content">${checkRecord.checkUnit}</span>
		</td>
	</tr>
	<tr>
		<td class="xh-field" colspan="2">
			<span class="lab">检修结果：</span>
			<span class="content">${checkRecord.result}</textarea>
		</td>
	</tr>
	<tr>
		<td class="xh-field" colspan="2">
			<span class="lab">备&emsp;&emsp;注：</span>
			<span class="content">${checkRecord.description}</span>
		</td>
	</tr>
	<tr>
		<td>
			<span class="lab">录入人员：</span>
			<span class="content">${checkRecord.createdUser.realName}</span>
		</td>
		<td>
			<span class="lab">录入时间：</span>
			<span class="content">${myFn:formatDate(checkRecord.createTime,'yyyy-MM-dd')}</span>
		</td>
	</tr>
</table>
</form>
</body>
</html>