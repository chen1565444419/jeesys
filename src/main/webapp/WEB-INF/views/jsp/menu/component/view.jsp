<%@ page contentType="text/html; charset=UTF-8"  pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
<head>
<%@include file="../../commons/taglibs.jsp" %>
<%@include file="../../commons/baseCSS.jsp" %>
<%@include file="../../commons/jquery.jsp" %>
<%@include file="../../commons/base.jsp" %>
<%@include file="../../commons/event.jsp" %>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>查看</title>
</head>
<body>
	<table class="xh-table">
	<tr>
		<td>
			<span class="lab">类型名称：</span>
			<span class="content">${component.name}</span>
		</td>
		<td>&nbsp;</td>
		<%-- <td>
			<span class="lab">排序序号：</span>
			<span class="content">${component.seq}</span>
		</td> --%>
	</tr>
	<tr>
		<td>
			<span class="lab">创建时间：</span>
			<span class="content">${myFn:formatDate(component.createTime,'yyyy-MM-dd HH:mm:ss')}</span>
		</td>
		<td>
			<span class="lab">类型描述：</span>
			<span class="content">${component.description}</span>
		</td>
	</tr>
</table>
</body>
</html>