<%@ page contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<%@include file="../../commons/taglibs.jsp" %>
<%@include file="../../commons/jquery.jsp" %>
<%@include file="../../commons/easyUI.jsp" %>
<%@include file="../../commons/baseCSS.jsp"%>
<%@include file="../../commons/base.jsp" %>
<%@include file="../../commons/event.jsp" %>
<script type="text/javascript" src="${resources}/js/data/all.js"></script>
<title>${initParam.title}</title>
</head>
<body>
<p style="color:#FF0000;font-weight: bolder;font-size: 20px;">注意:数据安全非常重要，非管理员请勿操作！</p>
<p align="left">
	<span id="result"></span><br /><br />
	<a href="#" id="dump" class="easyui-linkbutton" data-options="iconCls:'icon-save'">备份</a>
	<a href="#" id="imp" class="easyui-linkbutton" data-options="iconCls:'icon-redo'">恢复</a>
</p>
<div id="formDiv" style="display: none;">
	<form id="form" action="${base}/data/imp${initParam.suffix}" method="post" enctype="multipart/form-data">
		<input type="file" name="file" />
		<input type="button" id="subBtn" value="提交" />
	</form>
</div>
</body>
</html>