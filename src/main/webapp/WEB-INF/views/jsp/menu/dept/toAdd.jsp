<%@ page contentType="text/html; charset=UTF-8"  pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
<head>
<%@include file="../../commons/taglibs.jsp" %>
<%@include file="../../commons/jquery.jsp" %>
<%@include file="../../commons/easyUI.jsp" %>
<%@include file="../../commons/baseCSS.jsp" %>
<%@include file="../../commons/base.jsp" %>
<%@include file="../../commons/event.jsp" %>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<script type="text/javascript" src="${base}/resources/js/dept/toAdd.js"></script>
<title>添加部门</title>
</head>
<body>
<form id="frm" method="POST">
	<table class="xh-table">
		<tr>
			<td class="xh-field">部门名称：</td>
			<td width="220px"><input type="text" name="deptName" id="deptName" class="easyui-validatebox" data-options="required:true" /></td>
			
			<!-- 
			<td class="xh-field">排序序号：</td>
			<td><input type="text" name="seq" value="0" id="seq" class="easyui-numberbox" data-options="min:0" /></td> 
			-->
		</tr>
	</table>
</form>
</body>
</html>