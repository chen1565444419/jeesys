<%@ page contentType="text/html; charset=UTF-8"  pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
<head>
<%@include file="../../commons/taglibs.jsp" %>
<%@include file="../../commons/jquery.jsp" %>
<%@include file="../../commons/easyUI.jsp" %>
<%@include file="../../commons/baseCSS.jsp" %>
<%@include file="../../commons/base.jsp" %>
<%@include file="../../commons/event.jsp" %>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>查看部门</title>
</head>
<body>
	<table class="xh-table">
		<tr>
			<td>
				<span class="lab">部门名称：</span>
				<span class="content">${dept.deptName}</span>
			</td>
			<td>&nbsp;</td>
			<%-- <td>
				<span class="lab">排序序号：</span>
				<span class="content">${dept.seq}</span>	
			</td> --%>
		</tr>
		<tr>
			<td colspan="2">
				<span class="lab">创建时间：</span>
				<span class="content">${myFn:formatDate(dept.createTime,'yyyy-MM-dd hh:mm:ss')}</span>
			</td>
		</tr>
	</table>
</body>
</html>