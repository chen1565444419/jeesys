﻿<%@ page contentType="text/html; charset=UTF-8"  pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
<head>
<%@include file="../../commons/taglibs.jsp" %>
<%@include file="../../commons/jquery.jsp" %>
<%@include file="../../commons/easyUI.jsp" %>
<%@include file="../../commons/baseCSS.jsp" %>
<%@include file="../../commons/base.jsp" %>
<%@include file="../../commons/event.jsp" %>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
<script type="text/javascript" src="${base}/resources/js/${paramMap.path}/toAdd.js"></script>
<script type="text/javascript">
var path="${paramMap.path}";
var deptIdVal=null;
<shiro:hasPermission name="viewOwnerDevice">
	deptIdVal=${sessionScope.USER_KEY_IN_SESSION.deptId};
</shiro:hasPermission>
</script>
<title>${initParam.title}</title>
</head>
<body>
<form id="frm" method="POST">
<table class="xh-table">
	<tr>
		<td class="xh-field">
			工程分类：
			<select id="bigCateId" class="easyui-combobox" name="bigCateId" data-options="width:155,panelHeight:200,editable:false,required:true">
				<c:forEach items="${bigCateList}" var="b">
					<option value="${b.id}">${b.bigCateName}</option>
				</c:forEach>
			</select>
		</td>
		<td class="xh-field">设备名称：
			<input name="categoryId" id="categoryId" class="easyui-combobox" />  
			<%-- <select name="categoryId" id="categoryId" class="easyui-combobox" style="width:155px" data-options="fit:true,panelHeight:200,editable:false">
				<c:forEach items="${categoryList}" var="c">
					<option value="${c.id}">${c.categoryName}</option>
				</c:forEach>
			</select> --%>
		</td>
		<!-- <td class="xh-field">排序序号:
			<input type="text" name="seq" value="0" class="easyui-numberbox" data-options="min:0" />
		</td> -->
	</tr>
	<tr>
		<td class="xh-field" colspan="2">检修项目：
			<textarea name="name" id="name" maxlength="200" style="width:100%;height:50px;" class="easyui-validatebox" data-options="required:true"></textarea>
		</td>
	</tr>
	<tr>
		<td class="xh-field" colspan="2">标&nbsp;准&nbsp;值： 
			<textarea name="standard" id="standard" value="" maxlength="200" style="width:100%;height:50px;"></textarea>
		</td>
	</tr>
	<tr>
		<td class="xh-field" colspan="2">注意事项： 
			<textarea name="notice" id="notice" value="" maxlength="200" style="width:100%;height:50px;"></textarea>
		</td>
	</tr>
</table>
</form>
</body>
</html>