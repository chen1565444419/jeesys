<%@ page contentType="text/html; charset=UTF-8"  pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
<head>
<%@include file="../../commons/taglibs.jsp" %>
<%@include file="../../commons/baseCSS.jsp" %>
<%@include file="../../commons/jquery.jsp" %>
<%@include file="../../commons/base.jsp" %>
<%@include file="../../commons/event.jsp" %>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>查看</title>
</head>
<body>
	<table class="xh-table">
	<tr>
		<td>
			<span class="lab">工程分类：</span>
			<span class="content">${detectItem.category.bigCate.bigCateName}</span>
		</td>
		<td>
			<span class="lab">设备类型：</span>
			<span class="content">${detectItem.category.categoryName}</span>	
		</td>
		<%-- <td class="xh-field">
			<span class="lab">排序序号：</span>
			<span class="content">${detectItem.seq}</span>
		</td> --%>
	</tr>
	<tr>
		<td class="xh-field" colspan="2">
			<span class="lab">检修项目：</span><br />
			<span class="content">${detectItem.name}</span>
		</td>
	</tr>
	<tr>
		<td colspan="2">
			<span class="lab">标准值：</span><br />
			<span class="content">${detectItem.standard}</span>
		</td>
	</tr>
	<tr>
		<td colspan="2">
			<span class="lab">注意事项：</span><br />
			<span class="content">${detectItem.notice}</span>
		</td>
	</tr>
</table>
</body>
</html>