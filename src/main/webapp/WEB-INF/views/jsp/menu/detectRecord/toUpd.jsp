<%@ page contentType="text/html; charset=UTF-8"  pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
<head>
<%@include file="../../commons/taglibs.jsp" %>
<%@include file="../../commons/jquery.jsp" %>
<%@include file="../../commons/calendar.jsp" %>
<%@include file="../../commons/easyUI.jsp" %>
<%@include file="../../commons/baseCSS.jsp" %>
<%@include file="../../commons/base.jsp" %>
<%@include file="../../commons/event.jsp" %>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
<script type="text/javascript">
var path="${paramMap.path}";
var deptIdVal="${detectRecord.device.deptId}";
</script>
<title>${initParam.title}</title>
<script type="text/javascript" src="${resources}/js/${paramMap.path}/toUpd.js"></script>
</head>
<body>
<form id="frm" method="post">
<table class="xh-table">
	<tr>
		<td class="xh-field">
			工程分类：
			<select id="bigCateId" class="easyui-combobox" name="bigCateId" data-options="width:155,panelHeight:200,editable:false,required:true,disabled:true">
				<c:forEach items="${bigCateList}" var="b">
					<option value="${b.id}" <c:if test="${b.id eq detectRecord.device.category.bigCateId}">selected="selected"</c:if>>${b.bigCateName}</option>
				</c:forEach>
			</select>
		</td>
		<td class="xh-field">设备类型：
			<input type="text" id="categoryId" name="categoryId" value="${detectRecord.device.categoryId}" />
			<input type="hidden" name="id" value="${detectRecord.id}"/>
			<%--
				<select id="categoryId" style="width:155px" data-options="fit:true,panelHeight:'auto',editable:false">
					<c:forEach items="${categoryList}" var="c">
						<option value="${c.id}" <c:if test="${c.id==detectRecord.device.categoryId}">selected="selected"</c:if>>${c.categoryName}</option>
					</c:forEach>
				</select>		
			 --%>
		</td>
	</tr>
	<tr>
		<td class="xh-field">设备名称：
			<input type="text" name="deviceId" id="deviceId" value="${detectRecord.deviceId}" class="easyui-validatebox" data-options="required:true,disabled:true" /> 
		</td>
		<td class="xh-field">检修时间： 
			<input type="text" name="detectDate" value="${myFn:formatDate(detectRecord.detectDate,'yyyy-MM-dd')}" id="detectDate" readonly="readonly" onClick="WdatePicker({dateFmt:'yyyy-MM-dd',maxDate:'%y-%M-%d'})" class="xh-date"  />
		</td>
	</tr>
	<tr>
		<td class="xh-field">检修次数：
			<select id="count" class="easyui-combobox" name="count" style="width:155px;" data-options="panelHeight:200,editable:false,required:true">
				<option value="${detectRecord.count}">第${myFn:num2Str(detectRecord.count)}次</option>
			</select>
		</td>
		<td class="xh-field">检修人员：
		<input type="text" id="detectUserName" name="detectUserName" value="${detectRecord.detectUserName}" class="easyui-validatebox"  data-options="required:true" />
			<%--
			<input type="text" name="detectId" id="detectId" value="${detectRecord.detectId}" class="easyui-validatebox" data-options="required:true" />
			<select id="detectId" class="easyui-combobox" name="detectId">
				<c:forEach items="${userList}" var="u">
		    		<option value="${u.id}" <c:if test="${u.id eq detectRecord.detectId}"></c:if>>${u.realName}</option>   
				</c:forEach>   
			</select>
			 --%>
		</td>
	</tr>
	<tr>
		<td class="xh-field" colspan="2">检修项目：
			<input type="text" name="detectItemId" value="${detectRecord.detectItemId}" id="detectItemId" style="width:400px;" class="easyui-validatebox" data-options="required:true"/> 
		</td>
	</tr>
	<tr>
		<td class="xh-field" colspan="2">检修结果：
			<textarea name="result" id="result" maxlength="200" style="width:100%;height:50px;" class="easyui-validatebox" data-options="required:true">${detectRecord.result}</textarea>
		</td>
	</tr>
	<tr>
		<td class="xh-field" colspan="2">备&emsp;&emsp;注：
			<textarea name="description" style="width:100%;height:50px;">${detectRecord.description}</textarea>
		</td>
	</tr>
</table>
</form>
</body>
</html>