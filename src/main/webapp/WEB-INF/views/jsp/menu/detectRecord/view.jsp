<%@ page contentType="text/html; charset=UTF-8"  pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
<head>
<%@include file="../../commons/taglibs.jsp" %>
<%@include file="../../commons/jquery.jsp" %>
<%@include file="../../commons/baseCSS.jsp" %>
<%@include file="../../commons/base.jsp" %>
<%@include file="../../commons/event.jsp" %>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
<script type="text/javascript">
var path="${paramMap.path}";
</script>
<title>${initParam.title}</title>
</head>
<body>
<form id="frm" method="post">
<table class="xh-table">
	<tr>
		<td>
			<span class="lab">工程分类：</span> 
			<span class="content">${category.bigCate.bigCateName}</span>
		</td>
		<td>
			<span class="lab">设备类型：</span>
			<span class="content">${category.categoryName}</span>
		</td>
		
	</tr>
	<tr>
		<td >
			<span class="lab">设备名称：</span>
			<span class="content">${detectRecord.device.name}</span>
		</td>
		<td>
			<span class="lab">检修时间：</span> 
			<span class="content">${myFn:formatDate(detectRecord.detectDate,'yyyy-MM-dd')}</span>
		</td>
	</tr>
	<tr>
		<td >
			<span class="lab">检修次数：</span>
			<span class="content">第${myFn:num2Str(detectRecord.count)}次</span>
		</td>
		<td>
			<span class="lab">检修人员：</span> 
			<span class="content">${detectRecord.detectUserName}</span>
		</td>
	</tr>
	<tr>
		<td colspan="2">
			<span class="lab">检修项目：</span>
			<span class="content">${detectRecord.detectItem.name}</span>
		</td>
	</tr>
	<tr>
		<td colspan="2">
			<span class="lab">检修结果：</span>
			<span class="content">${detectRecord.result}</span>
		</td>
	</tr>
	<tr>
		<td colspan="2">
			<span class="lab">备&emsp;&emsp;注：</span>
			<span class="content">${detectRecord.description}</span>
		</td>
	</tr>
	<tr>
		<td>
			<span class="lab">录入人员：</span>
			<span class="content">${detectRecord.createdUser.realName}</span>
		</td>
		<td>
			<span class="lab">录入时间：</span>
			<span class="content">${myFn:formatDate(detectRecord.createTime,'yyyy-MM-dd')}</span>
		</td>
	</tr>
</table>
</form>
</body>
</html>