<%@ page contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<%@include file="../../commons/taglibs.jsp" %>
<%@include file="../../commons/jquery.jsp" %>
<%@include file="../../commons/easyUI.jsp" %>
<%@include file="../../commons/baseCSS.jsp"%>
<%@include file="../../commons/base.jsp" %>
<%@include file="../../commons/event.jsp" %>
<c:set var="rootPath" value="${powers[0].masterName}" scope="page"/>
<script type="text/javascript" src="${base}/resources/js/${fn:substring(rootPath,0,fn:indexOf(rootPath,'_'))}/all.js" charset="UTF-8"></script>
<%@include file="../../commons/permission.jsp"%>
<title>${initParam.title}</title>
</head>
<body>
<table id="resultTable"></table>
	<div id="searchbar" style="width:120px;white-space: nowrap;">按类型查找:
		工程分类:<input name="bigCateId" id="bigCateId" style="width:155px;" data-options="required:true"/>
		&emsp;&emsp;
    	类型:<input name="categoryId" id="categoryId" style="width:155px;" />
    	<a id="searchBtn" href="#" class="easyui-linkbutton" data-options="iconCls:'icon-search'">查询</a>
	</div>
</body>
</html>