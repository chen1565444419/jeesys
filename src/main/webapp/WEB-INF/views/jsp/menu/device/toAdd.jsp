<%@ page contentType="text/html; charset=UTF-8"  pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
<head>
<%@include file="../../commons/taglibs.jsp" %>
<%@include file="../../commons/jquery.jsp" %>
<%@include file="../../commons/easyUI.jsp" %>
<%@include file="../../commons/calendar.jsp" %>
<%@include file="../../commons/json2.jsp" %>
<%@include file="../../commons/baseCSS.jsp" %>
<%@include file="../../commons/base.jsp" %>
<%@include file="../../commons/event.jsp" %>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
<script type="text/javascript" src="${base}/resources/js/${paramMap.path}/toAdd.js"></script>
<script type="text/javascript">
var path="${paramMap.path}";
</script>
<style type="text/css">
.xh-table{
	border-collapse:collapse;
	border-spacing:30px;
	width:95%;
	height: 100%;
	margin-left: 10px;
}
#frm table,#frm th,#frm td {
	text-align: left;
	padding:0px;
}
</style>
<title>${initParam.title}</title>
</head>
<body>
<form id="frm" method="POST">
<table class="xh-table" >
	<tr>
		<td class="xh-field">名&emsp;&emsp;称：
			<input type="text" name="name" id="name" maxlength="10" class="easyui-validatebox" data-options="required:true" />
		</td>
		<td class="xh-field">编&emsp;&emsp;号：
			<input type="text" name="no" id="no" class="easyui-validatebox" data-options="required:true" />
		</td>
	</tr>
	<tr>
		<td class="xh-field" colspan="2">
			工程所属部门分类：
			<select id="bigCateId" class="easyui-combobox" name="bigCateId" data-options="width:155,panelHeight:200,editable:false,required:true">
				<c:forEach items="${bigCateList}" var="b">
					<option value="${b.id}">${b.bigCateName}</option>
				</c:forEach>
			</select>
		</td>
	</tr>
	<tr>
		<td class="xh-field">类&emsp;&emsp;型：
		<input name="categoryId" id="categoryId" class="easyui-combobox" /> 
			<%-- 
			<select name="categoryId" id="categoryId" class="easyui-combobox" style="width:155px" data-options="fit:true,required:true,panelHeight:200">
				<c:forEach items="${categoryList}" var="c">
					<option value="${c.id}">${c.categoryName}</option>
				</c:forEach>
			</select> 
			--%>
		</td>
		<td class="xh-field">工厂所属部门：
			<select name="deptId" id="deptId" class="easyui-combobox" style="width:155px" data-options="fit:true,panelHeight:'auto',editable:false,required:true">
				<c:forEach items="${deptList}" var="d">
					<option value="${d.id}" <c:if test="${d.id eq sessionScope.USER_KEY_IN_SESSION.deptId}">selected="selected"</c:if>>${d.deptName}</option>
				</c:forEach>
			</select>
		</td>
	</tr>
	<tr>
		<td class="xh-field">检修周期：
			<%@include file="type.jsp"%>
		</td>
		<td class="xh-field">开始时间：
			<input type="text" name="startDate" id="startDate" readonly="readonly" onClick="WdatePicker({dateFmt:'yyyy-MM-dd'})" class="xh-date"  />
<!-- 			<input type="text" name="startDate" id="startDate" readonly="readonly" onClick="WdatePicker({dateFmt:'yyyy-MM-dd',minDate:'%y-%M-{%d+1}'})" class="xh-date"  /> -->
		</td>
	</tr>
	<tr>
		<td class="xh-field">生产厂家：
			<input type="text" name="producer" id="producer" maxlength="20"/>
		</td>
		<td class="xh-field">型&emsp;&emsp;号：
			<input type="text" name="modelNumber" id="modelNumber" maxlength="20"/>
		</td>
	</tr>
	<tr>
		<td class="xh-field">使用地点： 
			<input type="text" name="usedSite" id="usedSite" maxlength="20"/>
		</td>
		<td class="xh-field">线&emsp;&emsp;别： 
			<input type="text" name="lineType" id="lineType" maxlength="20" />
		</td>
	</tr>
	<tr>
		<td class="xh-field">校验周期：
			<%@include file="checkType.jsp"%>
		</td>
		<td class="xh-field">描&emsp;&emsp;述： 
			<input type="text" name="description" maxlength="100"/>
		</td>
	</tr>
	<tr id="compTr">
		<td class="xh-field">定期替换备件:
			<input type="radio" name="component" value="1"/>是
			<input type="radio" name="component" value="0" checked="checked"/>否
			<input type="hidden" name="compentIds" id="compId" value="" />
			<input type="hidden" name="oilIds" id="oilId" value="" />
		</td>
		<td class="xh-field">定期润滑:
			<input type="radio" name="oil" value="1"/>是
			<input type="radio" name="oil" value="0" checked="checked"/>否
		</td>
	</tr>
	<tr id="dataTr">
		<td class="xh-field" valign="top">
			<div id="compDiv" style="display: none;">
				<table id="compgrid"></table>
				<div id="compTb" style="height:auto">
					<a href="javascript:void(0)" class="easyui-linkbutton" data-options="iconCls:'icon-add',plain:true" onclick="appendComp();">添加</a>
					<a href="javascript:void(0)" class="easyui-linkbutton" data-options="iconCls:'icon-remove',plain:true" onclick="removeComp();">删除</a>
					<a href="javascript:void(0)" class="easyui-linkbutton" data-options="iconCls:'icon-save',plain:true" onclick="compAccept();">保存</a>
				</div>
			</div>
		</td>
		<td class="xh-field" valign="top">
			<div id="oilDiv" style="display: none;">
				<table id="oilgrid"></table>
				<div id="oilTb" style="height:auto">
					<a href="javascript:void(0)" class="easyui-linkbutton" data-options="iconCls:'icon-add',plain:true" onclick="appendOil();">添加</a>
					<a href="javascript:void(0)" class="easyui-linkbutton" data-options="iconCls:'icon-remove',plain:true" onclick="removeOil();">删除</a>
					<a href="javascript:void(0)" class="easyui-linkbutton" data-options="iconCls:'icon-save',plain:true" onclick="oilAccept();">保存</a>
				</div>
			</div>
		</td>
	</tr>
</table>
</form>
</body>
</html>