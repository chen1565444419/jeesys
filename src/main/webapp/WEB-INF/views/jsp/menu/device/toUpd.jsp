<%@ page contentType="text/html; charset=UTF-8"  pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
<head>
<%@include file="../../commons/taglibs.jsp" %>
<%@include file="../../commons/jquery.jsp" %>
<%@include file="../../commons/easyUI.jsp" %>
<%@include file="../../commons/calendar.jsp" %>
<%@include file="../../commons/json2.jsp" %>
<%@include file="../../commons/baseCSS.jsp" %>
<%@include file="../../commons/base.jsp" %>
<%@include file="../../commons/event.jsp" %>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
<script type="text/javascript" src="${base}/resources/js/${paramMap.path}/toUpd.js"></script>
<script type="text/javascript">
var path="${paramMap.path}";
var compData=[];
var oilData=[];

<c:if test="${device.components!=null}">
	compData=${myFn:toJson(device.components,'yyyy-MM-dd')};
	$("#compgrid").datagrid("loadData",compData);
</c:if>
<c:if test="${device.oils!=null}">
	oilData=${myFn:toJson(device.oils,'yyyy-MM-dd')};
	$("#oilgrid").datagrid("loadData",oilData);
</c:if>

</script>
<style type="text/css">
.xh-table{
	border-collapse:collapse;
	border-spacing:30px;
	width:95%;
	height: 100%;
	margin-left: 10px;
}
#frm table,#frm th,#frm td {
	text-align: left;
	padding:0px;
}
</style>
<title>${initParam.title}</title>
</head>
<body>
<form id="frm" method="POST">
<table class="xh-table" >
	<tr>
		<td class="xh-field">名&emsp;&emsp;称：
			<input type="hidden" name="id" value="${device.id}" />
			<input type="text" name="name" id="name" value="${device.name}" maxlength="10" class="easyui-validatebox" data-options="required:true" />
		</td>
		<td class="xh-field">编&emsp;&emsp;号：
			<input type="text" name="no" id="no" value="${device.seq}" class="easyui-validatebox" data-options="required:true" />
		</td>
	</tr>
	<tr>
		<td class="xh-field" colspan="2">
			工程分类：
			<select id="bigCateId" class="easyui-combobox" name="bigCateId" data-options="width:155,panelHeight:200,editable:false,required:true">
				<c:forEach items="${bigCateList}" var="b">
					<option value="${b.id}" <c:if test="${b.id eq device.category.bigCate.id}">selected="selected"</c:if>>${b.bigCateName}</option>
				</c:forEach>
			</select>
		</td>
	</tr>
	<tr>
		<td class="xh-field">类&emsp;&emsp;型：
		<input name="categoryId" id="categoryId" value="${device.categoryId}" class="easyui-combobox" /> 
			<%-- 
			<select name="categoryId" id="categoryId" class="easyui-combobox" style="width:155px" data-options="fit:true,panelHeight:200">
				<c:forEach items="${categoryList}" var="c">
					<option value="${c.id}" <c:if test="${c.id eq device.categoryId}">selected="selected"</c:if>>${c.categoryName}</option>
				</c:forEach>
			</select>
			 --%>
		</td>
		<td class="xh-field">所属部门：
			<select name="deptId" id="deptId" class="easyui-combobox" style="width:155px" data-options="fit:true,panelHeight:'auto',editable:false,required:true">
				<c:forEach items="${deptList}" var="d">
					<option value="${d.id}" <c:if test="${d.id eq device.deptId}">selected="selected"</c:if>>${d.deptName}</option>
				</c:forEach>
			</select>
		</td>
	</tr>
	<tr>
		<td class="xh-field">检修周期：
			<%@include file="type.jsp"%>
		</td>
		<td class="xh-field">开始时间：
			<input type="text" name="startDate" <c:if test="${device.type==6&&device.checkType==6}">disabled="disabled"</c:if> value="${myFn:formatDate(device.startDate,'yyyy-MM-dd')}" id="startDate" readonly="readonly" onClick="WdatePicker({dateFmt:'yyyy-MM-dd'})" class="xh-date"  />
<%-- 			<input type="text" name="startDate" <c:if test="${device.type==6&&device.checkType==6}">disabled="disabled"</c:if> value="${myFn:formatDate(device.startDate,'yyyy-MM-dd')}" id="startDate" readonly="readonly" onClick="WdatePicker({dateFmt:'yyyy-MM-dd',minDate:'%y-%M-{%d+1}'})" class="xh-date"  /> --%>
		</td>
	</tr>
	<tr>
		<td class="xh-field">生产厂家：
			<input type="text" name="producer" id="producer" value="${device.producer}" maxlength="20"/>
		</td>
		<td class="xh-field">型&emsp;&emsp;号：
			<input type="text" name="modelNumber" id="modelNumber" value="${device.modelNumber}" maxlength="20"/>
		</td>
	</tr>
	<tr>
		<td class="xh-field">使用地点： 
			<input type="text" name="usedSite" id="usedSite" value="${device.usedSite}" maxlength="20"/>
		</td>
		<td class="xh-field">线&emsp;&emsp;别： 
			<input type="text" name="lineType" id="lineType" value="${device.lineType}" maxlength="20" />
		</td>
	</tr>
	<tr>
		<td class="xh-field">校验周期：
			<%@include file="checkType.jsp"%>
		</td>
		<td class="xh-field">描&emsp;&emsp;述： 
			<input type="text" name="description" value="${device.description}" maxlength="100"/>
		</td>
	</tr>
	<tr id="compTr">
		<td class="xh-field">定期替换备件:
			<input type="radio" name="component" value="1" <c:if test="${fn:length(device.components)>0}">checked="checked"</c:if>/>是
			<input type="radio" name="component" value="0" <c:if test="${fn:length(device.components)==0}">checked="checked"</c:if>/>否
		</td>
		<td class="xh-field">定期润滑:
			<input type="radio" name="oil" value="1" <c:if test="${fn:length(device.oils)>0}">checked="checked"</c:if>/>是
			<input type="radio" name="oil" value="0" <c:if test="${fn:length(device.oils)==0}">checked="checked"</c:if>/>否
		</td>
	</tr>
	<tr id="dataTr">
		<td class="xh-field" valign="top">
			<div id="compDiv" <c:if test="${fn:length(device.components)==0}">style="display: none;"</c:if>>
				<table id="compgrid" style="height:200px;"></table>
				<div id="compTb" style="height:auto">
					<a href="javascript:void(0)" class="easyui-linkbutton" data-options="iconCls:'icon-add',plain:true" onclick="appendComp();">添加</a>
					<a href="javascript:void(0)" class="easyui-linkbutton" data-options="iconCls:'icon-remove',plain:true" onclick="removeComp();">删除</a>
					<a href="javascript:void(0)" class="easyui-linkbutton" data-options="iconCls:'icon-save',plain:true" onclick="compAccept();">保存</a>
				</div>
			</div>
		</td>
		<td class="xh-field" valign="top">
			<div id="oilDiv" <c:if test="${fn:length(device.oils)==0}">style="display: none;"</c:if>>
				<table id="oilgrid" style="height:200px;"></table>
				<div id="oilTb" style="height:auto">
					<a href="javascript:void(0)" class="easyui-linkbutton" data-options="iconCls:'icon-add',plain:true" onclick="appendOil();">添加</a>
					<a href="javascript:void(0)" class="easyui-linkbutton" data-options="iconCls:'icon-remove',plain:true" onclick="removeOil();">删除</a>
					<a href="javascript:void(0)" class="easyui-linkbutton" data-options="iconCls:'icon-save',plain:true" onclick="oilAccept();">保存</a>
				</div>
			</div>
		</td>
	</tr>
</table>
</form>
</body>
</html>