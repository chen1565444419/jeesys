<%@ page contentType="text/html; charset=UTF-8"%>
<c:set var="selected" value="1" />
<c:if test="${device!=null}">
	<c:set var="selected" value="${device.type}" />
</c:if>
<select name="type" id="type" class="easyui-combobox" style="width:155px" data-options="fit:true,panelHeight:'auto'">
	<option value="1" <c:if test="${selected eq 1}">selected="selected"</c:if>>日检修</option>
	<option value="2" <c:if test="${selected eq 2}">selected="selected"</c:if>>周检修</option>
	<option value="3" <c:if test="${selected eq 3}">selected="selected"</c:if>>月检修</option>
	<option value="4" <c:if test="${selected eq 4}">selected="selected"</c:if>>季检修</option>
	<option value="5" <c:if test="${selected eq 5}">selected="selected"</c:if>>年检修</option>
	<option value="6" <c:if test="${selected eq 6}">selected="selected"</c:if>>其它</option>	
</select>