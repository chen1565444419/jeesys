<%@ page contentType="text/html; charset=UTF-8"  pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
<head>
<%@include file="../../commons/taglibs.jsp" %>
<%@include file="../../commons/jquery.jsp" %>
<%@include file="../../commons/easyUI.jsp" %>
<%@include file="../../commons/baseCSS.jsp" %>
<%@include file="../../commons/base.jsp" %>
<%@include file="../../commons/event.jsp" %>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
<script type="text/javascript" src="${base}/resources/js/device/view.js"></script>
<script type="text/javascript">
var path="${paramMap.path}";
var compData=[];
var oilData=[];

<c:if test="${device.components!=null}">
	compData=${myFn:toJson(device.components,'yyyy-MM-dd')};
</c:if>
<c:if test="${device.oils!=null}">
	oilData=${myFn:toJson(device.oils,'yyyy-MM-dd')};
</c:if>

</script>
<title>${initParam.title}</title>
</head>
<body>
<table class="xh-table" >
	<tr>
		<td>
			<span class="lab">名&emsp;&emsp;称：</span>
			<span class="content">${device.name}</span>
		</td>
		<td>
			<span class="lab">编&emsp;&emsp;号：</span>
			<span class="content">${device.seq}</span>
		</td>
	</tr>
	<tr>
		<td>
			<span class="lab">类&emsp;&emsp;型：</span>
			<span class="content">${device.category.categoryName}
		</td>
		<td>
			<span class="lab">所属部门：</span>
			<span class="content">${device.dept.deptName}</span>
		</td>
	</tr>
	<tr>
		<td>
			<span class="lab">检修周期：</span>
			<span class="content">
				<span id="typeSpan"></span>
				<div style="width:0;height:0px;display: none;">
				<%@include file="type.jsp"%>
				</div>
			</span>
		</td>
		<td>
			<span class="lab">开始时间：</span>
			<span class="content">
				<c:if test="${device.type!=6||device.checkType!=6}">${myFn:formatDate(device.startDate,'yyyy-MM-dd')}</c:if>
			</span>
		</td>
	</tr>
	<tr>
		<td>
			<span class="lab">生产厂家：</span>
			<span class="content">${device.producer}</span>
		</td>
		<td>
			<span class="lab">型&emsp;&emsp;号：</span>
			<span class="content">${device.modelNumber}</span>
		</td>
	</tr>
	<tr>
		<td>
			<span class="lab">使用地点：</span> 
			<span class="content">${device.usedSite}</span>
		</td>
		<td>
			<span class="lab">线&emsp;&emsp;别：</span> 
			<span class="content">${device.lineType}</span>
		</td>
	</tr>
	<tr>
		<td>
			<span class="lab">校验周期：</span>
			<span class="content">
				<span id="checkTypeSpan"></span>
				<div style="width:0;height:0px;display: none;">
				<%@include file="checkType.jsp"%>
				</div>
			</span>
		</td>
		<td>
			<span class="lab">描&emsp;&emsp;述：</span> 
			<span class="content">${device.description}</span>
		</td>
	</tr>
	<tr>
		<td>
			<span class="lab">定期替换备件：</span>
			<span class="content">
				<c:if test="${fn:length(device.components)>0}">是</c:if>
				<c:if test="${fn:length(device.components)==0}">否</c:if>
			</span>
		</td>
		<td>
			<span class="lab">定期润滑：</span>
			<span class="content">
				<c:if test="${fn:length(device.oils)>0}">是</c:if>
				<c:if test="${fn:length(device.oils)==0}">否</c:if>
			</span>
		</td>
	</tr>
	<tr>
		<td>
			<div id="compDiv" <c:if test="${fn:length(device.components)==0}">style="display: none;"</c:if>>
				<table id="compgrid"></table>
			</div>
		</td>
		<td valign="top">
			<div id="oilDiv" <c:if test="${fn:length(device.oils)==0}">style="display: none;"</c:if>>
				<table id="oilgrid"></table>
			</div>
		</td>
	</tr>
</table>
</body>
</html>