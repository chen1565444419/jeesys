<%@ page contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<%@include file="../../commons/taglibs.jsp" %>
<%@include file="../../commons/jquery.jsp" %>
<%@include file="../../commons/editor.jsp" %>
<%@include file="../../commons/easyUI.jsp" %>
<%@include file="../../commons/baseCSS.jsp"%>
<%@include file="../../commons/base.jsp" %>
<%@include file="../../commons/event.jsp" %>
<script type="text/javascript" src="${resources}/js/help/all.js"></script>
<title>${initParam.title}</title>
</head>
<body>
<p align="right">
	<a href="#" id="edit" class="easyui-linkbutton" data-options="iconCls:'icon-edit'" onclick="javascript:edit();">修改</a>
	<a href="#" id="save" class="easyui-linkbutton" data-options="iconCls:'icon-save'" onclick="javascript:save();">保存</a>
	<a href="#" id="cancel" class="easyui-linkbutton" data-options="iconCls:'icon-cancel'" onclick="javascript:cancel();">取消</a>
</p>
<div id="content_div">${info.content}</div>
<div id="content_form">
	<form id="info" name="info">
		<input type="hidden" name="id" value="${info.id}"/>
		<textarea name="content" id="content">${info.content}</textarea>
	</form>
</div>
</body>
</html>