<%@ page contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<%@include file="../../commons/taglibs.jsp" %>
<title>${initParam.title}</title>
</head>
<body>
<div id="content_div">${info.content}</div>
</body>
</html>