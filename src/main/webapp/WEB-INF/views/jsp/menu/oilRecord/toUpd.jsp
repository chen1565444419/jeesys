<%@ page contentType="text/html; charset=UTF-8"  pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
<head>
<%@include file="../../commons/taglibs.jsp" %>
<%@include file="../../commons/jquery.jsp" %>
<%@include file="../../commons/calendar.jsp" %>
<%@include file="../../commons/easyUI.jsp" %>
<%@include file="../../commons/baseCSS.jsp" %>
<%@include file="../../commons/base.jsp" %>
<%@include file="../../commons/event.jsp" %>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
<script type="text/javascript">
var path="${paramMap.path}";
var deptIdVal="${oilRecord.device.deptId}";
var deviceIdVal="${oilRecord.device.id}";
</script>
<title>${initParam.title}</title>
<script type="text/javascript" src="${resources}/js/${paramMap.path}/toUpd.js"></script>
</head>
<body>
<form id="frm" method="post">
<table class="xh-table">
	<tr>
		<td class="xh-field">
			工程分类：
			<select id="bigCateId" class="easyui-combobox" name="bigCateId" data-options="width:155,panelHeight:200,editable:false,required:true,disabled:true">
				<c:forEach items="${bigCateList}" var="b">
					<option value="${b.id}" <c:if test="${b.id eq oilRecord.device.category.bigCateId}">selected="selected"</c:if>>${b.bigCateName}</option>
				</c:forEach>
			</select>
		</td>
		<td class="xh-field">设备类型：
			<input type="text" id="categoryId" name="categoryId" value="${oilRecord.device.categoryId}" />
			<input type="hidden" name="id" value="${oilRecord.id}"/>	
		</td>
	</tr>
	<tr>
		<td class="xh-field">设备名称：
			<input type="text" name="deviceId" id="deviceId" value="${oilRecord.device.id}" class="easyui-validatebox" data-options="required:true" /> 
		</td>
		<td class="xh-field">润滑时间： 
			<input type="text" name="oiledDate" id="oiledDate" value="${myFn:formatDate(oilRecord.oiledDate,'yyyy-MM-dd')}" readonly="readonly" onClick="WdatePicker({dateFmt:'yyyy-MM-dd',maxDate:'%y-%M-%d'})" class="xh-date"  />
		</td>
	</tr>
	<tr>
		<td class="xh-field">润滑次数：
			<select id="count" class="easyui-combobox" name="count" style="width:155px;" data-options="disabled:true,panelHeight:200,editable:false,required:true">
				<option value="${oilRecord.count}">第${myFn:num2Str(oilRecord.count)}次</option>
			</select>
		</td>
		<td class="xh-field">费&emsp;&emsp;用：
			<input type="text" name="cost" id="cost" value="${oilRecord.cost}" class="easyui-validatebox easyui-numberbox" data-options="required:true,min:0,precision:2" />
		</td>
	</tr>
	<tr>
		<td class="xh-field">
			润滑项目：
			<input type="text" name="deviceOilId" id="deviceOilId" value="${oilRecord.deviceOilId}" class="easyui-validatebox" data-options="required:true" />
		</td>
		<td class="xh-field">润滑人员：
			<input type="text" id="oiledUserName" name="oiledUserName" value="${oilRecord.oiledUserName}" class="easyui-validatebox"  data-options="required:true" />
				<!-- 
				<input type="text" name="oiledBy" value="${oilRecord.oiledUser.id}" id="oiledBy" class="easyui-validatebox" data-options="required:true" />
				-->
		</td>
	</tr>
	<tr>
		<td class="xh-field" colspan="2">备&emsp;&emsp;注：
			<textarea name="description" style="width:100%;height:50px;">${oilRecord.description}</textarea>
		</td>
	</tr>
</table>
</form>
</body>
</html>