<%@ page contentType="text/html; charset=UTF-8"  pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
<head>
<%@include file="../../commons/taglibs.jsp" %>
<%@include file="../../commons/baseCSS.jsp" %>
<%@include file="../../commons/base.jsp" %>
<%@include file="../../commons/event.jsp" %>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
<title>${initParam.title}</title>
</head>
<body>
<table class="xh-table">
	<tr>
		<td>
			<span class="lab">工程分类：</span>
			<span class="content">${category.bigCate.bigCateName}</span>		
		</td>
		<td>
			<span class="lab">设备类型：</span>
			<span class="content">${category.categoryName}</span>		
		</td>
	</tr>
	<tr>
		<td>
			<span class="lab">设备名称：</span>
			<span class="content">${oilRecord.device.name}</span>
		</td>
		<td>
			<span class="lab">润滑时间： </span>
			<span class="content">${myFn:formatDate(oilRecord.oiledDate,'yyyy-MM-dd')}</span>
		</td>
	</tr>
	<tr>
		<td>
			<span class="lab">润滑项目：</span>
			<span class="content">${oilRecord.oil.name}</span>
		</td>
		<td>
			<span class="lab">费用(元)：</span>
			<span class="content">${oilRecord.cost}</span>
		</td>
	</tr>
	<tr>
		<td>
			<span class="lab">润滑次数：</span>
			<span class="content">第${myFn:num2Str(oilRecord.count)}次</span>
		</td>
		<td>
			<span class="lab">润滑人员： </span>
			<span class="content">${oilRecord.oiledUserName}</span>
		</td>
	</tr>
	<tr>
		<td colspan="2">
			<span class="lab">备&emsp;&emsp;注：</span><br />
			<span class="content">${oilRecord.description}</span>
		</td>
	</tr>
	<tr>
		<td>
			<span class="lab">录入人员：</span>
			<span class="content">${oilRecord.createdUser.realName}</span>
		</td>
		<td>
			<span class="lab">录入时间：</span>
			<span class="content">${myFn:formatDate(oilRecord.createTime,'yyyy-MM-dd HH:mm:ss')}</span>
		</td>
	</tr>
</table>
</body>
</html>