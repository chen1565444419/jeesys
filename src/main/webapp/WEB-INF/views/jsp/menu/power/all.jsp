<%@ page contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<%@include file="../../commons/taglibs.jsp" %>
<%@include file="../../commons/jquery.jsp" %>
<%@include file="../../commons/easyUI.jsp" %>
<%@include file="../../commons/baseCSS.jsp"%>
<%@include file="../../commons/base.jsp" %>
<%@include file="../../commons/event.jsp" %>
<c:set var="rootPath" value="${powers[0].masterName}" scope="page"/>
<script type="text/javascript" src="${base}/resources/js/${fn:substring(rootPath,0,fn:indexOf(rootPath,'_'))}/all.js" charset="UTF-8"></script>
<%@include file="../../commons/permission.jsp"%>
<title>${initParam.title}</title>
</head>
<body>
<div class="well well-small" style="margin-left: 5px;margin-top: 5px">
		<span class="badge">提示</span>
		<p>
			在此你可以对<span class="label-info"><strong>菜单功能</strong></span>进行编辑!&nbsp;<span class="label-info"><strong>注意</strong></span>操作功能是对菜单功能的操作权限！
			请谨慎填写程序编码，权限区分标志，请勿重复!
		</p>
</div>
<table id="resultTable"></table>
<div id="dlg" class="easyui-dialog">
	<input type="hidden" value="${paramMap.fatherId}" id="pId"/>
		<form id="frm" method="post">
				 <table>
					 <tr>
					    <th>菜单名称</th>
						<td><input name="powerName" id="powerName" placeholder="请输入菜单名称" class="easyui-textbox easyui-validatebox" data-options="required:true" type="text"/></td>
						<th>父菜单名称</th>
						<td><input name="fatherId"  class="easyui-textbox" id="fatherId" type="text"/><a id="clear" class="easyui-linkbutton" style="display: inline-block;">清空</a></td>
					 </tr>
					 <tr>
						<th>菜单编码</th>
						<td><input id="masterName" name="masterName" type="text" class="easyui-textbox easyui-validatebox" required="required"/></td>
						<th>排序序号</th>
						<td><input name="seq" id="seq" type="text" class="easyui-textbox easyui-numberbox easyui-validatebox" data-options="min:0,precision:0,value:0"/></td>
					 </tr>
					 <tr>
						<th>显示图标</th>
						<td><input id="iconCls" name="iconCls" type="text"/></td>
						<th>类型</th>
						<td>
							<select id="type" class="easyui-combobox" name="type" style="width:171px;" data-options="required:true,panelHeight:'auto',editable:false">
								<option value="F">菜单</option>
								<option value="O">操作</option>
							</select>
						</td>
					 </tr>
					 <tr>
					 	<th>显示窗口</th>
					 	<td>
					 		<select id="target" class="easyui-combobox" name="target" style="width:171px;" data-options="required:true,panelHeight:'auto',editable:false">
								<option value="center">内容窗口</option>
								<option value="_blank">新窗口</option>
							</select>
					 	</td>
						<th>是否启用</th>
						<td><select id="used" class="easyui-combobox" name="used" style="width:171px;" data-options="required:true,panelHeight:'auto',editable:false">
								<option value="1">是</option>
								<option value="0">否</option>
							</select>
						</td>
					 </tr>
					 <tr>
						<th class="hide"><span>根菜单</span></th>
						<td class="hide">
						 	<span><input type="radio" name="child" value="1" checked="checked"/>是<input type="radio" name="child" value="0"/>否</span>
						</td>
						<th class="position">显示位置</th>
						<td class="position">
							<select id="position" class="easyui-combobox" name="position" style="width:171px;" data-options="multiple:true,required:true,panelHeight:'auto',editable:false">
								<option value="top">顶部</option>
								<option value="middle">行中</option>
							</select>
						</td>
					    <th>访问路径</th>
						<td><input id="url" name="url" type="text" class="easyui-textbox easyui-validatebox"/></td>
					</tr>
					 <tr>
						<th>描述</th>
						<td colspan="3"><textarea class="easyui-textbox" name="description"  style="width: 435px;height:80px;"></textarea></td>
					</tr>
				 </table>
		</form>
</div>
</body>
</html>