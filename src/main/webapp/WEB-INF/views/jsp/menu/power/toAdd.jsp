<%@ page contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>${initParam.title}</title>
<%@include file="../../commons/taglibs.jsp" %>
<%@include file="../../commons/jquery.jsp" %>
<%@include file="../../commons/easyUI.jsp" %>
<%@include file="../../commons/baseCSS.jsp" %>
<%@include file="../../commons/base.jsp" %>
<%@include file="../../commons/event.jsp" %>
<link rel="stylesheet" type="text/css" href="${base}/resources/css/power/toAdd.css"/>
<script type="text/javascript" src="${base}/resources/js/power/toAdd.js" charset="utf-8"></script>
</head>
<body>
<div class="easyui-layout" data-options="fit:true,border:false">
	<div data-options="region:'center',border:false" title="" style="overflow: hidden;padding: 10px;">
		<input type="hidden" value="${paramMap.fatherId}" id="pId"/>
		<form id="form" method="post">
				 <table>
					 <tr>
					    <th>菜单名称</th>
						<td><input name="powerName" id="powerName" placeholder="请输入菜单名称" class="easyui-textbox easyui-validatebox" data-options="required:true" type="text"/></td>
						<th>父菜单名称</th>
						<td><input name="fatherId"  class="easyui-textbox" id="fatherId" type="text"/><a id="clear" class="easyui-linkbutton" style="display: inline-block;">清空</a></td>
					 </tr>
					 <tr>
						<th>菜单编码</th>
						<td><input id="masterName" name="masterName" type="text" class="easyui-textbox easyui-validatebox" required="required"/></td>
						<th>排序序号</th>
						<td><input name="seq" id="seq" type="text" class="easyui-textbox easyui-numberbox easyui-validatebox" data-options="min:0,precision:0,value:0"/></td>
					 </tr>
					 <tr>
						<th>显示图标</th>
						<td><input id="iconCls" name="iconCls" type="text"/></td>
						<th>类型</th>
						<td>
							<select id="type" class="easyui-combobox" name="type" style="width:171px;" data-options="required:true,panelHeight:'auto',editable:false">
								<option value="F">菜单</option>
								<option value="O">操作</option>
								<option value="P">权限</option>
							</select>
						</td>
					 </tr>
					 <tr>
					 	<th>显示窗口</th>
					 	<td>
					 		<select id="target" class="easyui-combobox" name="target" style="width:171px;" data-options="required:true,panelHeight:'auto',editable:false">
								<option value="center">内容窗口</option>
								<option value="_blank">新窗口</option>
							</select>
					 	</td>
						<th>是否启用</th>
						<td><select id="used" class="easyui-combobox" name="used" style="width:171px;" data-options="required:true,panelHeight:'auto',editable:false">
								<option value="1">是</option>
								<option value="0">否</option>
							</select>
						</td>
					 </tr>
					 <tr>
						<th class="hide"><span>根菜单</span></th>
						<td class="hide">
						 	<span><input type="radio" name="child" value="1" checked="checked"/>是<input type="radio" name="child" value="0"/>否</span>
						</td>
						<th class="position">显示位置</th>
						<td class="position">
							<select id="position" class="easyui-combobox" name="position" style="width:171px;" data-options="multiple:true,required:true,panelHeight:'auto',editable:false">
								<option value="top">顶部</option>
								<option value="middle">行中</option>
							</select>
						</td>
					    <th>访问路径</th>
						<td><input id="url" name="url" type="text" class="easyui-textbox easyui-validatebox"/></td>
					</tr>
					 <tr>
						<th>描述</th>
						<td colspan="3"><textarea class="easyui-textbox" name="description"  style="width: 435px;height:80px;"></textarea></td>
					</tr>
				 </table>
		</form>
	</div>
</div>
</body>
</html>