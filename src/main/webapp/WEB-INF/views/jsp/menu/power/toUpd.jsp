<%@ page contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>${initParam.title}</title>
<%@include file="../../commons/taglibs.jsp" %>
<%@include file="../../commons/jquery.jsp" %>
<%@include file="../../commons/easyUI.jsp" %>
<%@include file="../../commons/base.jsp" %>
<%@include file="../../commons/event.jsp" %>
<link rel="stylesheet" type="text/css" href="${base}/resources/css/power/toAdd.css"/>
<script type="text/javascript" src="${base}/resources/js/power/toUpd.js" charset="utf-8"></script>
<script type="text/javascript">
$(function(){
	var type="${power.type}";
	var positions="${power.position}";
	if(type=="O"){
		$(".hide").hide();
		$("input[name='child']").attr("disabled",true);
		$(".position").show();
		$("#position").attr("disabled",false);
		if(positions!=undefined&&positions!=""){
			   positions=positions.replace("[","").replace("]","").replace(" ","");
			   var positionArr=positions.split(",");
			   for(var i=0,len=positionArr.length;i<len;i++){
				   $("#position").combobox("select",positionArr[i]);
			   }
		}else{
			 $("#position").combobox("select","top");
		}
	}else{
		$(".position").hide();
		$("#position").attr("disabled",true);
		$(".hide").show();
		$("input[name='child']").attr("disabled",false);
	}
});
</script>
</head>
<body>
<div class="easyui-layout" data-options="fit:true,border:false">
	<div data-options="region:'center',border:false" title="" style="overflow: hidden;padding: 10px;">
		<input type="hidden" value="${power.fatherId}" id="pId"/>
		<input type="hidden" value="${power.iconCls}" id="iconVal"/>
		<form id="form" method="post">
			<input type="hidden" value="${power.id}" name="id"/>
				 <table>
					 <tr>
					    <th>菜单名称</th>
						<td><input name="powerName" id="powerName" value="${power.powerName}" placeholder="请输入菜单名称" class="easyui-textbox easyui-validatebox" data-options="required:true" type="text"/></td>
						<th>父菜单名称</th>
						<td><input name="fatherId"  class="easyui-textbox" id="fatherId" type="text"/><a id="clear" class="easyui-linkbutton" style="display: inline-block;">清空</a></td>
					 </tr>
					 <tr>
						<th>菜单编码</th>
						<td><input id="masterName" name="masterName" value="${power.masterName}" type="text" class="easyui-textbox easyui-validatebox" required="required"/></td>
						<th>排序序号</th>
						<td><input name="seq" id="seq" type="text" class="easyui-textbox easyui-numberbox easyui-validatebox" data-options="min:0,precision:0,value:${power.seq}"/></td>
					 </tr>
					 <tr>
						<th>显示图标</th>
						<td><input id="iconCls" name="iconCls" type="text"/></td>
						<th>类型</th>
						<td>
							<select id="type" class="easyui-combobox" name="type" style="width:171px;" data-options="required:true,panelHeight:'auto',editable:false">
								<option value="F" <c:if test="${power.type eq 'F' }">selected="selected"</c:if>>菜单</option>
								<option value="O" <c:if test="${power.type eq 'O' }">selected="selected"</c:if>>操作</option>
								<option value="P" <c:if test="${power.type eq 'P' }">selected="selected"</c:if>>权限</option>
							</select>
						</td>
					 </tr>
					 <tr>
					 	<th>显示窗口</th>
					 	<td>
					 		<select id="target" class="easyui-combobox" name="target" style="width:171px;" data-options="required:true,panelHeight:'auto',editable:false">
								<option value="center" <c:if test="${power.target eq 'center' }">selected="selected"</c:if>>内容窗口</option>
								<option value="_blank" <c:if test="${power.target eq '_blank' }">selected="selected"</c:if>>新窗口</option>
							</select>
					 	</td>
						<th>是否启用</th>
						<td><select id="used" class="easyui-combobox" name="used" style="width:171px;" data-options="required:true,panelHeight:'auto',editable:false">
								<option value="1" <c:if test="${power.used eq 1 }">selected="selected"</c:if>>是</option>
								<option value="0" <c:if test="${power.used eq 0 }">selected="selected"</c:if>>否</option>
							</select>
						</td>
					 </tr>
					 <tr>
						<th class="hide"><span>根菜单</span></th>
					 	<td class="hide">
						 	<span>
							 		<input type="radio" <c:if test="${power.child==1}">checked="checked"</c:if> name="child" value="1"/>是
							 		<input type="radio" <c:if test="${power.child==0}">checked="checked"</c:if> name="child" value="0"/>否
						 	</span>
					 	</td>
					 	<th class="position">显示位置</th>
						<td class="position">
							<select id="position" class="easyui-combobox" name="position" style="width:171px;" data-options="multiple:true,required:true,panelHeight:'auto',editable:false">
								<option value="top">顶部</option>
								<option value="middle">行中</option>
							</select>
						</td>
					    <th>访问路径</th>
						<td><input id="url" name="url"<c:if test="${power.child==0}">disabled="disabled"</c:if> value="${power.url}" type="text" class="easyui-textbox easyui-validatebox" data-options="required:true"/></td>
					</tr>
					 <tr>
						<th>描述</th>
						<td colspan="3"><textarea class="easyui-textbox" name="description"  style="width: 435px;height:80px;">${power.description }</textarea></td>
					</tr>
				 </table>
		</form>
	</div>
</div>
</body>
</html>