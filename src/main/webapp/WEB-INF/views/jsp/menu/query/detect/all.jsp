<%@ page contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<%@include file="../../../commons/taglibs.jsp" %>
<%@include file="../../../commons/jquery.jsp" %>
<%@include file="../../../commons/calendar.jsp" %>
<%@include file="../../../commons/easyUI.jsp" %>
<%@include file="../../../commons/baseCSS.jsp"%>
<%@include file="../../../commons/base.jsp" %>
<%@include file="../../../commons/event.jsp" %>
<script type="text/javascript" src="${resources}/js/query/detect/all.js" charset="UTF-8"></script>
<title>${initParam.title}</title>
</head>
<body>
<div style="width: 100%;height:30px;padding-top:5px;border-style:solid;border-width:1px;border-left: 0px;">
	<span style="font-size: 14px;padding-left:20;margin-left: 20px;padding-top:-20px;">查询方式:</span>
	<select id="searchType" class="easyui-combobox" data-options="panelHeight:'auto',editable:false" style="width:120px;">   
		<option value="">请选择</option>
		<option value="createTime">按录入时间</option>
		<option value="dept">按所属部门</option>
		<option value="category">按设备类型</option>
		<option value="detectUserName">按检修人员</option>
	</select>
	<span id="dynamic">
		<span id="createTime" style="display: none;">
			开始时间:<input type="text" id="startDate" readonly="readonly" onClick="WdatePicker({dateFmt:'yyyy-MM-dd'})" class="xh-date" />
			结束时间:<input type="text" id="endDate" readonly="readonly" onClick="WdatePicker({dateFmt:'yyyy-MM-dd'})" class="xh-date" />
		</span>
		<span id="dept" style="display: none;">
			<shiro:hasPermission name="viewAllDevice">
				<select id="deptId" class="easyui-combobox" style="width:155px" data-options="fit:true,panelHeight:'auto',editable:false">
					<c:forEach items="${deptList}" var="d">
						<option value="${d.id}">${d.deptName}</option>
					</c:forEach>
				</select>			
		</shiro:hasPermission>
			
		<shiro:hasPermission name="viewOwnerDevice">
			<select id="deptId" class="easyui-combobox" style="width:155px" data-options="fit:true,panelHeight:'auto',editable:false">
				<c:forEach items="${deptList}" var="d">
					<c:if test="${sessionScope.USER_KEY_IN_SESSION.deptId eq d.id}">
						<option value="${d.id}">${d.deptName}</option>
					</c:if>
				</c:forEach>
			</select>			
		</shiro:hasPermission>
				
		</span>
		<span id="category" style="display: none;">
			工程分类:
			<select id="bigCateId" class="easyui-combobox" name="bigCateId" data-options="width:155,panelHeight:200,editable:false,required:true">
				<c:forEach items="${bigCateList}" var="b">
					<option value="${b.id}">${b.bigCateName}</option>
				</c:forEach>
			</select>
			设备类型:
			<input name="categoryId" id="categoryId" class="easyui-combobox" />
			<%-- 
			<select id="categoryId" class="easyui-combobox" style="width:155px" data-options="fit:true,panelHeight:'200',editable:false">
				<c:forEach items="${categoryList}" var="c">
					<option value="${c.id}">${c.categoryName}</option>
				</c:forEach>
			</select> 
			--%>
		</span>
		
		<span id="detectUserName" style="display:none;">
			<input type="text" id="userName" name="userName" placeholder="请输入检修人员姓名" maxlength="20" value="" class="easyui-validatebox" data-options="required:true"/>
		</span>
	</span>
	
	<a id="searchBtn" href="#" class="easyui-linkbutton" data-options="iconCls:'icon-search'">查询</a>
</div>
<iframe id="ifr" src="" style="border:0px;" width="100%" height="100%">
</body>
</html>