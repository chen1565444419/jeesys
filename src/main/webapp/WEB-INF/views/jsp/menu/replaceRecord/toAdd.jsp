<%@ page contentType="text/html; charset=UTF-8"  pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
<head>
<%@include file="../../commons/taglibs.jsp" %>
<%@include file="../../commons/jquery.jsp" %>
<%@include file="../../commons/calendar.jsp" %>
<%@include file="../../commons/easyUI.jsp" %>
<%@include file="../../commons/baseCSS.jsp" %>
<%@include file="../../commons/base.jsp" %>
<%@include file="../../commons/event.jsp" %>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
<script type="text/javascript">
var path="${paramMap.path}";
var deptIdVal=null;
<shiro:hasPermission name="viewOwnerDevice">
	deptIdVal=${sessionScope.USER_KEY_IN_SESSION.deptId};
</shiro:hasPermission>
</script>
<title>${initParam.title}</title>
<script type="text/javascript" src="${resources}/js/${paramMap.path}/toAdd.js"></script>
</head>
<body>
<form id="frm" method="post">
<table class="xh-table">
	<tr>
		<td class="xh-field">
			工程分类：
			<select id="bigCateId" class="easyui-combobox" name="bigCateId" data-options="width:155,panelHeight:200,editable:false,required:true">
				<c:forEach items="${bigCateList}" var="b">
					<option value="${b.id}">${b.bigCateName}</option>
				</c:forEach>
			</select>
		</td>
		<td class="xh-field">设备类型：
			<input name="categoryId" id="categoryId" class="easyui-combobox" />
		<%-- 
			<select id="categoryId" class="easyui-combobox" style="width:155px" data-options="fit:true,panelHeight:200,editable:false">
				<c:forEach items="${categoryList}" var="c">
					<option value="${c.id}">${c.categoryName}</option>
				</c:forEach>
			</select>		
			 --%>
		</td>
	</tr>
	<tr>
		<td class="xh-field">设备名称：
			<input type="text" name="deviceId" id="deviceId" class="easyui-validatebox" data-options="required:true" /> 
		</td>
		<td class="xh-field">更换时间： 
			<input type="text" name="replacedDate" id="replacedDate" readonly="readonly" onClick="WdatePicker({dateFmt:'yyyy-MM-dd',maxDate:'%y-%M-%d'})" class="xh-date"  />
		</td>
	</tr>
	<tr>
		<td class="xh-field">更换次数：
			<select id="count" class="easyui-combobox" name="count" style="width:155px;" data-options="panelHeight:200,editable:false,required:true">
			</select>
		</td>
		<td class="xh-field">更换人员：
			<input type="text" id="replaceUserName" name="replaceUserName" class="easyui-validatebox"  data-options="required:true" />
			<!--
			<input type="text" name="replacedBy" id="replacedBy" class="easyui-validatebox" data-options="required:true" />
			-->
		</td>
	</tr>
	<tr>
		<td class="xh-field">
			备件名称：
			<input type="text" name="deviceCompId" id="compId" class="easyui-validatebox" data-options="required:true" />
		</td>
		<td class="xh-field">费&emsp;&emsp;用：
			<input type="text" name="cost" id="cost" class="easyui-validatebox easyui-numberbox" data-options="required:true,min:0,precision:2" />
		</td>
	</tr>
	<tr>
		<td class="xh-field" colspan="2">备&emsp;&emsp;注：
			<textarea name="description" style="width:100%;height:50px;"></textarea>
		</td>
	</tr>
</table>
</form>
</body>
</html>