<%@ page contentType="text/html; charset=UTF-8"  pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
<head>
<%@include file="../../commons/taglibs.jsp" %>
<%@include file="../../commons/jquery.jsp" %>
<%@include file="../../commons/calendar.jsp" %>
<%@include file="../../commons/easyUI.jsp" %>
<%@include file="../../commons/baseCSS.jsp" %>
<%@include file="../../commons/base.jsp" %>
<%@include file="../../commons/event.jsp" %>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
<script type="text/javascript">
var path="${paramMap.path}";
var deptIdVal="${replaceRecord.device.deptId}";
var deviceIdVal="${replaceRecord.device.id}";
var cateIdVal="${replaceRecord.device.categoryId}";
var countVal="${replaceRecord.count}";
</script>
<title>${initParam.title}</title>
<script type="text/javascript" src="${resources}/js/${paramMap.path}/toUpd.js"></script>
</head>
<body>
<form id="frm" method="post">
<table class="xh-table">
	<tr>
		<td class="xh-field">
			工程分类：
			<select id="bigCateId" class="easyui-combobox" name="bigCateId" data-options="width:155,panelHeight:200,editable:false,required:true">
				<c:forEach items="${bigCateList}" var="b">
					<option value="${b.id}" <c:if test="${b.id eq replaceRecord.device.category.bigCateId}">selected="selected"</c:if>>${b.bigCateName}</option>
				</c:forEach>
			</select>
		</td>
		<td class="xh-field">设备类型：
			<input id="categoryId" name="categoryId" value="${replaceRecord.device.categoryId}"/>
			<input type="hidden" name="id" value="${replaceRecord.id}"/>	
		</td>
	</tr>
	<tr>
		<td class="xh-field">设备名称：
			<input type="text" name="deviceId" id="deviceId" value="${replaceRecord.device.id}" class="easyui-validatebox" data-options="required:true" /> 
		</td>
		<td class="xh-field">更换时间： 
			<input type="text" name="replacedDate" id="replacedDate" value="${myFn:formatDate(replaceRecord.replacedDate,'yyyy-MM-dd')}" readonly="readonly" onClick="WdatePicker({dateFmt:'yyyy-MM-dd',maxDate:'%y-%M-%d'})" class="xh-date"  />
		</td>
	</tr>
	<tr>
		<td class="xh-field">更换次数：
			<select id="count" class="easyui-combobox" name="count" style="width:155px;" data-options="panelHeight:200,editable:false,required:true,disabled:true">
				<option value="${replaceRecord.count}">第${myFn:num2Str(replaceRecord.count)}次</option>
			</select>
		</td>
		<td class="xh-field">费&emsp;&emsp;用：
			<input type="text" name="cost" id="cost" value="${replaceRecord.cost}" class="easyui-validatebox easyui-numberbox" data-options="required:true,min:0,precision:2" />
		</td>
	</tr>
	<tr>
		<td class="xh-field">
		
			备件名称：
			<input type="text" name="deviceCompId" id="compId" value="${replaceRecord.deviceCompId}" class="easyui-validatebox" data-options="required:true" />
		</td>
		<td class="xh-field">更换人员：
			<input type="text" id="replaceUserName" name="replaceUserName" value="${replaceRecord.replaceUserName}" class="easyui-validatebox"  data-options="required:true" />
			<!-- 
			<input type="text" name="replacedBy" value="${replaceRecord.replacedUser.id}" id="replacedBy" class="easyui-validatebox" data-options="required:true" />
			 -->
		</td>
	</tr>
	<tr>
		<td class="xh-field" colspan="2">备&emsp;&emsp;注：
			<textarea name="description" style="width:100%;height:50px;">${replaceRecord.description}</textarea>
		</td>
	</tr>
</table>
</form>
</body>
</html>