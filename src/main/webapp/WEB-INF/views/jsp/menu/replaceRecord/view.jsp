<%@ page contentType="text/html; charset=UTF-8"  pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
<head>
<%@include file="../../commons/taglibs.jsp" %>
<%@include file="../../commons/baseCSS.jsp" %>
<%@include file="../../commons/jquery.jsp" %>
<%@include file="../../commons/base.jsp" %>
<%@include file="../../commons/event.jsp" %>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
<title>${initParam.title}</title>
</head>
<body>
<table class="xh-table">
	<tr>
		<td>
			<span class="lab">工程分类：</span>
			<span class="content">${category.bigCate.bigCateName}</span>		
		</td>
		<td>
			<span class="lab">设备类型：</span>
			<span class="content">${category.categoryName}</span>		
		</td>
	</tr>
	<tr>
		<td>
			<span class="lab">设备名称：</span>
			<span class="content">${replaceRecord.device.name}</span>
		</td>
		<td>
			<span class="lab">更换时间： </span>
			<span class="content">${myFn:formatDate(replaceRecord.replacedDate,'yyyy-MM-dd')}</span>
		</td>
	</tr>
	<tr>
		<td>
			<span class="lab">备件名称：</span>
			<span class="content">${replaceRecord.component.name}</span>
		</td>
		<td>
			<span class="lab">费用(元)：</span>
			<span class="content">${replaceRecord.cost}</span>
		</td>
	</tr>
	<tr>
		<td>
			<span class="lab">录入人员：</span>
			<span class="content">${replaceRecord.createdUser.realName}</span>
		</td>
		<td>
			<span class="lab">录入时间：</span>
			<span class="content">${myFn:formatDate(replaceRecord.createTime,'yyyy-MM-dd HH:mm:ss')}</span>
		</td>
	</tr>
	<tr>
		<td >
			<span class="lab">更换次数：</span>
			<span class="content">第${myFn:num2Str(replaceRecord.count)}次</span>
		</td>
		<td>
			<span class="lab">更换人员： </span>
			<span class="content">${replaceRecord.replaceUserName}</span>
		</td>
	</tr>
	<tr>
		<td colspan="2">
			<span class="lab">备&emsp;&emsp;注：</span><br />
			<span class="content">${replaceRecord.description}</span>
		</td>
	</tr>
</table>
</body>
</html>