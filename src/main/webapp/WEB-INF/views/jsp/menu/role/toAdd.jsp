﻿<%@ page contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title></title>
<%@include file="../../commons/taglibs.jsp" %>
<%@include file="../../commons/jquery.jsp" %>
<%@include file="../../commons/easyUI.jsp" %>
<%@include file="../../commons/base.jsp" %>
<%@include file="../../commons/event.jsp" %>
<%@include file="../../commons/baseCSS.jsp" %>
<script type="text/javascript" src="${base}/resources/js/common/regexp.js"></script>
<script type="text/javascript" src="${base}/resources/js/role/toAdd.js"></script>
</head>
<body>
<br />
	<div class="easyui-layout" data-options="fit:true,border:false">
		<div data-options="region:'center',border:false" title="" style="overflow: hidden;">
			<form action="" method="POST" id="frm">
				<table class="xh-table">
					<tr>
						<td class="xh-field">角色名称：
						<input type="text" name="roleName" id="roleName" class="easyui-validatebox" data-options="required:true"/></td>
						<!-- <td class="xh-field">排序序号：</td>
						<td>
						<input type="text" name="seq" value="0" id="seq" class="easyui-numberbox" data-options="min:0"/>
						</td> -->
					</tr>
				</table>
			</form>
		</div>
	</div>
</body>
</html>