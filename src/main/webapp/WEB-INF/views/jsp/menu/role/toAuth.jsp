<%@ page contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title></title>
<%@include file="../../commons/taglibs.jsp" %>
<%@include file="../../commons/jquery.jsp" %>
<%@include file="../../commons/easyUI.jsp" %>
<%@include file="../../commons/baseCSS.jsp" %>
<%@include file="../../commons/base.jsp" %>
<%@include file="../../commons/event.jsp" %>
<script type="text/javascript" src="${base}/resources/js/common/regexp.js"></script>
<script type="text/javascript" src="${base}/resources/js/role/toAuth.js"></script>
<style type="text/css">
	.tree-node {padding:0;}
</style>
</head>
	角色名称：${role.roleName}<br />
	<input type="hidden" value="${role.id}" id="roleid" />
	 <ul class="easyui-tree" id="powertree"></ul>
	 <script>
	var rps=[
		<c:forEach items="${rpList}" var="rp" varStatus="status">
			 {type:"${rp.type}",id:${rp.powerId}}<c:if test="${!status.last}">,</c:if>
		</c:forEach>
	];
	</script>
</body>
</html>