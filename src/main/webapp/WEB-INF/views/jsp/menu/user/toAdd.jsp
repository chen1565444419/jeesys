<%@ page contentType="text/html; charset=UTF-8"  pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
<head>
<%@include file="../../commons/taglibs.jsp" %>
<%@include file="../../commons/jquery.jsp" %>
<%@include file="../../commons/easyUI.jsp" %>
<%@include file="../../commons/baseCSS.jsp" %>
<%@include file="../../commons/base.jsp" %>
<%@include file="../../commons/event.jsp" %>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
<script type="text/javascript" src="${base}/resources/js/user/toAdd.js"></script>
<script type="text/javascript">
			var roles=[
			           <c:forEach items="${roleList}" var="r" varStatus="sta">
			           {"id":${r.id},"text":"${r.roleName}","open":true}<c:if test="${!sta.last}">,</c:if>
			           </c:forEach>
			           ];
</script>
<title>${initParam.title}</title>
</head>
<body>
<form id="frm" method="POST">
<table class="xh-table">
	<tr>
		<td class="xh-field">用&nbsp;户&nbsp;名：
			<input type="text" name="userName" id="userName" maxlength="10" class="easyui-validatebox" data-options="required:true" />
		</td>
		<td class="xh-field">密&emsp;&emsp;码：
			<input type="password" name="userPwd" id="userPwd" class="easyui-validatebox" data-options="required:true" />
		</td>
	</tr>
	<tr>
		<td class="xh-field">真实姓名： 
			<input type="text" name="realName" id="realName" value="" class="easyui-validatebox" data-options="required:true" maxlength="10" />
		</td>
		<td class="xh-field">职&emsp;&emsp;位： 
			<input type="text" name="post" id="post" maxlength="10" value="" />
		</td>
	</tr>
	<tr>
		<td class="xh-field">性&emsp;&emsp;别：
			<input type="radio" name="sex" value="男" checked="checked" >男
			<input type="radio" name="sex" value="女">女
		</td>
		<td class="xh-field">工&emsp;&emsp;号：
			<input type="text" name="number" id="number" />
		</td>
	</tr>
	<tr>
		<td class="xh-field">所属部门：
			<select name="deptId" id="deptId" class="easyui-combobox" style="width:155px" data-options="fit:true,panelHeight:'auto'">
				<c:forEach items="${deptList}" var="d">
					<option value="${d.id}">${d.deptName}</option>
				</c:forEach>
			</select>
		</td>
		<td class="xh-field">所属角色：
			<select id="roleTree" name="roleId" class="easyui-combotree" data-options="multiple:true,editable:false,
				checkbox:true,panelHeight:'auto',valueField:'id',textField:'text'" style="width:155px;">
			</select> 
		</td>
	</tr>
	<tr>
		<td class="xh-field">电子邮件： 
			<input type="text" name="email" id="email" />
		</td>
		<td class="xh-field">电话号码： 
			<input type="text" name="telphone" id="telphone" />
		</td>
	</tr>
	<!-- <tr>
		<td class="xh-field">排序序号： 
			<input type="text" name="seq" value="0" class="easyui-numberbox" data-options="min:0" />
		</td>
		<td>&nbsp;</td>
	</tr> -->
</table>
</form>
</body>
</html>