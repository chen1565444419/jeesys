<%@ page contentType="text/html; charset=UTF-8"  pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
<head>
<%@include file="../../commons/taglibs.jsp" %>
<%@include file="../../commons/jquery.jsp" %>
<%@include file="../../commons/easyUI.jsp" %>
<%@include file="../../commons/baseCSS.jsp" %>
<%@include file="../../commons/base.jsp" %>
<%@include file="../../commons/event.jsp" %>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<script type="text/javascript" src="${base}/resources/js/user/toUpd.js"></script>
<script type="text/javascript">
		var roles=[
			           <c:forEach items="${roleList}" var="r" varStatus="sta">
						<c:set var="isChecked" value="0"/>
						<c:forEach items="${user.roles}"  var="ur">
							<c:if test="${ur.id==r.id}">
								<c:set var="isChecked" value="1"/>
							</c:if>
						</c:forEach>
			           {"id":${r.id},"text":"${r.roleName}","open":false,"checked":
							<c:choose>
								<c:when test="${isChecked==1}">true</c:when>
								<c:otherwise>false</c:otherwise>
							</c:choose>
			           }<c:if test="${!sta.last}">,</c:if>
			           </c:forEach>
			      ];
</script>
<style type="text/css">
.xh-table{
	margin-left: 50px;
	margin-right: 50px;
}
</style>
<title>修改用户</title>
</head>
<body>
<form id="frm" method="POST">
<table class="xh-table">
	<tr>
		<td class="xh-field">用&nbsp;户&nbsp;名：
			<input type="hidden" name="id" value="${user.id}" id="id"/>
			<input type="text" name="userName" id="userName" value="${user.userName}" class="easyui-validatebox" data-options="required:true" />
		</td>
		<td class="xh-field">密&emsp;&emsp;码：
			<input type="password" name="userPwd" id="userPwd" title="*不修改密码请留空!" class="easyui-tooltip" data-options="position:'right'" />
		</td>
	</tr>
	<tr>
		<td class="xh-field">真实姓名： 
			<input type="text" name="realName" id="realName" value="${user.realName}" class="easyui-validatebox" data-options="required:true" />
		</td>
		<td class="xh-field">职&emsp;&emsp;位： 
			<input type="text" name="post" id="post" value="${user.post}" />
		</td>
	</tr>
	<tr>
		<td class="xh-field">性&emsp;&emsp;别：
			<input type="radio" name="sex" value="男" <c:if test="${user.sex eq '男'}"> checked="checked"</c:if>>男
			<input type="radio" name="sex" value="女" <c:if test="${user.sex eq '女'}"> checked="checked"</c:if>>女
		</td>
		<td class="xh-field">员&nbsp;工&nbsp;号：
			<input type="text" name="number" id="number" value="${user.number}" />
		</td>
	</tr>
	<tr>
		<td class="xh-field">所属部门：
			<select name="deptId" id="deptId" class="easyui-combobox" style="width:155px" data-options="fit:true,panelHeight:'auto'">
				<c:forEach items="${deptList}" var="d">
					<option value="${d.id}" <c:if test="${d.id eq user.deptId}">selected="selected"</c:if>>${d.deptName}</option>
				</c:forEach>
			</select>
		</td>
		<td class="xh-field">所属角色：
			<select id="roleTree" name="roleId" class="easyui-combotree" data-options="multiple:true,editable:false,
				checkbox:true,panelHeight:'auto',valueField:'id',textField:'text'" style="width:155px;">
			</select> 
		</td>
	</tr>
	<tr>
		<td class="xh-field">电子邮件： 
			<input type="text" name="email" id="email" value="${user.email}" />
		</td>
		<td class="xh-field">电话号码： 
			<input type="text" name="telphone" id="telphone" value="${user.telphone}" />
		</td>
	</tr>
	<tr>
		<td class="xh-field">排序序号： 
			<input type="text" name="seq" value="${user.seq}" class="easyui-numberbox" data-options="min:0" />
		</td>
		<td>&nbsp;</td>
	</tr>
</table>
</form>
</body>
</html>