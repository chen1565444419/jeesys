<%@ page contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
<head>
<%@include file="../../commons/taglibs.jsp" %>
<%@include file="../../commons/jquery.jsp"%>
<%@include file="../../commons/easyUI.jsp"%>
<%@include file="../../commons/baseCSS.jsp"%>
<%@include file="../../commons/base.jsp" %>
<%@include file="../../commons/event.jsp" %>
<script type="text/javascript" src="${base}/resources/js/user/toViewSatisfy.js"></script>
<script type="text/javascript">
var params={userId:${paramMap.user.id},type:${paramMap.type},startTime:"${paramMap.startTime}",endTime:"${paramMap.endTime}"};
</script>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>${initParam.title}</title>
</head>
<body>
<div>
	<table class="xh-table">
		<tr>
			<td class="xh-field" nowrap="nowrap">用户名：${paramMap.user.userName}</td>
			<td class="xh-field" nowrap="nowrap">编 &nbsp;&nbsp;号：${paramMap.user.number}</td>
		</tr>
		<tr>
			<td class="xh-field" colspan="2">查询时间:${paramMap.startTime}至${paramMap.endTime}</td>
		</tr>
	</table>
	<table id="resultTable"></table>
</div>
</body>
</html>