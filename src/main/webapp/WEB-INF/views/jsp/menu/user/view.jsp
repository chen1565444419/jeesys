<%@ page contentType="text/html; charset=UTF-8"  pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
<head>
<%@include file="../../commons/taglibs.jsp" %>
<%@include file="../../commons/baseCSS.jsp" %>
<%@include file="../../commons/jquery.jsp" %>
<%@include file="../../commons/base.jsp" %>
<%@include file="../../commons/event.jsp" %>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>查看用户</title>
</head>
<body>
	<table class="xh-table">
	<tr>
		<td>
			<span class="lab">用&nbsp;户&nbsp;名：</span>
			<span class="content">${user.userName}</span>
		</td>
		<td>
			<span class="lab">密&emsp;&emsp;码：</span>
			<span class="content">******</span>
		</td>
	</tr>
	<tr>
		<td>
			<span class="lab">真实姓名：</span>
			<span class="content">${user.realName}</span>
			</td>
		<td>
			<span class="lab">职&emsp;&emsp;位：</span>
			<span class="content">${user.post}</span>
		</td>		
	</tr>
	<tr>
		<td>
			<span class="lab">性&emsp;&emsp;别：</span>
			<span class="content">${user.sex}</span>
			</td>
		<td>
			<span class="lab">员&nbsp;工&nbsp;号：</span>
			<span class="content">${user.number}</span>
		</td>
	</tr>
	<tr>
		<td>
			<span class="lab">所属部门：</span>
			<span class="content">${user.deptName}</span>
		</td>
		<td>
			<span class="lab">所属角色：</span>
			<span class="content">
				<c:forEach items="${user.roles}" var="r" varStatus="sta">
					${r.roleName}<c:if test="${!sta.last}">,</c:if>
				</c:forEach>
			</span>
		</td>
	</tr>
	<tr>
		<td>
			<span class="lab">电子邮件： </span>
			<span class="content">${user.email}</span>
		</td>
		<td>
			<span class="lab">电话号码：</span>
			<span class="content">${user.telphone}</span>
		</td>
	</tr>
	<%-- <tr>
		<td>
			<span class="lab">排序序号：</span>
			<span class="content">${user.seq}</span>
		</td>
		<td>&nbsp;</td>
	</tr> --%>
</table>
</body>
</html>