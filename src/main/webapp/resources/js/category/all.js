var grid=null;
$(document).ready(function(){
	grid=$("#resultTable").datagrid({
		 height:$(document).height(),
		 url:base+rootPath+"/getData"+suffix,
		 queryParams:{},
		 sortName:"createTime",
		 sortOrder:"ASC",
		 idField:"id",
		 toolbar:toolbar,
		 columns:[[
		       {field:"no",checkbox:true},
		       {field:"id",title:"id",hidden:true},
		       {field:"categoryName",title:"类型名称",align:"center",sortable:true},
		       {field:"bigCateName",title:"所属部门分类",align:"center",sortable:true,formatter:formatBigCate},
		       {field:"description",title:"描述",align:"center",sortable:false,formatter:formatDescr},
		       {field:"seq",title:"排序",align:"center",sortable:true,hidden:true},
		       {field:"createTime",title:"创建时间",align:"center",sortable:true},
		       {field:"operate",title:"操作",align:"center",formatter:operateFunction}
		       ]]
	});
	
	$(".datagrid-toolbar").append($("#searchbar"));

	$("#searchBtn").click(function(){
		var bigCateIdVal= $("#bigCateId").combobox("getValue");
		var param=null;
		if(bigCateIdVal!=0){
			param={bigCateId:bigCateIdVal};
		}
		grid.datagrid("reload",param);
	});
	
});


function formatBigCate(value,row,index){
	if(row!=null){
		if(row.bigCate!=null){
			return row.bigCate.bigCateName;
		}
	}
	return "";
}

/**
 * 删除选中的
 * @param val
 */
function delMySelected(val){
	var ids=getIds(val);
	if(ids!=""){
		parent.$.messager.confirm("确认","您确认要删除记录吗？<br />",function(r){
			if(r){
				$.post(base+rootPath+"/del"+suffix,{ids:ids},function(data){
					if(data==true){
						parent.$.messager.alert("提示","删除成功！","info");
						if(grid!=null&&grid!=undefined){
							try{
								grid.datagrid("reload");
								grid.datagrid("clearSelections");
							}catch(e){
								
							}
							try{
								grid.treegrid("reload");
								grid.treegrid("clearSelections");
							}catch(e){
								
							}
						}
					}else{
						parent.$.messager.alert("提示","删除失败！","error");
					}
				},"json");
			}
		});
	}
}
