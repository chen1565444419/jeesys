var grid=null;
$(document).ready(function(){
	grid=$("#resultTable").datagrid({
		 height:$(document).height(),
		 url:base+rootPath+"/getData"+suffix,
		 queryParams:{},
		 sortName:"createTime",
		 sortOrder:"ASC",
		 idField:"id",
		 toolbar:toolbar,
		 columns:[[
		       {field:"no",checkbox:true},
		       {field:"id",title:"id",hidden:true},
		       {field:"devName",title:"设备名称",align:"center",sortable:true,formatter:formatDevice},
		       {field:"devNo",title:"设备编号",align:"center",sortable:false,formatter:formatDeviceNo},
		       {field:"result",title:"校验结果",align:"center",sortable:true,formatter:formatDescr},
		       {field:"checkCycle",title:"校验周期",align:"center",sortable:true,formatter:formatCycle},
		       {field:"checkDate",title:"校验时间",align:"center",sortable:true,formatter:formatCheckDate},
		       {field:"checkUnit",title:"校验单位",align:"center",sortable:true/*,formatter:formatOiledUser*/},
		       {field:"checkCount",title:"校验次数",align:"center",sortable:true,formatter:formatCount},
		       {field:"createdBy",title:"录入人员",align:"center",sortable:true,formatter:formatCreatedBy},
		       {field:"createTime",title:"录入时间",align:"center",sortable:true},
		       {field:"description",title:"备注",align:"center",sortable:false},
		       {field:"operate",title:"操作",align:"center",formatter:operateFunction}
		       ]]
	});
});

function formatDevice(value,row,index){
	if(row==null){
		return null;
	}
	return row.device.name;		
}
function formatDeviceNo(value,row,index){
	if(row==null){
		return null;
	}
	return row.device.no;		
}
function formatCheckDate(value,row,index){
	if(row==null){
		return null;
	}
	return value.substring(0,value.indexOf(" "));	
}
function formatCreatedBy(value,row,index){
	if(row==null){
		return null;
	}
	return row.createdUser.realName;		
}

function formatCount(value,row,index){
	if(row==null){
		return null;
	}
	return "第"+transNum2Chinese(value)+"次";		
}

function formatNo(value,row,index){
	if(row==null){
		return null;
	}
	return row.device.no;		
}

function formatStartDate(value,row,index){
	if(row==null){
		return null;
	}
	return value.substring(0,value.indexOf(" "));
}
function formatOiledDate(value,row,index){
	if(row==null){
		return null;
	}
	return value.substring(0,value.indexOf(" "));
}

function formatCreatedUser(value,row,index){
	if(row==null){
		return null;
	}
	return row.createdUser.realName;
}

function formatCate(value,row,index){
	if(row==null){
		return null;
	}
	return row.category.categoryName;
}
function formatCycle(value,row,index){
	if(row==null){
		return null;
	}
	var result="其它";
	switch(row.device.checkType){
	case 1:
		result="日校验";
		break;
	case 2:
		result="周校验";
		break;
	case 3:
		result="月校验";
		break;
	case 4:
		result="季校验";
		break;
	case 5:
		result="年校验";
		break;
	case 6:
		result="其它";
		break;
	}
	return result;
}