var submitForm = function($dialog,$grid,$pjq) {
	var bigCateIdVal=$("#bigCateId").combobox("getValue");
	var categoryIdVal=$("#categoryId").combobox("getValue");
	var checkUnitVal=$("#checkUnit").val();
	var deviceIdRow=$("#deviceId").combogrid("grid").datagrid("getSelected");
	var checkDateVal=$("#checkDate").val();
	var checkCountVal=$("#checkCount").combobox("getValue");
	var resultVal=$("#result").val();
	
	if(isEmpty(bigCateIdVal)){
		$pjq.messager.alert("提示","部门分类不能为空！","info");return;
	}
	if(isEmpty(categoryIdVal)){
		$pjq.messager.alert("提示","设备类型不能为空！","info");return;
	}
	if(isEmpty(deviceIdRow)){
		$pjq.messager.alert("提示","设备名称不能为空！","info");return;
	}
	if(isEmpty(checkUnitVal)){
		$pjq.messager.alert("提示","校验单位不能为空！","info");return;
	}
	if(isEmpty(checkDateVal)){
		$pjq.messager.alert("提示","校验时间不能为空！","info");return;
	}
	if(isEmpty(checkCountVal)){
		$pjq.messager.alert("提示","校验次数不能为空！","info");return;
	}
	if(isEmpty(resultVal)){
		$pjq.messager.alert("提示","校验结果不能为空！","info");return;
	}
	
	$.post(base+"/"+path+"/upd"+suffix,$("#frm").serialize(),function(data){
			if(data){
				$pjq.messager.alert("提示","提交成功！","info");
				$grid.datagrid("load");
				$dialog.dialog("destroy");
			}else{
				$pjq.messager.alert("提示","提交失败！","error");
			}
	},"json");
};
$(document).ready(function(){
	
	var categoryUrl=base+"/category/getData"+suffix+"?bigCateId=";
	$("#bigCateId").combobox({
		onChange:function(newValue,oldValue){
			$("#categoryId").combobox("clear");
			$("#deviceId").combogrid("clear");
			$("#categoryId").combobox("reload",categoryUrl+newValue);
		}
	});
	$("#categoryId").combobox({
		valueField:"id",
		textField:"text",
		url:$("#bigCateId").val()==null?categoryUrl:categoryUrl+$("#bigCateId").val(),
		panelHeight:200,
		required:true,
		editable:false,
		disabled:true,
		onChange:function(newValue,oldValue){
			$("#deviceId").combogrid("clear");
			$("#deviceId").combogrid("grid").datagrid("reload",{categoryId:newValue,checkType:6});
		},
		loadFilter:function(data){
			var newData=[];
			var rows=data.rows;
			if(data!=null&&rows!=null&&rows.length>0){
				for(var i=0,len=rows.length;i<len;i++){
					newData.push({id:rows[i].id,text:rows[i].categoryName});
				}
			}
			return newData;
		}
	});
	
	$("#deviceId").combogrid({
	    panelWidth:450,
	    idField:"id",
	    textField:"name",
	    method:"post",
	    rownumbers:true,
		pagination:true,
		editable:false,
	    queryParams:{categoryId:checkRecordCateIdVal,checkType:6},
	    url:base+"/device/getData"+suffix,
	    columns:[[
	        {field:"id",title:"id",hidden:true,width:0},
	        {field:"name",title:"设备名称",width:200},
	        {field:"no",title:"编号",width:100},
	        {field:"deptId",title:"部门id",hidden:true,formatter:formatDeptId},
	        {field:"deptName",title:"所属部门",width:100,formatter:formatDept}
	    ]],
	    onChange:function(newValue,oldValue){
	    	$("#checkCount").combobox("clear");
	    	$("#checkCount").combobox("reload",base+"/checkRecord/getCountMap"+suffix+"?deviceId="+newValue);
	    }
	});
	
	$("#checkCount").combobox({
		valueField:"count",
		textField:"times",
		editable:false,
		panelHeight:"auto"
	});
	
	function formatDept(value,row,index){
		if(row!=null){
			return row.dept.deptName;			
		}
		return null;
	}
	
	function formatDeptId(value,row,index){
		if(row!=null){
			return row.dept.id;			
		}
		return null;
	}
});