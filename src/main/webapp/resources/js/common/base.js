String.prototype.startWith=function(str){     
  var reg=new RegExp("^"+str);     
  return reg.test(this);        
};

String.prototype.endWith=function(str){     
  var reg=new RegExp(str+"$");     
  return reg.test(this);        
};

/**
 * 动态加载css和js
 * 使用: 
 * DynamicLoading.css("../css/blue.css");
    //动态加载JS
    DynamicLoading.js("../js/moment.js", "GBK");
    DynamicLoading.js("../js/moment.js", "UTF-8");
 * */
DynamicLoading = {
	    css: function(path){  //动态加载CSS
	        if(!path || path.length === 0){
	            throw new Error('CSS "path" is required !');
	        }
	        var head = document.getElementsByTagName('head')[0];
	        $(head).append("<link type='text/css' rel='stylesheet' href='"+path+"' />");
	    },
	    js: function(path, charset){  //动态加载JS
	        if(!path || path.length === 0){
	            throw new Error('JS "path" is required !');
	        }
	        var head = document.getElementsByTagName('head')[0];
	        $(head).append("<script type='text/javascript' language='javascript' src='"+path+"' charset='"+charset+"' /></script>");
	    }
}

/**
 * 根据日期格式，将字符串转换成Date对象。 
格式：yyyy-年，MM-月，dd-日，HH-时，mm-分，ss-秒。 
（格式必须写全，例如:yy-M-d，是不允许的，否则返回null；格式与实际数据不符也返回null。） 
默认格式：yyyy-MM-dd HH:mm:ss,yyyy-MM-dd。*/ 

function getDateByFormat(str){
	var format;
	var y,M,d,H,m,s,yi,Mi,di,Hi,mi,si;
	if((arguments[1] + "") == "undefined") format = "yyyy-MM-dd HH:mm:ss";
	else format = arguments[1];
	yi = format.indexOf("yyyy");
	Mi = format.indexOf("MM");
	di = format.indexOf("dd");
	Hi = format.indexOf("HH");
	mi = format.indexOf("mm");
	si = format.indexOf("ss");
	if(yi == -1 || Mi == -1 || di == -1) return null;
	else{
		y = parseInt(str.substring(yi, yi+4));
		M = parseInt(str.substring(Mi, Mi+2));
		d = parseInt(str.substring(di, di+2));
	}
	if(isNaN(y) || isNaN(M) || isNaN(d)) return null;
	if(Hi == -1 || mi == -1 || si == -1) return new Date(y, M-1, d);
	else{
		H = str.substring(Hi, Hi+4);
		m = str.substring(mi, mi+2);
		s = str.substring(si, si+2);
	}
	if(isNaN(parseInt(y)) || isNaN(parseInt(M)) || isNaN(parseInt(d))) return new Date(y, M-1, d);
	else return new Date(y, M-1, d,H, m, s);
}

/**
 * 格式化日期
 * @param format
 * @returns
 */
Date.prototype.format = function(format) {
	var o = {
		"M+" : this.getMonth() + 1, // month
		"d+" : this.getDate(), // day
		"h+" : this.getHours(), // hour
		"m+" : this.getMinutes(), // minute
		"s+" : this.getSeconds(), // second
		"q+" : Math.floor((this.getMonth() + 3) / 3), // quarter
		"S" : this.getMilliseconds()
	};

	if (/(y+)/.test(format)) {
		format = format.replace(RegExp.$1, (this.getFullYear() + "")
				.substr(4 - RegExp.$1.length));
	}

	for ( var k in o) {
		if (new RegExp("(" + k + ")").test(format)) {
			format = format.replace(RegExp.$1, RegExp.$1.length == 1 ? o[k]
					: ("00" + o[k]).substr(("" + o[k]).length));
		}
	}
	return format;
};

/**
 * 判断对象是否为空
 * @param obj
 * @returns {Boolean}
 */
function isEmpty(obj){
	var flg=false;
	if(obj==null||obj==undefined||$.trim(obj)==""){
		flg=true;
	}
	return flg;
}

/**
 *查看提示框
 */
function viewAlert(){
	parent.$.messager.alert("提示","请选择一行数据进行查看！","info");
}

/**
 *修改提示框
 */
function updAlert(){
	parent.$.messager.alert("提示","请选择一行数据进行修改！","info");
}

/**
 * 未选择提示框
 */
function unSelectedAlert(){
	parent.$.messager.alert("提示","请先选择数据再进行操作！","info");
}
/**
 * 提示选择一行
 */
function selectedOneAlert(){
	parent.$.messager.alert("提示","请先选择一行数据再进行操作！","info");
}

/**
 *格式化时间类型
 */
function formatDate(value,row,index){
	return new Date(value).format("yyyy-MM-dd hh:mm:ss");
}
/**
 * 格式化描述
 */
function formatDescr(value,row,index){
	if(value==null||value==""){
		return "";
	}else if(value.length>10){
		return "<a id='tip' href='#' class='note' title='"+value+"'>"+value.substring(0,10)+"...</a>";
	}else{
		return value;
	}
}

/**
 * 获取选中的行
 * @returns
 */
function getSelectedRows(){
	var rows=grid.datagrid("getSelections");
	if(rows==null||rows.length<1){
		unSelectedAlert();
		return null;
	}else{
		return rows;
	}
}

/**
 * 获取单个id
 * @param val
 * @returns
 */
function getId(val){
	var id="";
	if(val!=null&&(typeof(val)=="string")){
		id=val;
	}else{
		var rows=getSelectedRows();
		if(rows!=null){
			if(rows.length>1){
				selectedOneAlert();
			}else{
				id=rows[0].id;
			}
		}
	}
	return id;
}

/**
 * 获取id的集合
 * @param val
 * @returns
 */
function getIds(val){
	var ids="";
	if(val!=null&&(typeof(val)=="string")){
		ids=val;
	}else{
		var rows=getSelectedRows();
		if(rows!=null){
			for(var i=0,len=rows.length;i<len;i++){
				ids+=rows[i].id+",";
			}
			ids=ids.substring(0,ids.length-1);
		}
	}
	return ids;
}


/**
 * 修改选中的
 * @param val
 */
function updSelected(val){
	var id=getId(val);
	if(id!=""){
		var dialog =null;
		dialog = parent.my.modalDialog({
			title:"修改",
			height:490,
			width:630,
			resizable:true,
			url:base+rootPath+"/toUpd/"+id+suffix,
			buttons:[{
				text : "保存",
				iconCls : "icon-ok",
				handler:function(){
					dialog.find("iframe").get(0).contentWindow.submitForm(dialog,grid,parent.$);
				}
			}, {
				text : "取消",
				iconCls : "icon-cancel",
				handler : function() {
					dialog.dialog("destroy");
				}
			}]
		});
	}
}

/**
 * 查看选中的
 * @param val
 */
function viewSelected(val){
	var id=getId(val);
	if(id!=""){
		var url=base+rootPath+"/view/"+id+suffix;
		if(viewTarget!=""){
			window.open(url);
			return;
		}
		var dialog =null;
		dialog = parent.my.modalDialog({
			title:"查看",
			height:490, 
			width:650,
			resizable:true,
			url:url,
			buttons:[{
				text:"关闭",
				iconCls:"icon-ok",
				handler:function(){
					dialog.dialog("destroy");
				}
			}]
		});
	}
}

/**
 * 删除选中的
 * @param val
 */
function delSelected(val){
	var ids=getIds(val);
	if(ids!=""){
		parent.$.messager.confirm("确认","您确认要删除记录吗？",function(r){
			if(r){
				$.post(base+rootPath+"/del"+suffix,{ids:ids},function(data){
					if(data==true){
						parent.$.messager.alert("提示","删除成功！","info");
						if(grid!=null&&grid!=undefined){
							try{
								grid.datagrid("reload");
								grid.datagrid("clearSelections");
							}catch(e){
								
							}
							try{
								grid.treegrid("reload");
								grid.treegrid("clearSelections");
							}catch(e){
								
							}
						}
					}else{
						parent.$.messager.alert("提示","删除失败！","error");
					}
				},"json");
			}
		});
	}
}

/**
 * 添加
 */
function toAdd(){
	var dialog=null;	
	dialog = parent.my.modalDialog({
		title:"添加",
		height:490, 
		width:630,
		resizable:true,
		url:base+rootPath+"/toAdd"+suffix,
		buttons:[{
			text : "保存",
			iconCls : "icon-ok",
			handler:function(){
				dialog.find("iframe").get(0).contentWindow.submitForm(dialog,grid,parent.$);
			}
		}, {
			text : "取消",
			iconCls : "icon-cancel",
			handler : function() {
				dialog.dialog("destroy");
			}
		}]
	});
}

function formatCount(value,row,index){
	if(row==null){
		return null;
	}
	return "第"+transNum2Chinese(row.count)+"次";		
}

function transNum2Chinese(num) {
	if (!/^\d*(\.\d*)?$/.test(num)) {
		return false;
	}
	var AA = new Array("零", "一", "二", "三", "四", "五", "六", "七", "八", "九");
	var BB = new Array("", "十", "百", "千", "万", "亿", "点", "");
	var a = ("" + num).replace(/(^0*)/g, "").split("."), k = 0, re = "";
	for (var i = a[0].length - 1; i >= 0; i--) {
		switch (k) {
		case 0:
			re = BB[7] + re;
			break;
		case 4:
			if (!new RegExp("0{4}\\d{" + (a[0].length - i - 1) + "}$")
					.test(a[0]))
				re = BB[4] + re;
			break;
		case 8:
			re = BB[5] + re;
			BB[7] = BB[5];
			k = 0;
			break;
		}
		if (k % 4 == 2 && a[0].charAt(i) == "0" && a[0].charAt(i + 2) != "0")
			re = AA[0] + re;
		if (a[0].charAt(i) != 0)
			re = AA[a[0].charAt(i)] + BB[k % 4] + re;
		k++;
	}
	if (a.length > 1) {
		re += BB[6];
		for (var i = 0; i < a[1].length; i++)
			re += AA[a[1].charAt(i)];
	}
	return re;
}