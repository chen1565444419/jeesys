/**
 * 添加cookie
 * @param name
 * @param value
 */
function setCookie(name,value){ 
    var Days = 365; 
    var exp  = new Date();
    exp.setTime(exp.getTime() + Days*24*60*60); 
    document.cookie = name + "="+ escape (value) + ";expires=" + exp.toLocaleDateString();
}

/**
 * 获取cookie
 * @param name
 * @returns
 */
function getCookie(name){ 
    var arr,reg=new RegExp("(^| )"+name+"=([^;]*)(;|$)"); 
    if(arr=reg.test(document.cookie)){
    	return unescape(arr[2]); 
    }
    else{
    	return null; 
    } 
} 


/**
 * 删除cookie
 * @param name
 */
function delCookie(name){ 
    var exp = new Date(); 
    exp.setTime(exp.getTime() - 1); 
    var cval=getCookie(name); 
    if(cval!=null){
    	document.cookie= name + "="+cval+";expires="+exp.toLocaleDateString();
    } 
} 