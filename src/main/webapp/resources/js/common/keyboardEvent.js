/**
只能输入数字
*/
function keyUp(value){
	return value.replace(/[^\d]/g,'');
}

/**
 * 只能粘贴数字
 */
function beforePaset(){
	var clipData=window.clipboardData;
	clipData.setData('text',clipData.getData('text').replace(/[^\d]/g,''));
}

/**
 * 禁止回车提交表单
 * @param evt
 * @returns {Boolean}
 */
function forbidEnter(evt){
	 evt = (evt) ? evt : ((window.event) ? window.event : ""); //兼容IE和Firefox获得keyBoardEvent对象
	 var key = evt.keyCode?evt.keyCode:evt.which; //兼容IE和Firefox获得keyBoardEvent对象的键值
	 if(key == 13){ //判断是否是回车事件。
	   return false;
	 }
}