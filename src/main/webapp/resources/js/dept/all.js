var grid=null;
$(document).ready(function(){
	grid=$("#resultTable").datagrid({
		 height:$(document).height(),
		 url:base+rootPath+"/getData"+suffix,
		 queryParams:{},
		 sortName:"seq",
		 sortOrder:"desc",
		 idField:"id",
		 toolbar:toolbar,
		 columns:[[
		       {field:"no",checkbox:true},
		       {field:"id",title:"id",width:100,hidden:true,checkbox:true},
		       {field:"deptName",title:"部门名称",align:"center",sortable:true},
		       {field:"seq",title:"排序序号",align:"center",sortable:true,hidden:true},
		       {field:"createTime",title:"创建时间",align:"center",sortable:true,formatter:formatDate},
		       {field:"operate",title:"操作",align:"center",formatter:operateFunction}
		 ]]  
	});
});
	
    /**
     * 删除选中行
     */
	function delMySelected(val){
		var ids=getIds(val);
		if(ids!=""){
			parent.$.messager.confirm("确认","您确认要删除记录吗？",function(r){
				if(r){
					$.post(base+rootPath+"/checkExistUser"+suffix,{ids:ids},function(data){
						if(data==true){
							parent.$.messager.alert("提示","部门下面有相应的用户,应先删除用户后再删除部门！","error");
						}else{
							$.post(base+rootPath+"/del"+suffix,{ids:ids},function(data){
								if(data==true){
									parent.$.messager.alert("提示","删除成功！","info");
									grid.datagrid("reload");
									grid.datagrid("clearSelections");
								}else{
									parent.$.messager.alert("提示","删除失败！","error");
								}
							},"json");
						}
					},"json");
				}
			});
		}
	}