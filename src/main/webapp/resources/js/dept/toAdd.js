var submitForm = function($dialog,$grid,$pjq) {
	 var deptNameVal=$("#deptName").val();
	 var seqVal=$("#seq").val();
	 
	 if($.trim(deptNameVal)==""){$pjq.messager.alert("提示","部门名称不能为空！","info"); return;}
	 var deptdata={deptName:deptNameVal,seq:seqVal};
	 $.post(base+"/dept/checkDeptName.shtml?_d="+new Date().toTimeString(),"deptName="+deptNameVal,function(data){
		 	if(data){
				$pjq.messager.alert("提示","该部门名称已存在！","info");
			}else{
				 $.post(base+"/dept/add.shtml?_d="+new Date().toTimeString(),deptdata,function(data){
						if(data){
							$pjq.messager.alert("提示","提交成功！","info");
							$grid.datagrid("load");
							$dialog.dialog("destroy");
						}else{
							$pjq.messager.alert("提示","提交失败！","error");
						}
				},"json");
			}
	 },"json");
};