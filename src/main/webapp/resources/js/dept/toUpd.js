var submitForm = function($dialog,$grid,$pjq) {
	 var deptNameVal=$("#deptName").val();
	 var seqVal=$("#seq").val();
	 var idVal=$("#id").val();
	 if($.trim(deptNameVal)==""){$pjq.messager.alert("提示","部门名称不能为空！","info"); return;}
	 var deptdata={id:idVal,deptName:deptNameVal,seq:seqVal};
	 $.post(base+"/dept/checkDeptName.shtml?_d="+new Date().toTimeString(),"deptName="+deptNameVal+"&id="+idVal,function(data){
			if(data){
				$pjq.messager.alert("提示","该名称已存在！","info");
			}else{
				 $.post(base+"/dept/upd.shtml?_d="+new Date().toTimeString(),deptdata,function(data){
						if(data){
							$pjq.messager.alert("提示","修改成功！","info");
							$grid.datagrid("load");
							$dialog.dialog("destroy");
						}else{
							$pjq.messager.alert("提示","提交失败！","error");
						}
				},"json");
			}
	 },"json");
};