var grid=null;
$(document).ready(function(){
	grid=$("#resultTable").datagrid({
		 height:$(document).height(),
		 url:base+rootPath+"/getData"+suffix,
		 queryParams:{},
		 sortName:"seq",
		 sortOrder:"ASC",
		 idField:"id",
		 toolbar:toolbar,
		 columns:[[
		       {field:"no",checkbox:true},
		       {field:"id",title:"id",hidden:true},
		       {field:"name",title:"检修项目名称",align:"center",sortable:true,formatter:formatDescr},
		       {field:"standard",title:"标准值",align:"center",sortable:false,formatter:formatDescr},
		       {field:"notice",title:"注意事项",align:"center",sortable:false,formatter:formatDescr},
		       {field:"bigCateName",title:"部门分类",align:"center",sortable:false,formatter:formatBigCateName},
		       {field:"categoryName",title:"设备类型",align:"center",sortable:false,formatter:formatCate},
		       {field:"seq",title:"排序序号",align:"center",sortable:true,hidden:true},
		       {field:"createTime",title:"创建时间",align:"center",sortable:true},
		       {field:"operate",title:"操作",align:"center",formatter:operateFunction}
		       ]]
	});
	
	
	$("#bigCateId").combogrid({
		url:base+"/bigCate/getData"+suffix,    
	    panelWidth:155,
	    idField:"id",    
	    textField:"bigCateName",
	    fit:true,
	    editable:false,
	    rownumbers:false,
	    pagination:false,
	    columns:[[    
	            {field:"id",title:"id",hidden:true,width:0},
	            {field:"bigCateName",title:"部门分类名称",width:150}
	    ]],
	    onChange:function(newValue,oldValue){
	    	$("#categoryId").combogrid("clear");
	    	var g= $("#categoryId").combogrid("grid");
	    	g.datagrid("reload",{bigCateId:newValue});
	    }
	});
	
	
	$("#categoryId").combogrid({    
	    url:base+"/category/getData"+suffix,    
	    panelWidth:210,
	    idField:"id",    
	    textField:"categoryName",
	    fit:true,
	    editable:false,
	    rownumbers:false,
	    pagination:false,
	    columns:[[    
	            {field:"id",title:"id",hidden:true,width:0},
	            {field:"categoryName",title:"类型名称",width:100},
	            {field:"description",title:"描述",width:100}
	    ]],
	    loadFilter:function(data){
	    	if(data.total==0){
	    		return data;
	    	}
	    	data.total=data.total+1;
	    	data.rows.push({id:0,categoryName:"请选择",select:true});
	    	data.rows.sort();
	    	return data;
	    }
	});
	
	$(".datagrid-toolbar").append($("#searchbar"));
	
	$("#searchBtn").click(function(){
		var bigCateGrid= $("#bigCateId").combogrid("grid");// 获取数据表格对象
		var bigCaterow = bigCateGrid.datagrid("getSelected");
		if(bigCaterow==null){
			parent.$.messager.alert("提示","请选择部门分类！","info");
			return;
		}
		var g= $("#categoryId").combogrid("grid");// 获取数据表格对象
		var row = g.datagrid("getSelected");
		var param=null;
		if(row!=null&&row.id!=0){
			param={bigCateId:bigCaterow.id,categoryId:row.id};
		}else{
			param={bigCateId:bigCaterow.id};
		}
		grid.datagrid("reload",param);
	});
	
});

function formatCate(value,row,index){
	return row.category.categoryName;		
}
function formatBigCateName(value,row,index){
	return row.category.bigCate.bigCateName;		
}
