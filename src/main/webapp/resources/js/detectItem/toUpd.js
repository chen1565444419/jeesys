var submitForm = function($dialog,$grid,$pjq) {
	 if($("#frm").form("validate")){
		 $.post(base+"/"+path+"/upd"+suffix,$("#frm").serialize(),function(data){
				if(data){
					$pjq.messager.alert("提示","提交成功！","info");
					$grid.datagrid("load");
					$dialog.dialog("destroy");
				}else{
					$pjq.messager.alert("提示","提交失败！","error");
				}
		},"json");
	 }
};

$(document).ready(function(){
	var categoryUrl=base+"/category/getData"+suffix+"?bigCateId=";
	$("#bigCateId").combobox({
		onChange:function(newValue,oldValue){
			$("#categoryId").combobox("clear");
			$("#categoryId").combobox("reload",categoryUrl+newValue);
		}
	});
	$("#categoryId").combobox({
		valueField:"id",
		textField:"text",
		url:$("#bigCateId").val()==null?categoryUrl:categoryUrl+$("#bigCateId").val(),
		panelHeight:200,
		required:true,
		editable:false,
		loadFilter:function(data){
			var newData=[];
			var rows=data.rows;
			if(data!=null&&rows!=null&&rows.length>0){
				for(var i=0,len=rows.length;i<len;i++){
					newData.push({id:rows[i].id,text:rows[i].categoryName});
				}
			}
			return newData;
		}
	});
});