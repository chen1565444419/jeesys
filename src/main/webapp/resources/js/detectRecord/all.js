var grid=null;
$(document).ready(function(){
	grid=$("#resultTable").datagrid({
		 height:$(document).height(),
		 url:base+rootPath+"/getData"+suffix,
		 queryParams:{},
		 sortName:"createTime",
		 sortOrder:"ASC",
		 idField:"id",
		 toolbar:toolbar,
		 columns:[[
		       {field:"no",checkbox:true},
		       {field:"id",title:"id",hidden:true},
		       {field:"deviceId",title:"设备名称",align:"center",sortable:true,formatter:formatDevice},
		       {field:"device.no",title:"设备编号",align:"center",sortable:false,formatter:formatNo},
		       {field:"detectItem.name",title:"检修项目名称",align:"center",sortable:false,formatter:formatItem},
		       {field:"result",title:"检修结果",align:"center",sortable:false,formatter:formatResult},
		       {field:"detectUserName",title:"检修人员",align:"center",sortable:true/*formatter:formatDetectUser*/},
		       {field:"type",title:"检修周期",align:"center",sortable:false,formatter:formatType},
		       {field:"detectDate",title:"检修时间",align:"center",sortable:true,formatter:formatDetectDate},
		       {field:"count",title:"检修次数",align:"center",sortable:true,formatter:formatCount},
		       {field:"createdBy",title:"录入人员",align:"center",sortable:true,formatter:formatCreatedUser},
		       {field:"createTime",title:"录入时间",align:"center",sortable:true},
		       {field:"operate",title:"操作",align:"center",formatter:operateFunction}
		       ]]
	});
});

function formatDevice(value,row,index){
	if(row==null){
		return null;
	}
	return row.device.name;		
}

function formatItem(value,row,index){
	if(row.detectItem.name==null){
		return null;
	}else if(row.detectItem.name.length>10){
		return "<a id='tip' href='#' class='note' title='"+row.detectItem.name+"'>"+row.detectItem.name.substring(0,10)+"...</a>";
	}else{
		return row.detectItem.name;
	}
}
function formatResult(value,row,index){
	if(value==null){
		return null;
	}else if(value.length>10){
		return "<a id='tip' href='#' class='note' title='"+value+"'>"+value.substring(0,10)+"...</a>";
	}else{
		return value;
	}
}

function formatNo(value,row,index){
	if(row==null){
		return null;
	}
	return row.device.no;		
}

function formatDetectDate(value,row,index){
	if(row==null){
		return null;
	}
	return value.substring(0,value.indexOf(" "));
}

function formatCreatedUser(value,row,index){
	if(row==null){
		return null;
	}
	return row.createdUser.realName;
}

function formatDetectUser(value,row,index){
	if(row==null){
		return null;
	}
	return row.detectUser.realName;
}

function formatCate(value,row,index){
	if(row==null){
		return null;
	}
	return row.category.categoryName;		
}
function formatType(value,row,index){
	if(row==null){
		return null;
	}
	var result="日检";
	switch(row.device.type){
		case 1:
			result="日检";
			break;
		case 2:
			result="周检";
			break;
		case 3:
			result="月检";
			break;
		case 4:
			result="季检";
			break;
		case 5:
			result="年检";
			break;
		case 6:
			result="其它";
			break;
	}
	return result;
}
