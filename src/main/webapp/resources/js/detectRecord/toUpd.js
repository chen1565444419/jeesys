var submitForm = function($dialog,$grid,$pjq) {
	var bigCateIdVal=$("#bigCateId").combobox("getValue");
	var categoryIdVal=$("#categoryId").combobox("getValue");
	//var detectIdVal=$("#detectId").combobox("getValue");
	var detectIdVal=$("#detectUserName").val();
	var deviceIdRow=$("#deviceId").combogrid("grid").datagrid("getSelected");
	var detectDateVal=$("#detectDate").val();
	var detectItemIdVal=$("#detectItemId").combogrid("grid").datagrid("getSelected");
	if(isEmpty(bigCateIdVal)){
		$pjq.messager.alert("提示","部门分类不能为空！","info");return;
	}
	if(isEmpty(categoryIdVal)){
		$pjq.messager.alert("提示","设备类型不能为空！","info");return;
	}
	if(isEmpty(detectIdVal)){
		$pjq.messager.alert("提示","检修人员不能为空！","info");return;
	}
	if(isEmpty(deviceIdRow)){
		$pjq.messager.alert("提示","设备名称不能为空！","info");return;
	}
	if(isEmpty(detectDateVal)){
		$pjq.messager.alert("提示","检修时间不能为空！","info");return;
	}
	if(isEmpty(detectItemIdVal)){
		$pjq.messager.alert("提示","检修项目不能为空！","info");return;
	}
	$.post(base+"/"+path+"/upd"+suffix,$("#frm").serialize(),function(data){
			if(data){
				$pjq.messager.alert("提示","提交成功！","info");
				$grid.datagrid("load");
				$dialog.dialog("destroy");
			}else{
				$pjq.messager.alert("提示","提交失败！","error");
			}
	},"json");
};
$(document).ready(function(){
	var categoryUrl=base+"/category/getData"+suffix+"?rows="+gridParams.pageSize+"&bigCateId=";
	$("#categoryId").combobox({
		valueField:"id",
		textField:"text",
		url:$("#bigCateId").val()==null?categoryUrl:categoryUrl+$("#bigCateId").val(),
		panelHeight:200,
		required:true,
		editable:false,
		disabled:true,
		loadFilter:function(data){
			var newData=[];
			var rows=data.rows;
			if(data!=null&&rows!=null&&rows.length>0){
				for(var i=0,len=rows.length;i<len;i++){
					newData.push({id:rows[i].id,text:rows[i].categoryName});
				}
			}
			return newData;
		}
	});
	
		$("#deviceId").combogrid({
			panelWidth:450,
			idField:"id",
			textField:"name",
			method:"post",
			rownumbers:true,
			pagination:true,
			editable:false,
			queryParams:{categoryId:$("#categoryId").combobox("getValue")},
			url:base+"/device/getData"+suffix,
			columns:[[
			          {field:"id",title:"id",hidden:true,width:0},
			          {field:"name",title:"设备名称",width:200},
			          {field:"no",title:"编号",width:100},
			          {field:"deptId",title:"部门id",hidden:true,formatter:formatDeptId},
			          {field:"deptName",title:"所属部门",width:100,formatter:formatDept}
			          ]],
			 onChange:function(newValue,oldValue){
			       var row=$("#deviceId").combogrid("grid").datagrid("getSelected");
			       var deptId=null;
			       if(row!=null){
			        	deptId=row.deptId;
			       }
			       //$("#detectId").combobox("clear");
			       //$("#detectId").combobox("reload",base+"/user/getUserByDept"+suffix+"?deptId="+deptId);
			}
		});

		$("#detectItemId").combogrid({
			panelWidth:450,
			idField:"id",
			textField:"name",
			method:"post",
			rownumbers:true,
			pagination:true,
			editable:false,
			queryParams:{categoryId:$("#categoryId").combobox("getValue")},
			url:base+"/detectItem/getData"+suffix,
			columns:[[
			          {field:"id",title:"id",hidden:true,width:0},
			          {field:"name",title:"检修项目",width:150},
			          {field:"standard",title:"标准值",width:150},
			          {field:"notice",title:"注意事项",width:150}
			          ]]
		});
	
	/*
	$("#detectId").combobox({    
			valueField:"id",
			textField:"realName",
			editable:false,
			panelHeight:"auto",
			url:base+"/user/getUserByDept"+suffix+"?deptId="+deptIdVal
	});
	*/
	function formatDept(value,row,index){
		if(row!=null){
			return row.dept.deptName;			
		}
		return null;
	}
	
	function formatDeptId(value,row,index){
		if(row!=null){
			return row.dept.id;			
		}
		return null;
	}
});