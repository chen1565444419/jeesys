var grid=null;
$(document).ready(function(){
	grid=$("#resultTable").datagrid({
		 height:$(document).height(),
		 url:base+rootPath+"/getData"+suffix,
		 queryParams:{},
		 sortName:"seq",
		 sortOrder:"ASC",
		 idField:"id",
		 toolbar:toolbar,
		 columns:[[
		       {field:"rowNum",checkbox:true},
		       {field:"id",title:"id",hidden:true},
		       {field:"name",title:"名称",align:"center",sortable:true},
		       {field:"no",title:"编号",align:"center",sortable:true},
		       {field:"bigCateName",title:"所属部门分类",align:"center",sortable:false,formatter:formatBigCate},
		       {field:"categoryName",title:"类型",align:"center",sortable:true,formatter:formatCate},		       
		       {field:"deptName",title:"所属部门",align:"center",sortable:true,formatter:formatDept},
		       {field:"producer",title:"生产厂家",align:"center",sortable:true},
		       {field:"modelNumber",title:"型号",align:"center",sortable:true},
		       {field:"usedSite",title:"使用地点",align:"center",sortable:true},
		       {field:"lineType",title:"线别",align:"center",sortable:true},
		       {field:"type",title:"检修周期",align:"center",sortable:true,formatter:formatType},
		       {field:"componentName",title:"定期更换备件",align:"center",sortable:true,formatter:formatComponents},
		       {field:"startDate",title:"开始时间",align:"center",sortable:false,formatter:formatStartDate},
		       {field:"checkType",title:"校验周期",align:"center",sortable:true,formatter:formatCheckType},
		       {field:"createTime",title:"创建时间",align:"center",sortable:true,formatter:formatDate},
		       {field:"operate",title:"操作",align:"center",formatter:operateFunction}
		       ]]
	});
	
	$("#bigCateId").combogrid({
		url:base+"/bigCate/getData"+suffix,    
	    panelWidth:155,
	    idField:"id",    
	    textField:"bigCateName",
	    fit:true,
	    editable:false,
	    rownumbers:false,
	    pagination:false,
	    columns:[[    
	            {field:"id",title:"id",hidden:true,width:0},
	            {field:"bigCateName",title:"部门分类名称",width:150}
	    ]],
	    onChange:function(newValue,oldValue){
	    	$("#categoryId").combogrid("clear");
	    	var g= $("#categoryId").combogrid("grid");
	    	g.datagrid("reload",{bigCateId:newValue});
	    }
	});
	
	$("#categoryId").combogrid({    
	    url:base+"/category/getData"+suffix,    
	    panelWidth:155,
	    idField:"id",    
	    textField:"categoryName",
	    fit:true,
	    editable:false,
	    rownumbers:false,
	    pagination:false,
	    columns:[[    
	            {field:"id",title:"id",hidden:true,width:0},
	            {field:"categoryName",title:"类型名称",width:140}
	    ]],
	    loadFilter:function(data){
	    	if(data.total==0){
	    		return data;
	    	}
	    	data.total=data.total+1;
	    	data.rows.push({id:0,categoryName:"请选择",select:true});
	    	data.rows.sort();
	    	return data;
	    }
	});
	
	$(".datagrid-toolbar").append($("#searchbar"));
	
	$("#searchBtn").click(function(){
		var bigCateGrid= $("#bigCateId").combogrid("grid");// 获取数据表格对象
		var bigCaterow = bigCateGrid.datagrid("getSelected");
		if(bigCaterow==null){
			parent.$.messager.alert("提示","请选择部门分类！","info");
			return;
		}
		var g= $("#categoryId").combogrid("grid");// 获取数据表格对象
		var row = g.datagrid("getSelected");
		if(row!=null){
			var param=null;
			if(row.id==0){
				param={bigCateId:bigCaterow.id};
			}else{
				param={categoryId:row.id,bigCateId:bigCaterow.id};
			}
			grid.datagrid("reload",param);
		}
	});
	
});
function formatType(value,row,index){
	var result="日检";
	switch(value){
		case 1:
			result="日检";
			break;
		case 2:
			result="周检";
			break;
		case 3:
			result="月检";
			break;
		case 4:
			result="季检";
			break;
		case 5:
			result="年检";
			break;
		case 6:
			result="其它";
			break;
	}
	return result;
}
function formatCheckType(value,row,index){
	var result="日校验";
	switch(value){
	case 1:
		result="日校验";
		break;
	case 2:
		result="周校验";
		break;
	case 3:
		result="月校验";
		break;
	case 4:
		result="季校验";
		break;
	case 5:
		result="年校验";
		break;
	case 6:
		result="其它";
		break;
	}
	return result;
}

function formatStartDate(value,row,index){
	if(value!=null){
		return new Date(value).format("yyyy-MM-dd");		
	}else{
		return "";
	}
}
function formatBigCate(value,row,index){
	return row.category.bigCate.bigCateName;
}
function formatDept(value,row,index){
	return row.dept.deptName;
}

function formatCate(value,row,index){
	return row.category.categoryName;
}

function formatComponents(value,row,index){
	var components=row.components;
	var msg="";
	if(components==null||components==undefined){
		return msg;
	}
	var len=components.length;
	if(len>0){
		var name=null;
		for(var i=0;i<len;i++){
			name=components[i].name;
			if(name!=undefined){
				msg+=name+",";				
			}
		}
		msg=msg.length>0?msg.substring(0,msg.length-1):msg;
		if(msg.length>4){
			msg="<a href='#' class='note' title='"+msg+"'>"+msg.substring(0,2)+"</a>";
		}
	}
	return msg;
}

function toMyAdd(){
	var dialog=null;	
	dialog = parent.my.modalDialog({
		title:"添加",
		height:550, 
		width:650,
		resizable:true,
		url:base+rootPath+"/toAdd"+suffix,
		buttons:[{
			text : "保存",
			iconCls : "icon-ok",
			handler:function(){
				dialog.find("iframe").get(0).contentWindow.submitForm(dialog,grid,parent.$);
			}
		}, {
			text : "取消",
			iconCls : "icon-cancel",
			handler : function() {
				dialog.dialog("destroy");
			}
		}]
	});
}

/**
 * 修改选中的
 * @param val
 */
function updMySelected(val){
	var id=getId(val);
	if(id!=""){
		var dialog =null;
		dialog = parent.my.modalDialog({
			title:"修改",
			height:550,
			width:650,
			resizable:true,
			url:base+rootPath+"/toUpd/"+id+suffix,
			buttons:[{
				text : "保存",
				iconCls : "icon-ok",
				handler:function(){
					dialog.find("iframe").get(0).contentWindow.submitForm(dialog,grid,parent.$);
				}
			}, {
				text : "取消",
				iconCls : "icon-cancel",
				handler : function() {
					dialog.dialog("destroy");
				}
			}]
		});
	}
}

/**
 * 删除选中的
 * @param val
 */
function delMySelected(val){
	var ids=getIds(val);
	if(ids!=""){
		parent.$.messager.confirm("确认","您确认要删除记录吗？您确认要删除记录吗？<br />",function(r){
			if(r){
				$.post(base+rootPath+"/del"+suffix,{ids:ids},function(data){
					if(data==true){
						parent.$.messager.alert("提示","删除成功！","info");
						if(grid!=null&&grid!=undefined){
							try{
								grid.datagrid("reload");
								grid.datagrid("clearSelections");
							}catch(e){
								
							}
							try{
								grid.treegrid("reload");
								grid.treegrid("clearSelections");
							}catch(e){
								
							}
						}
					}else{
						parent.$.messager.alert("提示","删除失败！","error");
					}
				},"json");
			}
		});
	}
}

