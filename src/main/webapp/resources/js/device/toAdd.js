var submitForm = function($dialog,$grid,$pjq) {
	 var nameVal=$("#name").val();
	 var noVal=$("#no").val();
	 var bigCateIdVal=$("#bigCateId").combobox("getValue");
	 var categoryIdVal=$("#categoryId").combobox("getValue");
	 var deptIdVal=$("#deptId").combobox("getValue");
	 var compArray=[];
	 var oilArray=[];
		 var compgridRows=null;
		 if($("input[type='radio'][name='component']:checked").val()==="1"){
			 if(!compEndEditing()){$pjq.messager.alert("提示","请先保存定期更换的备件！","info");return;}
			 if(compgrid!=null){
				 compgridRows=$("#compgrid").datagrid("getRows");
				 if(compgridRows.length>0){
					 var comp=null;
					 for(var i=0,len=compgridRows.length;i<len;i++){
						 comp=new Object();
						 comp.id=compgridRows[i].id;
						 comp.count=compgridRows[i].count;
						 comp.cycle=compgridRows[i].cycle;
						 comp.startDate=compgridRows[i].startDate;
						 compArray.push(comp);
					 }
				 }
			 }
		 }
		 
		 var oilgridRows=null;
		 if($("input[type='radio'][name='oil']:checked").val()==="1"){
			 if(!oilEndEditing()){$pjq.messager.alert("提示","请先保存定期润滑项目！","info");return;}
			 if(oilgrid!=null){
				 oilgridRows=$("#oilgrid").datagrid("getRows");
				 if(oilgridRows.length>0){
					 var oil=null;
					 for(var i=0,len=oilgridRows.length;i<len;i++){
						 oil=new Object();
						 oil.id=oilgridRows[i].id;
						 oil.cycle=oilgridRows[i].cycle;
						 oil.startDate=oilgridRows[i].startDate;
						 oilArray.push(oil);
					 }
				 }
			 }		 
		 }
	 
	 
	 if($.trim(nameVal)==""){$pjq.messager.alert("提示","名称不能为空！","info"); return;}
	 if($.trim(noVal)==""){$pjq.messager.alert("提示","编号不能为空！","info"); return;}
	 if($.trim(bigCateIdVal)==""){$pjq.messager.alert("提示","部门分类不能为空！","info"); return;}
	 if($.trim(categoryIdVal)==""){$pjq.messager.alert("提示","所属类型不能为空！","info"); return;}
	 if($.trim(deptIdVal)==""){$pjq.messager.alert("提示","所属部门不能为空！","info");return;}
	 
	 if($("#type").combobox("getValue")!="6"){
		 if($("#startDate").val()==""){
			 $pjq.messager.alert("提示","开始时间不能为空！","info");return; 
		 }
	 }
	 if($("#type").combobox("options").disabled){
		 $("#type").combobox("enable");
	 }
	 if($("#checkType").combobox("options").disabled){
		 $("#checkType").combobox("enable");
	 }
	 $.post(base+"/"+path+"/add"+suffix,
			$("#frm").serialize()+"&compStr="+JSON.stringify(compArray)+"&oilStr="+JSON.stringify(oilArray),
			function(data){
			if(data){
				$pjq.messager.alert("提示","提交成功！","info");
				$grid.datagrid("load");
				$dialog.dialog("destroy");
			}else{
				$pjq.messager.alert("提示","提交失败！","error");
				if($("#type").combobox("options").disabled){
					$("#type").combobox("disable");
				}
				if($("#checkType").combobox("options").disabled){
					$("#checkType").combobox("disable");
				}
			}
	},"json");
};

var compEditIndex=undefined;
var compgrid=null;
var oilgrid=null;
var oilEditIndex=undefined;
$(document).ready(function(){
	var categoryUrl=base+"/category/getData"+suffix+"?rows="+gridParams.pageSize+"&bigCateId=";
	$("#bigCateId").combobox({
		onChange:function(newValue,oldValue){
			$("#categoryId").combobox("clear");
			$("#categoryId").combobox("reload",categoryUrl+newValue);
		}
	});
	$("#categoryId").combobox({
		valueField:"id",
		textField:"text",
		url:$("#bigCateId").val()==null?categoryUrl:categoryUrl+$("#bigCateId").val(),
		panelHeight:200,
		required:true,
		editable:false,
		onSelect:function(record){
			var newText=record["text"];
			if(newText==="仪器"){
				$("#type").combobox("setValue","6");
				$("#type").combobox("disable");
			}else{
				$("#type").combobox("enable");
			}
		},
		loadFilter:function(data){
			if(data==null){
				return [];
			}
			var newData=[];
			var rows=data.rows;
			if(data!=null&&rows!=null&&rows.length>0){
				for(var i=0,len=rows.length;i<len;i++){
					newData.push({id:rows[i].id,text:rows[i].categoryName});
				}
			}
			return newData;
		}
	});

	$("#type").combobox({
		editable:false,
		required:true,
		onChange:function(newValue, oldValue){
			if(newValue==="6"){
				$("#startDate").attr("disabled","disabled");
				if($("#checkType").combobox("getValue")!="6"){
					$("#startDate").removeAttr("disabled");
				}else{
					$("#startDate").attr("disabled","disabled");
				}
				$("#checkType").combobox("enable");
				$("input[type='radio'][name='component']").get(1).checked=true;
				$("input[type='radio'][name='oil']").get(1).checked=true;
				$("#compDiv").hide();
				$("#oilDiv").hide();
				$("#compTr").hide();
			}else{
				$("#checkType").combobox("setValue","6");
				$("#checkType").combobox("disable");
				$("#startDate").removeAttr("disabled");
				$("#compTr").show();
			}
		}
	});
	
	$("#checkType").combobox({
		editable:false,
		value:"6",
		disabled:true,
		onChange:function(newValue, oldValue){
			if(newValue!="6"){
				if($("#type").combobox("getValue")!="6"){
					$("#startDate").removeAttr("disabled");
				}else{
					$("#startDate").removeAttr("disabled");
				}
			}else{
				if($("#type").combobox("getValue")!="6"){
					$("#startDate").removeAttr("disabled");
				}else{
					$("#startDate").attr("disabled","disabled");
				}
			}
		}
	});
	$("input[name='component']").change(function(){
		if($(this).val()==="1"){
			$("#compDiv").show();
			if(compgrid==null){
				var parentWidth=$(this).parent().css("width");
				parentWidth=parentWidth.substring(0,parentWidth.length-2);
				compgrid=$("#compgrid").datagrid({
					singleSelect: true,
					rownumbers:false,
					pagination:false,
					autoRowHeight:false,
					fitColumns:false,
					width:parentWidth,
					//panelHeight:"auto",
					height:200,
					toolbar:"#compTb",
					columns:[[
					          	{field:"id",title:"名称",align:"left",width:parentWidth*0.32,
					          		formatter:function(value,row){
					          			return row.name;
					          		},
					          		editor:{
										type:"combobox",
										options:{
											valueField:"id",
											textField:"name",
											method:"post",
											panelHeight:"auto",
											url:base+"/component/getComponents"+suffix,
											required:true
										}
					          		}
					          	},
					          	{field:"count",title:"数量",align:"left",width:parentWidth*0.1,
					          		editor:{
					          			type:"numberbox",
					          			options:{
					          				value:1,
					          				required:true,
					          				min:1,
					          				precision:0
					          			}
					          		}
					          	},
					          	{field:"cycle",title:"周期(天)",align:"left",width:parentWidth*0.2,
					          		editor:{
					          			type:"numberbox",
					          			options:{
					          				value:30,
					          				required:true,
					          				precision:0
					          			}
					          		}
					          	},
					          	{field:"startDate",title:"开始日期",align:"left",width:parentWidth*0.35,
					          		editor:{
					          			type:"datebox",
					          			options:{
					          				editable:false,
					          				required:true
					          			}
					          		}
					          	}
					          ]],
					onClickRow: compOnClickRow,
					onBeforeLoad:function(parama){return true;}
				});
			}
		}else{
			$("#compDiv").hide();
		}
	});
	
	$("input[name='oil']").change(function(){
		if($(this).val()==="1"){
			$("#oilDiv").show();
			if(oilgrid==null){
				var parentWidth=$(this).parent().css("width");
				parentWidth=parentWidth.substring(0,parentWidth.length-2);
				oilgrid=$("#oilgrid").datagrid({
					singleSelect: true,
					rownumbers:false,
					pagination:false,
					autoRowHeight:false,
					fitColumns:false,
					width:parentWidth,
					//panelHeight:"auto",
					height:200,
					toolbar:"#oilTb",
					columns:[[
					          	{field:"id",title:"润滑项目",align:"left",width:parentWidth*0.44,
					          		formatter:function(value,row){
					          			return row.name;
					          		},
					          		editor:{
										type:"combobox",
										options:{
											valueField:"id",
											textField:"name",
											method:"post",
											panelHeight:"auto",
											url:base+"/oil/getAllOil"+suffix,
											required:true
										}
					          		}
					          	},
					          	{field:"cycle",title:"周期(天)",align:"left",width:parentWidth*0.21,
					          		editor:{
					          			type:"numberbox",
					          			options:{
					          				value:30,
					          				required:true,
					          				precision:0
					          			}
					          		}
					          	},
					          	{field:"startDate",title:"开始日期",align:"left",width:parentWidth*0.32,
					          		editor:{
					          			type:"datebox",
					          			options:{
					          				editable:false,
					          				required:true
					          			}
					          		}
					          	}
					          ]],
					onClickRow: oilOnClickRow,
					onBeforeLoad:function(parama){return true;}
				});
			}
		}else{
			$("#oilDiv").hide();
		}
	});
});

function compEndEditing(){
	if (compEditIndex == undefined){return true;}
	if ($("#compgrid").datagrid("validateRow", compEditIndex)){
		var ed = $("#compgrid").datagrid("getEditor", {index:compEditIndex,field:"id"});
		var name = $(ed.target).combobox("getText");
		$("#compgrid").datagrid("getRows")[compEditIndex]["name"] = name;
		$("#compgrid").datagrid("endEdit", compEditIndex);
		compEditIndex = undefined;
		return true;
	} else {
		return false;
	}
}

function appendComp(){
	var date=new Date();
	date.setDate(date.getDate()+1);
	if (compEndEditing()){
		$("#compgrid").datagrid("appendRow",{count:1,cycle:30,startDate:date.format("yyyy-MM-dd")});
		compEditIndex = $("#compgrid").datagrid("getRows").length-1;
		$("#compgrid").datagrid("selectRow", compEditIndex)
				.datagrid("beginEdit", compEditIndex);
	}
}

function removeComp(){
	if (compEditIndex == undefined){return;}
	$("#compgrid").datagrid("cancelEdit", compEditIndex)
			.datagrid("deleteRow", compEditIndex);
	compEditIndex = undefined;
}

function compOnClickRow(index){
	if (compEditIndex != index){
		if (compEndEditing()){
			$("#compgrid").datagrid("selectRow", index)
					.datagrid("beginEdit", index);
			compEditIndex = index;
		} else {
			$("#compgrid").datagrid("selectRow", compEditIndex);
		}
	}
}

function compAccept(){
	if (compEndEditing()){
		$("#compgrid").datagrid("acceptChanges");
	}
}



function oilEndEditing(){
	if (oilEditIndex == undefined){return true;}
	if ($("#oilgrid").datagrid("validateRow", oilEditIndex)){
		var ed = $("#oilgrid").datagrid("getEditor", {index:oilEditIndex,field:"id"});
		var name = $(ed.target).combobox("getText");
		$("#oilgrid").datagrid("getRows")[oilEditIndex]["name"] = name;
		$("#oilgrid").datagrid("endEdit", oilEditIndex);
		oilEditIndex = undefined;
		return true;
	} else {
		return false;
	}
}

function appendOil(){
	if (oilEndEditing()){
		var date=new Date();
		date.setDate(date.getDate()+1);
		$("#oilgrid").datagrid("appendRow",{cycle:30,startDate:date.format("yyyy-MM-dd")});
		oilEditIndex = $("#oilgrid").datagrid("getRows").length-1;
		$("#oilgrid").datagrid("selectRow", oilEditIndex)
				.datagrid("beginEdit", oilEditIndex);
	}
}

function removeOil(){
	if (oilEditIndex == undefined){return;}
	$("#oilgrid").datagrid("cancelEdit", oilEditIndex)
			.datagrid("deleteRow", oilEditIndex);
	oilEditIndex = undefined;
}

function oilOnClickRow(index){
	if (oilEditIndex != index){
		if (oilEndEditing()){
			$("#oilgrid").datagrid("selectRow", index)
					.datagrid("beginEdit", index);
			oilEditIndex = index;
		} else {
			$("#oilgrid").datagrid("selectRow",oilEditIndex);
		}
	}
}

function oilAccept(){
	if (oilEndEditing()){
		$("#oilgrid").datagrid("acceptChanges");
	}
}
