$(document).ready(function(){
	var parentWidth=280;
	$("#compgrid").datagrid({
		singleSelect:true,
		rownumbers:false,
		pagination:false,
		fitColumns:false,
		width:parentWidth,
		//panelHeight:"auto",
		height:150,
		columns:[[
		          	{field:"id",title:"名称",align:"left",width:parentWidth*0.32,
		          		formatter:function(value,row){
		          			return row.name;
		          		}
		          	},
		          	{field:"count",title:"数量",align:"left",width:parentWidth*0.1},
		          	{field:"cycle",title:"周期(天)",align:"left",width:parentWidth*0.2},
		          	{field:"startDate",title:"开始日期",align:"left",width:parentWidth*0.35}
		          ]],
		onBeforeLoad:function(parama){return true;}
	});
	
	if(compData.length>0){
		$("#compgrid").datagrid("loadData",compData);
		$("#compgrid").datagrid("resize");
	}
	oilgrid=$("#oilgrid").datagrid({
		singleSelect: true,
		rownumbers:false,
		pagination:false,
		fitColumns:false,
		width:parentWidth,
		//panelHeight:"auto",
		height:150,
		columns:[[
		          	{field:"id",title:"润滑项目",align:"left",width:parentWidth*0.44,
		          		formatter:function(value,row){
		          			return row.name;
		          		}
		          	},
		          	{field:"cycle",title:"周期(天)",align:"left",width:parentWidth*0.21},
		          	{field:"startDate",title:"开始日期",align:"left",width:parentWidth*0.32}
		          ]],
		onBeforeLoad:function(parama){return true;}
	});
	if(oilData.length>0){
		$("#oilgrid").datagrid("loadData",oilData);
		$("#oilgrid").datagrid("resize");
	}
	$("#typeSpan").html($("#type").combobox("getText"));
	$("#checkTypeSpan").html($("#checkType").combobox("getText"));
	
});
