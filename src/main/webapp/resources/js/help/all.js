$(document).ready(function(){
	$("#content_div").show();
	$("#content_form").hide();
	$("#save").hide();
	$("#cancel").hide();
});

function edit(){
	$("#content_div").hide();
	$("#content_form").show();
	$("#save").show();
	$("#cancel").show();
	$("#edit").hide();
}

function cancel(){
	$("#content_div").show();
	$("#content_form").hide();
	$("#save").hide();
	$("#cancel").hide();
	$("#edit").show();
}
function save(){
	$.post(base+"/help/upd"+suffix,$("#info").serialize(),function(data){
		if(data){
			parent.$.messager.alert("提示","修改成功！","info");
			window.location.reload();
		}else{
			parent.$.messager.alert("提示","修改失败！","error");
		}
	});
}