$(document).ready(function(){
	window.setInterval(setCurrTime,1000);
	var mainTabs=null;
	var tabsMenu=null;
	if(getOs()=="MSIE"){
		$.messager.confirm("提示","您当前使用的是IE浏览器，使用谷歌浏览器可以有较好的体验，是否下载谷歌浏览器?",function(flg){
			if(flg){
				window.open(base+"/resources/uploadFiles/ChromeStandaloneSetup33.0.exe");
			}
		});
	}
	$("#mainLayout").layout("panel", "center").panel({
		onResize : function(width, height) {
			my.setIframeHeight("centerIframe", $("#mainLayout").layout("panel", "center").panel("options").height-130);
		}
	});
	mainTabs = $("#mainTabs").tabs({
		fit : true,
		border : false,
		tools : [
		         
		   /*{
				iconCls : "icon-my-arrow_up",
				handler : function() {
					mainTabs.tabs({
						tabPosition : "top"
					});
				}
			},
			{
				iconCls : "icon-my-arrow_left",
				handler : function() {
					mainTabs.tabs({
						tabPosition : "left"
					});
				}
			}, 
			{
				iconCls : "icon-my-arrow_down",
				handler : function() {
					mainTabs.tabs({
						tabPosition : "bottom"
					});
				}
			},
			{
				iconCls : "icon-my-arrow_right",
				handler : function() {
					mainTabs.tabs({
						tabPosition : "right"
					});
				}
			},*/
		{
			text : "刷新",
			iconCls : "icon-my-arrow_refresh",
			handler : function() {
				var panel = mainTabs.tabs("getSelected").panel("panel");
				var frame = panel.find("iframe");
				try {
					if (frame.length > 0) {
						for (var i = 0; i < frame.length; i++) {
							frame[i].contentWindow.document.write("");
							frame[i].contentWindow.close();
							frame[i].src = frame[i].src;
						}
						if (navigator.userAgent.indexOf("MSIE") > 0) {// IE特有回收内存方法
							try {
								CollectGarbage();
							} catch (e) {
							}
						}
					}
				} catch (e) {
					alert(e);
				}
			}
		}, {
			text : "关闭",
			iconCls : "icon-my-arrow_cross",
			handler : function() {
				var index = mainTabs.tabs("getTabIndex", mainTabs.tabs("getSelected"));
				var tab = mainTabs.tabs("getTab", index);
				if (tab.panel("options").closable) {
					mainTabs.tabs("close", index);
				} else {
					$.messager.alert("提示", "[" + tab.panel("options").title + "]不可以被关闭！", "error");
				}
			}
		}],
		onContextMenu : function(e, title) {
			e.preventDefault();
			tabsMenu.menu("show", {
				left : e.pageX,
				top : e.pageY
			}).data("tabTitle", title);
			}
	});
	
	function refreshTab(title) {
		var index = mainTabs.tabs("getTabIndex", mainTabs.tabs("getSelected"));
		var tab = mainTabs.tabs("getTab", index);
		mainTabs.tabs("update", {
			tab : tab,
			options : tab.panel("options")
		});
	}
	
	tabsMenu = $("#tabsMenu").menu({
		onClick : function(item) {
			var curTabTitle = $(this).data("tabTitle");
			var type = $(item.target).attr("type");

			if (type === "refresh") {
				refreshTab(curTabTitle);
				return;
			}

			if (type === "close") {
				var t = mainTabs.tabs("getTab", curTabTitle);
				if (t.panel("options").closable) {
					mainTabs.tabs("close", curTabTitle);
				}else{
					$.messager.alert("提示", "[" + t.panel("options").title + "]不可以被关闭！", "error");
				}
				return;
			}

			var allTabs = mainTabs.tabs("tabs");
			var closeTabsTitle = [];

			$.each(allTabs, function() {
				var opt = $(this).panel("options");
				if (opt.closable && opt.title != curTabTitle && type === "closeOther") {
					closeTabsTitle.push(opt.title);
				} else if (opt.closable && type === "closeAll") {
					closeTabsTitle.push(opt.title);
				}
			});

			for ( var i = 0; i < closeTabsTitle.length; i++) {
				mainTabs.tabs("close", closeTabsTitle[i]);
			}
		}
	});
	
	$(".easyui-tree").tree({
		onBeforeLoad:function(node, param){
			if(node==null){
				return true;
			}
			var attr=node.attributes;
			if(!attr.child){
				return true;
			}else{
				return false;
			}
		},
		onClick:function(node){
			var attr=node.attributes;
			if(attr.url!=null&&$.trim(attr.url)!="") {
				var src = base+"/"+attr.url+"/"+attr.masterName+suffix;
				var tabs =mainTabs;
				if(attr.url.startWith("http")){
					src=attr.url;
				}
				if(attr.target=="_blank"){
					window.open(src,"");
				}else{
						var opts = {
								title:node.text,
								closable:true,
								iconCls:node.iconCls,
								content:"<iframe src='"+src+"' allowTransparency='true' style='border:0;width:100%;height:100%; overflow: scroll;' name='centerIframe'  scrolling='auto' id='centerIframe' frameBorder='0'></iframe>",
								border:false,
								fit:true,
								width:"auto",
								height:"auto"
						};
						if (tabs.tabs("exists", opts.title)){
							tabs.tabs("select", opts.title);
							//tabs.tabs("getSelected").panel("refresh");//刷新页面
						} else {
							tabs.tabs("add", opts);
						}
				}
			}
		}
	});
	
	var loginFun = function() {
		if ($("#loginDialog form").form("validate")) {
			$("#loginBtn").linkbutton("disable");
			$("#userPwd").val(hex_md5($("#userPwd").val()));
			$.post(base+"/relogin"+suffix,$("#loginDialog form").serialize(), function(result) {
				if (result=="true") {
					$("#loginDialog").dialog("close");
				} else {
					$.messager.alert("提示","登录失败", "error", function() {
						$("#loginDialog form :input:eq(1)").focus();
					});
				}
				$("#loginBtn").linkbutton("enable");
			}, "text");
		}
	};
	
	$("#loginDialog").show().dialog({
		modal : true,
		closable : false,
		iconCls : "ext-icon-lock_open",
		buttons : [ {
			id : "loginBtn",
			text : "登录",
			handler : function() {
				loginFun();
			}
		} ],
		onOpen : function() {
			$("#loginDialog form :input[name='userPwd']").val("");
			$("form :input").keyup(function(event) {
				if (event.keyCode == 13) {
					loginFun();
				}
			});
		}
	}).dialog("close");
	
	$("#passwordDialog").show().dialog({
		modal : true,
		closable : true,
		width:250,
		iconCls : "ext-icon-lock_edit",
		buttons : [ {
			text : "修改",
			handler : function() {
				if ($("#passwordDialog form").form("validate")) {
					$.post(base+"/user/updPwd"+suffix,{"newPwd":hex_md5($("#pwd").val()),"oldPwd":hex_md5($("#oldPwd").val())},
						function(result) {
						if (result=="true") {
							$.messager.alert("提示","密码修改成功！","info");
							$("#passwordDialog").dialog("close");
						}else{
							$.messager.alert("提示",result,"error");
						}
					},"text");
				}
			}
		}],
		onOpen : function() {
			$("#passwordDialog form :input").val("");
		}
	}).dialog("close");
	
});

function loadData(data,parent){
	if(data.length>0){
		var newData=[];
		var dataObj=null;
		var temp=null;
		var attribute=null;
		for(var i=0,len=data.length;i<len;i++){
			temp=data[i];
			attribute={};
			dataObj={};
			dataObj.id=temp.id;
			dataObj.text=temp.powerName;
			dataObj.iconCls=temp.iconCls;
			dataObj.state=temp.status;
			
			attribute.url=temp.url;
			attribute.target=temp.target;
			attribute.type=temp.type;
			attribute.masterName=temp.masterName;
			attribute.child=temp.child;
			
			dataObj.attributes=attribute;
			newData.push(dataObj);
		}
		data=newData;
	}
	return data;
}

/**
 * 判断浏览器类型
 * @returns
 */
function getOs(){
   if(navigator.userAgent.indexOf("MSIE")>0){
        return "MSIE";  
   }
   if(navigator.userAgent.indexOf("Firefox")>0){  
        return "Firefox";  
   }
   if(navigator.userAgent.indexOf("Chrome")>0){
	return "Chrome";   
   }
   if(navigator.userAgent.indexOf("Safari")>0) {  
        return "Safari";  
   }
   if(navigator.userAgent.indexOf("Camino")>0){  
        return "Camino";  
   }
   if(navigator.userAgent.indexOf("Gecko/")>0){  
        return "Gecko";  
   }
}

function expandMenu(){
	var isExpand=$(this).attr("data-expand");
	if(isExpand==="false"){
		$(this).attr("data-expand","true");
		var ulObj=$(this).children();
		$(ulObj).tree({
			loadFilter:loadData,
			url:$(ulObj).attr("data-url")
		});
	}
}

function setCurrTime(){
	var date=new Date();
	var str=date.getFullYear()+"年";
	str+=(date.getMonth()+1)+"月";
	str+=date.getDate()+"日";
	var hours=date.getHours();
	var minutes=date.getMinutes();
	var seconds=date.getSeconds();
	var weekDay=date.getDay();
	var weekArray=["日","一","二","三","四","五","六"];
	str+="&emsp;星期"+weekArray[weekDay];
	if(hours<10){
		hours="0"+hours;
	}
	if(minutes<10){
		minutes="0"+minutes;
	}
	if(seconds<10){
		seconds="0"+seconds;
	}
	
	str+="&emsp;"+hours+":"+minutes+":"+seconds;
	$("#currTime").html(str);
}