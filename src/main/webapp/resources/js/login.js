$(document).ready(function(){
	if(msg!=""&&msg!="null"){
		$.messager.alert("信息",decodeURI(msg),"info",function(){
			$("#username").focus();
		});
	}else{
		$("#username").focus();		
	}
	$("form").submit(function(){
		var usernameVal=$("#username").val();
		var passwordVal=$("#password").val();
		if(usernameVal==null||$.trim(usernameVal)==""){
			$.messager.alert("警告","用户名不能为空!");    
			return false;
		}
		if(passwordVal==null||$.trim(passwordVal)==""){
			$.messager.alert("警告","密码不能为空!"); 
			return false;
		}
		$("#password").val(hex_md5(passwordVal));
	});
});	