var grid=null;
$(document).ready(function(){
	grid=$("#resultTable").datagrid({
		 height:$(document).height(),
		 url:base+rootPath+"/getData"+suffix,
		 queryParams:{},
		 sortName:"deviceOilId",
		 sortOrder:"ASC",
		 idField:"id",
		 toolbar:toolbar,
		 columns:[[
		       {field:"no",checkbox:true},
		       {field:"id",title:"id",hidden:true},
		       {field:"oilName",title:"润滑项目",align:"center",sortable:false,formatter:formatOil},
		       {field:"devName",title:"所属设备",align:"center",sortable:true,formatter:formatDevice},
		       {field:"devNo",title:"所属设备编号",align:"center",sortable:false,formatter:formatDeviceNo},
		       {field:"oiledUserName",title:"润滑人员",align:"center",sortable:true/*,formatter:formatOiledUser*/},
		       {field:"oiledCycle",title:"润滑周期(天)",align:"center",sortable:false,formatter:formatCycle},
		       {field:"oiledDate",title:"润滑时间",align:"center",sortable:true,formatter:formatOiledDate},
		       {field:"count",title:"润滑次数",align:"center",sortable:true,formatter:formatCount},
		       {field:"cost",title:"费用(元)",align:"center",sortable:false},
		       {field:"createdBy",title:"录入人员",align:"center",sortable:true,formatter:formatCreatedBy},
		       {field:"createTime",title:"录入时间",align:"center",sortable:true},
		       {field:"description",title:"备注",align:"center",sortable:false},
		       {field:"operate",title:"操作",align:"center",formatter:operateFunction}
		       ]]
	});
});

function formatDevice(value,row,index){
	if(row==null){
		return null;
	}
	return row.device.name;		
}
function formatDeviceNo(value,row,index){
	if(row==null){
		return null;
	}
	return row.device.no;		
}
function formatOiledBy(value,row,index){
	if(row==null){
		return null;
	}
	return row.oiledUser.realName;		
}
function formatCreatedBy(value,row,index){
	if(row==null){
		return null;
	}
	return row.createdUser.realName;		
}

function formatOil(value,row,index){
	if(row==null){
		return null;
	}
	return row.oil.name;		
}

function formatCount(value,row,index){
	if(row==null){
		return null;
	}
	return "第"+transNum2Chinese(row.count)+"次";		
}

function formatNo(value,row,index){
	if(row==null){
		return null;
	}
	return row.device.no;		
}

function formatStartDate(value,row,index){
	if(row==null){
		return null;
	}
	return value.substring(0,value.indexOf(" "));
}
function formatOiledDate(value,row,index){
	if(row==null){
		return null;
	}
	return value.substring(0,value.indexOf(" "));
}

function formatCreatedUser(value,row,index){
	if(row==null){
		return null;
	}
	return row.createdUser.realName;
}

function formatOiledUser(value,row,index){
	if(row==null){
		return null;
	}
	return row.oiledUser.realName;
}

function formatCate(value,row,index){
	if(row==null){
		return null;
	}
	return row.category.categoryName;
}
function formatCycle(value,row,index){
	if(row==null){
		return null;
	}
	return row.oil.cycle;
}