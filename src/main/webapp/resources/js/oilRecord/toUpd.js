var submitForm = function($dialog,$grid,$pjq) {
	var categoryIdVal=$("#categoryId").combobox("getValue");
	var oiledByVal=$("#oiledUserName").val();
	var deviceIdRow=$("#deviceId").combogrid("grid").datagrid("getSelected");
	var oiledDateVal=$("#oiledDate").val();
	var deviceOilIdVal=$("#deviceOilId").combogrid("grid").datagrid("getSelected");
	
	if(isEmpty(categoryIdVal)){
		$pjq.messager.alert("提示","设备类型不能为空！","info");return;
	}
	if(isEmpty(oiledByVal)){
		$pjq.messager.alert("提示","润滑人员不能为空！","info");return;
	}
	if(isEmpty(deviceIdRow)){
		$pjq.messager.alert("提示","设备名称不能为空！","info");return;
	}
	if(isEmpty(oiledDateVal)){
		$pjq.messager.alert("提示","润滑时间不能为空！","info");return;
	}
	if(isEmpty(deviceOilIdVal)){
		$pjq.messager.alert("提示","润滑项目不能为空！","info");return;
	}
	$.post(base+"/"+path+"/upd"+suffix,$("#frm").serialize(),function(data){
			if(data){
				$pjq.messager.alert("提示","提交成功！","info");
				$grid.datagrid("load");
				$dialog.dialog("destroy");
			}else{
				$pjq.messager.alert("提示","提交失败！","error");
			}
	},"json");
};
$(document).ready(function(){
	$("#categoryId").combobox({
		valueField:"id",
	    textField:"categoryName",
	    editable:false,
	    disabled:true,
	    panelHeight:200,
	    url:base+"/category/getAllCategory"+suffix,
		onChange:function(newValue,oldValue){
			$("#deviceId").combogrid("clear");
			$("#deviceId").combogrid("grid").datagrid("reload",{categoryId:newValue});
		}
	});
	$("#deviceOilId").combogrid({
		panelWidth:350,
		idField:"deviceOilId",
		textField:"name",
		editable:false,
		disabled:true,
		method:"post",
		queryParams:{deviceId:deviceIdVal},
		url:base+"/oil/getAllOilByDeviceId"+suffix,
		rownumbers:false,
		pagination:false,
		columns:[[
		 		  {field:"deviceOilId",title:"id",hidden:true,width:0},
		 		  {field:"name",title:"备件名称",width:100},
		 		  {field:"cycle",title:"更换周期",width:100},
		 		  {field:"startDate",title:"开始时间",width:100}
		 		]]
	});
	var categoryId=$("#categoryId").combobox("getValue");
	$("#deviceId").combogrid({
	    panelWidth:450,
	    idField:"id",
	    textField:"name",
	    method:"post",
	    rownumbers:true,
		pagination:true,
		disabled:true,
		editable:false,
	    queryParams:{categoryId:categoryId},
	    url:base+"/device/getData"+suffix,
	    columns:[[
	        {field:"id",title:"id",hidden:true,width:0},
	        {field:"name",title:"设备名称",width:200},
	        {field:"no",title:"编号",width:100},
	        {field:"deptId",title:"部门id",hidden:true,formatter:formatDeptId},
	        {field:"deptName",title:"所属部门",width:100,formatter:formatDept}
	    ]],
	    onChange:function(newValue,oldValue){
	    	$("#deviceOilId").combogrid("clear");
	    	$("#deviceOilId").combogrid("grid").datagrid("reload",{deviceId:newValue});
	    }
	});
	
	function formatDept(value,row,index){
		if(row!=null){
			return row.dept.deptName;			
		}
		return null;
	}
	
	function formatDeptId(value,row,index){
		if(row!=null){
			return row.dept.id;			
		}
		return null;
	}
});