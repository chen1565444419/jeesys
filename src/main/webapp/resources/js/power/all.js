var grid=null;
$(document).ready(function(){
	var currWidth=$(document).width();
	grid=$("#resultTable").treegrid({
		 height:$(document).height(),
		 url:base+rootPath+"/getData"+suffix,
		 idField:"id",
		 treeField:"text",
		 parentField:"fatherId",
		 pagination:false,
		 animate: false,
		 collapsible: true,
		 border:true,
		 singleSelect:true,
		 rownumbers:false,
		 fitColumns:false,
		 width:"auto",
		 height:"auto",
		 toolbar:toolbar,
		 columns:[[
		       {field:"id",title:"id",hidden:true},
		       {field:"text",title:"菜单名称",align:"left",width:currWidth*0.15},
		       {field:"fatherName ",title:"父菜单名称 ",align:"center",width:currWidth*0.1,formatter:function(value,row){return row.fatherName;}},
		       {field:"masterName ",title:"编码 ",align:"center",width:currWidth*0.1,formatter:function(value,row){return row.masterName;}},
		       {field:"iconCls ",title:"菜单图标 ",align:"center",width:currWidth*0.05,
	            	  formatter:function(value,row){
	            		  return "<span class='"+row.iconCls+"' style='display:inline-block;vertical-align:middle;width:16px;height:16px;'></span>";
						}},
		       {field:"url ",title:"访问路径 ",align:"center",width:currWidth*0.1,
					  formatter:function(value,row){
						return row.url;
					  }
				},
		       {field:"type ",title:"类型 ",align:"center",width:currWidth*0.05,
		    	   formatter:function(value,row){
         		  if("F"==row.type){
         			  return "<font color='green'>菜单</font>";
         		  }else if("O"==row.type){
          			  return "<font color='red'>操作</font>";
          		  }else{
          			  return "<font color='blue'>权限</font>";
          		  }
				}},
		       {field:"used ",title:"启用 ",align:"center",width:currWidth*0.05,
					formatter:function(value,row){
	         		  if(1==row.used){
	         			  return "<font color='green'>是</font>";
	         		  }
	          		  else{
	          			  return "<font color='red'>否</font>";
	          		  }
					}
				},
			   {field:"child",title:"根菜单",align:"center",width:currWidth*0.05,
					formatter:function(value,row){
						if(row.type=="F"){
							if(1==row.child){
								return "<font color='green'>是</font>";							
							}else{
								return "<font color='red'>否</font>";								
							}
						}else{
							return "";
						}
					}
				},
			   {field:"position",title:"显示位置",align:"center",width:currWidth*0.05,formatter:function(value,row){
				   var positions=row.position;
				   var result="";
				   if(positions!=undefined){
					   positions=positions.replace("[","").replace("]","").replace(" ","");
					   var positionArr=positions.split(",");
					   for(var i=0,len=positionArr.length;i<len;i++){
						   result+=positionArr[i]=="top"?"顶部/":"行中/";
					   }
					   result=result.substring(0,result.length-1);
				   }
				   return result;
			   }},
			   {field:"seq",title:"排序序号",align:"center",width:currWidth*0.05},
		       {field:"createTime",title:"创建日期",align:"center",width:currWidth*0.12},
		       {field:"description ",title:"菜单描述 ",align:"center",width:currWidth*0.1,formatter:formatDescr}
		 ]] 
	});
});
/**
 * 添加菜单
 */
function toMyAdd(){
	var addDialog=null;
	var url=base+"/power/toAdd"+suffix;
	var row=grid.treegrid("getSelected");
	if(row!=null){
		if(row.type!="F"){
			parent.$.messager.alert("提示","只允许选择菜单类型添加!","info");
			return;
		}
		url=url+"?fatherId="+row.id;
	}
	addDialog=parent.my.modalDialog({
			title : "添加菜单",
			width : 620,
			height : 400,
			url:url,
			buttons : [ {
				text : "保存",
				iconCls : "icon-ok",
				handler : function() {
					addDialog.find("iframe").get(0).contentWindow.submitForm(addDialog,grid,parent.$);
				}
			}, {
				text : "取消",
				iconCls : "icon-cancel",
				handler : function() {
					addDialog.dialog("destroy");
				}
			}
			]
		});
}
/**
 * 删除菜单
 * */
function delMySelected(){
	var row = grid.treegrid("getSelected");
	var url=base+"/power";
	if(row!=null){
		parent.$.messager.confirm("确认","您确认要删除记录吗？",function(r){
			if(r){
				$.post(url+"/checkPower/"+row.id+suffix+"?_d="+new Date().toTimeString(),function(data){
					if(data){
						parent.$.messager.alert("提示","该菜单下有子菜单,请先删除子菜单","info");
					}else{
						$.post(url+"/rem/"+row.id+suffix+"?_d="+new Date().toTimeString(),function(data){
							if(data){
								parent.$.messager.alert("提示", "删除成功！", "info");
								grid.treegrid("reload");
							}
						},"json");
					}
				},"json");
			}
		});
	}else{
		parent.$.messager.alert("提示","请选择一行记录!","info");
	}
}