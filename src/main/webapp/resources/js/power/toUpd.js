$(document).ready(function(){
	
	var menuTree=null;
	menuTree=$("#fatherId").combotree({
		width:171,
		url:base+"/power/getPowerEdit"+suffix,
		idField:"id",
		textFiled:"powerName",
		parentField:"fatherId",
	 	editable:false,
	 	panelHeight:"auto",
	 	onLoadSuccess:function(node,data){
	 		if($("#pId").val()!=null&&$("#pId").val()!=0){
	 			menuTree.combotree("setValue",$("#pId").val());
	 			var t=menuTree.combotree("tree");
	 			var pNode = t.tree("find",$("#pId").val());
	 			if(pNode!=null){
		 			var parentNode=t.tree("getParent",pNode.target);
		 			while(parentNode!=null){
		 				t.tree("expand",parentNode.target);
		 				parentNode=t.tree("getParent",parentNode.target);
		 			}
	 			}
	 		}
	 	},
	 	loadFilter:	function(data, parent){
	 		var opt = $(this).data().tree.options;
	 		var idFiled,
	 		textFiled,
	 		parentField;
	 		if (opt.parentField) {
	 			idFiled = opt.idFiled || "id";
	 			textFiled = opt.textFiled || "powerName";
	 			parentField = opt.parentField || "fatherId";
	 			
	 			var i,
	 			l,
	 			treeData = [],
	 			tmpMap = [];
	 			var attribute=null;
	 			for (i = 0, l = data.length; i < l; i++) {
	 				tmpMap[data[i][idFiled]] = data[i];
	 			}
	 			
	 			for (i = 0, l = data.length; i < l; i++) {
	 				data[i].state=data[i].status;
	 				data[i].targets=data[i].target;
	 				
	 				attribute={};
	 				attribute.url=data[i].url;
	 				attribute.type=data[i].type;
	 				attribute.child=data[i].child;
	 				attribute.masterName=data[i].masterName;
	 				data[i].attributes=attribute;
	 				
	 				if(data[i].type=="F" && data[i].child==1){
	 					data[i].state="open";
	 				}
	 				
	 				if (tmpMap[data[i][parentField]] && data[i][idFiled] != data[i][parentField]) {
	 					if (!tmpMap[data[i][parentField]]["children"])
	 						tmpMap[data[i][parentField]]["children"] = [];
	 					data[i]["text"] = data[i][textFiled];
	 					tmpMap[data[i][parentField]]["children"].push(data[i]);
	 				} else {
	 					data[i]["text"] = data[i][textFiled];
	 					treeData.push(data[i]);
	 				}
	 			}
	 			return treeData;
	 		}
	 		return data;
	 	}
	});
	//清空父类tree选中项
	$("#clear").click(function(){
		$("#fatherId").combotree("clear");
	});
	
	$("input[name='child'][type='radio']").change(function(){
		if($(this).val()==="0"){
			$("#url").validatebox("disableValidation");
			$("#url").attr("disabled",true);
		}else if($(this).val()==="1"){
			$("#url").validatebox("enableValidation");
			$("#url").attr("disabled",false);
		}
	});
	//图标下拉菜单
	$("#iconCls").combobox({
		width:171,
		data:my.iconData,
		editable:false,
		valueField:"value",    
	    textField:"text",
	    onLoadSuccess:function(none){
			$("#iconCls").combobox("setValue",$("#iconVal").val());
	    },
		formatter: function(v){
			return "<span class='"+v.value+"' style='display:inline-block;vertical-align:middle;width:16px;height:16px;'></span>"+v.value;
		}
	});
	/**
	 * 类型变动事件
	 */
	$("#type").combobox({
		onChange:function(newValue, oldValue){
			if(newValue=="O"){
				$(".hide").hide();
				$("input[name='child']").attr("disabled",true);
				$(".position").show();
				$("#position").attr("disabled",false);
				$("#url").validatebox("enableValidation");
				$("#url").attr("disabled",false);
			}else if(newValue=="F"){
				$(".position").hide();
				$("#position").attr("disabled",true);
				$(".hide").show();
				$("input[name='child']").attr("disabled",false);
				$("#url").attr("disabled",true);
				$("#url").validatebox("disableValidation");
			}else if(newValue=="P"){
				$(".position").hide();
				$("#position").attr("disabled",true);
				$(".hide").show();
				$("input[name='child']").attr("disabled",false);
				$("#url").attr("disabled",true);
				$("#url").validatebox("disableValidation");
			}
		}
	});
});

var submitForm = function($dialog,$grid,$pjq) {
	 var powerNameVal=$("#powerName").val();
	 var urlVal=$("#url").val();
	 var masterNameVal=$("#masterName").val();
	 var isMenuVal=$("input[name='child'][type='radio']:checked").val();
	 
	 if($.trim(powerNameVal)==""){
		 $pjq.messager.alert("提示","请输入菜单名称！","info");
		 return;
	 }
	 if(isMenuVal=="1"){
		 if($.trim(urlVal)==""){
			 $pjq.messager.alert("提示","请输入访问路径！","info");
			 return;
		 }
	 }
	 if($.trim(masterNameVal)==""){
		 $pjq.messager.alert("提示","请输入菜单编码！","info");
		 return;
	 }
	 
	 var t = $("#fatherId").combotree("tree");	// 获取树对象
	 var n = t.tree("getSelected");		// 获取父级菜单选择的节点
	 
	 var typeVal = $("#type").combobox("getValue");	// 获取类型下拉对象的值
	 
	 if(n!=null){
		 if(n.attributes.child==1 && typeVal=="F"){
			 $pjq.messager.alert("提示","父级菜单为根菜单时,类型不能为菜单","info");
			 return;
		 }else if(n.attributes.child==0 && typeVal=="O"){
			 $pjq.messager.alert("提示","父级菜单为非根菜单时,类型不能为操作","info");
			 return;
		 }
	 }else if(typeVal=="O"){
		 $pjq.messager.alert("提示","父级菜单为空时，类型不能为操作","info");
		 return;
	 }
	 
	 $.post(base+"/power/checkMasterName"+suffix,$("#form").serialize(),function(data){
			if(data==true){
				$pjq.messager.alert("提示","该编码已存在！","info");
			}else{
				$.post(base+"/power/upd"+suffix,$("#form").serialize(),function(result){
					if(result==true){
						$pjq.messager.alert("提示","保存成功！","info");
						$grid.treegrid("reload");
						$dialog.dialog("destroy");
					}else{
						$pjq.messager.alert("提示","保存失败！","error");
					}
				},"json");
			}
	 },"json");
	 
};
