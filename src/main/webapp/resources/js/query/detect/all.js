$(document).ready(function(){
	
	$("#ifr").css("height",$(window).height()-40);
	
	$("#searchType").combobox({
		onChange:function(newValue,oldValue){
			var spans=$("#dynamic").children();
			$(spans).css("display","none");
			if(newValue!=null){
				$("#"+newValue).css("display","inline");
			}
		}
	});
	
	var categoryUrl=base+"/category/getData"+suffix+"?bigCateId=";
	$("#bigCateId").combobox({
		onChange:function(newValue,oldValue){
			$("#categoryId").combobox("clear");
			$("#categoryId").combobox("reload",categoryUrl+newValue);
		}
	});
	$("#categoryId").combobox({
		valueField:"id",
		textField:"text",
		url:$("#bigCateId").val()==null?categoryUrl:categoryUrl+$("#bigCateId").val(),
		panelHeight:200,
		required:true,
		editable:false,
		loadFilter:function(data){
			var newData=[];
			var rows=data.rows;
			//newData.push({id:0,text:"请选择",selected:true});
			if(data!=null&&rows!=null&&rows.length>0){
				for(var i=0,len=rows.length;i<len;i++){
					newData.push({id:rows[i].id,text:rows[i].categoryName});
				}
			}
			return newData;
		}
	});
	
	$("#searchBtn").click(function(){
		var selectedType=$("#searchType").combobox("getValue");
		if(selectedType==null||selectedType==""){
			parent.$.messager.alert("提示","请选择查询方式！","info");return;
		}
		var params="&";
		switch(selectedType){
			case "createTime":
				var startDateVal=$("#startDate").val();
				var endDateVal=$("#endDate").val();
				if(isEmpty(startDateVal)){
					parent.$.messager.alert("提示","请输入开始时间！","info");
					return;
				}
				if(isEmpty(endDateVal)){
					parent.$.messager.alert("提示","请输入结束时间！","info");
					return;
				}
				if(startDateVal>endDateVal){
					parent.$.messager.alert("提示","开始时间应小于结束时间！","info");
					return;
				}
				params+="startDate="+startDateVal+"&endDate="+endDateVal;					
				break;
			case "dept":
				var deptId=$("#"+selectedType+"Id").combobox("getValue");
				if(deptId==null||deptId==""){
					parent.$.messager.alert("提示","所属部门不能为空！","info");
					return;
				}
				params+="deptId="+deptId;
				break;
			case "category":
				var bigCateIdVal=$("#bigCateId").combobox("getValue");
				if(bigCateIdVal==null){
					parent.$.messager.alert("提示","部门分类不能为空！","info");
					return;
				}
				params+="bigCateId="+bigCateIdVal;
				var categoryId=$("#"+selectedType+"Id").combobox("getValue");
				if(categoryId==null||categoryId==""){
					parent.$.messager.alert("提示","设备类型不能为空！","info");
					return;
				}
				params+="&categoryId="+categoryId;
				break;
			case "detectUserName":
				var detectUserNameVal=$("#userName").val();
				if(detectUserNameVal==null||detectUserNameVal==""){
					parent.$.messager.alert("提示","维修人员不能为空！","info");
					return;
				}
				params+="detectUserName="+detectUserNameVal;
				break;
		}
		$("#ifr").attr("src",birtBaseUrl+"detectReport_"+selectedType+".rptdesign"+params);
	});
});