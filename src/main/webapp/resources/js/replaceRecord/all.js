var grid=null;
$(document).ready(function(){
	grid=$("#resultTable").datagrid({
		 height:$(document).height(),
		 url:base+rootPath+"/getData"+suffix,
		 queryParams:{},
		 sortName:"deviceCompId",
		 sortOrder:"ASC",
		 idField:"id",
		 toolbar:toolbar,
		 columns:[[
		       {field:"no",checkbox:true},
		       {field:"id",title:"id",hidden:true},
		       {field:"compName",title:"备件名称",align:"center",sortable:false,formatter:formatComp},
		       {field:"devName",title:"所属设备",align:"center",sortable:false,formatter:formatDevice},
		       {field:"devNo",title:"所属设备编号",align:"center",sortable:false,formatter:formatDeviceNo},
		       {field:"replaceUserName",title:"更换人员",align:"center",sortable:true},
		       {field:"count",title:"更换次数",align:"center",sortable:false,formatter:formatCount},
		       {field:"comp.cycle",title:"更换周期(天)",align:"center",sortable:true,formatter:formatCycle},
		       {field:"replacedDate",title:"更换时间",align:"center",sortable:true,formatter:formatReplaceDate},
		       {field:"compCount",title:"更换数量",align:"center",sortable:false,formatter:formatCompCount},
		       {field:"cost",title:"费用(元)",align:"center",sortable:false},
		       {field:"createdBy",title:"录入人员",align:"center",sortable:true,formatter:formatCreatedBy},
		       {field:"createTime",title:"录入时间",align:"center",sortable:true},
		       {field:"description",title:"备注",align:"center",sortable:false,formatter:formatDescr},
		       {field:"operate",title:"操作",align:"center",formatter:operateFunction}
		       ]]
	});
});

function formatDevice(value,row,index){
	if(row==null){
		return null;
	}
	return row.device.name;		
}
function formatDeviceNo(value,row,index){
	if(row==null){
		return null;
	}
	return row.device.no;		
}
function formatReplacedBy(value,row,index){
	if(row==null){
		return null;
	}
	return row.replacedUser.realName;		
}
function formatCreatedBy(value,row,index){
	if(row==null){
		return null;
	}
	return row.createdUser.realName;		
}

function formatComp(value,row,index){
	if(row==null){
		return null;
	}
	return row.component.name;		
}

function formatCompCount(value,row,index){
	if(row==null){
		return null;
	}
	return row.component.count;		
}

function formatNo(value,row,index){
	if(row==null){
		return null;
	}
	return row.device.no;		
}

function formatStartDate(value,row,index){
	if(row==null){
		return null;
	}
	return value.substring(0,value.indexOf(" "));
}
function formatReplaceDate(value,row,index){
	if(row==null){
		return null;
	}
	return value.substring(0,value.indexOf(" "));
}

function formatCreatedUser(value,row,index){
	if(row==null){
		return null;
	}
	return row.createdUser.realName;
}

function formatDetectUser(value,row,index){
	if(row==null){
		return null;
	}
	return row.detectUser.realName;
}

function formatCate(value,row,index){
	if(row==null){
		return null;
	}
	return row.category.categoryName;		
}
function formatCycle(value,row,index){
	if(row==null){
		return null;
	}
	return row.component.cycle;
}