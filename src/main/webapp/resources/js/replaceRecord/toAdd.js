var submitForm = function($dialog,$grid,$pjq) {
	var bigCateIdVal=$("#bigCateId").combobox("getValue");
	var categoryIdVal=$("#categoryId").combobox("getValue");
	//var replacedByVal=$("#replacedBy").combobox("getValue");
	var replacedByVal=$("#replaceUserName").val();
	var deviceIdRow=$("#deviceId").combogrid("grid").datagrid("getSelected");
	var replacedDateVal=$("#replacedDate").val();
	var compIdVal=$("#compId").combogrid("grid").datagrid("getSelected");
	
	if(isEmpty(bigCateIdVal)){
		$pjq.messager.alert("提示","部门分类不能为空！","info");return;
	}
	if(isEmpty(categoryIdVal)){
		$pjq.messager.alert("提示","设备类型不能为空！","info");return;
	}
	if(isEmpty(replacedByVal)){
		$pjq.messager.alert("提示","更换人员不能为空！","info");return;
	}
	if(isEmpty(deviceIdRow)){
		$pjq.messager.alert("提示","设备名称不能为空！","info");return;
	}
	if(isEmpty(replacedDateVal)){
		$pjq.messager.alert("提示","更换时间不能为空！","info");return;
	}
	if(compIdVal==null){
		$pjq.messager.alert("提示","备件不能为空！","info");return;
	}
	$.post(base+"/"+path+"/add"+suffix,$("#frm").serialize(),function(data){
			if(data){
				$pjq.messager.alert("提示","提交成功！","info");
				$grid.datagrid("load");
				$dialog.dialog("destroy");
			}else{
				$pjq.messager.alert("提示","提交失败！","error");
			}
	},"json");
};
$(document).ready(function(){
	var categoryUrl=base+"/category/getData"+suffix+"?rows="+gridParams.pageSize+"&bigCateId=";
	$("#bigCateId").combobox({
		onChange:function(newValue,oldValue){
			$("#categoryId").combobox("clear");
			$("#deviceId").combogrid("clear");
			$("#categoryId").combobox("reload",categoryUrl+newValue);
		}
	});
	$("#categoryId").combobox({
		valueField:"id",
		textField:"text",
		url:$("#bigCateId").val()==null?categoryUrl:categoryUrl+$("#bigCateId").val(),
		panelHeight:200,
		required:true,
		editable:false,
		onChange:function(newValue,oldValue){
			$("#deviceId").combogrid("clear");
			$("#deviceId").combogrid("grid").datagrid("reload",{categoryId:newValue,deptId:deptIdVal});
		},
		loadFilter:function(data){
			var newData=[];
			var rows=data.rows;
			if(data!=null&&rows!=null&&rows.length>0){
				for(var i=0,len=rows.length;i<len;i++){
					newData.push({id:rows[i].id,text:rows[i].categoryName});
				}
			}
			return newData;
		}
	});
	
	$("#compId").combogrid({
		panelWidth:350,
		idField:"deviceCompId",
		textField:"name",
		editable:false,
		method:"post",
		queryParams:{},
		url:base+"/device/getAllCompByDeviceId"+suffix,
		rownumbers:false,
		pagination:false,
		columns:[[
		 		  {field:"deviceCompId",title:"id",hidden:true,width:0},
		 		  {field:"name",title:"备件名称",width:100},
		 		  {field:"count",title:"数量",width:30},
		 		  {field:"cycle",title:"更换周期",width:100},
		 		  {field:"startDate",title:"开始时间",width:100}
		 		]]
		/*
		 onChange:function(newValue,oldValue){
			var deviceIdVal=$("#deviceId").combogrid("grid").datagrid("getSelected").id;
			$("#count").combobox("reload",base+"/replaceRecord/getCountMap"+suffix+"?deviceId="+deviceIdVal+"&deviceCompId="+newValue);
		 }
		 */
	});
	var categoryId=$("#categoryId").combobox("getValue");
	$("#deviceId").combogrid({
	    panelWidth:450,
	    idField:"id",
	    textField:"name",
	    method:"post",
	    rownumbers:true,
		pagination:true,
		editable:false,
	    queryParams:{categoryId:categoryId},
	    url:base+"/device/getData"+suffix,
	    columns:[[
	        {field:"id",title:"id",hidden:true,width:0},
	        {field:"name",title:"设备名称",width:200},
	        {field:"no",title:"编号",width:100},
	        {field:"deptId",title:"部门id",hidden:true,formatter:formatDeptId},
	        {field:"deptName",title:"所属部门",width:100,formatter:formatDept}
	    ]],
	    onChange:function(newValue,oldValue){
	    	$("#count").combobox("clear");
	    	$("#count").combobox("reload",base+"/replaceRecord/getCountMap"+suffix+"?deviceId="+newValue);
	    }
	});

	$("#count").combobox({
		valueField:"count",
		textField:"times",
		editable:false,
		panelHeight:"auto",
		onChange:function(newValue,oldValue){
			var row=$("#deviceId").combogrid("grid").datagrid("getSelected");
			$("#compId").combogrid("clear");
			if(row!=null){
				var deviceIdVal=row.id;			
				$("#compId").combogrid("grid").datagrid("reload",{deviceId:deviceIdVal,count:newValue});
			}
		}
	});
	
	function formatDept(value,row,index){
		if(row!=null){
			return row.dept.deptName;			
		}
		return null;
	}
	
	function formatDeptId(value,row,index){
		if(row!=null){
			return row.dept.id;			
		}
		return null;
	}
});