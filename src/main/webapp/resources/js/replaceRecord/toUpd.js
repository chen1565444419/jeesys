var submitForm = function($dialog,$grid,$pjq) {
	var bigCateIdVal=$("#bigCateId").combobox("getValue");
	var categoryIdVal=$("#categoryId").combobox("getValue");
	//var replacedByVal=$("#replacedBy").combobox("getValue");
	var replacedByVal=$("#replaceUserName").val();
	var deviceIdRow=$("#deviceId").combogrid("grid").datagrid("getSelected");
	var replacedDateVal=$("#replacedDate").val();
	var compIdVal=$("#compId").combogrid("grid").datagrid("getSelected");
	
	if(isEmpty(bigCateIdVal)){
		$pjq.messager.alert("提示","部门分类不能为空！","info");return;
	}
	if(isEmpty(categoryIdVal)){
		$pjq.messager.alert("提示","设备类型不能为空！","info");return;
	}
	if(isEmpty(replacedByVal)){
		$pjq.messager.alert("提示","更换人员不能为空！","info");return;
	}
	if(isEmpty(deviceIdRow)){
		$pjq.messager.alert("提示","设备名称不能为空！","info");return;
	}
	if(isEmpty(replacedDateVal)){
		$pjq.messager.alert("提示","更换时间不能为空！","info");return;
	}
	if(compIdVal==null){
		$pjq.messager.alert("提示","备件不能为空！","info");return;
	}
	$.post(base+"/"+path+"/upd"+suffix,$("#frm").serialize(),function(data){
			if(data){
				$pjq.messager.alert("提示","提交成功！","info");
				$grid.datagrid("load");
				$dialog.dialog("destroy");
			}else{
				$pjq.messager.alert("提示","提交失败！","error");
			}
	},"json");
};
$(document).ready(function(){
	
	var categoryUrl=base+"/category/getData"+suffix+"?bigCateId=";
	$("#bigCateId").combobox({
		disabled:true,
		onChange:function(newValue,oldValue){
			$("#categoryId").combobox("clear");
			$("#deviceId").combogrid("clear");
			$("#categoryId").combobox("reload",categoryUrl+newValue);
		}
	});
	$("#categoryId").combobox({
		valueField:"id",
		textField:"text",
		url:$("#bigCateId").val()==null?categoryUrl:categoryUrl+$("#bigCateId").val(),
		panelHeight:200,
		required:true,
		editable:false,
		disabled:true,
		onChange:function(newValue,oldValue){
			$("#deviceId").combogrid("clear");
			$("#deviceId").combogrid("grid").datagrid("reload",{categoryId:newValue,deptId:deptIdVal});
		},
		loadFilter:function(data){
			var newData=[];
			var rows=data.rows;
			if(data!=null&&rows!=null&&rows.length>0){
				for(var i=0,len=rows.length;i<len;i++){
					if(cateIdVal==rows[i].id){
						newData.push({id:rows[i].id,text:rows[i].categoryName,selected:true});						
					}else{
						newData.push({id:rows[i].id,text:rows[i].categoryName});						
					}
				}
			}
			return newData;
		}
	});
	
	/*
	$("#categoryId").combobox({
		valueField:"id",
	    textField:"categoryName",
	    editable:false,
	    disabled:true,
	    panelHeight:200,
	    url:base+"/category/getAllCategory"+suffix,
		onChange:function(newValue,oldValue){
			$("#deviceId").combogrid("clear");
			$("#deviceId").combogrid("grid").datagrid("reload",{categoryId:newValue});
			//$("#replacedBy").combobox("clear");
		}
	});
	*/
	$("#compId").combogrid({
		panelWidth:350,
		idField:"deviceCompId",
		textField:"name",
		editable:false,
		disabled:true,
		method:"post",
		queryParams:{deviceId:deviceIdVal},
		url:base+"/device/getAllCompByDeviceId"+suffix,
		rownumbers:false,
		pagination:false,
		columns:[[
		 		  {field:"deviceCompId",title:"id",hidden:true,width:0},
		 		  {field:"name",title:"备件名称",width:100},
		 		  {field:"count",title:"数量",width:30},
		 		  {field:"cycle",title:"更换周期",width:100},
		 		  {field:"startDate",title:"开始时间",width:100}
		 		]]
	});
	var categoryId=$("#categoryId").combobox("getValue");
	$("#deviceId").combogrid({
	    panelWidth:450,
	    idField:"id",
	    textField:"name",
	    method:"post",
	    rownumbers:true,
		pagination:true,
		editable:false,
		disabled:true,
	    queryParams:{categoryId:categoryId},
	    url:base+"/device/getData"+suffix,
	    columns:[[
	        {field:"id",title:"id",hidden:true,width:0},
	        {field:"name",title:"设备名称",width:200},
	        {field:"no",title:"编号",width:100},
	        {field:"deptId",title:"部门id",hidden:true,formatter:formatDeptId},
	        {field:"deptName",title:"所属部门",width:100,formatter:formatDept}
	    ]],
	    onChange:function(newValue,oldValue){
	    	$("#compId").combogrid("clear");
	    	$("#compId").combogrid("grid").datagrid("reload",{deviceId:newValue});
	    }
	});
	
	/*
	 $("#replacedBy").combobox({
		valueField:"id",
	    textField:"realName",
	    editable:false,
	    panelHeight:"auto",
	    url:base+"/user/getUserByDept"+suffix+"?deptId="+deptIdVal
	});
	*/
	
	function formatDept(value,row,index){
		if(row!=null){
			return row.dept.deptName;			
		}
		return null;
	}
	
	function formatDeptId(value,row,index){
		if(row!=null){
			return row.dept.id;			
		}
		return null;
	}
});