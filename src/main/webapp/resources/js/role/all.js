var grid=null;
$(document).ready(function(){
	grid=$("#resultTable").datagrid({
		 height:$(document).height(),
		 width:"auto",
		 url:base+rootPath+"/getData"+suffix,
		 sortName:"createTime",
		 sortOrder:"desc",
		 idField:"id",
		 toolbar:toolbar,
		 columns:[[
		       {field:"no",checkbox:true},
		       {field:"id",title:"id",width:100,hidden:true,checkbox:true},
		       {field:"roleName",title:"角色名称",align:"center",sortable:true},
		       {field:"seq",title:"排序序号",align:"center",sortable:true,hidden:true},
		       {field:"createTime",title:"创建日期",align:"center",sortable:true,formatter:formatDate},
		       {field:"operate",title:"操作",align:"center",formatter:operateFunction}
		 ]]  
	});
});
//删除用户
function delMySelected(val){
	var ids=getIds(val);
	if(ids!=""){
		parent.$.messager.confirm("确认","您确认要删除记录吗？",function(r){
			if(r){
				$.post(base+"/role/queryUsersCount.shtml?_d="+new Date().toString(),{roleIds:ids},function(data){
					if(data){
						parent.$.messager.alert("提示","角色下拥有用户，不能删除！","info");
						return;
					}else{
						$.post(base+"/role/del.shtml?_d="+new Date().toString(),{ids:ids},function(data){
							if(data){
								parent.$.messager.alert("提示","删除成功！","info");
								grid.datagrid("reload");
								grid.datagrid("clearSelections");
							}else{
								parent.$.messager.alert("提示","删除失败！","error");
							}
						},"json");
					}
				},"json");
			}
		});
	}
}
/* 授权	*/
function auth(id){
	var url=base+"/role/toAuth/"+id+suffix;
	var dialog =null;
	dialog = parent.my.modalDialog({
		title:"授权管理",
		height:450, 
		width:630,
		resizable:true,
		url:url,
		buttons:[{
			text:"保存",
			iconCls : "icon-ok",
			handler:function(){
				dialog.find("iframe").get(0).contentWindow.submitForm(dialog,grid,parent.$);
			}
		}, {
			text : "取消",
			iconCls : "icon-cancel",
			handler : function() {
				dialog.dialog("destroy");
			}
		}]
	});
}
