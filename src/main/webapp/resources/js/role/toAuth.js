var submitForm = function($dialog,$grid,$pjq){
	var nodes1 = $("#powertree").tree("getChecked");
	var nodes2 = $("#powertree").tree("getChecked", "indeterminate");	// 获取不确定的节点
	var powerIds =[];
	for(var i=0;i<nodes1.length;i++){
		powerIds.push(nodes1[i].id);
	}
	for(var j=0;j<nodes2.length;j++){
		powerIds.push(nodes2[j].id);
	}
	$.post(base + "/rolePower/savePower"+suffix+"?_d=" + new Date().toTimeString(),"roleId="+$("#roleid").val()+"&power="+powerIds,function(data) {
			if (data) {
				$pjq.messager.alert("提示", "提交成功！", "info");
				$grid.datagrid("load");
				$dialog.dialog("destroy");
			} else {
				$pjq.messager.alert("提示", "提交失败！", "error");
			}
	}, "json");
};

$(document).ready(function(){
	var powertree=null;
	powertree=$("#powertree").tree({
		url:base+"/power/getPowerEdit"+suffix+"?mark=true",
		text:"text",
		idFiled:"id",
		lines:true,
		parentField:"fatherId",
		cascadeCheck:false,
		checkbox:true,
	 	onLoadSuccess:function(node,data){
	 		for(var i=0;i<rps.length;i++){
	 			var n = powertree.tree("find",parseInt(rps[i].id));
	 			powertree.tree("check", n.target);
	 		}
	 		powertree.tree("options","cascadeCheck").cascadeCheck=true;
	 	}
	});
});