var submitForm = function($dialog,$grid,$pjq) {
	 var roleNameVal=$("#roleName").val();
	 var seqVal=$("#seq").val();
	 var roleIdVal=$("#roleId").val();
	 if(!isCNOrNumOrLett(trim(roleNameVal))){
		 $pjq.messager.alert("提示", "角色名输入格式有误！", "info");
		 return;
	 }
	 if($.trim(roleNameVal)==""){
		 $pjq.messager.alert("提示","角色名不能为空！","info");
		 return;
	 }
	 var data={id:roleIdVal,roleName:roleNameVal,seq:seqVal};
	
	$.post(base + "/role/checkRoleName.shtml?_d=" + new Date().toTimeString(),data, function(data) {
			if (data) {
				$pjq.messager.alert("提示", "角色已存在！", "info");
			} else {
				$.post(base + "/role/upd.shtml?_d=" + new Date().toTimeString(), {id:roleIdVal,roleName:roleNameVal,seq:seqVal}, function(data) {
					if (data) {
						$pjq.messager.alert("提示", "提交成功！", "info");
						$grid.datagrid("load");
						$dialog.dialog("destroy");
					} else {
						$pjq.messager.alert("提示", "提交失败！", "error");
					}
				}, "json");
			}
		}, "json");
	};