var lockWindowFun = function() {
		$.post(base+"/exit.shtml", function(data) {
			if(data=="success"){
				$("#loginDialog").dialog("open");
			}
		}, "text");
	};
	var showMyInfoFun = function(id) {
		parent.my.modalDialog({
			height:250,
			title:"我的信息",
			url:base+"/user/view/"+id+suffix
		});
	};
	
	function changeTheme(themeName) {
		var $easyuiTheme = $("#easyuiTheme");
		var url = $easyuiTheme.attr("href");
		var href = url.substring(0, url.indexOf("themes")) + "themes/" + themeName + "/easyui.css";
		$easyuiTheme.attr("href", href);

		var $iframe = $("iframe");
		if ($iframe.length > 0) {
			for (var i = 0; i < $iframe.length; i++) {
				var ifr = $iframe[i];
				try {
					$(ifr).contents().find("#easyuiTheme").attr("href", href);
				} catch (e) {
					try {
						ifr.contentWindow.document.getElementById("easyuiTheme").href = href;
					} catch (e) {
					}
				}
			}
		}
		setCookie("easyuiTheme",themeName);
	}
	
	function logoutFun(){
		$.messager.confirm("提示","确认退出吗？",function(r){
			if(r){
				$.ajax({
					type: "POST",
					url: base+"/exit.shtml",
					success:function(msg){
						if(msg=="success"){
								window.location.href =base; 
						}
					}
				});
			}
		});
	}