var grid=null;
$(document).ready(function(){
	grid=$("#resultTable").datagrid({
		 height:$(document).height(),
		 url:base+rootPath+"/getData"+suffix,
		 queryParams:{},
		 sortName:"seq",
		 sortOrder:"ASC",
		 idField:"id",
		 toolbar:toolbar,
		 columns:[[
		       {field:"no",checkbox:true},
		       {field:"id",title:"id",hidden:true},
		       {field:"userName",title:"用户名",align:"center",sortable:true},
		       {field:"realName",title:"真实姓名",align:"center",sortable:true},
		       {field:"sex",title:"性别",align:"center",sortable:true},
		       {field:"post",title:"职位",align:"center",sortable:false},
		       {field:"number",title:"工号",align:"center",sortable:false},
		       {field:"roleName",title:"角色",align:"center",sortable:false,formatter:formatRole},
		       {field:"deptName",title:"部门",align:"center",sortable:false},
		       {field:"email",title:"电子邮件",align:"center",sortable:false},
		       {field:"telphone",title:"电话",align:"center",sortable:false},
		       {field:"seq",title:"排序",align:"center",sortable:true,hidden:true},
		       {field:"createTime",title:"创建时间",align:"center",sortable:true,formatter:formatDate},
		       {field:"operate",title:"操作",align:"center",formatter:operateFunction}
		       ]],
		 onBeforeLoad:function(param){
			 if(param.sort=="roleName"){
				 param.sort="r."+param.sort;
			 }else if(param.sort=="deptName"){
				 param.sort="d."+param.sort;
			 }else{
				 param.sort="u."+param.sort;
			 }
			 return true;
		 }
	});
	
	function formatRole(value,row,index){
		var len=row.roles.length;
		var roleName="";
		if(len>0){
			var roles=row.roles;
			for(var i=0;i<len;i++){
				roleName+=roles[i].roleName+",";
			}
			roleName=roleName.substring(0,roleName.length-1);
		}
		return formatDescr(roleName);
	}
});

/**
 * 添加
 */
function toMyAdd(){
	var dialog=null;
	dialog = parent.my.modalDialog({
		title:"添加",
		height:400, 
		width:600,
		resizable:true,
		url:base+rootPath+"/toAdd"+suffix,
		buttons:[{
			text : "保存",
			iconCls : "icon-ok",
			handler:function(){
				dialog.find("iframe").get(0).contentWindow.submitForm(dialog,grid,parent.$);
			}
		}, {
			text : "取消",
			iconCls : "icon-cancel",
			handler : function() {
				dialog.dialog("destroy");
			}
		}]
	});
}

/**
 * 查看选中的
 * @param val
 */
function viewMySelected(val){
	var id=getId(val);
	if(id!=""){
		var url=base+rootPath+"/view/"+id+suffix;
		if(viewTarget!=""){
			window.open(url);
			return;
		}
		var dialog =null;
		dialog = parent.my.modalDialog({
			title:"查看",
			height:400, 
			width:600,
			resizable:true,
			url:url,
			buttons:[{
				text:"关闭",
				iconCls:"icon-ok",
				handler:function(){
					dialog.dialog("destroy");
				}
			}]
		});
	}
}

/**
 * 修改选中的
 * @param val
 */
function updMySelected(val){
	var id=getId(val);
	if(id!=""){
		var dialog =null;
		dialog = parent.my.modalDialog({
			title:"修改",
			height:400, 
			width:600,
			resizable:true,
			url:base+rootPath+"/toUpd/"+id+suffix,
			buttons:[{
				text : "保存",
				iconCls : "icon-ok",
				handler:function(){
					dialog.find("iframe").get(0).contentWindow.submitForm(dialog,grid,parent.$);
				}
			}, {
				text : "取消",
				iconCls : "icon-cancel",
				handler : function() {
					dialog.dialog("destroy");
				}
			}]
		});
	}
}
