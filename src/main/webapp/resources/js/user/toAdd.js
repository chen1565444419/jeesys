var isAddImg=false;
var submitForm = function($dialog,$grid,$pjq) {
	 var userNameVal=$("#userName").val();
	 var realNameVal=$("#realName").val();
	 var userPwdVal=$("#userPwd").val();
	 var deptIdVal=$("#deptId").val();
	 
	 var t=$("#roleTree").combotree("tree");
	 var roleIds=t.tree("getChecked");
	 
	 var rows=$grid.datagrid("getRows");
	 var ids="";
	 for(var i=0,len=rows.length;i<len;i++){
		 ids+=rows[i].id+",";
	 }
	 ids=ids.length>1?ids.substring(0,ids.length-1):"";

	 if($.trim(userNameVal)==""){$pjq.messager.alert("提示","人员名不能为空！","info"); return;}
	 if($.trim(realNameVal)==""){$pjq.messager.alert("提示","真实姓名不能为空！","info"); return;}
	 if($.trim(userPwdVal)==""){$pjq.messager.alert("提示","密码不能为空！","info"); return;}
	 if($.trim(deptIdVal)==""){$pjq.messager.alert("提示","所属部门不能为空！","info");return;}
	 if(roleIds==""||roleIds==null){$pjq.messager.alert("提示","角色不能为空！","info");return;}
	 $.post(base+"/user/checkUserName"+suffix,{userName:userNameVal},function(data){
		 	if(data){
				$pjq.messager.alert("提示","该用户名已存在！","info");
			}else{
				 $.post(base+"/user/add"+suffix,$("#frm").serialize(),function(data){
						if(data){
							$pjq.messager.alert("提示","提交成功！","info");
							$grid.datagrid("load");
							$dialog.dialog("destroy");
						}else{
							$pjq.messager.alert("提示","提交失败！","error");
						}
				},"json");
			}
	 },"json");
};

$(document).ready(function(){
	$("#roleTree").combotree("loadData",roles);
});
