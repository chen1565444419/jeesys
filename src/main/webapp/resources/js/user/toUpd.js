var submitForm = function($dialog,$grid,$pjq) {
	 var userNameVal=$("#userName").val();
	 var realNameVal=$("#realName").val();
	 var deptIdVal=$("#deptId").val();
	 var idVal=$("#id").val();
	 
	 var t=$("#roleTree").combotree("tree");
	 var roleIds=t.tree("getChecked");
	 
	 var rows=$grid.datagrid("getRows");
	 var ids="";
	 for(var i=0,len=rows.length;i<len;i++){
		 ids+=rows[i].id+",";
	 }
	 ids=ids.length>1?ids.substring(0,ids.length-1):"";
	 
	 if($.trim(userNameVal)==""){$pjq.messager.alert("提示","用户名不能为空！","info"); return;}
	 if($.trim(realNameVal)==""){$pjq.messager.alert("提示","真实姓名不能为空！","info"); return;}
	 if($.trim(deptIdVal)==""){$pjq.messager.alert("提示","所属部门不能为空！","info");return;}
	 if(roleIds==""||roleIds==null){$pjq.messager.alert("提示","角色不能为空！","info");return;}
	 $.post(base+"/user/checkUserName"+suffix,{userName:userNameVal,id:idVal},function(data){
			if(data){
				$pjq.messager.alert("提示","该用户名已存在！","info");
			}else{
				$.post(base+"/user/upd"+suffix,$("#frm").serialize(),function(data){
					if(data){
						$pjq.messager.alert("提示","修改成功！","info");
						$grid.datagrid("load");
						$dialog.dialog("destroy");
					}else{
						$pjq.messager.alert("提示","提交失败！","error");
					}
				},"json");
			}
	 },"json");
};
$(document).ready(function(){
	$("#roleTree").combotree("loadData",roles);
});