var grid=null;
$(document).ready(function(){
	grid=$("#resultTable").datagrid({
		 url:base+"/user/satisfy/getData"+suffix,
		 queryParams:params,
		 pageSize:10,
		 sortName:"createTime",
		 sortOrder:"asc",
		 columns:[[
		       {field:"number",title:"用户编号",align:"center",sortable:false},
		       {field:"userName",title:"用户姓名",align:"center",sortable:false},
		       {field:"deptName",title:"所属部门",align:"center",sortable:false},
		       {field:"typeCount",title:"次数",align:"center",sortable:false},
		       {field:"createDay",title:"评价时间",align:"center",sortable:true},
		       {field:"type",title:"满意度",align:"center",sortable:true,formatter:function(value,row,index){
		    	   if(value==3){
		    		   return "<font color='red'>不满意</font>";
		    	   }else if(value==2){
		    		   return "<font color='green'>较满意</font>";
		    	   }else{
		    		   return "<font color='green'>满意</font>";
		    	   }
		       }}
		 ]],
	});
});