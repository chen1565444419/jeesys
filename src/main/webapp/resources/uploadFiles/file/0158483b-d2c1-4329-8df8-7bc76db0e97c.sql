-- MySQL dump 10.13  Distrib 5.6.14, for Win64 (x86_64)
--
-- Host: localhost    Database: jeesys
-- ------------------------------------------------------
-- Server version	5.6.14-log

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `y_category`
--

DROP TABLE IF EXISTS `y_category`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `y_category` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT COMMENT '主键',
  `categoryName` varchar(50) NOT NULL COMMENT '类型名称',
  `description` varchar(200) DEFAULT NULL COMMENT '备注说明',
  `seq` int(11) NOT NULL DEFAULT '0' COMMENT '序号',
  `state` int(11) NOT NULL DEFAULT '1' COMMENT '状态，默认为1，0为删除',
  `createdBy` bigint(20) NOT NULL COMMENT '创建人id',
  `createTime` datetime NOT NULL COMMENT '创建时间默认Now()',
  `deletedBy` bigint(20) DEFAULT NULL COMMENT '删除人id',
  `deleteTime` datetime DEFAULT NULL COMMENT '删除时间',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=11 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `y_category`
--

LOCK TABLES `y_category` WRITE;
/*!40000 ALTER TABLE `y_category` DISABLE KEYS */;
INSERT INTO `y_category` VALUES (1,'机械','机械',1,1,1,'2014-07-04 23:46:38',1,'2014-07-08 23:30:48'),(2,'电气','电气',2,1,1,'2014-07-04 23:46:54',1,'2014-07-08 23:30:48'),(3,'动力','动力',3,1,1,'2014-07-04 23:47:06',NULL,NULL),(4,'模具','模具',3,1,1,'2014-07-04 23:47:26',NULL,NULL),(5,'2吨电瓶叉车','2吨电瓶叉车',12222,0,1,'2014-07-06 18:48:23',1,'2014-07-08 23:31:06'),(6,'2吨电瓶叉车','2吨电瓶叉车',1,0,1,'2014-07-06 19:11:04',NULL,NULL),(7,'14出冲杯机','14出冲杯机',0,1,1,'2014-08-04 19:42:03',NULL,NULL),(8,'魂牵梦萦22','魂牵梦萦魂牵梦萦',3232,0,1,'2014-08-04 19:42:31',1,'2014-08-05 21:59:44'),(9,'秦暮楚4444','444444',4444,0,1,'2014-08-04 19:42:42',1,'2014-08-05 21:59:44'),(10,'222','2222',222,1,1,'2014-08-04 19:42:49',NULL,NULL);
/*!40000 ALTER TABLE `y_category` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `y_component`
--

DROP TABLE IF EXISTS `y_component`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `y_component` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT COMMENT '主键',
  `name` varchar(20) NOT NULL COMMENT '备件名称',
  `description` varchar(200) DEFAULT NULL COMMENT '备件描述',
  `state` int(11) NOT NULL DEFAULT '1' COMMENT '状态，默认为1，0为删除',
  `createdBy` bigint(20) NOT NULL COMMENT '创建人id',
  `deletedBy` bigint(20) DEFAULT NULL COMMENT '删除人id',
  `createTime` datetime NOT NULL COMMENT '创建时间',
  `deleteTime` datetime DEFAULT NULL COMMENT '删除时间',
  `seq` int(11) NOT NULL DEFAULT '0' COMMENT '排序序号',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=6 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `y_component`
--

LOCK TABLES `y_component` WRITE;
/*!40000 ALTER TABLE `y_component` DISABLE KEYS */;
INSERT INTO `y_component` VALUES (1,'过滤器','过滤器',1,1,NULL,'2014-07-04 23:51:46',NULL,1),(2,'设备零部件','设备零部件',1,1,NULL,'2014-07-04 23:52:02',NULL,2),(3,'皮带','皮带',1,1,NULL,'2014-07-04 23:52:31',NULL,3),(4,'其它备件','其它备件其它备件其它备件',1,1,NULL,'2014-07-06 19:29:27',NULL,233),(5,'皮带1','皮带皮带皮带',0,1,1,'2014-07-06 19:30:13','2014-07-08 23:31:29',1);
/*!40000 ALTER TABLE `y_component` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `y_dept`
--

DROP TABLE IF EXISTS `y_dept`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `y_dept` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `createTime` datetime NOT NULL COMMENT '创建时间',
  `state` int(11) NOT NULL DEFAULT '1' COMMENT '状态，默认为1,0为删除',
  `createdBy` bigint(20) NOT NULL COMMENT '创建人id',
  `deptName` varchar(50) NOT NULL COMMENT '部门名称',
  `seq` bigint(20) DEFAULT '0' COMMENT '排序序号',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=43 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `y_dept`
--

LOCK TABLES `y_dept` WRITE;
/*!40000 ALTER TABLE `y_dept` DISABLE KEYS */;
INSERT INTO `y_dept` VALUES (1,'2013-09-29 00:06:48',1,1,'管理部',0),(39,'2014-07-01 23:22:35',1,1,'工程部',1),(40,'2014-07-01 23:22:51',1,1,'生产部',2),(41,'2014-07-01 23:23:05',1,1,'质控部',3),(42,'2014-07-01 23:23:21',1,1,'储运部',4);
/*!40000 ALTER TABLE `y_dept` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `y_detect_item`
--

DROP TABLE IF EXISTS `y_detect_item`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `y_detect_item` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT COMMENT '主键',
  `categoryId` bigint(20) NOT NULL COMMENT '类型id',
  `name` varchar(255) NOT NULL COMMENT '类型名称',
  `standard` varchar(255) NOT NULL COMMENT '标准',
  `notice` text COMMENT '注意事项',
  `createdBy` bigint(20) NOT NULL COMMENT '创建人id',
  `createTime` datetime NOT NULL COMMENT '创建时间',
  `seq` int(11) NOT NULL DEFAULT '0' COMMENT '排序序号',
  `state` int(11) NOT NULL DEFAULT '1' COMMENT '默认为1，0为删除',
  `deletedBy` bigint(20) DEFAULT NULL COMMENT '删除人id',
  `deleteTime` datetime DEFAULT NULL COMMENT '删除时间',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=6 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `y_detect_item`
--

LOCK TABLES `y_detect_item` WRITE;
/*!40000 ALTER TABLE `y_detect_item` DISABLE KEYS */;
INSERT INTO `y_detect_item` VALUES (1,1,'魂牵梦萦魂牵梦萦','柑大本营魂牵梦萦','魂牵梦萦霜期 栽魂牵梦萦',1,'2014-07-06 21:42:30',0,1,NULL,NULL),(2,1,'etwrwetrewtrew','trwetrwetrew','trwetrewtrewtrew',1,'2014-08-03 02:36:06',0,1,NULL,NULL),(3,1,'uuuuuuuuuuuuuu','uuuuuuuuuuuu','uuuuuuuuuuuuuuuu',1,'2014-08-03 02:42:07',0,1,NULL,NULL),(4,7,'彻底清理设备内外的废罐、杯子、铝屑及其他杂物，用高压水枪冲洗油污堆积物','清洁','清理废罐时可以检查拉伸机腹腔里的油管。',1,'2014-08-04 20:01:09',0,1,NULL,NULL),(5,7,'检查润滑系统的管路、接头是否渗漏和损坏','油位正常，系统无渗漏','手动加油检查各润滑点加油情况。',1,'2014-08-04 20:01:38',0,1,NULL,NULL);
/*!40000 ALTER TABLE `y_detect_item` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `y_detect_record`
--

DROP TABLE IF EXISTS `y_detect_record`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `y_detect_record` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT COMMENT '主键',
  `deviceId` bigint(20) NOT NULL COMMENT '设备编号',
  `detectItemId` bigint(20) NOT NULL COMMENT '检修项目编号',
  `description` text NOT NULL COMMENT '描述',
  `state` int(11) NOT NULL DEFAULT '1' COMMENT '默认为1，0为删除',
  `createdBy` bigint(20) NOT NULL COMMENT '创建(录入)人id',
  `deletedBy` bigint(20) DEFAULT NULL COMMENT '删除人id',
  `detectId` bigint(20) DEFAULT NULL COMMENT '检修人员id',
  `createTime` datetime NOT NULL COMMENT '默认Now()',
  `deleteTime` datetime DEFAULT NULL COMMENT '删除时间',
  `detectDate` date NOT NULL COMMENT '检修时间',
  `result` text NOT NULL COMMENT '检修结果',
  `detectUserName` varchar(20) NOT NULL COMMENT '检修人员',
  `count` int(11) NOT NULL DEFAULT '1' COMMENT '第几次检修',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=19 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `y_detect_record`
--

LOCK TABLES `y_detect_record` WRITE;
/*!40000 ALTER TABLE `y_detect_record` DISABLE KEYS */;
INSERT INTO `y_detect_record` VALUES (14,10,4,'fdsafdsa',1,1,NULL,NULL,'2014-08-04 20:02:30',NULL,'2014-08-04','fdsafdsafsda','rewrew',1),(15,10,5,'fffffffffffffffffffff',1,1,NULL,NULL,'2014-08-04 20:09:27',NULL,'2014-08-04','fffffffffffffffffffffffff','fffffffff',1),(16,10,4,'rrrr',1,1,NULL,NULL,'2014-08-08 20:13:47',NULL,'2014-08-08','rrrrr','rrr',2),(17,10,5,'rrrrrrrr',1,1,NULL,NULL,'2014-08-08 20:14:03',NULL,'2014-08-08','rrrrrrrrrr','rrrr',2),(18,9,1,'Fdsafsdafsda',1,1,NULL,NULL,'2014-08-05 13:51:29',NULL,'2014-08-08','Fdsafsdafdsafdsa魂牵梦萦魂牵梦萦','魂牵梦萦地',1);
/*!40000 ALTER TABLE `y_detect_record` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `y_device`
--

DROP TABLE IF EXISTS `y_device`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `y_device` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT COMMENT '主键',
  `no` varchar(20) NOT NULL COMMENT '设备编号',
  `name` varchar(50) NOT NULL COMMENT '设备名称',
  `deptId` bigint(20) NOT NULL COMMENT '设备所属部门',
  `categoryId` bigint(20) NOT NULL COMMENT '设备类型id',
  `type` int(11) NOT NULL COMMENT '1日检，2周检，3月检，4季检，5年检,6其它',
  `seq` int(11) NOT NULL DEFAULT '0' COMMENT '序号,默认为0,排序',
  `state` int(11) NOT NULL DEFAULT '1' COMMENT '状态，默认为1，0为删除',
  `createdBy` bigint(20) NOT NULL COMMENT '创建人id',
  `createTime` datetime NOT NULL COMMENT '默认Now()',
  `startDate` date DEFAULT NULL COMMENT '开始时间',
  `description` varchar(200) DEFAULT NULL COMMENT '描述',
  `producer` varchar(50) DEFAULT NULL COMMENT '生产厂家',
  `modelNumber` varchar(20) DEFAULT NULL COMMENT '型号',
  `usedSite` varchar(20) DEFAULT NULL COMMENT '使用地点',
  `lineType` varchar(20) DEFAULT NULL COMMENT '线型',
  `deleteTime` datetime DEFAULT NULL COMMENT '删除时间',
  `deletedBy` bigint(20) DEFAULT NULL COMMENT '删除人id',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=15 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `y_device`
--

LOCK TABLES `y_device` WRITE;
/*!40000 ALTER TABLE `y_device` DISABLE KEYS */;
INSERT INTO `y_device` VALUES (1,'1','2吨电瓶叉车',42,1,1,1,0,1,'2014-07-04 23:57:20','2014-07-04','2吨电瓶叉车','林德','E20P','本厂库房','1',NULL,NULL),(2,'2','2吨电瓶叉车',42,1,1,2,0,1,'2014-07-04 23:58:38','2014-07-04','2吨电瓶叉车','林德','E20P','生产线','2',NULL,NULL),(3,'rr','rr',1,1,1,0,0,1,'2014-07-10 00:14:42','2014-07-10','fff','fff','ff','ff','ff',NULL,NULL),(6,'111','111',40,3,5,111,0,1,'2014-07-10 00:41:50','2014-07-30','测试111','测试111','测试111','测试111','测试111',NULL,NULL),(7,'测试2','测试2',1,1,2,0,0,1,'2014-07-10 23:17:23','2014-07-11','测试2','测试2','测试2','测试2','测试2',NULL,NULL),(8,'测试3','测试3',1,1,2,0,0,1,'2014-07-10 23:17:23','2014-07-11',NULL,'测试3',NULL,NULL,NULL,NULL,NULL),(9,'0','氟离子检修仪',1,1,2,0,0,109,'2014-07-26 16:05:07','2014-07-27',NULL,'ORION','4 STAR','化验室',NULL,NULL,NULL),(10,'1','14出冲杯机',1,7,2,0,1,1,'2014-08-04 19:55:28','2014-08-05',NULL,'MINSTER','PAC-6262-PDI','生产车间','2',NULL,NULL),(11,'11','工程设备1',39,7,4,0,0,1,'2014-08-08 20:17:56','2014-08-09','11','111','11','111','111','2014-08-05 22:17:20',1),(12,'342','fdsa',1,8,2,0,0,1,'2014-08-05 21:08:57','2014-08-06','fdsa','fdsa','fdsa','fdsa','fdsa','2014-08-05 21:59:56',1),(13,'32','4444',1,8,3,0,0,1,'2014-08-05 21:09:35','2014-09-03',NULL,'fdsa',NULL,'fdsa',NULL,'2014-08-05 21:59:56',1),(14,'rew','rewqrew',1,9,1,0,0,1,'2014-08-05 21:10:03','2014-08-21',NULL,NULL,NULL,NULL,NULL,'2014-08-05 21:59:56',1);
/*!40000 ALTER TABLE `y_device` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `y_device_compt`
--

DROP TABLE IF EXISTS `y_device_compt`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `y_device_compt` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT COMMENT '主键',
  `deviceId` bigint(20) NOT NULL COMMENT '设备id',
  `compId` bigint(20) NOT NULL COMMENT '备件id',
  `cycle` int(11) NOT NULL COMMENT '替换周期',
  `lastReplTime` datetime DEFAULT NULL COMMENT '最后替换时间',
  `lastReplPer` bigint(20) DEFAULT NULL COMMENT '最后替换人id',
  `createdBy` bigint(20) NOT NULL COMMENT '创建人id',
  `createTime` datetime NOT NULL COMMENT '创建时间',
  `startDate` date NOT NULL COMMENT '开始时间',
  `count` int(11) NOT NULL DEFAULT '1' COMMENT '数量',
  `state` int(11) NOT NULL DEFAULT '1' COMMENT '状态,默认为1，0为删除',
  `deletedBy` bigint(20) DEFAULT NULL COMMENT '删除人id',
  `deleteTime` datetime DEFAULT NULL COMMENT '删除时间',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=14 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `y_device_compt`
--

LOCK TABLES `y_device_compt` WRITE;
/*!40000 ALTER TABLE `y_device_compt` DISABLE KEYS */;
INSERT INTO `y_device_compt` VALUES (6,6,1,30,NULL,NULL,1,'2014-07-10 00:41:57','2014-07-11',1,0,1,'2014-07-11 01:45:27'),(7,6,2,30,NULL,NULL,1,'2014-07-10 00:41:57','2014-07-11',1,0,1,'2014-07-11 01:45:27'),(8,7,1,30,NULL,NULL,1,'2014-07-10 00:45:56','2014-07-11',1,1,NULL,NULL),(9,7,2,30,NULL,NULL,1,'2014-07-10 00:45:56','2014-07-11',1,1,NULL,NULL),(10,7,3,30,NULL,NULL,1,'2014-07-10 00:45:56','2014-07-11',1,1,NULL,NULL),(11,7,4,30,NULL,NULL,1,'2014-07-10 00:45:56','2014-07-11',1,1,NULL,NULL),(12,6,1,30,NULL,NULL,1,'2014-07-11 01:46:01','2014-07-11',1,1,NULL,NULL),(13,10,1,30,NULL,NULL,109,'2014-08-08 20:22:25','2014-08-09',1,1,NULL,NULL);
/*!40000 ALTER TABLE `y_device_compt` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `y_device_oil`
--

DROP TABLE IF EXISTS `y_device_oil`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `y_device_oil` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT COMMENT '主键',
  `deviceId` bigint(20) NOT NULL COMMENT '设备id',
  `oilId` bigint(20) NOT NULL COMMENT '润滑项目id',
  `cycle` int(11) NOT NULL DEFAULT '1' COMMENT '润滑周期',
  `startDate` date NOT NULL COMMENT '开始日期',
  `createdBy` bigint(20) NOT NULL COMMENT '创建人id',
  `createTime` datetime NOT NULL COMMENT '创建时间',
  `state` int(11) NOT NULL DEFAULT '1' COMMENT '状态,默认1，0为删除',
  `deletedBy` bigint(20) DEFAULT NULL COMMENT '删除人id',
  `deleteTime` datetime DEFAULT NULL COMMENT '删除时间',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=6 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `y_device_oil`
--

LOCK TABLES `y_device_oil` WRITE;
/*!40000 ALTER TABLE `y_device_oil` DISABLE KEYS */;
INSERT INTO `y_device_oil` VALUES (1,6,2,30,'2014-07-11',1,'2014-07-10 00:43:02',0,1,'2014-07-11 01:46:09'),(2,6,1,30,'2014-07-11',1,'2014-07-10 00:43:02',0,1,'2014-07-11 01:46:09'),(3,7,2,30,'2014-07-11',1,'2014-07-10 00:46:02',1,NULL,NULL),(4,7,1,30,'2014-07-11',1,'2014-07-10 00:46:02',1,NULL,NULL),(5,6,1,30,'2014-07-11',1,'2014-07-11 01:46:13',1,NULL,NULL);
/*!40000 ALTER TABLE `y_device_oil` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `y_info`
--

DROP TABLE IF EXISTS `y_info`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `y_info` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT COMMENT '主键',
  `content` text COMMENT '内容',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `y_info`
--

LOCK TABLES `y_info` WRITE;
/*!40000 ALTER TABLE `y_info` DISABLE KEYS */;
INSERT INTO `y_info` VALUES (1,'<blockquote>\r\n	魂牵梦萦朝秦暮楚朝秦暮楚魂牵梦萦 魂牵梦萦 魂牵梦萦 魂牵梦萦霏霏楳af朝秦暮楚\r\n</blockquote>\r\n<p>\r\n	魂牵梦萦<img src=\"/jeesys/resources/uploadFiles/images/ddc38232-4dc6-4b8c-bd23-4b0d9466c5bc.jpg\" alt=\"\" height=\"136\" width=\"209\" /><img src=\"http://localhost:8080/jeesys/resources/plugins/kindeditor/plugins/emoticons/images/21.gif\" alt=\"\" border=\"0\" /><img src=\"http://localhost:8080/jeesys/resources/plugins/kindeditor/plugins/emoticons/images/11.gif\" alt=\"\" border=\"0\" />\r\n	<iframe src=\"http://localhost:8080/jeesys/resources/plugins/kindeditor/plugins/baidumap/index.html?center=114.3162%2C30.581084&zoom=11&width=558&height=360&markers=114.3162%2C30.581084&markerStyles=l%2CA\" style=\"width:560px;height:362px;\" frameborder=\"0\">\r\n	</iframe>\r\n</p>\r\n<p>\r\n	震荡\r\n</p>');
/*!40000 ALTER TABLE `y_info` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `y_item`
--

DROP TABLE IF EXISTS `y_item`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `y_item` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT COMMENT '主键',
  `name` varchar(255) NOT NULL COMMENT '检修项目',
  `standard` varchar(255) NOT NULL COMMENT '检修标准',
  `notice` text COMMENT '注意事项',
  `categoryId` bigint(20) NOT NULL COMMENT '设备类型id',
  `seq` int(11) NOT NULL DEFAULT '0' COMMENT '序号,默认为0,排序',
  `state` int(11) NOT NULL DEFAULT '1' COMMENT '状态，默认为1，0为删除',
  `createdBy` bigint(20) NOT NULL COMMENT '创建人id',
  `createTime` datetime NOT NULL COMMENT '默认Now()',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `y_item`
--

LOCK TABLES `y_item` WRITE;
/*!40000 ALTER TABLE `y_item` DISABLE KEYS */;
/*!40000 ALTER TABLE `y_item` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `y_oil`
--

DROP TABLE IF EXISTS `y_oil`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `y_oil` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT COMMENT '主键',
  `name` varchar(20) NOT NULL COMMENT '润滑项目',
  `state` int(11) NOT NULL DEFAULT '1' COMMENT '状态,默认为1，0为删除',
  `seq` int(11) NOT NULL DEFAULT '0' COMMENT '排序序号',
  `description` varchar(200) DEFAULT NULL COMMENT '描述',
  `createdBy` bigint(20) NOT NULL COMMENT '创建人id',
  `createTime` datetime NOT NULL COMMENT '创建时间',
  `deletedBy` bigint(20) DEFAULT NULL COMMENT '删除人id',
  `deleteTime` datetime DEFAULT NULL COMMENT '删除时间',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `y_oil`
--

LOCK TABLES `y_oil` WRITE;
/*!40000 ALTER TABLE `y_oil` DISABLE KEYS */;
INSERT INTO `y_oil` VALUES (1,'滚轴1',1,22,'滚轴1',1,'2014-07-08 21:56:03',1,'2014-07-08 22:53:43'),(2,'轴承',1,2,'轴承',1,'2014-07-08 22:25:51',1,'2014-07-08 22:53:43');
/*!40000 ALTER TABLE `y_oil` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `y_oil_record`
--

DROP TABLE IF EXISTS `y_oil_record`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `y_oil_record` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT COMMENT '主键',
  `deviceOilId` bigint(20) NOT NULL COMMENT '设备润滑项目id',
  `cost` double NOT NULL COMMENT '费用',
  `state` int(11) NOT NULL DEFAULT '1' COMMENT '状态，默认为1，0为删除',
  `createdBy` bigint(20) NOT NULL COMMENT '创建人id',
  `oiledBy` bigint(20) DEFAULT NULL COMMENT '润滑人id',
  `oiledDate` date NOT NULL COMMENT '润滑日期',
  `createTime` datetime NOT NULL COMMENT '添加日期',
  `deleteTime` datetime DEFAULT NULL COMMENT '删除日期',
  `deletedBy` bigint(20) DEFAULT NULL COMMENT '删除人员id',
  `description` varchar(200) DEFAULT NULL COMMENT '描述',
  `oiledUserName` varchar(20) NOT NULL COMMENT '润滑人员',
  `count` int(11) NOT NULL DEFAULT '1' COMMENT '第几次润滑',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=8 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `y_oil_record`
--

LOCK TABLES `y_oil_record` WRITE;
/*!40000 ALTER TABLE `y_oil_record` DISABLE KEYS */;
INSERT INTO `y_oil_record` VALUES (5,4,222,1,1,NULL,'2014-07-29','2014-07-29 07:39:17',NULL,NULL,'22222222','222',1),(6,4,232,1,1,NULL,'2014-07-29','2014-07-29 07:41:57',NULL,NULL,'fafdsa','rwerw',2),(7,3,44,1,1,NULL,'2014-07-29','2014-07-29 07:42:25',NULL,NULL,'34343','43434',1);
/*!40000 ALTER TABLE `y_oil_record` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `y_power`
--

DROP TABLE IF EXISTS `y_power`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `y_power` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `createTime` datetime NOT NULL COMMENT '创建时间',
  `fatherId` bigint(20) DEFAULT '0' COMMENT '父类id',
  `masterName` varchar(50) NOT NULL,
  `powerName` varchar(50) NOT NULL COMMENT '权限名称',
  `seq` bigint(20) NOT NULL DEFAULT '0' COMMENT '排序序号',
  `iconCls` varchar(255) DEFAULT NULL COMMENT '图标类',
  `url` varchar(255) DEFAULT NULL COMMENT 'url地址',
  `type` varchar(11) NOT NULL DEFAULT 'F' COMMENT '类型，F为菜单，O为操作',
  `used` int(11) NOT NULL DEFAULT '1' COMMENT '是否启用，1为启用，0为禁用',
  `description` varchar(255) DEFAULT NULL COMMENT '描述',
  `createdBy` bigint(20) NOT NULL COMMENT '创建人id',
  `target` varchar(20) NOT NULL DEFAULT 'center' COMMENT '显示位置',
  `status` varchar(20) NOT NULL DEFAULT 'closed' COMMENT '状态，展开还是折叠',
  `child` int(11) NOT NULL DEFAULT '1' COMMENT '是否是根节点,1为是，0为否',
  `updateTime` datetime DEFAULT NULL COMMENT '更新时间',
  `position` varchar(20) DEFAULT NULL COMMENT '显示位置',
  PRIMARY KEY (`id`),
  UNIQUE KEY `masterName` (`masterName`)
) ENGINE=InnoDB AUTO_INCREMENT=246 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `y_power`
--

LOCK TABLES `y_power` WRITE;
/*!40000 ALTER TABLE `y_power` DISABLE KEYS */;
INSERT INTO `y_power` VALUES (11,'2013-09-29 00:17:53',15,'power_all','菜单管理',2,'icon-my-shipping','power/all','F',1,NULL,1,'center','closed',1,'2014-04-23 22:34:21','[top]'),(14,'2013-09-29 00:18:24',0,'usermgr','用户管理',1,'icon-my-shipping','','F',1,NULL,0,'center','closed',0,'2014-04-25 20:22:16','[top]'),(15,'2013-09-29 00:18:24',0,'sysmgr','系统管理',3,'icon-my-shipping','','F',1,NULL,0,'center','closed',0,'2014-02-24 13:41:48','[top]'),(16,'2013-09-29 00:18:24',14,'user_all','用户信息',1,'icon-role','user/all','F',1,NULL,0,'center','closed',1,'2014-04-25 20:22:27','[top]'),(18,'2013-09-29 00:18:24',14,'dept_all','部门信息',3,'icon-pro','dept/all','F',1,NULL,0,'center','closed',1,'2014-04-04 22:51:56','[top]'),(19,'2013-09-29 00:18:24',15,'role_all','角色信息',1,'icon-role','role/all','F',1,NULL,1,'center','closed',1,'2014-04-23 22:34:22',NULL),(52,'2014-02-19 10:43:21',16,'user_add','添加',2,'icon-add','toMyAdd','O',1,NULL,1,'center','open',1,'2014-04-29 10:52:03','[top]'),(65,'2014-02-20 16:18:28',16,'user_upd','修改',3,'icon-edit','updMySelected','O',1,'用户修改',1,'center','open',1,'2014-04-29 13:32:12','[top, middle]'),(70,'2014-02-21 15:49:37',16,'user_del','删除',4,'icon-remove','delSelected','O',1,NULL,1,'center','open',1,'2014-03-11 11:44:55','[top, middle]'),(71,'2014-02-22 10:40:00',16,'user_view','查看',1,'icon-search','viewMySelected','O',1,NULL,1,'center','open',1,'2014-04-29 11:51:08','[top, middle]'),(72,'2014-02-22 11:38:36',11,'power_add','添加',2,'icon-add','toMyAdd','O',1,NULL,1,'center','open',1,'2014-04-23 22:36:22','[top]'),(74,'2014-02-22 11:50:49',18,'dept_view','查看',0,'icon-search','viewSelected','O',1,NULL,1,'center','open',1,'2014-03-10 15:29:11','[top, middle]'),(75,'2014-02-22 11:53:28',19,'role_view','查看',1,'icon-search','viewSelected','O',1,NULL,1,'center','open',1,'2014-03-06 16:05:24','[top, middle]'),(89,'2014-02-24 11:36:20',11,'power_upd','修改',3,'icon-edit','updSelected','O',1,NULL,1,'center','open',1,'2014-04-23 22:36:24','[top]'),(96,'2014-02-24 17:26:48',11,'power_del','删除',4,'icon-remove','delMySelected','O',1,NULL,1,'center','open',1,'2014-04-23 22:36:23','[top]'),(111,'2014-02-25 17:20:42',18,'dept_add','添加',0,'icon-add','toAdd','O',1,NULL,1,'center','open',1,'2014-03-11 17:28:27','[top]'),(112,'2014-02-27 10:12:35',18,'dept_upd','修改',0,'icon-edit','updSelected','O',1,'修改部门',1,'center','open',1,'2014-03-12 10:55:51','[top, middle]'),(113,'2014-02-27 10:13:51',18,'dept_del','删除',4,'icon-remove','delMySelected','O',1,'删除部门',1,'center','open',1,'2014-03-12 11:06:44','[top, middle]'),(114,'2014-02-27 10:23:25',19,'role_add','添加',2,'icon-add','toAdd','O',1,'添加角色',1,'center','open',1,'2014-03-06 16:05:06','[top]'),(115,'2014-02-27 10:25:46',19,'role_upd','修改',3,'icon-edit','updSelected','O',1,'修改角色',1,'center','open',1,'2014-03-06 16:05:51','[middle]'),(116,'2014-02-27 10:27:06',19,'role_del','删除',4,'icon-remove','delMySelected','O',1,'删除角色',1,'center','open',1,'2014-03-11 17:29:34','[top]'),(139,'2014-03-06 16:11:02',19,'role_auth','授权',5,'icon-config','auth','O',1,NULL,1,'center','open',1,'2014-04-25 17:00:18','[middle]'),(141,'2014-04-16 13:24:03',11,'power_view','查看',1,'icon-search','viewSelected','O',1,NULL,1,'center','open',1,'2014-04-16 13:27:28','[middle]'),(142,'2014-04-23 23:07:06',0,'datamgr','数据管理',2,'icon-config','','F',1,NULL,1,'center','closed',0,'2014-04-23 23:40:02','[top]'),(192,'2014-05-23 18:30:26',142,'basedatamgr','基础数据管理',1,'icon-db','','F',1,'基础数据管理',1,'center','closed',0,NULL,'[top]'),(197,'2014-05-23 18:57:50',142,'detectRecord_all','检修记录管理',2,'icon-sys','detectRecord/all','F',1,NULL,1,'center','closed',1,NULL,'[top]'),(198,'2014-05-23 18:59:20',197,'detectRecord_add','添加',2,'icon-add','toAdd','O',1,NULL,1,'center','open',1,NULL,'[top]'),(199,'2014-05-23 19:01:46',197,'detectRecord_view','查看',1,'icon-search','viewSelected','O',1,NULL,1,'center','open',1,NULL,'[top, middle]'),(200,'2014-05-23 19:02:45',197,'detectRecord_upd','修改',3,'icon-edit','updSelected','O',1,NULL,1,'center','open',1,NULL,'[top, middle]'),(201,'2014-05-23 19:03:19',197,'detectRecord_del','删除',4,'icon-remove','delSelected','O',1,NULL,1,'center','open',1,NULL,'[top, middle]'),(202,'2014-07-01 23:29:21',192,'category_all','设备类型管理',1,'icon-comp','category/all','F',1,'设备类型管理',1,'center','closed',1,NULL,'[top]'),(203,'2014-07-01 23:58:15',202,'category_view','查看',1,'icon-search','viewSelected','O',1,NULL,1,'center','open',1,NULL,'[top, middle]'),(204,'2014-07-02 00:41:59',202,'category_add','添加',2,'icon-add','toAdd','O',1,NULL,1,'center','open',1,NULL,'[top]'),(205,'2014-07-02 00:42:45',202,'category_upd','修改',3,'icon-edit','updSelected','O',1,NULL,1,'center','open',1,NULL,'[top, middle]'),(206,'2014-07-02 00:43:42',202,'category_del','删除',4,'icon-remove','delMySelected','O',1,NULL,1,'center','open',1,NULL,'[top, middle]'),(207,'2014-07-03 22:13:57',192,'component_all','备件管理',2,'icon-sys','component/all','F',1,NULL,1,'center','closed',1,NULL,'[top]'),(208,'2014-07-03 22:15:26',207,'component_view','查看',1,'icon-search','viewSelected','O',1,NULL,1,'center','open',1,NULL,'[top, middle]'),(209,'2014-07-03 22:16:37',207,'component_add','添加',2,'icon-add','toAdd','O',1,NULL,1,'center','open',1,NULL,'[top]'),(210,'2014-07-03 22:17:42',207,'component_upd','修改',3,'icon-edit','updSelected','O',1,NULL,1,'center','open',1,NULL,'[top, middle]'),(211,'2014-07-03 22:19:14',207,'component_del','删除',4,'icon-remove','delSelected','O',1,NULL,1,'center','open',1,NULL,'[top, middle]'),(212,'2014-07-03 22:52:24',192,'device_all','设备管理',3,'icon-pro','device/all','F',1,NULL,1,'center','closed',1,NULL,'[top]'),(213,'2014-07-03 22:53:00',212,'device_view','查看',1,'icon-search','viewSelected','O',1,NULL,1,'center','open',1,NULL,'[top, middle]'),(214,'2014-07-03 22:53:38',212,'device_add','添加',2,'icon-add','toMyAdd','O',1,NULL,1,'center','open',1,NULL,'[top]'),(215,'2014-07-03 22:54:19',212,'device_upd','修改',3,'icon-edit','updMySelected','O',1,NULL,1,'center','open',1,NULL,'[top, middle]'),(216,'2014-07-03 22:55:12',212,'device_del','删除',4,'icon-remove','delMySelected','O',1,NULL,1,'center','open',1,NULL,'[top, middle]'),(217,'2014-07-05 21:34:24',192,'oil_all','润滑项目管理',4,'icon-db','oil/all','F',1,NULL,1,'center','closed',1,NULL,'[top]'),(218,'2014-07-05 21:35:13',217,'oil_view','查看',1,'icon-search','viewSelected','O',1,NULL,1,'center','open',1,NULL,'[top, middle]'),(219,'2014-07-05 21:35:42',217,'oil_add','添加',2,'icon-add','toAdd','O',1,NULL,1,'center','open',1,NULL,'[top]'),(220,'2014-07-05 21:36:27',217,'oil_upd','修改',3,'icon-edit','updSelected','O',1,NULL,1,'center','open',1,NULL,'[top, middle]'),(221,'2014-07-05 21:37:01',217,'oil_del','删除',4,'icon-remove','delSelected','O',1,NULL,1,'center','open',1,NULL,'[top, middle]'),(222,'2014-07-06 00:45:59',192,'detectItem_all','检修项目管理',5,'icon-config','detectItem/all','F',1,NULL,1,'center','closed',1,NULL,'[top]'),(223,'2014-07-06 00:46:36',222,'detectItem_view','查看',1,'icon-search','viewSelected','O',1,NULL,1,'center','open',1,NULL,'[top, middle]'),(224,'2014-07-06 00:47:09',222,'detectItem_add','添加',2,'icon-add','toAdd','O',1,NULL,1,'center','open',1,NULL,'[top]'),(225,'2014-07-06 00:47:47',222,'detectItem_upd','修改',3,'icon-edit','updSelected','O',1,NULL,1,'center','open',1,NULL,'[top, middle]'),(226,'2014-07-06 00:48:22',222,'detectItem_del','删除',4,'icon-remove','delSelected','O',1,NULL,1,'center','open',1,NULL,'[top, middle]'),(227,'2014-07-06 16:41:01',142,'replaceRecord_all','更换记录管理',3,'icon-save','replaceRecord/all','F',1,NULL,1,'center','closed',1,NULL,'[top]'),(228,'2014-07-06 16:41:39',142,'oilRecord_all','润滑记录管理',4,'icon-comp','oilRecored/all','F',1,NULL,1,'center','closed',1,NULL,'[top]'),(229,'2014-07-06 16:47:46',142,'query_all','查询记录管理',5,'icon-search','','F',1,NULL,1,'center','closed',0,NULL,'[top]'),(230,'2014-07-06 16:49:52',229,'queryDetect_all','检修记录查询',1,'icon-comp','query/detect/all','F',1,NULL,1,'center','closed',1,NULL,'[top]'),(231,'2014-07-06 16:50:58',229,'queryReplace_all','更换记录查询',2,'icon-save','query/replace/all','F',1,NULL,1,'center','closed',1,NULL,'[top]'),(232,'2014-07-06 16:52:04',229,'queryOil_all','润滑记录查询',3,'icon-sys','query/oil/all','F',1,NULL,1,'center','closed',1,NULL,'[top]'),(233,'2014-07-06 17:24:02',142,'help_all','维修帮助信息管理',6,'icon-tip','help/all','F',1,NULL,1,'center','closed',1,NULL,'[top]'),(234,'2014-07-13 21:46:10',227,'replaceRecord_view','查看',1,'icon-search','viewSelected','O',1,NULL,1,'center','open',1,NULL,'[top, middle]'),(235,'2014-07-13 21:47:12',227,'replaceRecord_add','添加',2,'icon-add','toAdd','O',1,NULL,1,'center','open',1,NULL,'[top]'),(236,'2014-07-13 21:48:51',227,'replaceRecord_upd','修改',3,'icon-edit','updSelected','O',1,NULL,1,'center','open',1,NULL,'[top, middle]'),(237,'2014-07-13 21:49:36',227,'replaceRecord_del','删除',4,'icon-remove','delSelected','O',1,NULL,1,'center','open',1,NULL,'[top, middle]'),(238,'2014-07-13 21:59:38',228,'oilRecord_view','查看',1,'icon-search','viewSelected','O',1,NULL,1,'center','open',1,NULL,'[top, middle]'),(239,'2014-07-13 22:00:24',228,'oilRecord_add','添加',2,'icon-add','toAdd','O',1,NULL,1,'center','open',1,NULL,'[top]'),(240,'2014-07-13 22:02:54',228,'oilRecord_upd','修改',3,'icon-edit','updSelected','O',1,NULL,1,'center','open',1,NULL,'[top, middle]'),(241,'2014-07-13 22:04:21',228,'oilRecord_del','删除',4,'icon-remove','delSelected','O',1,NULL,1,'center','open',1,NULL,'[top, middle]'),(242,'2014-07-28 21:14:42',0,'viewOwnerDevice','查看本部门设备',7,'icon-pro','','P',1,'查看本部门设备',1,'center','closed',0,NULL,'[top]'),(243,'2014-07-28 21:16:07',0,'viewAllDevice','查看所有部门设备',8,'icon-role','','P',1,NULL,1,'center','closed',0,NULL,'[top]'),(244,'2014-08-08 20:15:33',15,'data_all','数据备份及恢复',3,'icon-sys','data/all','F',1,NULL,1,'center','closed',1,NULL,'[top]'),(245,'2014-08-05 23:02:26',244,'view_data','查看',1,'icon-search','viewSelected','O',1,NULL,1,'center','open',1,NULL,'[top]');
/*!40000 ALTER TABLE `y_power` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `y_record_compt`
--

DROP TABLE IF EXISTS `y_record_compt`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `y_record_compt` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT COMMENT '主键',
  `devcmpId` bigint(20) NOT NULL COMMENT '设备备件中间表id',
  `createdBy` bigint(20) NOT NULL COMMENT '录入人id',
  `replacedBy` bigint(20) NOT NULL COMMENT '替换人id',
  `createTime` datetime NOT NULL COMMENT '创建时间',
  `replaceTime` datetime NOT NULL COMMENT '替换时间',
  `description` text COMMENT '描述',
  `cost` double NOT NULL COMMENT '花费',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `y_record_compt`
--

LOCK TABLES `y_record_compt` WRITE;
/*!40000 ALTER TABLE `y_record_compt` DISABLE KEYS */;
/*!40000 ALTER TABLE `y_record_compt` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `y_repair`
--

DROP TABLE IF EXISTS `y_repair`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `y_repair` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT COMMENT '主键',
  `deviceId` bigint(20) NOT NULL COMMENT '设备id',
  `itemId` bigint(20) NOT NULL COMMENT '维修项目id',
  `cost` double DEFAULT NULL COMMENT '维修费用',
  `person` varchar(20) DEFAULT NULL COMMENT '维修人员',
  `description` text COMMENT '维修结果',
  `state` int(11) NOT NULL DEFAULT '1' COMMENT '默认为1，0为删除',
  `createdBy` bigint(20) NOT NULL COMMENT '创建(录入)人id',
  `deletedBy` bigint(20) DEFAULT NULL COMMENT '删除人id',
  `createTime` datetime NOT NULL COMMENT '默认Now()',
  `deleteTime` datetime DEFAULT NULL COMMENT '删除时间',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `y_repair`
--

LOCK TABLES `y_repair` WRITE;
/*!40000 ALTER TABLE `y_repair` DISABLE KEYS */;
/*!40000 ALTER TABLE `y_repair` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `y_replace_record`
--

DROP TABLE IF EXISTS `y_replace_record`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `y_replace_record` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT COMMENT '主键',
  `deviceCompId` bigint(20) NOT NULL COMMENT '设备备件id',
  `cost` double NOT NULL COMMENT '花费',
  `state` int(11) NOT NULL DEFAULT '1' COMMENT '状态,默认1，0为删除',
  `createdBy` bigint(20) NOT NULL COMMENT '创建人id',
  `replacedBy` bigint(20) DEFAULT NULL COMMENT '更换人id',
  `replacedDate` date NOT NULL COMMENT '更换日期',
  `createTime` datetime NOT NULL COMMENT '创建时间',
  `deletedBy` bigint(20) DEFAULT NULL COMMENT '删除人员id',
  `deleteTime` datetime DEFAULT NULL COMMENT '删除时间',
  `description` varchar(200) DEFAULT NULL COMMENT '备注',
  `replaceUserName` varchar(20) NOT NULL COMMENT '更换人员',
  `count` int(11) NOT NULL DEFAULT '1' COMMENT '第几次更换',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=7 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `y_replace_record`
--

LOCK TABLES `y_replace_record` WRITE;
/*!40000 ALTER TABLE `y_replace_record` DISABLE KEYS */;
INSERT INTO `y_replace_record` VALUES (1,8,3333,1,1,107,'2014-07-16','2014-07-17 00:56:07',NULL,NULL,'rr要人胸3fdsaFdsafdsaf魂牵梦萦震荡','张三',1),(2,8,4444,1,1,NULL,'2014-08-08','2014-08-08 14:09:57',NULL,NULL,NULL,'rrr',2),(3,8,4343,1,1,NULL,'2014-08-08','2014-08-08 14:10:28',NULL,NULL,'43434crf3cfc','44',3),(4,9,4343,1,1,NULL,'2014-08-08','2014-08-08 14:10:57',NULL,NULL,'434343','4334',1),(5,9,3434,1,1,NULL,'2014-08-08','2014-08-08 14:11:22',NULL,NULL,'434343','434343',2),(6,13,44,1,109,NULL,'2014-08-08','2014-08-08 20:22:46',NULL,NULL,'fsdffdsf','rrr',1);
/*!40000 ALTER TABLE `y_replace_record` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `y_role`
--

DROP TABLE IF EXISTS `y_role`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `y_role` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `createTime` datetime NOT NULL,
  `state` int(11) NOT NULL DEFAULT '1',
  `createdBy` bigint(20) NOT NULL,
  `roleName` varchar(50) NOT NULL,
  `seq` bigint(20) DEFAULT '0',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=50 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `y_role`
--

LOCK TABLES `y_role` WRITE;
/*!40000 ALTER TABLE `y_role` DISABLE KEYS */;
INSERT INTO `y_role` VALUES (1,'2013-09-29 00:05:19',1,1,'超级管理员',1),(4,'2013-09-29 01:33:00',0,1,'管理员',0),(5,'2013-09-30 18:08:05',0,1,'普通用户',99),(28,'2014-02-12 13:05:10',0,1,'行政总监',3),(29,'2014-02-13 09:39:02',1,1,'质控部经理',5),(30,'2014-02-13 09:39:32',0,1,'副总经理',2),(31,'2014-02-13 12:41:39',0,1,'总经理',1),(34,'2014-02-25 17:03:40',0,1,'商务行政部经理',0),(39,'2014-02-28 15:08:27',0,1,'软件部经理',104),(41,'2014-03-12 11:41:15',0,1,'财务部经理',105),(42,'2014-03-18 12:08:55',0,1,'技术部经理',106),(43,'2014-03-18 12:09:03',0,1,'系统集成部经理',107),(44,'2014-03-21 09:58:51',0,1,'商务行政部',108),(45,'2014-04-05 00:24:46',1,1,'工程部副经理',3),(46,'2014-07-26 15:49:16',1,1,'工程部经理',2),(47,'2014-07-26 15:49:43',1,1,'工厂厂长',1),(48,'2014-07-28 20:46:53',1,1,'生产部经理',4),(49,'2014-07-28 20:47:22',1,1,'储运部经理',6);
/*!40000 ALTER TABLE `y_role` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `y_rolepower`
--

DROP TABLE IF EXISTS `y_rolepower`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `y_rolepower` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `powerId` bigint(20) NOT NULL,
  `roleId` bigint(20) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=3653 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `y_rolepower`
--

LOCK TABLES `y_rolepower` WRITE;
/*!40000 ALTER TABLE `y_rolepower` DISABLE KEYS */;
INSERT INTO `y_rolepower` VALUES (31,1,15),(32,1,17),(33,1,9),(34,14,9),(35,16,9),(36,18,9),(37,15,9),(38,19,9),(39,11,9),(40,1,9),(41,14,9),(42,16,9),(43,18,9),(44,15,9),(45,19,9),(68,1,19),(69,1,20),(70,1,21),(71,1,22),(72,1,23),(73,1,24),(75,25,18),(76,24,18),(77,1,18),(78,12,18),(124,1,27),(125,26,27),(126,30,27),(127,27,27),(128,32,27),(129,37,27),(130,41,27),(131,42,27),(133,1,28),(134,14,28),(135,16,28),(136,18,28),(137,43,28),(138,26,28),(139,30,28),(140,27,28),(141,32,28),(142,37,28),(143,31,28),(144,33,28),(145,34,28),(146,35,28),(147,36,28),(148,40,28),(149,41,28),(150,42,28),(233,1,32),(234,1,33),(546,11,1),(547,14,1),(548,15,1),(549,16,1),(550,18,1),(551,19,1),(552,26,1),(553,27,1),(554,30,1),(555,31,1),(556,32,1),(557,33,1),(558,34,1),(559,35,1),(560,37,1),(561,40,1),(562,41,1),(563,42,1),(564,43,1),(565,45,1),(566,52,1),(567,62,1),(568,63,1),(569,64,1),(570,65,1),(571,66,1),(572,67,1),(573,68,1),(574,69,1),(575,70,1),(576,71,1),(577,72,1),(578,73,1),(579,74,1),(580,75,1),(581,76,1),(582,77,1),(583,78,1),(584,79,1),(585,80,1),(586,81,1),(609,82,1),(610,83,1),(646,89,1),(647,90,1),(648,92,1),(649,93,1),(650,94,1),(651,95,1),(652,96,1),(653,97,1),(654,98,1),(655,99,1),(656,100,1),(657,101,1),(658,102,1),(659,103,1),(660,104,1),(661,105,1),(662,106,1),(663,107,1),(675,14,25),(676,16,25),(677,71,25),(678,52,25),(679,65,25),(680,70,25),(681,18,25),(682,74,25),(683,91,25),(684,43,25),(685,81,25),(686,108,1),(687,109,1),(688,110,1),(690,111,1),(691,112,1),(692,113,1),(693,1,35),(694,114,1),(695,115,1),(696,116,1),(697,1,36),(698,1,37),(699,1,38),(707,117,1),(761,117,1),(762,118,1),(763,119,1),(764,120,1),(765,121,1),(766,122,1),(767,123,1),(768,124,1),(769,125,1),(770,126,1),(771,127,1),(772,128,1),(773,129,1),(774,130,1),(775,131,1),(776,132,1),(777,133,1),(778,134,1),(779,135,1),(780,136,1),(781,137,1),(782,138,1),(835,139,1),(836,140,1),(837,141,1),(935,16,26),(936,71,26),(937,52,26),(938,65,26),(939,70,26),(940,74,26),(941,33,26),(942,66,26),(943,67,26),(944,68,26),(945,69,26),(946,83,26),(947,14,26),(948,18,26),(949,31,26),(951,127,40),(952,128,40),(953,71,40),(954,74,40),(955,43,40),(956,81,40),(957,108,40),(958,109,40),(959,110,40),(960,26,40),(961,30,40),(962,76,40),(963,73,40),(964,82,40),(965,104,40),(966,27,40),(967,78,40),(968,90,40),(969,92,40),(970,93,40),(971,32,40),(972,141,40),(973,95,40),(974,94,40),(975,107,40),(976,37,40),(977,80,40),(978,97,40),(979,98,40),(980,99,40),(981,105,40),(982,100,40),(983,33,40),(984,66,40),(985,67,40),(986,68,40),(987,69,40),(988,83,40),(989,41,40),(990,42,40),(991,77,40),(992,123,40),(993,120,40),(994,121,40),(995,122,40),(996,14,40),(997,16,40),(998,18,40),(999,31,40),(1153,142,1),(1154,143,1),(1194,144,1),(1195,145,1),(1196,146,1),(1364,40,4),(1365,31,4),(1769,147,1),(1853,148,1),(1897,149,1),(1941,127,30),(1942,128,30),(1943,147,30),(1944,40,30),(1945,148,30),(1946,14,30),(1947,16,30),(1948,71,30),(1949,52,30),(1950,65,30),(1951,70,30),(1952,18,30),(1953,74,30),(1954,111,30),(1955,112,30),(1956,113,30),(1957,43,30),(1958,81,30),(1959,108,30),(1960,109,30),(1961,110,30),(1962,15,30),(1963,19,30),(1964,75,30),(1965,114,30),(1966,115,30),(1967,116,30),(1968,139,30),(1969,11,30),(1970,72,30),(1971,89,30),(1972,96,30),(1973,76,30),(1974,73,30),(1975,78,30),(1976,32,30),(1977,141,30),(1978,95,30),(1979,94,30),(1980,107,30),(1981,37,30),(1982,80,30),(1983,97,30),(1984,98,30),(1985,99,30),(1986,105,30),(1987,100,30),(1988,33,30),(1989,66,30),(1990,67,30),(1991,68,30),(1992,69,30),(1993,83,30),(1994,41,30),(1995,42,30),(1996,77,30),(1997,123,30),(1998,120,30),(1999,121,30),(2000,122,30),(2001,129,30),(2002,130,30),(2003,131,30),(2004,132,30),(2005,133,30),(2006,134,30),(2007,135,30),(2008,136,30),(2009,137,30),(2010,138,30),(2011,140,30),(2012,26,30),(2013,30,30),(2014,27,30),(2015,31,30),(2157,150,1),(2158,151,1),(2159,152,1),(2160,153,1),(2161,154,1),(2162,127,41),(2163,128,41),(2164,147,41),(2165,40,41),(2166,148,41),(2167,71,41),(2168,43,41),(2169,81,41),(2170,108,41),(2171,109,41),(2172,110,41),(2173,30,41),(2174,76,41),(2175,73,41),(2176,82,41),(2177,104,41),(2178,27,41),(2179,78,41),(2180,90,41),(2181,92,41),(2182,93,41),(2183,32,41),(2184,141,41),(2185,95,41),(2186,94,41),(2187,107,41),(2188,37,41),(2189,80,41),(2190,97,41),(2191,98,41),(2192,99,41),(2193,105,41),(2194,100,41),(2195,66,41),(2196,67,41),(2197,83,41),(2198,150,41),(2199,151,41),(2200,152,41),(2201,77,41),(2202,123,41),(2203,122,41),(2204,14,41),(2205,16,41),(2206,26,41),(2207,31,41),(2208,33,41),(2209,41,41),(2210,42,41),(2237,127,43),(2238,128,43),(2239,147,43),(2240,40,43),(2241,148,43),(2242,71,43),(2243,43,43),(2244,81,43),(2245,108,43),(2246,109,43),(2247,110,43),(2248,30,43),(2249,76,43),(2250,73,43),(2251,82,43),(2252,104,43),(2253,27,43),(2254,78,43),(2255,90,43),(2256,92,43),(2257,93,43),(2258,32,43),(2259,141,43),(2260,95,43),(2261,94,43),(2262,107,43),(2263,37,43),(2264,80,43),(2265,97,43),(2266,98,43),(2267,99,43),(2268,105,43),(2269,100,43),(2270,66,43),(2271,67,43),(2272,83,43),(2273,150,43),(2274,151,43),(2275,152,43),(2276,77,43),(2277,123,43),(2278,122,43),(2279,131,43),(2280,132,43),(2281,136,43),(2282,137,43),(2283,14,43),(2284,16,43),(2285,26,43),(2286,31,43),(2287,33,43),(2288,41,43),(2289,42,43),(2290,129,43),(2291,130,43),(2292,135,43),(2293,127,42),(2294,128,42),(2295,147,42),(2296,40,42),(2297,148,42),(2298,71,42),(2299,43,42),(2300,81,42),(2301,108,42),(2302,109,42),(2303,110,42),(2304,30,42),(2305,76,42),(2306,73,42),(2307,82,42),(2308,104,42),(2309,27,42),(2310,78,42),(2311,90,42),(2312,92,42),(2313,93,42),(2314,32,42),(2315,141,42),(2316,95,42),(2317,94,42),(2318,107,42),(2319,37,42),(2320,80,42),(2321,97,42),(2322,98,42),(2323,99,42),(2324,105,42),(2325,100,42),(2326,66,42),(2327,67,42),(2328,83,42),(2329,150,42),(2330,151,42),(2331,152,42),(2332,77,42),(2333,123,42),(2334,122,42),(2335,131,42),(2336,132,42),(2337,136,42),(2338,137,42),(2339,14,42),(2340,16,42),(2341,26,42),(2342,31,42),(2343,33,42),(2344,41,42),(2345,42,42),(2346,129,42),(2347,130,42),(2348,135,42),(2462,127,31),(2463,128,31),(2464,147,31),(2465,40,31),(2466,148,31),(2467,16,31),(2468,71,31),(2469,52,31),(2470,65,31),(2471,70,31),(2472,66,31),(2473,67,31),(2474,69,31),(2475,83,31),(2476,150,31),(2477,151,31),(2478,152,31),(2479,77,31),(2480,123,31),(2481,121,31),(2482,122,31),(2483,14,31),(2484,31,31),(2485,33,31),(2486,41,31),(2487,42,31),(2489,81,44),(2490,108,44),(2491,109,44),(2492,26,44),(2493,30,44),(2494,76,44),(2495,73,44),(2496,82,44),(2497,104,44),(2498,153,44),(2499,154,44),(2500,27,44),(2501,78,44),(2502,90,44),(2503,92,44),(2504,93,44),(2505,32,44),(2506,141,44),(2507,95,44),(2508,94,44),(2509,107,44),(2510,37,44),(2511,80,44),(2512,97,44),(2513,98,44),(2514,99,44),(2515,105,44),(2516,100,44),(2517,14,44),(2518,43,44),(2519,127,34),(2520,128,34),(2521,147,34),(2522,40,34),(2523,148,34),(2524,71,34),(2525,43,34),(2526,81,34),(2527,108,34),(2528,109,34),(2529,110,34),(2530,30,34),(2531,76,34),(2532,73,34),(2533,82,34),(2534,104,34),(2535,27,34),(2536,78,34),(2537,90,34),(2538,92,34),(2539,93,34),(2540,32,34),(2541,141,34),(2542,95,34),(2543,94,34),(2544,107,34),(2545,37,34),(2546,80,34),(2547,97,34),(2548,98,34),(2549,99,34),(2550,105,34),(2551,100,34),(2552,66,34),(2553,67,34),(2554,83,34),(2555,150,34),(2556,151,34),(2557,152,34),(2558,41,34),(2559,42,34),(2560,77,34),(2561,123,34),(2562,120,34),(2563,121,34),(2564,122,34),(2565,131,34),(2566,132,34),(2567,136,34),(2568,137,34),(2569,14,34),(2570,16,34),(2571,26,34),(2572,31,34),(2573,33,34),(2574,129,34),(2575,130,34),(2576,135,34),(2577,155,1),(2578,156,1),(2607,157,1),(2608,62,39),(2609,63,39),(2610,144,39),(2611,145,39),(2612,146,39),(2613,149,39),(2614,127,39),(2615,128,39),(2616,147,39),(2617,157,39),(2618,40,39),(2619,148,39),(2620,16,39),(2621,71,39),(2622,52,39),(2623,65,39),(2624,70,39),(2625,18,39),(2626,74,39),(2627,111,39),(2628,112,39),(2629,113,39),(2630,15,39),(2631,19,39),(2632,75,39),(2633,114,39),(2634,115,39),(2635,116,39),(2636,139,39),(2637,11,39),(2638,72,39),(2639,89,39),(2640,96,39),(2641,66,39),(2642,67,39),(2643,83,39),(2644,150,39),(2645,151,39),(2646,152,39),(2647,77,39),(2648,123,39),(2649,122,39),(2650,129,39),(2651,130,39),(2652,131,39),(2653,132,39),(2654,133,39),(2655,134,39),(2656,135,39),(2657,136,39),(2658,137,39),(2659,138,39),(2660,140,39),(2661,14,39),(2662,31,39),(2663,33,39),(2664,41,39),(2665,42,39),(2666,127,5),(2667,128,5),(2668,147,5),(2669,157,5),(2670,40,5),(2671,148,5),(2672,81,5),(2673,108,5),(2674,66,5),(2675,67,5),(2676,83,5),(2677,34,5),(2678,156,5),(2679,77,5),(2680,123,5),(2681,122,5),(2682,131,5),(2683,132,5),(2684,136,5),(2685,137,5),(2686,14,5),(2687,43,5),(2688,31,5),(2689,33,5),(2690,41,5),(2691,42,5),(2692,129,5),(2693,130,5),(2694,135,5),(2695,158,1),(2696,159,1),(2697,160,1),(2698,161,1),(2706,140,1),(2707,141,1),(2708,142,1),(2709,143,1),(2710,144,1),(2711,145,1),(2712,146,1),(2713,147,1),(2714,148,1),(2715,149,1),(2716,150,1),(2717,151,1),(2718,152,1),(2719,153,1),(2720,154,1),(2721,155,1),(2722,156,1),(2723,157,1),(2724,158,1),(2725,159,1),(2726,160,1),(2727,161,1),(2728,162,1),(2729,163,1),(2799,143,29),(2800,152,29),(2801,144,29),(2802,145,29),(2803,146,29),(2804,150,29),(2805,149,29),(2806,155,29),(2807,157,29),(2808,160,29),(2809,162,29),(2810,71,29),(2811,65,29),(2812,74,29),(2813,112,29),(2814,75,29),(2815,115,29),(2816,141,29),(2817,142,29),(2818,147,29),(2819,153,29),(2820,154,29),(2821,159,29),(2822,14,29),(2823,16,29),(2824,18,29),(2825,15,29),(2826,19,29),(2827,11,29),(2828,164,1),(2829,165,1),(2830,166,1),(2831,167,1),(2832,168,1),(2833,169,1),(2834,170,1),(2835,171,1),(2836,172,1),(2837,173,1),(2838,174,1),(2839,175,1),(2840,176,1),(2841,177,1),(2842,178,1),(2843,179,1),(2844,180,1),(2845,181,1),(2846,182,1),(2847,183,1),(2848,184,1),(2849,185,1),(2850,186,1),(2851,187,1),(2852,188,1),(2853,189,1),(2854,190,1),(2855,191,1),(2856,192,1),(2857,193,1),(2858,194,1),(2859,195,1),(2860,196,1),(2861,197,1),(2862,198,1),(2863,199,1),(2864,200,1),(2865,201,1),(2866,203,1),(2867,202,1),(2868,204,1),(2869,205,1),(2870,206,1),(2871,207,1),(2872,208,1),(2873,209,1),(2874,210,1),(2875,211,1),(2876,212,1),(2877,213,1),(2878,214,1),(2879,215,1),(2880,216,1),(2881,217,1),(2882,218,1),(2883,219,1),(2884,220,1),(2885,221,1),(2886,222,1),(2887,223,1),(2888,224,1),(2889,225,1),(2890,226,1),(2891,227,1),(2892,228,1),(2893,229,1),(2894,230,1),(2895,231,1),(2896,232,1),(2897,233,1),(2898,234,1),(2899,235,1),(2900,236,1),(2901,237,1),(2902,238,1),(2903,239,1),(2904,240,1),(2905,241,1),(2906,71,45),(2907,74,45),(2908,14,45),(2909,16,45),(2910,18,45),(2992,1,49),(2993,242,1),(2994,243,1),(2995,244,1),(3161,212,48),(3162,213,48),(3163,214,48),(3164,215,48),(3165,216,48),(3166,229,48),(3167,230,48),(3168,231,48),(3169,232,48),(3170,242,48),(3171,142,48),(3172,192,48),(3534,14,47),(3535,16,47),(3536,71,47),(3537,52,47),(3538,65,47),(3539,70,47),(3540,18,47),(3541,74,47),(3542,111,47),(3543,112,47),(3544,113,47),(3545,192,47),(3546,202,47),(3547,203,47),(3548,204,47),(3549,205,47),(3550,206,47),(3551,207,47),(3552,208,47),(3553,209,47),(3554,210,47),(3555,211,47),(3556,212,47),(3557,213,47),(3558,214,47),(3559,215,47),(3560,216,47),(3561,217,47),(3562,218,47),(3563,219,47),(3564,220,47),(3565,221,47),(3566,222,47),(3567,223,47),(3568,224,47),(3569,225,47),(3570,226,47),(3571,197,47),(3572,199,47),(3573,198,47),(3574,200,47),(3575,201,47),(3576,227,47),(3577,234,47),(3578,235,47),(3579,236,47),(3580,237,47),(3581,228,47),(3582,238,47),(3583,239,47),(3584,240,47),(3585,241,47),(3586,229,47),(3587,230,47),(3588,231,47),(3589,232,47),(3590,243,47),(3591,142,47),(3592,244,1),(3632,213,46),(3633,197,46),(3634,199,46),(3635,198,46),(3636,200,46),(3637,201,46),(3638,227,46),(3639,234,46),(3640,235,46),(3641,236,46),(3642,237,46),(3643,228,46),(3644,238,46),(3645,239,46),(3646,240,46),(3647,241,46),(3648,242,46),(3649,142,46),(3650,192,46),(3651,212,46),(3652,245,1);
/*!40000 ALTER TABLE `y_rolepower` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `y_user`
--

DROP TABLE IF EXISTS `y_user`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `y_user` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `createTime` datetime NOT NULL COMMENT '创建时间',
  `state` int(11) NOT NULL DEFAULT '1' COMMENT '状态默认为1，0为删除',
  `createdBy` bigint(20) NOT NULL COMMENT '创建人id',
  `deptId` bigint(20) NOT NULL COMMENT '部门id',
  `identity` int(11) NOT NULL DEFAULT '1' COMMENT '标记位,admin是2，默认为1',
  `post` varchar(255) DEFAULT NULL COMMENT '职位',
  `seq` int(20) DEFAULT '0' COMMENT '排序序号',
  `sex` varchar(5) NOT NULL COMMENT '性别',
  `userName` varchar(50) NOT NULL COMMENT '用户名',
  `userPwd` varchar(50) DEFAULT NULL COMMENT '密码',
  `number` varchar(20) DEFAULT NULL COMMENT '用户编号',
  `email` varchar(20) DEFAULT NULL COMMENT '电子邮箱',
  `telphone` varchar(20) DEFAULT NULL COMMENT '电话号码',
  `realName` varchar(20) NOT NULL COMMENT '真实姓名',
  PRIMARY KEY (`id`),
  UNIQUE KEY `number` (`number`),
  KEY `FK_USER_DEPT` (`deptId`)
) ENGINE=InnoDB AUTO_INCREMENT=111 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `y_user`
--

LOCK TABLES `y_user` WRITE;
/*!40000 ALTER TABLE `y_user` DISABLE KEYS */;
INSERT INTO `y_user` VALUES (1,'2014-01-07 15:43:13',1,1,1,2,'超级管理员',1,'男','admin','c4ca4238a0b923820dcc509a6f75849b','0',NULL,NULL,'超级管理员'),(84,'2014-04-04 22:53:10',0,1,1,1,'管理员',0,'男','管理员','202cb962ac59075b964b07152d234b70','123',NULL,NULL,'管理员'),(105,'2014-04-29 19:00:46',0,1,1,1,'111',0,'男','张三','c4ca4238a0b923820dcc509a6f75849b','11123',NULL,NULL,'111'),(107,'2014-04-29 19:06:48',0,1,1,1,'111',0,'男','张三12','202cb962ac59075b964b07152d234b70','1101123',NULL,NULL,'张三12'),(108,'2014-07-01 23:04:31',0,1,1,1,'经理',1,'男','lisi','202cb962ac59075b964b07152d234b70','110',NULL,NULL,'李四'),(109,'2014-07-26 15:48:21',1,1,1,1,'工厂厂长',0,'男','hby','c4ca4238a0b923820dcc509a6f75849b',NULL,NULL,NULL,'黄宝勇'),(110,'2014-07-26 15:49:01',1,1,39,1,'工程部经理',0,'男','xp','c4ca4238a0b923820dcc509a6f75849b',NULL,NULL,NULL,'许平');
/*!40000 ALTER TABLE `y_user` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `y_userrole`
--

DROP TABLE IF EXISTS `y_userrole`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `y_userrole` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT COMMENT '主键',
  `userId` bigint(20) NOT NULL COMMENT '用户id',
  `roleId` bigint(20) NOT NULL COMMENT '角色id',
  PRIMARY KEY (`id`),
  KEY `FK_USER_ID` (`userId`)
) ENGINE=InnoDB AUTO_INCREMENT=216 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `y_userrole`
--

LOCK TABLES `y_userrole` WRITE;
/*!40000 ALTER TABLE `y_userrole` DISABLE KEYS */;
INSERT INTO `y_userrole` VALUES (2,1,1),(76,79,1),(214,109,47),(215,110,46);
/*!40000 ALTER TABLE `y_userrole` ENABLE KEYS */;
UNLOCK TABLES;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2014-08-05 23:39:40
