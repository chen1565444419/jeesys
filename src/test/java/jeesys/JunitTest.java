package jeesys;

import java.util.HashMap;
import java.util.Map;

import org.junit.Test;

/**
 * 创建人:朱顺福<br />
 * 创建日期:2014年8月5日<br />
 * 创建时间:下午9:03:01<br />
 * 说      明:<br />
 */
public class JunitTest {

	@Test
	public void test1(){
		Map<String,Object> paramMap=new HashMap<String,Object>();
		paramMap.put("name","张三");
		System.out.println(paramMap.get("name"));
		paramMap.put("name","李四");
		System.out.println(paramMap.get("name"));
	}
}
